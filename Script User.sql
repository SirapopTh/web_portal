USE [PORTAL]
GO
/****** Object:  Table [dbo].[users]    Script Date: 11/9/2563 14:27:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[users]') AND type in (N'U'))
DROP TABLE [dbo].[users]
GO
/****** Object:  Table [dbo].[users]    Script Date: 11/9/2563 14:27:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[users](
	[user_id_id] [int] IDENTITY(1,1) NOT NULL,
	[user_id] [nvarchar](30) NOT NULL,
	[user_name] [nvarchar](50) NULL,
	[user_password] [nvarchar](40) NOT NULL,
	[id_number] [nvarchar](30) NULL,
	[medical_recno] [nvarchar](30) NULL,
	[gender] [nchar](1) NULL,
	[birthday] [char](8) NULL,
	[email] [nvarchar](50) NOT NULL,
	[mobile_phone] [nvarchar](16) NULL,
	[address] [nvarchar](200) NULL,
	[header_icon] [nvarchar](250) NULL,
	[institute_id] [varchar](20) NULL,
	[department_id] [varchar](10) NULL,
	[role_id] [varchar](10) NULL,
	[user_authority] [int] NULL,
	[delete_mark] [bit] NULL,
	[is_verified] [bit] NULL,
	[is_administrator] [bit] NULL,
	[is_initial] [bit] NULL,
	[user_theme] [bit] NULL,
	[source] [int] NULL,
	[create_date] [datetime] NULL,
	[pwd_update] [datetime] NULL,
 CONSTRAINT [PK_users] PRIMARY KEY CLUSTERED 
(
	[user_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[users] ON 
GO
INSERT [dbo].[users] ([user_id_id], [user_id], [user_name], [user_password], [id_number], [medical_recno], [gender], [birthday], [email], [mobile_phone], [address], [header_icon], [institute_id], [department_id], [role_id], [user_authority], [delete_mark], [is_verified], [is_administrator], [is_initial], [user_theme], [source], [create_date], [pwd_update]) VALUES (2, N'admin@ebmtech.com', N'Admin', N'c4ca4238a0b923820dcc509a6f75849b', N'A1234', N'A1234', N'M', N'19800203', N'admin@ebmtech.com', N'', N'', N'', N'', N'', N'4', 0, 0, 1, 0, 1, 1, NULL, NULL, NULL)
GO
INSERT [dbo].[users] ([user_id_id], [user_id], [user_name], [user_password], [id_number], [medical_recno], [gender], [birthday], [email], [mobile_phone], [address], [header_icon], [institute_id], [department_id], [role_id], [user_authority], [delete_mark], [is_verified], [is_administrator], [is_initial], [user_theme], [source], [create_date], [pwd_update]) VALUES (1, N'manager@ebmtech.com', N'manager', N'c4ca4238a0b923820dcc509a6f75849b', N'I1234', N'm1234', N'M', N'19800203', N'manager@ebmtech.com', N'', N'', N'', N'', N'', N'3', 0, 0, 1, 0, 1, 1, NULL, NULL, NULL)
GO
SET IDENTITY_INSERT [dbo].[users] OFF
GO
