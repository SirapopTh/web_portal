﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Goheer.EXIF;

namespace DicomKit
{
    public class DicomTool
    {
        public static bool IsDicomFile(string sFilePath)
        {
         
            return false;
        }
        //public static string GetDicomTagStr(string sFilePath,ushort group,ushort elem)
        //{
        //    DcmData data = new DcmData();
        //    DcmObject dcmObject = data.ReadData(sFilePath);
        //    if(dcmObject != null)
        //    {
        //        DcmDataElement dcmDataElement = dcmObject.GetTag(group, elem);
        //        if (dcmDataElement != null)
        //            return dcmDataElement.ToDisplayString();
        //    }
        //    return string.Empty;
        //}
            public static  int CreateImgDCM
     (

         string imgFilePath,             // Full path of image file
         string outFilePath,
                                         // --- Patient/Study/Report Information ---
         string studyInstanceUID,        // Study Instance UID, max 64 chars plus null terminator, or NULL
         string patientID,               // Patient ID, max 64 chars plus null terminator
         string patientName,             // Patient Name in Shift JIS, max 64 chars plus null terminator
         string patientBirthDate,        // Patient Birth Date in YYYYMMDD format plus null terminator, or NULL
         string patientSex,              // 'M' or 'F' plus null terminator, or NULL
         string studyDate,               // Study Date in YYYYMMDD format, plus null terminator
         string studyTime,               // Study Time in HHMMSS format, plus null terminator
         string accessionNumber,         // Accession Number, max 64 chars plus null terminator, or NULL
         string studyID,                 // Study ID, max 64 char plus null terminator, or NULL
         string studyDescription,       // Study Description, max 64 char plus null terminator, or NULL
         string modality,               // Modality, max 64 char plus null terminator, or NULL
         string instanceNumber,         // Modality, max 12 numberal char plus null terminator, or NULL
                                        //    char*  verifyingObserver,       // Verifying Observer Name, max 64 chars in Shift JIS plus null terminator
                                        //    char*  verifyingOrganization,   // Verifying Organization, max 64 chars in Shift JIS plus null terminator
                                        //    char*  verifyDateTime,          // Verify Date Time in YYYYMMDDHHMMSS format plus null terminator
         string seriesInstanceUid,               // 
         string sopInstanceUid,//SOPInstanceUID ,
         string sopClassUid,  //sop class uid
         string seriesNumber,               // 
         string pixelSpacing  ,              // 
         string refphysician
     )
        {
            if (!System.IO.File.Exists(imgFilePath))
                return -1;
       
            return 0;
        }
        /// <summary>
        /// 从dicom 文件中解出原始文件..
        /// 因为dicom 中保存文件名时，取出来显示时可能会有乱码，所以用输入的文件名
        /// </summary>
        /// <param name="sdcmFilePath"></param>
        /// <returns></returns> //ref orignalFileName 
        public static byte[] OriginalFileFromDicom(string sdcmFilePath, string orignalFileName,ref int ires)
        {
            string inFileName = string.Empty;
            byte[] bytes = null;
            ires = 0;

            FileStream fs = new FileStream(sdcmFilePath, FileMode.Open, FileAccess.Read);
            bytes = new byte[fs.Length];

            fs.Read(bytes, 0, (int)fs.Length);
            fs.Close();
            fs.Dispose();

            return bytes;

        }
        static void CorrectJpegExif(ref string imgFilePath)
        {
           // try
            //{
                var bmp = new Bitmap(imgFilePath);


                //try
               // {

                    var exif = new EXIFextractor(ref bmp, "\n"); // get source from http://www.codeproject.com/KB/graphics/exifextractor.aspx?fid=207371

                    if (exif["Orientation"] != null)
                    {
                        RotateFlipType flip = OrientationToFlipType(exif["Orientation"].ToString());

                        if (flip != RotateFlipType.RotateNoneFlipNone) // don't flip of orientation is correct
                        {
                            bmp.RotateFlip(flip);
                            exif.setTag(0x112, "1"); // Optional: reset orientation tag
                            bmp.Save(imgFilePath, ImageFormat.Jpeg);

                        }

                    }
            bmp.Dispose();

                //}
                //catch (Exception ex)
                //{
                //    throw ex;// new Exception(ex.Message);
                //}
                //finally
                //{
                //    bmp.Dispose();
                //}



            //}
            //catch (Exception ex)
            //{
            //    throw new Exception(ex.Message);
           // }

        }
        private static RotateFlipType OrientationToFlipType(string orientation)
        {
            RotateFlipType rotateFilpType;
            int irotate = 0;
            int.TryParse(orientation, out irotate);

            switch (irotate)//int.Parse(orientation)
            {
                case 1:
                    rotateFilpType = RotateFlipType.RotateNoneFlipNone;
                    break;
                case 2:
                    rotateFilpType = RotateFlipType.RotateNoneFlipX;
                    break;
                case 3:
                    rotateFilpType = RotateFlipType.Rotate180FlipNone;
                    break;
                case 4:
                    rotateFilpType = RotateFlipType.Rotate180FlipX;
                    break;
                case 5:
                    rotateFilpType = RotateFlipType.Rotate90FlipX;
                    break;
                case 6:
                    rotateFilpType = RotateFlipType.Rotate90FlipNone;
                    break;
                case 7:
                    rotateFilpType = RotateFlipType.Rotate270FlipX;
                    break;
                case 8:
                    rotateFilpType = RotateFlipType.Rotate270FlipNone;
                    break;
                default:
                    rotateFilpType = RotateFlipType.RotateNoneFlipNone;
                    break;
            }

            return rotateFilpType;
        }


        public static string GenerateStudyInstanceUID(string SerialNumber, string AssignedNumber)
        {

            int ms = DateTime.Now.Millisecond;

            Random counter = new Random(Guid.NewGuid().GetHashCode());
            string hashCode = System.Math.Abs(Guid.NewGuid().GetHashCode()).ToString();

            if (SerialNumber.Length == 0)
            {
                SerialNumber = "0";
            }

            string InstanceUID = "1.2.840.113820" + "." + AssignedNumber + ".8882." + SerialNumber +
                ".7" + hashCode + "." + ms.ToString();

            return InstanceUID;

        }

        public static void GetDicomTagInfo(string fileName, UInt16 group,UInt16 elem,ref string value)
        {
     
        }
        public static void SetDicomTagInfo(string fileName, UInt16 group, UInt16 elem,  string value)
        {
   
        }
        public static void DelDicomTag(string fileName, UInt16 group, UInt16 elem)
        {
         
        }
    }
}
