﻿
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace DicomKit
{
    public class StoreKit //:IDisposable
    {


       // CountdownEvent handler;
       // private Stack<string> MessageStackOP = new Stack<string>();//泛型的堆栈
        // private static int logNumAll = 0;
        // private static int logNumUp = 0;
        // //ManualResetEvent _menuRestEvent = new ManualResetEvent(false);
        // List<string> mFailedFiles ;

        Dictionary<string, CountdownEvent> fileCountDownPairs; //每个文件有对应的计数事件
        Dictionary<string, int> fileResultPairs; //每个文件的结果

        public string storeSopclassuid { get; set; }

        private static StoreKit _Instance = null;

        public static StoreKit Instance
        {
            get
            {
                if (_Instance == null)
                {
                    _Instance = new StoreKit();
                }
                return _Instance;
            }
        }

        private StoreKit()
        {


            fileCountDownPairs = new Dictionary<string, CountdownEvent>();
            fileResultPairs = new Dictionary<string, int>();
        }
        //作为全局的class
    /*
        public void Dispose()
        {
            mCommu.onCommuEvent = null;
            mCommu.Dispose();
            mCommu = null;
            mAE.Dispose();
            mAE = null;
        }*/
        public int DicomStoreU(string[] DicomFileList, string LocalAE, string EpsAE, string EpsIP, string EpsPort, string Pud,ref List<string> lstFailed)
        {
            try
            {

             

  
            }
            catch (Exception ex)
            {
                WriteLog(ex.ToString(), "ErrorDcmUp", false);
            }

            return 0;
        }


        protected void Execution(object state)
        {
           // _menuRestEvent.WaitOne();

            try
            {
                string newDicom = state as string;// MessageStackOP.Pop();

                #region
       
                #endregion
            }
            catch (Exception ex)
            {
                WriteLog(ex.ToString(), "Error", false);
            }
        }



        private IFormatProvider culture = new System.Globalization.CultureInfo("en-US", true);

        private Encoding ec = Encoding.UTF8;//Encoding.GetEncoding("gb2312"); 

        private void WriteLog(string message, string UserID, bool isOK)
        {
            string address = "\\Log" + "\\" + System.DateTime.Now.ToString("yyyy", culture) + "\\" + System.DateTime.Now.ToString("MMdd", culture);

            if (!Directory.Exists(AppDomain.CurrentDomain.SetupInformation.ApplicationBase + address))
            {
                Directory.CreateDirectory(AppDomain.CurrentDomain.SetupInformation.ApplicationBase + address);
            }

            string strFilePath = AppDomain.CurrentDomain.SetupInformation.ApplicationBase + address + "\\" + UserID + ".txt";
            try
            {
                using (StreamWriter sw = new StreamWriter(strFilePath, true, ec))
                {
                    sw.WriteLine(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff", culture) + ":  " + UserID);
                    sw.WriteLine(message);
                    sw.WriteLine("");
                    sw.Flush();
                    sw.Close();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

    }
}
