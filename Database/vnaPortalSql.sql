USE [master]
GO
/****** Object:  Database [PORTAL]    Script Date: 2019/4/16 下午 03:29:10 ******/
CREATE DATABASE [PORTAL]
GO
ALTER DATABASE [PORTAL] SET COMPATIBILITY_LEVEL = 110
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [PORTAL].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [PORTAL] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [PORTAL] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [PORTAL] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [PORTAL] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [PORTAL] SET ARITHABORT OFF 
GO
ALTER DATABASE [PORTAL] SET AUTO_CLOSE ON 
GO
ALTER DATABASE [PORTAL] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [PORTAL] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [PORTAL] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [PORTAL] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [PORTAL] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [PORTAL] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [PORTAL] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [PORTAL] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [PORTAL] SET  DISABLE_BROKER 
GO
ALTER DATABASE [PORTAL] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [PORTAL] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [PORTAL] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [PORTAL] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [PORTAL] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [PORTAL] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [PORTAL] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [PORTAL] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [PORTAL] SET  MULTI_USER 
GO
ALTER DATABASE [PORTAL] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [PORTAL] SET DB_CHAINING OFF 
GO
ALTER DATABASE [PORTAL] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [PORTAL] SET TARGET_RECOVERY_TIME = 60 SECONDS 

GO

USE [PORTAL]
GO
CREATE USER [dicom] FOR LOGIN [dicom]
GO
USE [PORTAL]
GO
ALTER ROLE [db_owner] ADD MEMBER [dicom]
GO


USE [PORTAL]

/****** Object:  Table [dbo].[department]    Script Date: 2019/4/16 下午 03:29:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[department](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[department_id] [varchar](10) NOT NULL,
	[department_name] [nvarchar](40) NULL,
	[institute_id] [varchar](20) NULL,
 CONSTRAINT [PK_department] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[download]    Script Date: 2019/4/16 下午 03:29:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[download](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[download_uid] [varchar](40) NOT NULL,
	[creator_id] [int] NOT NULL,
	[user_id_id] [int] NOT NULL,
	[create_date] [datetime] NOT NULL,
	[expire_time] [datetime] NOT NULL,
	[native_id] [varchar](30) NULL,
	[medical_recno] [varchar](30) NULL,
	[patient_name] [varchar](30) NULL,
	[ref_physician] [nvarchar](30) NULL,
	[source_dept] [varchar](10) NULL,
	[study_date] [char](8) NULL,
	[modality_instudy] [varchar](8) NULL,
	[study_uid] [varchar](64) NULL,
	[study_file] [varchar](384) NULL,
	[is_valid] [bit] NOT NULL,
 CONSTRAINT [PK_down_link] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[download_item]    Script Date: 2019/4/16 下午 03:29:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[download_item](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[download_id] [int] NOT NULL,
	[src_filename] [varchar](128) NULL,
	[src_filetype] [varchar](128) NULL,
	[file_instanceid] [varchar](64) NULL,
	[file_path] [varchar](384) NULL,
	[status] [varchar](2) NULL,
 CONSTRAINT [PK_download_item] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[hl7_msg]    Script Date: 2019/4/16 下午 03:29:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[hl7_msg](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[msg_type] [varchar](20) NULL,
	[msg_text] [text] NULL,
 CONSTRAINT [PK_hl7_msg] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[role]    Script Date: 2019/4/16 下午 03:29:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[role](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[role_id] [varchar](10) NULL,
	[role_name] [nvarchar](30) NULL,
	[upload_enable] [bit] NULL,
	[download_enable] [bit] NULL,
	[view_enable] [bit] NULL,
	[send_enable] [bit] NULL,
	[monitor_enable] [bit] NULL,
	[senddown_enable] [bit] NULL,
	[downloadpacs_enable] [bit] NULL,
	[editgroup_enable] [bit] NULL,
	[editperson_enable] [bit] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[sys_log]    Script Date: 2019/4/16 下午 03:29:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[sys_log](
	[idno] [int] IDENTITY(1,1) NOT NULL,
	[log_datetime] [datetime] NULL,
	[account] [nvarchar](50) NULL,
	[user_name] [nvarchar](50) NULL,
	[module_name] [nvarchar](50) NULL,
	[log_type] [nvarchar](50) NULL,
	[ip_address] [varchar](50) NULL,
	[result] [bit] NULL,
	[description] [nvarchar](500) NULL,
 CONSTRAINT [PK_sys_log] PRIMARY KEY CLUSTERED 
(
	[idno] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[uplink]    Script Date: 2019/4/16 下午 03:29:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[uplink](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[uplink_uid] [varchar](40) NOT NULL,
	[creator_id] [int] NOT NULL,
	[user_id] [varchar](30) NOT NULL,
	[expire_time] [datetime] NULL,
	[is_valid] [bit] NULL,
 CONSTRAINT [PK_up_link] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[upload]    Script Date: 2019/4/16 下午 03:29:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[upload](
	[upload_id] [varchar](40) NOT NULL,
	[upload_id_id] [int] IDENTITY(1,1) NOT NULL,
	[upload_sessionid] [varchar](40) NULL,
	[user_id_id] [int] NULL,
	[medical_num] [varchar](50) NULL,
	[native_id] [varchar](50) NULL,
	[first_name] [nvarchar](50) NULL,
	[middle_name] [nvarchar](50) NULL,
	[last_name] [nvarchar](50) NULL,
	[create_date] [datetime] NULL,
	[study_date] [datetime] NULL,
	[ref_physician] [nvarchar](50) NULL,
	[report_type] [nvarchar](20) NULL,
	[source_dept] [nchar](10) NULL,
	[source_ofdata] [nchar](10) NULL,
	[comment] [nvarchar](50) NULL,
	[study_uid] [varchar](64) NULL,
	[series_uid] [varchar](64) NULL,
 CONSTRAINT [PK_upload] PRIMARY KEY CLUSTERED 
(
	[upload_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[upload_item]    Script Date: 2019/4/16 下午 03:29:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[upload_item](
	[upload_item_id] [varchar](40) NOT NULL,
	[upload_id_id] [int] NOT NULL,
	[src_file_name] [nvarchar](200) NOT NULL,
	[dest_file_name] [nvarchar](200) NOT NULL,
	[upload_at] [datetime] NOT NULL,
	[instance_uid] [varchar](128) NULL,
	[status] [varchar](2) NULL,
 CONSTRAINT [PK_upload_item] PRIMARY KEY CLUSTERED 
(
	[upload_item_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[upload_relation]    Script Date: 2019/4/16 下午 03:29:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[upload_relation](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[upload_id_id] [int] NULL,
	[dest_user] [nvarchar](30) NULL,
	[send_date] [datetime] NULL,
	[expire_date] [datetime] NULL,
	[operator] [nvarchar](30) NULL,
	[description] [nvarchar](50) NULL,
	[enabled] [bit] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[user_authordept]    Script Date: 2019/4/16 下午 03:29:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[user_authordept](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[user_id_id] [int] NOT NULL,
	[dept_id_id] [int] NOT NULL,
 CONSTRAINT [PK_user_authordept] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[user_forgetpw]    Script Date: 2019/4/16 下午 03:29:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[user_forgetpw](
	[req_id] [varchar](40) NOT NULL,
	[user_email] [varchar](64) NOT NULL,
	[req_date] [datetime] NOT NULL,
	[is_open] [bit] NOT NULL,
	[open_date] [datetime] NULL,
 CONSTRAINT [PK_user1_forgetpw] PRIMARY KEY CLUSTERED 
(
	[req_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[user_logon]    Script Date: 2019/4/16 下午 03:29:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[user_logon](
	[id] [varchar](50) NOT NULL,
	[user_id] [nvarchar](50) NULL,
	[user_password] [varchar](50) NULL,
	[user_secretkey] [varchar](50) NULL,
	[first_visit_time] [datetime] NULL,
	[previous_visit_time] [datetime] NULL,
	[last_visit_time] [datetime] NULL,
	[multi_user_login] [bit] NULL,
	[logon_count] [int] NULL,
	[user_online] [bit] NULL,
 CONSTRAINT [PK_SYS_USERLOGON] PRIMARY KEY NONCLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[user_reg]    Script Date: 2019/4/16 下午 03:29:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[user_reg](
	[regid] [varchar](64) NOT NULL,
	[user_email] [varchar](64) NOT NULL,
	[reg_date] [datetime] NOT NULL,
	[is_verified] [bit] NULL,
	[verified_date] [datetime] NULL,
 CONSTRAINT [PK__user_reg__184A6B04ED222EF6] PRIMARY KEY CLUSTERED 
(
	[regid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[users]    Script Date: 2019/4/16 下午 03:29:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[users](
	[user_id_id] [int] IDENTITY(1,1) NOT NULL,
	[user_id] [nvarchar](30) NOT NULL,
	[user_name] [nvarchar](50) NULL,
	[user_password] [nvarchar](40) NOT NULL,
	[id_number] [nvarchar](30) NULL,
	[medical_recno] [nvarchar](30) NULL,
	[gender] [nchar](1) NULL,
	[birthday] [char](8) NULL,
	[email] [nvarchar](50) NOT NULL,
	[mobile_phone] [nvarchar](16) NULL,
	[address] [nvarchar](200) NULL,
	[header_icon] [nvarchar](4000) NULL,
	[institute_id] [varchar](20) NULL,
	[department_id] [varchar](10) NULL,
	[role_id] [varchar](10) NULL,
	[user_authority] [int] NULL,
	[delete_mark] [bit] NULL,
	[is_verified] [bit] NULL,
	[is_administrator] [bit] NULL,
	[is_initial] [bit] NULL,
	[source] [int] NULL,
	[create_date] [datetime] NULL,
 CONSTRAINT [PK_users] PRIMARY KEY CLUSTERED 
(
	[user_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[vna_fileinfo]    Script Date: 2019/4/16 下午 03:29:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[vna_fileinfo](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[user_id_id] [int] NOT NULL,
	[upload_date] [datetime] NULL,
	[org_filename] [varchar](50) NULL,
	[org_filetype] [varchar](30) NULL,
	[study_uid] [varchar](64) NULL,
	[series_uid] [varchar](64) NULL,
	[instance_uid] [varchar](40) NULL,
	[vna_filepath] [varchar](384) NULL,
	[status] [varchar](2) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[vna_users]    Script Date: 2019/4/16 下午 03:29:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[vna_users](
	[user_id_id] [int] NOT NULL,
	[user_id] [varchar](40) NOT NULL,
	[user_name] [varchar](50) NOT NULL,
	[medical_recno] [varchar](30) NOT NULL,
	[id_number] [varchar](30) NULL,
	[department_id] [varchar](10) NULL,
 CONSTRAINT [PK_vna_files] PRIMARY KEY CLUSTERED 
(
	[user_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_download]    Script Date: 2019/4/16 下午 03:29:12 ******/
CREATE NONCLUSTERED INDEX [IX_download] ON [dbo].[download]
(
	[medical_recno] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_download_1]    Script Date: 2019/4/16 下午 03:29:12 ******/
CREATE NONCLUSTERED INDEX [IX_download_1] ON [dbo].[download]
(
	[native_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_download_2]    Script Date: 2019/4/16 下午 03:29:12 ******/
CREATE NONCLUSTERED INDEX [IX_download_2] ON [dbo].[download]
(
	[study_date] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_download_3]    Script Date: 2019/4/16 下午 03:29:12 ******/
CREATE NONCLUSTERED INDEX [IX_download_3] ON [dbo].[download]
(
	[study_uid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_download_4]    Script Date: 2019/4/16 下午 03:29:12 ******/
CREATE NONCLUSTERED INDEX [IX_download_4] ON [dbo].[download]
(
	[user_id_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_upload]    Script Date: 2019/4/16 下午 03:29:12 ******/
CREATE NONCLUSTERED INDEX [IX_upload] ON [dbo].[upload]
(
	[upload_id_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_upload_1]    Script Date: 2019/4/16 下午 03:29:12 ******/
CREATE NONCLUSTERED INDEX [IX_upload_1] ON [dbo].[upload]
(
	[upload_sessionid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_upload_2]    Script Date: 2019/4/16 下午 03:29:12 ******/
CREATE NONCLUSTERED INDEX [IX_upload_2] ON [dbo].[upload]
(
	[user_id_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_upload_3]    Script Date: 2019/4/16 下午 03:29:12 ******/
CREATE NONCLUSTERED INDEX [IX_upload_3] ON [dbo].[upload]
(
	[create_date] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_upload_4]    Script Date: 2019/4/16 下午 03:29:12 ******/
CREATE NONCLUSTERED INDEX [IX_upload_4] ON [dbo].[upload]
(
	[first_name] ASC,
	[middle_name] ASC,
	[last_name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_upload_5]    Script Date: 2019/4/16 下午 03:29:12 ******/
CREATE NONCLUSTERED INDEX [IX_upload_5] ON [dbo].[upload]
(
	[medical_num] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_upload_6]    Script Date: 2019/4/16 下午 03:29:12 ******/
CREATE NONCLUSTERED INDEX [IX_upload_6] ON [dbo].[upload]
(
	[native_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_upload_item]    Script Date: 2019/4/16 下午 03:29:12 ******/
CREATE NONCLUSTERED INDEX [IX_upload_item] ON [dbo].[upload_item]
(
	[upload_at] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_upload_item_1]    Script Date: 2019/4/16 下午 03:29:12 ******/
CREATE NONCLUSTERED INDEX [IX_upload_item_1] ON [dbo].[upload_item]
(
	[src_file_name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_upload_item_2]    Script Date: 2019/4/16 下午 03:29:12 ******/
CREATE NONCLUSTERED INDEX [IX_upload_item_2] ON [dbo].[upload_item]
(
	[upload_id_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_Users]    Script Date: 2019/4/16 下午 03:29:12 ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_Users] ON [dbo].[users]
(
	[user_id_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[role] ADD  CONSTRAINT [DF_role_senddown_enable]  DEFAULT ((0)) FOR [senddown_enable]
GO
ALTER TABLE [dbo].[role] ADD  CONSTRAINT [DF_role_downloadpacs_enable]  DEFAULT ((0)) FOR [downloadpacs_enable]
GO
ALTER TABLE [dbo].[role] ADD  CONSTRAINT [DF_role_editgroup_enable]  DEFAULT ((0)) FOR [editgroup_enable]
GO
ALTER TABLE [dbo].[role] ADD  CONSTRAINT [DF_role_editperson_enable]  DEFAULT ((0)) FOR [editperson_enable]
GO
ALTER TABLE [dbo].[user_reg] ADD  CONSTRAINT [DF__user_reg__is_ver__52593CB8]  DEFAULT ((0)) FOR [is_verified]
GO
USE [master]
GO
ALTER DATABASE [PORTAL] SET  READ_WRITE 
GO
