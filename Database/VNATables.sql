USE [DICOMDB]
GO

/****** Object:  Table [dbo].[vna_fileinfo]    Script Date: 2019/4/17 �W�� 08:53:36 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[vna_fileinfo](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[upload_id_id] [int] NOT NULL,
	[upload_date] [datetime] NULL,
	[org_filename] [varchar](120) NULL,
	[org_filetype] [varchar](30) NULL,
	[study_uid] [varchar](64) NULL,
	[series_uid] [varchar](64) NULL,
	[instance_uid] [varchar](64) NULL,
	[vna_filepath] [varchar](384) NULL,
	[status] [varchar](2) NULL,
 CONSTRAINT [PK_vna_fileinfo] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO



CREATE TABLE [dbo].[vna_upload](
	[upload_id] [varchar](40) NOT NULL,
	[upload_id_id] [int] NOT NULL,
	[upload_sessionid] [varchar](40) NULL,
	[user_id_id] [int] NULL,
	[medical_num] [varchar](50) NULL,
	[native_id] [varchar](50) NULL,
	[first_name] [nvarchar](50) NULL,
	[middle_name] [nvarchar](50) NULL,
	[last_name] [nvarchar](50) NULL,
	[create_date] [datetime] NULL,
	[study_date] [datetime] NULL,
	[ref_physician] [nvarchar](50) NULL,
	[report_type] [varchar](20) NULL,
	[source_dept] [varchar](10) NULL,
	[source_ofdata] [varchar](10) NULL,
	[comment] [nvarchar](50) NULL,
	[study_uid] [varchar](64) NULL,
	[series_uid] [varchar](64) NULL,
 CONSTRAINT [PK_vna_upload] PRIMARY KEY CLUSTERED 
(
	[upload_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

