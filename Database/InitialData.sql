/****** Script for SelectTopNRows command from SSMS  ******/
USE [PORTAL]
GO

INSERT INTO [dbo].[role]
           ([role_id]
           ,[role_name]
           ,[upload_enable]
           ,[download_enable]
           ,[view_enable]
           ,[send_enable]
           ,[monitor_enable]
           ,[senddown_enable]
           ,[downloadpacs_enable]
           ,[editgroup_enable]
           ,[editperson_enable])
     VALUES
           ('3'
           ,NULL
           ,1
           ,1
           ,1
           ,1
           ,1
           ,1
           ,1
           ,0
           ,0)
GO

USE [PORTAL]
GO

INSERT INTO [dbo].[users]
           ([user_id]
           ,[user_name]
           ,[user_password]
           ,[id_number]
           ,[medical_recno]
           ,[gender]
           ,[birthday]
           ,[email]
           ,[mobile_phone]
           ,[address]
           ,[header_icon]
           ,[institute_id]
           ,[department_id]
           ,[role_id]
           ,[user_authority]
           ,[delete_mark]
           ,[is_verified]
           ,[is_administrator]
           ,[is_initial])
     VALUES
           ('manager@ebmtech.com'
           ,'manager'
           ,'c4ca4238a0b923820dcc509a6f75849b'
           ,'I1234'
           ,'m1234'
           ,'M'
           ,'19800203'
           ,'manager@ebmtech.com'
           ,''
           ,''
           ,''
           ,''
           ,''
           ,'3'
           ,0
           ,0
           ,1
           ,0
           ,1)
GO


INSERT INTO [dbo].[users]
           ([user_id]
           ,[user_name]
           ,[user_password]
           ,[id_number]
           ,[medical_recno]
           ,[gender]
           ,[birthday]
           ,[email]
           ,[mobile_phone]
           ,[address]
           ,[header_icon]
           ,[institute_id]
           ,[department_id]
           ,[role_id]
           ,[user_authority]
           ,[delete_mark]
           ,[is_verified]
           ,[is_administrator]
           ,[is_initial])
     VALUES
           ('admin@ebmtech.com'
           ,'Admin'
           ,'c4ca4238a0b923820dcc509a6f75849b'
           ,'A1234'
           ,'A1234'
           ,'M'
           ,'19800203'
           ,'admin@ebmtech.com'
           ,''
           ,''
           ,''
           ,''
           ,''
           ,'4'
           ,0
           ,0
           ,1
           ,0
           ,1)
GO

insert department (department_id,department_name,institute_id)  values ( '100','Internal Medicine',NULL)
insert department (department_id,department_name,institute_id)  values ( '101','Department of Geriatrics and Gerontology',NULL)
insert department (department_id,department_name,institute_id)  values ( '102','Family Medicine',NULL)
insert department (department_id,department_name,institute_id)  values ( '103','Neurology',NULL)
insert department (department_id,department_name,institute_id)  values ( '104','Medical Genetics',NULL)
insert department (department_id,department_name,institute_id)  values ( '105','Test Department',NULL)
insert department (department_id,department_name,institute_id)  values ( 'D6','General Medicine',NULL)
insert department (department_id,department_name,institute_id)  values ( 'D7','Gastroenterology',NULL)
insert department (department_id,department_name,institute_id)  values ( 'D8','Chest',NULL)
insert department (department_id,department_name,institute_id)  values ( 'D9','Rheumatology',NULL)
insert department (department_id,department_name,institute_id)  values ( 'D10','Psychiatry',NULL)
insert department (department_id,department_name,institute_id)  values ( 'D11','Genetics',NULL)
insert department (department_id,department_name,institute_id)  values ( 'D12','Otolaryngology',NULL)
insert department (department_id,department_name,institute_id)  values ( 'D13','Cardiovascular Surgery',NULL)
insert department (department_id,department_name,institute_id)  values ( 'D14','Cardiology',NULL)
insert department (department_id,department_name,institute_id)  values ( 'D15','Obstetrics & Gynecology',NULL)
insert department (department_id,department_name,institute_id)  values ( 'D16','Pediatrics',NULL)
insert department (department_id,department_name,institute_id)  values ( 'D17','Plastic Surgery',NULL)
insert department (department_id,department_name,institute_id)  values ( 'D18','Pain Management',NULL)