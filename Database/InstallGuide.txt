On Portal computer
0. Install EPS (version 11.0.2019.322 or above) and SoliPACS Web (version 11.1.2320 or above) and HTML5 Viewer (UniWeb 360 version 0.1.2.0117 or above). Modify ebmsrv.ini to add below configuration:

[Private Store UID]
1.2.840.113820.5.1.4.1.1.104.4

1. Run Tools\vcredist_2012_x64.exe (if EPS is 64-bit, can ignore this). Run Tools\vcredist_2013_x64.exe. Run Tools\AccessDatabaseEngine_X64.exe.

2. Install "Web Server (IIS) -> Web Server -> Application Development -> ASP.NET 4.6" from adding OS  Roles, and ".NET Framework 4.6 Features -> WCF Services (All functions)" from adding OS Features.

For Win 10, "Internet Information Services -> World Wide Web Services -> Application Development Features -> ASP.NET 4.8", and ".NET Framework 4.8 Advanced Services -> ASP.NET 4.8, and WCF Services -> All".

3. Copy Portal folder to C:\inetpub\wwwroot\. Grant Write permission to IIS_IUSRS for Portal\Log and Portal\App_Data folders.

4. Run IIS Manager, right-click Portal folder choose "Convert to Application".

5. Run SSMS, open file DBSQL\vnaPortalSql.sql and execute, open file DBSQL\InitialData.sql and execute.

6. Modify Portal\Web.config, replace with correct info.

  <connectionStrings>
    <add name="VNADbContext" connectionString="Server=localhost;Initial Catalog=PORTAL;User ID=xxx;Password=xxx" providerName="System.Data.SqlClient" />
  </connectionStrings>

7. Modify Portal\Web.config, replace with correct info (need to allow TCP 2104 inbound on OS firewall if enabled). Don't add IIS Application Name behind the URL.

  <VNASection>
    <Site Home="http://192.168.168.202" ImageHome="http://192.168.168.202" Html5Home="http://192.168.168.202/UW360" UniUser="xxx" UniPwd="xxx" H5User="xxx" H5Pwd="xxx" />
	  <SiteV Home="http://192.168.168.202/VNAService" ImageHome="http://192.168.168.202" Html5Home="http://192.168.168.202/UW360" UniUser="xxx" UniPwd="xxx" H5User="xxx" H5Pwd="xxx" />
    <Mail SmtpHost="ebmtech.com" SmtpLoginId="xxx" SmtpLoginPasswd="xxx" SenderName="VNA Service" SenderAddr="xxx@ebmtech.com" />
    <Eps LocalAE="PORTAL"  LocalPort="2104" EpsAE="EPS" EpsIP="127.0.0.1" EpsPort="104"></Eps>
    <ViewerRegion IDOViewerRegion="iDoRSNA" />
	<LDAP Domain="" Url=""></LDAP>
	<ServerList>
      <Server AE="TESTSERVER" IP="192.168.168.201" Port="104" DisplayName="PACS Server 1"></Server>
    </ServerList>
  </VNASection>

8. Modify Portal\Web.config, replace with correct info.

    <client>
      <endpoint address="http://192.168.168.202/VNAService/VNAService1.svc" binding="basicHttpBinding" bindingConfiguration="BasicHttpBinding_IVNAService1" contract="VNAServiceReference1.IVNAService1" name="BasicHttpBinding_IVNAService1" />
    </client>

9. Setup EPS, add Portal's AE/Port/IP/PDU/Modality info to "Users" list.


On eDC computer
0. Install EPS (version 11.0.2019.322 or above) and SoliPACS Web (version 11.1.2320 or above) and HTML5 Viewer (UniWeb 360 version 0.1.2.0117 or above). Modify ebmsrv.ini to add below configuration:

[Private Store UID]
1.2.840.113820.5.1.4.1.1.104.4

1. Run Tools\vcredist_2012_x64.exe (if EPS is 64-bit, can ignore this). Run Tools\vcredist_2013_x64.exe.

2. Install "IIS->Application Development->ASP.NET 4.6" and ".NET Framework 4.6 Features->WCF Services" from adding OS features.

3. Copy VNAService folder to C:\inetpub\wwwroot\. Grant Write permission to IIS_IUSRS for VNAService\Log and VNAService\App_Data folders.

4. Run IIS Manager, right-click VNAService folder choose "Convert to Application".

5. Run SSMS, open file DBSQL\VNATables.sql and execute.

6. Modify VNAService\Web.config, replace with correct info.

  <connectionStrings>
    <add name="VNADbContext" connectionString="Server=localhost;Initial Catalog=DICOMDB;User ID=xxx;Password=xxx" providerName="System.Data.SqlClient" />
  </connectionStrings>

7. Modify VNAService\Web.config, replace with correct info.

  <VNASection>
    <Eps LocalAE="VNA" LocalPort="3104" EpsAE="EPS" EpsIP="127.0.0.1" EpsPort="104"></Eps>
  </VNASection>

8. Modify VNAService\Web.config, replace with correct info.  
     <client>
      <endpoint address="http://192.168.168.202/VNAService/VNAService1.svc" binding="basicHttpBinding" bindingConfiguration="BasicHttpBinding_IVNAService1_IVNAService1" contract="ServiceReference1.IVNAService1" name="BasicHttpBinding_IVNAService1_IVNAService1" />
    </client>

9. Setup EPS, add eDC's AE/Port/IP/PDU/Modality info to "Users" list.


On client computer
1. Login as Admin (default account: admin@ebmtech.com, pw: 1), create a patient account.

2. Login as Manager (default account: manager@ebmtech.com, pw: 1)