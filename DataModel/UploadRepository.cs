﻿using BaseData;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel
{
    public interface IUploadRepository:IRepositoryBase
    {
        Upload GetUpload(string upload_id);
        List<Upload> GetUploads(string upload_sessionid);
        void InsertUpload(Upload upload);
        void UpdateUpload(Upload upload);
    }
    public class UploadRepository :RepositoryBase,IUploadRepository
    {
        public UploadRepository(VnaDbContext vnaDbContext )
            :base(vnaDbContext)
        {

        }
        public Upload GetUpload(string upload_id)
        {
            return this.FindEntity<Upload>(t => t.upload_id == upload_id);
        }
        public List<Upload> GetUploads(string upload_sessionid)
        {
            string sql = "select * from upload where upload_sessionid=@upload_sessionid";

            List<Upload>  uploads = this.FindList<Upload>(sql, new SqlParameter[] { new SqlParameter("upload_sessionid", upload_sessionid) });
            return uploads;
        }
        public void InsertUpload(Upload upload)
        {
            this.Insert<Upload>(upload);

        }
        public void UpdateUpload(Upload upload)
        {
            this.Update<Upload>(upload);
        }
    }
}
