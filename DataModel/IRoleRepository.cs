﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel
{
    public interface IRoleRepository : BaseData.IRepositoryBase
    {
        void InsertRole(Role role);
        Role GetRole(string roleid);
        void UpdateRole(Role role);
        List<Role> GetRoleList();
    }
}
