﻿using BaseData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel
{

    public interface IUplinkRepository : IRepositoryBase
    {
        void InsertUplink(Uplink upload);
        void UpdateUplink(Uplink upload);

        void DeleteUplink(string upload_item_id);
    }
    public class UplinkRepository : RepositoryBase, IUplinkRepository
    {
        public UplinkRepository(VnaDbContext vnaDbContext)
            : base(vnaDbContext)
        {

        }
        public void InsertUplink(Uplink upload)
        {
            this.Insert(upload);
        }
        public void UpdateUplink(Uplink upload)
        {
            this.Update(upload);
        }
        public void DeleteUplink(string upload_item_id)
        {
            this.Delete<Uplink>(t => t.uplink_uid == upload_item_id);
        }
    }
}
