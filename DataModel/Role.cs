﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel
{
    public class Role
    {
        //ID 属性就被默认为是自增的...
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }

        public string role_id { get; set; }
        public string role_name { get; set; }
        public bool upload_enable { get; set; }
        public bool download_enable { get; set; }
        
        public bool view_enable { get; set; }
        public bool send_enable { get; set; }
        public bool monitor_enable { get; set; }

        public bool senddown_enable { get; set; }
        public bool downloadpacs_enable { get; set; }
        public bool editgroup_enable { get; set; }
        public bool editperson_enable { get; set; }
    }
}
