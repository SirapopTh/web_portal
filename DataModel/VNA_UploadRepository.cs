﻿using BaseData;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel
{
    public interface IVNA_UploadRepository : IRepositoryBase
    {
        VNA_Upload GetUpload(string upload_id);
        List<VNA_Upload> GetUploads(string upload_sessionid);
        void InsertUpload(VNA_Upload upload);
        void UpdateUpload(VNA_Upload upload);
    }
    public class VNA_UploadRepository:RepositoryBase,IVNA_UploadRepository
    {
        public VNA_UploadRepository(VnaDbContext vnaDbContext)
           : base(vnaDbContext)
        {

        }
        public VNA_Upload GetUpload(string upload_id)
        {
            return this.FindEntity<VNA_Upload>(t => t.upload_id == upload_id);
        }
        public List<VNA_Upload> GetUploads(string upload_sessionid)
        {
            string sql = "select * from vna_upload where upload_sessionid=@upload_sessionid";
            
            List<VNA_Upload> uploads = this.FindList<VNA_Upload>(sql, new SqlParameter[] { new SqlParameter("upload_sessionid", upload_sessionid) });
            
            return uploads;
        }
        public void InsertUpload(VNA_Upload upload)
        {
            this.Insert<VNA_Upload>(upload);

        }
        public void UpdateUpload(VNA_Upload upload)
        {
            this.Update<VNA_Upload>(upload);
        }
    
    }
}
