﻿using BaseData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel
{
    public interface IUserAuthorDeptRepository : IRepositoryBase
    {
        void InsertUserAuthorDept(UserAuthorDept dept);
        List<UserAuthorDept> GetUserAuthorDept(int user_id_id);
        //List<UserAuthorDept> GetUserAuthorDeptList();
        void UpdateUserAuthorDept(UserAuthorDept dept);
        void DeleteUserAuthorDept(int user_id_id);
    }
    public class UserAuthorDeptRepository : RepositoryBase, IUserAuthorDeptRepository
    {
        public UserAuthorDeptRepository(VnaDbContext vnaDbContext)
            :base(vnaDbContext)
        {

        }
        public void InsertUserAuthorDept(UserAuthorDept dept)
        {
            this.Insert(dept);
        }
        public List<UserAuthorDept> GetUserAuthorDept(int user_id_id)
        {
            IQueryable<UserAuthorDept> userAuthorDepts = this.IQueryable<UserAuthorDept>(t => t.user_id_id == user_id_id);// //FindList<UserAuthorDept>(( t => t.user_id_id == user_id_id);

            if (userAuthorDepts != null)
                return userAuthorDepts.ToList();
            return null;
        }
        //public List<UserAuthorDept> GetUserAuthorDeptList()
        //{
        //    return this.IQueryable<UserAuthorDept>().ToList();
        //}
        public void UpdateUserAuthorDept(UserAuthorDept dept)
        {
            this.Update<UserAuthorDept>(dept);
        }
        public void DeleteUserAuthorDept(int user_id_id)
        {
            this.Delete<UserAuthorDept>(t => t.user_id_id == user_id_id);
        }
    }
}
