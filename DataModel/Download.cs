﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel
{
    /// <summary>
    /// 是只保留id信息，查询时从dicom db关联，还是在download里保留一份信息，方便查询...但万一dicomdb里数据变化经了呢，不同步?
    /// Is it only to keep the id information and to associate with the dicom db during the query, or to keep a piece of information in the download for easy query ... But in case the data in the dicomdb changes, it is not synchronized?
    /// </summary>
    public abstract class IDownload
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }

        public string download_uid { get; set; }
        public int creator_id { get; set; }

        public int user_id_id { get; set; } //patient id
        
        public DateTime create_date { get; set; }
        public DateTime expire_time { get; set; }

        public string native_id { get; set; }

        public string medical_recno { get; set; }
        public string patient_name { get; set; }

        public string ref_physician { get; set; }

        public string source_dept { get; set; }

        public string study_date { get; set; }

        public string modality_instudy { get; set; }

        public string study_uid { get; set; }

        public string study_file { get; set; } //only one file for some study...只存了study的一个文件..for显示
        public bool is_valid { get; set; }
    }
    public  class Download : IDownload
    {

    }
}
