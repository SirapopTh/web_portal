﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel
{
    public class ForgetPasswd
    {
        public string req_id { get; set; }
        public string user_email { get; set; }
        public DateTime? req_date { get; set; }
        public bool is_open { get; set; }
        public DateTime? open_date { get; set; }

    }
}
