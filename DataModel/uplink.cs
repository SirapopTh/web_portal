﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel
{
    public class Uplink
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }

        public string uplink_uid { get; set; }
        public int creator_id { get; set; }

        public string user_id { get; set; }

        public DateTime expire_time { get; set; }
        public bool is_valid { get; set; }
    }
}
