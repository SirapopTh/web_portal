﻿using BaseData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel
{
  
    public interface IVna_FileinfoRepository : IRepositoryBase
    {
        void InsertVna_Fileinfo(vna_fileinfo vna_file);
        vna_fileinfo GetVna_Fileinfo(int user_id_id);
        List<vna_fileinfo> GetVna_FileinfoList();
        void UpdateVna_Fileinfo(vna_fileinfo vna_fileinfo);
    }
    public class Vna_FileinfoRepository : RepositoryBase, IVna_FileinfoRepository
    {
        public Vna_FileinfoRepository(VnaDbContext vnaDbContext)
            : base(vnaDbContext)
        {

        }
        public void InsertVna_Fileinfo(vna_fileinfo vna_file)
        {
            this.Insert(vna_file);
        }
        public vna_fileinfo GetVna_Fileinfo(int upload_id_id)
        {
            return this.FindEntity<vna_fileinfo>(t => t.upload_id_id == upload_id_id);
        }
        public List<vna_fileinfo> GetVna_FileinfoList()
        {
            return this.IQueryable<vna_fileinfo>().ToList();
        }
        public void UpdateVna_Fileinfo(vna_fileinfo vna_fileinfo)
        {
            this.Update<vna_fileinfo>(vna_fileinfo);
        }

    }
}
