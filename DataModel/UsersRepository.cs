﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BaseData;

namespace DataModel
{
    public interface IUserRepository :IRepositoryBase
    {
        void CreateUser(Users users);
        void UpdateUser(Users users);
        void DisableUser(Users users);
        void EnableUser(Users users);
        void DeleteUser(Users users);
        Users GetUser(string userid);
        Users GetValidUser(string userid);
    }
    public class UserRepository :RepositoryBase,IUserRepository
    {
        public UserRepository(VnaDbContext vnaDb)
            :base(vnaDb)
        {

        }
        public void CreateUser(Users users)
        {
            this.Insert(users);
        }
        public Users GetUser(string userid)
        {
            Users uss = this.FindEntity<Users>(t => t.user_id == userid);
            return uss;
        }
        public Users GetValidUser(string userid)
        {
            Users uss = this.FindEntity<Users>(t => t.user_id == userid && !t.delete_mark && t.is_verified);
            return uss;
        }
        public void UpdateUser(Users users)
        {
            this.Update(users);
        }
        public void DisableUser(Users users)
        {
            users.delete_mark = true;

            Update(users);
        }
        public void EnableUser(Users users)
        {
            users.delete_mark = false;
            //
            Update(users);
        }
        public void DeleteUser(Users users)
        {
            Delete<Users>(t => t.user_id == users.user_id);
        }
    }
}
