﻿using BaseData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel
{
 
    public interface IDownloadRepository : IRepositoryBase
    {
        void InsertDownload(Download upload);
        void UpdateDownload(Download upload);

        void DeleteDownload(string upload_item_id);
    }
    public class DownloadRepository : RepositoryBase, IDownloadRepository
    {
        public DownloadRepository(VnaDbContext vnaDbContext)
            : base(vnaDbContext)
        {

        }
        public void InsertDownload(Download upload)
        {
            this.Insert(upload);
        }
        public void UpdateDownload(Download upload)
        {
            this.Update(upload);
        }
        public void DeleteDownload(string upload_item_id)
        {
            this.Delete<Download>(t => t.download_uid == upload_item_id);
        }
    }
}
