﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel
{
    public class vna_users
    {
        public int user_id_id { get; set; }
        public string user_id { get; set; }
        public string user_name { get; set; }

        public string medical_recno { get; set; }

        public string id_number { get; set; }

        public string department_id { get; set; }
  
    }
}
