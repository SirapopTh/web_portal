﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel.DICOMDB
{
    public class  series1
    {
        public string series_uid { get; set; }
        public int series_uid_id { get; set; }
        public int study_uid_id { get; set; }
        public Nullable<int> series_number { get; set; }
        public string modality { get; set; }
        public string body_part { get; set; }
        public Nullable<int> series_date { get; set; }
        public Nullable<decimal> series_time { get; set; }
        public string series_desc { get; set; }
        public string req_proc_id { get; set; }
        public string sched_proc_id { get; set; }
        public Nullable<int> pps_date { get; set; }
        public Nullable<decimal> pps_time { get; set; }
        public string protocol_name { get; set; }
        public string operator_name { get; set; }
        public string model_name { get; set; }
    }
}
