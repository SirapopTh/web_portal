﻿using BaseData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel.DICOMDB
{

    public interface Istudy1Repository : IRepositoryBase
    {
        void InsertStudy1(study1 upload);
        void UpdateStudy1(study1 upload);

        void DeleteStudy1(string study_uid);
    }
    public class study1Repository : RepositoryBase, Istudy1Repository
    {
        public study1Repository(VnaDbContext vnaDbContext)
            : base(vnaDbContext)
        {

        }
        public void InsertStudy1(study1 upload)
        {
            this.Insert(upload);
        }
        public void UpdateStudy1(study1 upload)
        {
            this.Update(upload);
        }
        public void DeleteStudy1(string study_uid)
        {
            this.Delete<study1>(t => t.study_uid == study_uid);
        }
    }
}
