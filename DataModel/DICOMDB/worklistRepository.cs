﻿using BaseData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel.DICOMDB
{
    public interface IworklistRepository : IRepositoryBase
    {
        void Insertworklist(worklist upload);
        void Updateworklist(worklist upload);

        void Deleteworklist(string instance_uid);
    }
    public class worklistRepository : RepositoryBase, IworklistRepository
    {
        public worklistRepository(VnaDbContext vnaDbContext)
            : base(vnaDbContext)
        {

        }
        public void Insertworklist(worklist upload)
        {
            this.Insert(upload);
        }
        public void Updateworklist(worklist upload)
        {
            this.Update(upload);
        }
        public void Deleteworklist(string study_uid)
        {
            this.Delete<worklist>(t => t.study_instance_uid == study_uid);
        }
    }
}
