﻿using BaseData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel.DICOMDB
{

    public interface Iseries1Repository : IRepositoryBase
    {
        void InsertSeries1(series1 upload);
        void UpdateSeries1(series1 upload);

        void DeleteSeries1(string series_uid);
    }
    public class series1Repository : RepositoryBase, Iseries1Repository
    {
        public series1Repository(VnaDbContext vnaDbContext)
            : base(vnaDbContext)
        {

        }
        public void InsertSeries1(series1 upload)
        {
            this.Insert(upload);
        }
        public void UpdateSeries1(series1 upload)
        {
            this.Update(upload);
        }
        public void DeleteSeries1(string series_uid)
        {
            this.Delete<series1>(t => t.series_uid == series_uid);
        }
    }
}
