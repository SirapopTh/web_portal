﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel.DICOMDB
{
    public  class study1
    {
        public string study_uid { get; set; }
        public int study_uid_id { get; set; }
        public int ptn_id_id { get; set; }
        public Nullable<int> study_date { get; set; }
        public Nullable<decimal> study_time { get; set; }
        public string study_id { get; set; }
        public string accession_number { get; set; }
        public string physician_name { get; set; }
        public string study_desc { get; set; }
        public string request_service { get; set; }
        public string request_physician { get; set; }
    }
}
