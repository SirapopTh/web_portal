﻿using BaseData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel.DICOMDB
{

    public interface Iimage1Repository : IRepositoryBase
    {
        void InsertImage1(image1 upload);
        void UpdateImage1(image1 upload);

        void DeleteImage1(string instance_uid);
    }
    public class image1Repository : RepositoryBase, Iimage1Repository
    {
        public image1Repository(VnaDbContext vnaDbContext)
            : base(vnaDbContext)
        {

        }
        public void InsertImage1(image1 upload)
        {
            this.Insert(upload);
        }
        public void UpdateImage1(image1 upload)
        {
            this.Update(upload);
        }
        public void DeleteImage1(string instance_uid)
        {
            this.Delete<image1>(t => t.instance_uid == instance_uid);
        }
    }
}
