﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel.DICOMDB
{
    public class image1
    {
        public string instance_uid { get; set; }
        public int series_uid_id { get; set; }
        public Nullable<int> image_number { get; set; }
        public string image_file { get; set; }
        public string coded_file { get; set; }
        public string transfer_uid { get; set; }
        public string class_uid_id { get; set; }
        public string backup_label { get; set; }
        public string source_ae { get; set; }
        public Nullable<int> rcvd_date { get; set; }
        public Nullable<int> rcvd_time { get; set; }
        public Nullable<int> file_size { get; set; }
        public string transaction_uid { get; set; }
        public string deleted { get; set; }
        public string key_image { get; set; }
        public Nullable<int> n_rows { get; set; }
        public Nullable<int> n_columns { get; set; }
        public Nullable<int> bits_allocated { get; set; }
        public Nullable<int> n_frames { get; set; }
        public string pr_label { get; set; }
        public string pr_desc { get; set; }
        public Nullable<int> pr_date { get; set; }
        public Nullable<decimal> pr_time { get; set; }
        public string pr_creator { get; set; }
        public string completion { get; set; }
        public string verification { get; set; }
        public string observation_dt { get; set; }
        public string v_organization { get; set; }
        public string v_datetime { get; set; }
        public string v_observer_name { get; set; }
        public string titile_value { get; set; }
        public string title_designator { get; set; }
        public string title_meaning { get; set; }
        public string template_id { get; set; }
       // public Nullable<int> stage_number { get; set; }
      //  public Nullable<int> view_number { get; set; }
        public string slice_thickness { get; set; }
    }
}
