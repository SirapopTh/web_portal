﻿using BaseData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel.DICOMDB
{

    public interface Ipatient1Repository : IRepositoryBase
    {
        void Insertpatient1(patient1 upload);
        void Updatepatient1(patient1 upload);

        void Deletepatient1(string patient_id);
    }
    public class patient1Repository : RepositoryBase, Ipatient1Repository
    {
        public patient1Repository(VnaDbContext vnaDbContext)
            : base(vnaDbContext)
        {

        }
        public void Insertpatient1(patient1 upload)
        {
            this.Insert(upload);
        }
        public void Updatepatient1(patient1 upload)
        {
            this.Update(upload);
        }
        public void Deletepatient1(string patient_id)
        {
            this.Delete<patient1>(t => t.ptn_id == patient_id);
        }
    }
}
