﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel.DICOMDB
{
    public  class worklist
    {
        public string study_instance_uid { get; set; }
        public string patient_id { get; set; }
        public string patient_name { get; set; }
        public string patient_birth { get; set; }
        public string patient_sex { get; set; }
        public Nullable<decimal> patient_size { get; set; }
        public Nullable<decimal> patient_weight { get; set; }
        public string medical_alert { get; set; }
        public string contrast_allergy { get; set; }
        public string occupation { get; set; }
        public Nullable<short> pregnancy_status { get; set; }
        public string modality { get; set; }
        public string station_aet { get; set; }
        public string sched_start_date { get; set; }
        public string sched_start_time { get; set; }
        public string perform_physician { get; set; }
        public string request_physician { get; set; }
        public string refer_physician { get; set; }
        public string accession_num { get; set; }
        public string sched_procedure_id { get; set; }
        public string sched_procedure_desc { get; set; }
        public string req_procedure_id { get; set; }
        public string req_procedure_desc { get; set; }
        public string station_name { get; set; }
        public string req_proc_location { get; set; }
        public string sched_proc_location { get; set; }
        public string pre_medication { get; set; }
        public string admission_id { get; set; }
        public string special_needs { get; set; }
        public string patient_location { get; set; }
        public string patient_state { get; set; }
        public string req_proc_priority { get; set; }
        public string patient_transport { get; set; }
        public string confidentiality { get; set; }
        public string perfrmd_aet { get; set; }
        public string perfrmd_start_date { get; set; }
        public string perfrmd_start_time { get; set; }
        public string perfrmd_status { get; set; }
        public string mpps_sop_uid { get; set; }
        public string perfrmd_proc_id { get; set; }
        public string perfrmd_end_date { get; set; }
        public string perfrmd_end_time { get; set; }
        public string character_set { get; set; }
        public string service { get; set; }
        public string residence { get; set; }
        public string code_value { get; set; }
        public string code_designator { get; set; }
        public string code_meaning { get; set; }
    }
}
