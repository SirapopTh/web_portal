﻿using BaseData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel
{
    public interface IVna_UsersRepository : IRepositoryBase
    {
        void InsertVna_Users(vna_users vna_Users);
        vna_users GetVna_Users(string user_id);
        List<vna_users> GetVnaUsersList();
        void UpdateVna_Users(vna_users vna_Users);
    }
    public class Vna_UsersRepository : RepositoryBase,IVna_UsersRepository
    {
        public Vna_UsersRepository(VnaDbContext vnaDbContext)
            :base(vnaDbContext)
        {

        }
        public void InsertVna_Users(vna_users vna_Users)
        {
            this.Insert(vna_Users);
        }
        public vna_users GetVna_Users(string user_id)
        {
            return this.FindEntity<vna_users>(t => t.user_id == user_id);
        }
        public List<vna_users> GetVnaUsersList()
        {
            return this.IQueryable<vna_users>().ToList();
        }
        public void UpdateVna_Users(vna_users vna_Users)
        {
            this.Update<vna_users>(vna_Users);
        }

    }
}
