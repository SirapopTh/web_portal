﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel
{
    public class Download_Item
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }

        public int download_id { get; set; }
        public string src_filename { get; set; }
        public string src_filetype { get; set; }
        public string file_instanceid { get; set; }

        public string file_path { get; set; }

        public string status { get; set; }
    }
}
