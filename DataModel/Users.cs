﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel
{
    public class Users
    {
        //ID 属性就被默认为是自增的...
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int user_id_id { get; set; }//set; 不能用int64...

        public string user_id { get; set; }
        public string user_name { get; set; }
        public string user_password { get; set; }

        public string id_number { get; set; }
        public string medical_recno { get; set; }
        public string gender { get; set; }
        public string birthday { get; set; }
        public string email { get; set; }
        public string mobile_phone { get; set; }
        public string address { get; set; }
        public string header_icon { get; set; }
        public string institute_id { get; set; }
        public string department_id { get; set; }
        public string role_id { get; set; }
        public int user_authority { get; set; }
        public bool delete_mark { get; set; }
        public bool is_verified { get; set; }
        public bool is_administrator { get; set; }
        public bool is_initial { get; set; }
        public bool user_theme { get; set; }
        public int? source { get; set; }//
        public DateTime? create_date { get; set; }
    }
}
