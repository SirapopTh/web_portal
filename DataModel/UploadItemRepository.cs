﻿using BaseData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel
{
    public interface IUploadItemRepository : IRepositoryBase
    {
        void InsertUpload(UploadItem upload);
        void UpdateUpload(UploadItem upload);

        void DeleteUpload(string upload_item_id);
    }
    public class UploadItemRepository : RepositoryBase,IUploadItemRepository
    {
        public UploadItemRepository(VnaDbContext vnaDbContext)
            :base(vnaDbContext)
        {

        }
        public void InsertUpload(UploadItem upload)
        {
            this.Insert(upload);
        }
        public void UpdateUpload(UploadItem upload)
        {
            this.Update(upload);
        }
        public void DeleteUpload(string upload_item_id)
        {
            this.Delete<UploadItem>(t => t.upload_item_id == upload_item_id);
        }
    }
}
