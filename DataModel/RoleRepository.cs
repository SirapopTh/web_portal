﻿using BaseData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel
{
    public class RoleRepository :RepositoryBase, IRoleRepository
    {
        public RoleRepository(VnaDbContext vnaDb)
            : base(vnaDb)
        {

        }
        public void InsertRole(Role role)
        {
            base.Insert<Role>(role);
        }
        public Role GetRole(string roleid)
        {
            return base.FindEntity<Role>(t => t.role_id == roleid);
        }
        public void UpdateRole(Role role)
        {
            Update<Role>(role);
        }
        public List<Role> GetRoleList()
        {
            return this.IQueryable<Role>().ToList();
        }
    }
}
