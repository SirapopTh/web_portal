﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel
{
    //  TPH，Table Per Hierarchy，就是基类和派生类都映射到一张表，使用辨别列区分。

    //TPT，Table Per Type，就是基类存放到一张主表，每个派生类存放到一张表，通过外键与主表关联。

    //TPC，Table Per Concrete Type，就是基类和派生类都存放到单独的表中，没有主表。
    //在Map的时候，用MapInheritedProperties 配置
    // [Table("vna_upload")]
    public class VNA_Upload //: IUpload
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public  int upload_id_id { get; set; }//new

        public string upload_sessionid { get; set; }
        public string upload_id { get; set; }
  

        public int user_id_id { get; set; }
        public string medical_num { get; set; }
        public string native_id { get; set; }
        public string first_name { get; set; }
        public string middle_name { get; set; }
        public string last_name { get; set; }

        public DateTime? create_date { get; set; }

        public DateTime? study_date { get; set; }
        public string ref_physician { get; set; }

        public string report_type { get; set; }
        public string source_dept { get; set; }
        public string source_ofdata { get; set; }
        public string comment { get; set; }
        public string study_uid { get; set; }
        public string series_uid { get; set; }

        //
        // public string modality { get; set; }
    }
}
