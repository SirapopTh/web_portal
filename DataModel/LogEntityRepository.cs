﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BaseData;

namespace DataModel
{
    public interface ILogEntityRepository:IRepositoryBase
    {
        void InsertLog(LogEntity log);
    }
    public class LogEntityRepository :RepositoryBase,ILogEntityRepository
    {
        public LogEntityRepository(VnaDbContext vnaDb)
            : base(vnaDb)
        {

        }

        public void InsertLog(LogEntity log)
        {
            this.Insert(log);
        }
    }
}
