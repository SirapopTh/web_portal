﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BaseData;

namespace DataModel
{
    public class UserRegRepository : RepositoryBase, IUserRegRepository
    {
        public UserRegRepository(VnaDbContext vnaDb)
            :base(vnaDb)
        {

        }
        public bool CreateUserReg(UserReg reg)
        {
            /*
            using (var db = new BaseData.RepositoryBase()) //).BeginTrans()
            {
                if (reg != null)
                    db.Insert(reg);

              //  db.Commit();
            }*/
            this.Insert(reg); //no transaction..

            return true;
        }
        /// <summary>
        /// transactiong不在这里，在外面..
        /// </summary>
        /// <param name="sUserId"></param>
        public void ValidateUserReg(string sUserId)
        {
            /*
            using (var db = new BaseData.RepositoryBase())
            {
                
                UserReg userReg = db.FindEntity<UserReg>(t => t.user_email == sUserId);
                userReg.is_verified = true;
                userReg.verified_date = DateTime.Now;
                //
                db.Update<UserReg>(userReg);
            }
            */
            UserReg userReg = this.FindEntity<UserReg>(t => t.user_email == sUserId);
            userReg.is_verified = true;
            userReg.verified_date = DateTime.Now;

            Update(userReg);
        }
        public void ValidateUserReg(UserReg userReg)
        {
            //using (var db = new BaseData.RepositoryBase())
            //{
            //}
            userReg.is_verified = true;
            userReg.verified_date = DateTime.Now;
            //
            Update<UserReg>(userReg);
        }
            public UserReg GetUser(string sUserId)
        {
            UserReg userReg = null;
     

            userReg = FindEntity<UserReg>(t => t.user_email == sUserId);
                
            
            return userReg;
        }
        public UserReg GetUserByRegid(string sRegid)
        {
            UserReg userReg = null;


            userReg = FindEntity<UserReg>(t => t.regid == sRegid);


            return userReg;
        }
    }
}
