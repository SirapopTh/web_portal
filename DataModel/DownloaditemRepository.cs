﻿using BaseData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel
{

    public interface IDownloadItemRepository : IRepositoryBase
    {
        void InsertDownloadItem(Download_Item upload);
        void UpdateDownloadItem(Download_Item upload);

        void DeleteDownloadItem(int upload_item_id);
    }
    public class Download_ItemRepository : RepositoryBase, IDownloadItemRepository
    {
        public Download_ItemRepository(VnaDbContext vnaDbContext)
            : base(vnaDbContext)
        {

        }
        public void InsertDownloadItem(Download_Item upload)
        {
            this.Insert(upload);
        }
        public void UpdateDownloadItem(Download_Item upload)
        {
            this.Update(upload);
        }
        public void DeleteDownloadItem(int upload_item_id)
        {
            this.Delete<Download_Item>(t => t.download_id == upload_item_id);
        }
    }
}
