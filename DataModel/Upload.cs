﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel
{
    public abstract class IUpload
    {
        public string upload_sessionid { get; set; }
        public string upload_id { get; set; }
    //    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
     //   public int upload_id_id { get; set; }

        public int user_id_id { get; set; }
        public string medical_num { get; set; }
        public string native_id { get; set; }
        public string first_name { get; set; }
        public string middle_name { get; set; }
        public string last_name { get; set; }

        public DateTime? create_date { get; set; }

        public DateTime? study_date { get; set; }
        public string ref_physician { get; set; }

        public string report_type { get; set; }
        public string source_dept { get; set; }
        public string source_ofdata { get; set; }
        public string comment { get; set; }
        public string study_uid { get; set; }
        public string series_uid { get; set; }
    }
    
    /// <summary>
    /// EF 类的继承关系，有好几种，TPT,TPC,TPH，为了简单使用abstract作为base
    /// </summary>
    public class Upload: IUpload
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int upload_id_id { get; set; }
    }
}
