﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel
{
   public class UserReg
    {
        public string regid { get; set; }
        public string user_email { get; set; }
        public DateTime reg_date { get; set; }
        public bool is_verified { get; set; }
        public DateTime? verified_date {get;set;}
    }
}
