﻿using BaseData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel
{
    public interface IForgetPswRepository :IRepositoryBase
    {
        ForgetPasswd GetForgetPsw(string reqid);
        void CreateForgetPsw(string userid);
        void UpdateForgetPsw(ForgetPasswd forgetPasswd);
    }
    public class ForgetPasswdRepository:RepositoryBase,IForgetPswRepository
    {
        public ForgetPasswdRepository(VnaDbContext dbContext)
            :base(dbContext)
        {

        }
        public ForgetPasswd GetForgetPsw(string reqid)
        {
            return this.FindEntity<ForgetPasswd>(t => t.req_id == reqid);
        }
        public void CreateForgetPsw(string userid)
        {
            ForgetPasswd fp = new ForgetPasswd();
            //
            fp.req_id = Common.Common.GuId();
            fp.is_open = false;
            fp.user_email = userid;
            //
            this.Insert(fp);
        }
        public void UpdateForgetPsw(ForgetPasswd forgetPasswd)
        {
            this.Update(forgetPasswd);
        }

    }
}
