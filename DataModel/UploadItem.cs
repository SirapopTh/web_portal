﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel
{
    public class UploadItem
    {
        public string upload_item_id { get; set; }
        public int upload_id_id { get; set; }

       // public string upload_id { get; set; }
        public string src_file_name { get; set; }

        public string dest_file_name { get; set; }

        public DateTime? upload_at { get; set; }

        public string instance_uid { get; set; }
        public string status { get; set; }
    }
}
