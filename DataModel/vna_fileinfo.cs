﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel
{
    public class vna_fileinfo
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }
        public int upload_id_id { get; set; }
        public DateTime upload_date { get; set; }

        public string org_filename { get; set; }
        public string org_filetype { get; set; }
        public string study_uid { get; set; }
        public string series_uid { get; set; }
        public string instance_uid { get; set; }
        public string vna_filepath { get; set; }

        public string status { get; set; }
   
    }
}
