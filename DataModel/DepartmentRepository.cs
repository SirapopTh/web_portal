﻿using BaseData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel
{
    public interface IDepartmentRepository : IRepositoryBase
    {
        void InsertDepartment(Department dept);
        Department GetDepartment(string deptid);
        List<Department> GetDepartmentList();
        void UpdateDepartment(Department dept);
    }
    public class DepartmentRepository :RepositoryBase, IDepartmentRepository
    {
        public DepartmentRepository(VnaDbContext vnaDbContext)
            :base(vnaDbContext)
        {

        }
        public void InsertDepartment(Department dept)
        {
            this.Insert(dept);
        }
        public Department GetDepartment(string deptid)
        {
            return this.FindEntity<Department>( u => u.department_id == deptid);
        }
        public List<Department> GetDepartmentList()
        {
            return this.IQueryable<Department>().ToList();
        }
        public void UpdateDepartment(Department dept)
        {
            this.Update<Department>(dept);
        }
    }
}
