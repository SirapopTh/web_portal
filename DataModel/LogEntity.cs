﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel
{
    public class LogEntity // IEntity<LogEntity>
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int idno { get; set; }//set; string
        public DateTime? log_datetime { get; set; }
        public string account { get; set; }
        public string user_name { get; set; }
        public string log_type { get; set; }
        public string ip_address { get; set; }
       // public string F_IPAddressName { get; set; }
        //public string F_ModuleId { get; set; }
        public string module_name { get; set; }
        public bool? result { get; set; }
        public string description { get; set; }
        //public DateTime? F_CreatorTime { get; set; }
        //public string F_CreatorUserId { get; set; }
    }
}
