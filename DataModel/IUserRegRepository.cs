﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel
{
    public interface IUserRegRepository :BaseData.IRepositoryBase
    {
        bool CreateUserReg(UserReg userReg);
        UserReg GetUser(string sUserId);
        UserReg GetUserByRegid(string sRegid);
        void ValidateUserReg(string sUserId);
        void ValidateUserReg(UserReg userReg);
    }
}
