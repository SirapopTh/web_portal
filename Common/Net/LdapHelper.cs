﻿using System;
using System.Collections.Generic;
using System.DirectoryServices;
using System.DirectoryServices.Protocols;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public class LdapHelper
    {
        //参考下： https://docs.microsoft.com/en-us/previous-versions/dotnet/articles/bb332056(v=msdn.10)

        //username: test
        //domain: example.com
        //password: test
        //url: localhost:389

        /// <summary>
        /// https://auth0.com/blog/using-ldap-with-c-sharp/
        /// Another way of validating a user is to perform a bind. In this case, the server
        /// queries its own database to validate the credentials. The server defines
        /// how a user is mapped to its directory.
        /// </summary>
        /// <param name="username">Username</param>
        /// <param name="password">Password</param>
        /// <returns>true if the credentials are valid, false otherwise</returns>
        public static bool validateUserByBind(string username, string domain, string password, string url)
        {
            var credentials = new NetworkCredential(username, password, domain);
            var serverId = new LdapDirectoryIdentifier(url);

            LdapConnection connection = new LdapConnection(serverId, credentials);

            bool result = true;
           // var credentials = new NetworkCredential(username, password);
            //var serverId = new LdapDirectoryIdentifier(connection.SessionOptions.HostName);

            //var conn = new LdapConnection(serverId, credentials);
            try
            {
                connection.Bind();
            }
            catch (Exception)
            {
                result = false;
            }
            //DirectoryEntry root = new DirectoryEntry("LDAP://directory.example.com", uid, password, AuthenticationTypes.None);

            connection.Dispose();

            return result;
        }
        /// <summary>
        /// Searches for a user and compares the password.
        /// We assume all users are at base DN ou=users,dc=example,dc=com and that passwords are
        /// hashed using SHA1 (no salt) in OpenLDAP format.
        /// </summary>
        /// <param name="username">Username</param>
        /// <param name="password">Password</param>
        /// <returns>true if the credentials are valid, false otherwise</returns>
        public static bool validateUser(string username, string domain, string password, string url)
        {
            var credentials = new NetworkCredential(username, password, domain);
            var serverId = new LdapDirectoryIdentifier(url);

            LdapConnection connection = new LdapConnection(serverId, credentials);
            string[] domarrs = domain.Split(new char[] { ',' });
            string sdc = string.Empty;
            for(int ii=0;ii < domarrs.Length;++ii)
            {
                string stp = "dc=" + domarrs[ii];

                if (ii > 0)
                    sdc += ",";
                sdc += stp;
            }
            var sha1 = new SHA1Managed();
            var digest = Convert.ToBase64String(sha1.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password)));
            var request = new CompareRequest(string.Format("uid={0},ou=users,{1}", username,sdc),
                "userPassword", "{SHA}" + digest); //dc=example,dc=com
            bool result = true;

            try
            {
                var response = (CompareResponse)connection.SendRequest(request);

                result = response.ResultCode == ResultCode.CompareTrue;
                //
                connection.Dispose();
            }
            catch (Exception)
            {
                result = false;
            }

            return result;
        }
        /// <summary>
        /// Get Ladp users 
        /// http://www.kouti.com/tables/userattributes.htm  active directory user attributes
        /// https://stackoverflow.com/questions/23693209/how-to-get-all-the-users-details-from-active-directory-using-ldap
        /// 
        /// </summary>
        /// <param name="dominName"></param>
        /// <param name="ldapPath"></param>
        /// <param name="sUser"></param>
        /// <param name="sPass"></param>
        public static List<LDAPUser> GetLDAPUsers(string dominName, string ldapPath,string sUser,string sPass)
        {
            //string dominName = ConfigurationManager.AppSettings["DominName"].ToString();
            //string ldapPath = ConfigurationManager.AppSettings["ldapPath"].ToString();
            List<LDAPUser> lstUsers = new List<LDAPUser>();

            if (!String.IsNullOrEmpty(dominName) && !String.IsNullOrEmpty(ldapPath))
            {
                //LDAP://192.168.66.1/DC=ebm,DC=home
                if (!ldapPath.ToUpper().Contains("LDAP:"))
                    ldapPath = "LDAP://" + ldapPath;
                if (ldapPath[ldapPath.Length-1] != '/')
                    ldapPath += "/";
                //将domainname分拆
                string[] domainParts = dominName.Split(new char[] { '.' });
                for (int ii = 0; ii < domainParts.Length; ++ii)
                {
                    string sdm = domainParts[ii];
                    if (ii > 0)
                        ldapPath = ldapPath + ",DC=" + sdm;
                    else
                        ldapPath = ldapPath + "DC=" + sdm;
                   
                }
                DirectoryEntry entry = new DirectoryEntry(ldapPath, sUser, sPass, AuthenticationTypes.Secure);
                try
                {
                   // Object obj = entry.NativeObject;
                    //
                    DirectorySearcher search = new DirectorySearcher(entry);
                    //search user..
                    search.Filter = "(&(objectClass=user)(objectCategory=person))";
                    search.PropertiesToLoad.Add("samaccountname");
                    search.PropertiesToLoad.Add("mail");
                    search.PropertiesToLoad.Add("usergroup");
                    search.PropertiesToLoad.Add("givenName");//displayname // first name
                    search.PropertiesToLoad.Add("UserAccountControl");
                    search.PropertiesToLoad.Add("sn");
                    search.PropertiesToLoad.Add("telephoneNumber");
                    SearchResultCollection resultCollection = search.FindAll();

                    foreach (System.DirectoryServices.SearchResult resEnt in resultCollection)
                    {
                        System.DirectoryServices.DirectoryEntry de = resEnt.GetDirectoryEntry();
                        //if (de.Properties["sAMAccountName"].Value != null && de.Properties["userAccountControl"].Value != null)
                        //{
                        //    StringBuilder sb = new StringBuilder();
                        //    sb.AppendLine("Name = " + de.Properties["sAMAccountName"].Value.ToString());
                        //    sb.AppendLine("Email = " + de.Properties["mail"].Value.ToString());
                        //}
                        var userAccountControlValue = 0;
                        int.TryParse(de.Properties["UserAccountControl"].Value.ToString(), out userAccountControlValue);
                        var isAccountDisabled = Convert.ToBoolean(userAccountControlValue & 0x0002);
                        var isNormalAccount = Convert.ToBoolean(userAccountControlValue & 0x0200);
                    
                        //&& de.Properties["userAccountControl"].Value != null && de.Properties["userPrincipalName"].Value != null 
                        if (de.Properties["sAMAccountName"].Value != null && !isAccountDisabled && isNormalAccount)
                        {
                            //Add Employee details from AD
                            string sFirstName = de.Properties["givenName"].Value != null ? (string)de.Properties["givenName"].Value : "";
                            string sEmail = de.Properties["mail"].Value != null ? (string)de.Properties["mail"].Value : "";//userPrincipalName
                            string sLastName = de.Properties["sn"].Value != null ?(string)de.Properties["sn"].Value : "";

                            string sTelePhone = de.Properties["telephoneNumber"].Value != null ? (string)de.Properties["telephoneNumber"].Value : "";
                            string sUserAccount = de.Properties["sAMAccountName"].Value != null ? (string)de.Properties["sAMAccountName"].Value : "";

                            LDAPUser ldapUser = new LDAPUser()
                            {
                                UserAccount = sUserAccount,
                                Email = sEmail,
                                FirstName = sFirstName,
                                LastName = sLastName,
                                PhoneNumber = sTelePhone
                            };
                            //
                            lstUsers.Add(ldapUser);
                        }
                    }
                }
                catch(Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }

            return lstUsers;
        }


        /// <summary>
        /// //ds.Filter = "(&((&(objectCategory=Person)(objectClass=User)))(samaccountname=*"+ username + "*))";
        /// https://docs.microsoft.com/zh-cn/windows/win32/adsi/search-filter-syntax?redirectedfrom=MSDN
        /// </summary>
        /// <param name="dominName"></param>
        /// <param name="ldapPath"></param>
        /// <param name="sUser"></param>
        /// <param name="sPass"></param>
        /// <returns></returns>
        public static List<LDAPUser> GetLDAPUsersFilter(string dominName, string ldapPath, string sUser, string sPass,
            string account,string firstname,string lastname)
        {
           
            List<LDAPUser> lstUsers = new List<LDAPUser>();

            if (!String.IsNullOrEmpty(dominName) && !String.IsNullOrEmpty(ldapPath))
            {
                //LDAP://192.168.66.1/DC=ebm,DC=home
                if (!ldapPath.ToUpper().Contains("LDAP:"))
                    ldapPath = "LDAP://" + ldapPath;
                if (ldapPath[ldapPath.Length - 1] != '/')
                    ldapPath += "/";
                //将domainname分拆
                string[] domainParts = dominName.Split(new char[] { '.' });
                for (int ii = 0; ii < domainParts.Length; ++ii)
                {
                    string sdm = domainParts[ii];
                    if (ii > 0)
                        ldapPath = ldapPath + ",DC=" + sdm;
                    else
                        ldapPath = ldapPath + "DC=" + sdm;

                }
                DirectoryEntry entry = new DirectoryEntry(ldapPath, sUser, sPass, AuthenticationTypes.Secure);
                try
                {
                   // (&(objectClass = user)(objectCategory = person)(sn = *)(givenName = *)(samaccountname=*))
                    string sfilter = "&(objectClass = user)(objectCategory = person)";
                        if (!string.IsNullOrEmpty(account) || !string.IsNullOrEmpty(firstname)||
                        !string.IsNullOrEmpty(lastname))
                    {
                        if (!string.IsNullOrEmpty(account))
                            sfilter += $"(samaccountname = *{account}*)";
                        if(!string.IsNullOrEmpty(firstname))
                            sfilter += $"(givenName = *{firstname}*)";
                        if (!string.IsNullOrEmpty(lastname))
                            sfilter += $"(sn = *{lastname}*)";
                    }
                    sfilter = "(" + sfilter + ")";
                    //
                    DirectorySearcher search = new DirectorySearcher(entry);
                    //search user..
                    search.Filter = sfilter;// "(&(objectClass=user)(objectCategory=person))";
                    search.PropertiesToLoad.Add("samaccountname");
                    search.PropertiesToLoad.Add("mail");
                    search.PropertiesToLoad.Add("usergroup");
                    search.PropertiesToLoad.Add("givenName");//displayname // first name
                    search.PropertiesToLoad.Add("UserAccountControl");
                    search.PropertiesToLoad.Add("sn");
                    search.PropertiesToLoad.Add("telephoneNumber");
                    SearchResultCollection resultCollection = search.FindAll();

                    foreach (System.DirectoryServices.SearchResult resEnt in resultCollection)
                    {
                        System.DirectoryServices.DirectoryEntry de = resEnt.GetDirectoryEntry();
                        //if (de.Properties["sAMAccountName"].Value != null && de.Properties["userAccountControl"].Value != null)
                        //{
                        //    StringBuilder sb = new StringBuilder();
                        //    sb.AppendLine("Name = " + de.Properties["sAMAccountName"].Value.ToString());
                        //    sb.AppendLine("Email = " + de.Properties["mail"].Value.ToString());
                        //}
                        var userAccountControlValue = 0;
                        int.TryParse(de.Properties["UserAccountControl"].Value.ToString(), out userAccountControlValue);
                        var isAccountDisabled = Convert.ToBoolean(userAccountControlValue & 0x0002);
                        var isNormalAccount = Convert.ToBoolean(userAccountControlValue & 0x0200);

                        //&& de.Properties["userAccountControl"].Value != null && de.Properties["userPrincipalName"].Value != null 
                        if (de.Properties["sAMAccountName"].Value != null && !isAccountDisabled && isNormalAccount)
                        {
                            //Add Employee details from AD
                            string sFirstName = de.Properties["givenName"].Value != null ? (string)de.Properties["givenName"].Value : "";
                            string sEmail = de.Properties["mail"].Value != null ? (string)de.Properties["mail"].Value : "";//userPrincipalName
                            string sLastName = de.Properties["sn"].Value != null ? (string)de.Properties["sn"].Value : "";

                            string sTelePhone = de.Properties["telephoneNumber"].Value != null ? (string)de.Properties["telephoneNumber"].Value : "";
                            string sUserAccount = de.Properties["sAMAccountName"].Value != null ? (string)de.Properties["sAMAccountName"].Value : "";

                            LDAPUser ldapUser = new LDAPUser()
                            {
                                UserAccount = sUserAccount,
                                Email = sEmail,
                                FirstName = sFirstName,
                                LastName = sLastName,
                                PhoneNumber = sTelePhone
                            };
                            //
                            lstUsers.Add(ldapUser);
                        }
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }

            return lstUsers;
        }

    }
    public class LDAPUser
    {
        public string UserAccount { get; set; }
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PhoneNumber { get; set; }
    }
}
