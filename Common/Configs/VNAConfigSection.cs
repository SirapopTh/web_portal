﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common//.Configs
{
    public class VNAConfigSection : ConfigurationSection
    {
        public VNAConfigSection()
        {
        }
        private static VNAConfigSection _Instance = null;

        public static VNAConfigSection Instance
        {
            get
            {
                if (_Instance == null)
                {
                    _Instance = ConfigurationManager.GetSection("VNASection") as VNAConfigSection;
                }
                return _Instance;
            }
        }

        //
        [ConfigurationProperty("Site")]
        public SiteElement SiteElement
        {
            get { return (SiteElement)this["Site"]; }
        }
        //sitev to vna from portal
        [ConfigurationProperty("SiteV",IsRequired =false)]
        public SiteElement SiteElementV
        {
            get { return (SiteElement)this["SiteV"]; }
        }
        [ConfigurationProperty("Mail")]
        public EmailElement emailElement
        {
            get { return (EmailElement)this["Mail"]; }
        }

        [ConfigurationProperty("Eps")]
        public EpsElement epsElement
        {
            get { return (EpsElement)this["Eps"]; }
        }

        [ConfigurationProperty("ViewerRegion")]
        public ViewerRegionElement viewerRegionElement
        {
            get { return (ViewerRegionElement)this["ViewerRegion"]; }
        }
        [ConfigurationProperty("ServerList", IsDefaultCollection = true)]
        [ConfigurationCollection(typeof(ServerListElement),AddItemName = "Server")]
        public ServerListElement ServerList
        {
            get { return (ServerListElement)this["ServerList"]; }
            set { this["ServerList"] = value; }
        }

        [ConfigurationProperty("LDAP")]
        public LDAPElement LdapElement
        {
            get { return (LDAPElement)this["LDAP"]; }
        }
    }
    public class SiteElement : ConfigurationElement
    {
        [ConfigurationProperty("Home", IsRequired = false)]
        public string Home
        {
            get { return this["Home"].ToString(); }
        }

        [ConfigurationProperty("ImageHome", IsRequired = false, DefaultValue = "")]
        public string ImageHome
        {
            get { return (string)this["ImageHome"]; }
            set { this["ImageHome"] = value; }
        }
        
        [ConfigurationProperty("Html5Home", IsRequired = false, DefaultValue = "")]
        public string Html5Home
        {
            get { return (string)this["Html5Home"]; }
            set { this["Html5Home"] = value; }
        }
        [ConfigurationProperty("UniUser", IsRequired = false, DefaultValue = "")]
        public string UniUser
        {
            get { return (string)this["UniUser"]; }
            set { this["UniUser"] = value; }
        }
        [ConfigurationProperty("UniPwd", IsRequired = false, DefaultValue = "")]
        public string UniPwd
        {
            get { return (string)this["UniPwd"]; }
            set { this["UniPwd"] = value; }
        }
        
          [ConfigurationProperty("H5Pwd", IsRequired = false, DefaultValue = "")]
        public string H5Pwd
        {
            get { return (string)this["H5Pwd"]; }
            set { this["H5Pwd"] = value; }
        }
        
        [ConfigurationProperty("H5User", IsRequired = false, DefaultValue = "")]
        public string H5User
        {
            get { return (string)this["H5User"]; }
            set { this["H5User"] = value; }
        }
        //待用
        [ConfigurationProperty("UrlPath", IsRequired = false, DefaultValue = "")]
        public string UrlPath
        {
            get { return (string)this["UrlPath"]; }
            set { this["UrlPath"] = value; }
        }
    }
    public class EmailElement: ConfigurationElement
    {
        [ConfigurationProperty("SmtpHost",IsRequired =true)]
        public string SmtpHost
        {
            get { return this["SmtpHost"].ToString(); }
        }

        [ConfigurationProperty("SmtpLoginId", IsRequired = true)]
        public string SmtpLoginId
        {
            get { return this["SmtpLoginId"].ToString(); }
        }

        [ConfigurationProperty("SmtpLoginPasswd", IsRequired = true)]
        public string SmtpLoginPasswd
        {
            get { return this["SmtpLoginPasswd"].ToString(); }
        }

        [ConfigurationProperty("SenderName", IsRequired = true)]
        public string SenderName
        {
            get { return this["SenderName"].ToString(); }
        }

        [ConfigurationProperty("SenderAddr", IsRequired = true)]
        public string SenderAddr
        {
            get { return this["SenderAddr"].ToString(); }
        }
    }

    public class EpsElement : ConfigurationElement
    {
        [ConfigurationProperty("LocalAE", IsRequired = true)]
        public string LocalAE
        {
            get { return this["LocalAE"].ToString(); }
        }
        [ConfigurationProperty("EpsAE", IsRequired = true)]
        public string EpsAE
        {
            get { return this["EpsAE"].ToString(); }
        }

        [ConfigurationProperty("EpsIP", IsRequired = true)]
        public string EpsIP
        {
            get { return this["EpsIP"].ToString(); }
        }

        [ConfigurationProperty("EpsPort", IsRequired = true)]
        public string EpsPort
        {
            get { return this["EpsPort"].ToString(); }
        }

        [ConfigurationProperty("LocalPort", IsRequired = true)]
        public string LocalPort
        {
            get { return this["LocalPort"].ToString(); }
        }
    }


    public class ViewerRegionElement : ConfigurationElement
    {
        [ConfigurationProperty("IDOViewerRegion", IsRequired = true, DefaultValue = "")]
        public string IDOViewerRegion
        {
            get { return (string)this["IDOViewerRegion"]; }
            set { this["IDOViewerRegion"] = value; }
        }
    }

    public class ServerElement : ConfigurationElement
    {
        [ConfigurationProperty("AE", IsRequired = true)]
        public string AE
        {
            get { return this["AE"].ToString(); }
        }

        [ConfigurationProperty("IP", IsRequired = true)]
        public string IP
        {
            get { return this["IP"].ToString(); }
        }

        [ConfigurationProperty("Port", IsRequired = true)]
        public string Port
        {
            get { return this["Port"].ToString(); }
        }
        [ConfigurationProperty("DisplayName", IsRequired = false)]
        public string DisplayName
        {
            get { return this["DisplayName"].ToString(); }
        }
    }
    public class ServerListElement : ConfigurationElementCollection
    {
        /// <summary>
        /// 创建新元素
        /// </summary>
        /// <returns></returns>
        protected override ConfigurationElement CreateNewElement()
        {
            return new ServerElement();
        }

        /// <summary>
        /// 获取元素的键
        /// </summary>
        /// <param name="element"></param>
        /// <returns></returns>
        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((ServerElement)element).AE;
        }

        /// <summary>
        /// 获取所有键
        /// </summary>
        public IEnumerable<string> AllKeys { get { return BaseGetAllKeys().Cast<string>(); } }

        /// <summary>
        /// 索引器
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public new ServerElement this[string key]
        {
            get { return (ServerElement)BaseGet(key); }
        }
        public new ServerElement this[int index]
        {
            get { return (ServerElement)BaseGet(index); }
        }
    }

    public class LDAPElement : ConfigurationElement
    {
        [ConfigurationProperty("Url", IsRequired = false, DefaultValue = "")]
        public string LDAPUrl
        {
            get { return (string)this["Url"]; }
            set { this["Url"] = value; }
        }
        
         [ConfigurationProperty("Domain", IsRequired = false, DefaultValue = "")]
        public string Domain
        {
            get { return (string)this["Domain"]; }
            set { this["Domain"] = value; }
        }
    }
}
