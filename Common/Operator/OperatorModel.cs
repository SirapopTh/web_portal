﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public class OperatorModel
    {
        public string UserId { get; set; }
        public string UserCode { get; set; }
        public string UserName { get; set; }
        public string UserPwd { get; set; }
        public string InstituteId { get; set; }
        public string DepartmentId { get; set; }
        public string RoleId { get; set; }
        public string headImageFile { get; set; }

        public string Authority { get; set; } //
        public string LoginIPAddress { get; set; }
        public string LoginIPAddressName { get; set; }
        public string LoginToken { get; set; }
        public DateTime LoginTime { get; set; }
        public bool IsSystem { get; set; }
        public bool IsLDAP { get; set; }
        public bool UserTheme { get; set; }
    }
}
