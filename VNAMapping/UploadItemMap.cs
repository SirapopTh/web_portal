﻿using DataModel;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VNAMapping
{
    public class UploadItemMap :EntityTypeConfiguration<UploadItem>
    {
        public UploadItemMap()
        {
            this.HasKey(t => t.upload_item_id);
            this.ToTable("upload_item");
        }
    }
}
