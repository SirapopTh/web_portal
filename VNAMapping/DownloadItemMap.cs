﻿using DataModel;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VNAMapping
{
    public class DownloadItemMap : EntityTypeConfiguration<Download_Item>
    {
        public DownloadItemMap()
        {
            this.HasKey(t => t.id);
            this.ToTable("download_item");
        }
    }
}
