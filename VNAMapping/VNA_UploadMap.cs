﻿using DataModel;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VNAMapping
{

    public class VNA_UploadMap : EntityTypeConfiguration<VNA_Upload>
    {
        public VNA_UploadMap()
        {
            this.HasKey(t => t.upload_id);
            this.ToTable("vna_upload");
            /*
            Map(m => {
                m.ToTable("vna_upload");
                m.MapInheritedProperties(); //从基类继承属性到table TPC
            });*/
        }
    }
}
