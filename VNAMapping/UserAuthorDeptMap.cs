﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataModel;

namespace VNAMapping
{
    public class UserAuthorDeptMap : EntityTypeConfiguration<UserAuthorDept>
    {
        public UserAuthorDeptMap()
        {
            this.ToTable("user_authordept");
            this.HasKey(t => t.id);
        }
    }
}
