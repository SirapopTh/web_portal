﻿using DataModel;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VNAMapping
{
    class vna_usersMap :EntityTypeConfiguration<vna_users>
    {
        public vna_usersMap()
        {
            this.HasKey(t => t.user_id);
            this.ToTable("vna_users");
        }
    }
}
