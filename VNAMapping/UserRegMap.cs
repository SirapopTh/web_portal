﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity.ModelConfiguration;
using DataModel;

namespace VNAMapping
{
    /// <summary>
    /// 将table 与class 映射..
    /// </summary>
    public class UserRegMap :EntityTypeConfiguration<UserReg>
    {
        public UserRegMap()
        {
            this.ToTable("user_reg");
            this.HasKey(t => t.regid);
        }
    }
}
