﻿using DataModel;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VNAMapping
{
    public class UsersMap:EntityTypeConfiguration<Users>
    {
        public UsersMap()
        {
            this.ToTable("users");
            this.HasKey(t => t.user_id);
        }
    }
}
