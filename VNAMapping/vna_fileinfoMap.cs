﻿using DataModel;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VNAMapping
{
    public class vna_fileinfoMap :EntityTypeConfiguration<vna_fileinfo>
    {
        public vna_fileinfoMap()
        {
            this.HasKey(t => t.id);
            this.ToTable("vna_fileinfo");
        }
    }
}
