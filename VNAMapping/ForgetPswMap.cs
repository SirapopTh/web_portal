﻿using DataModel;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VNAMapping
{
    public class ForgetPswMap: EntityTypeConfiguration<ForgetPasswd>
    {
        public ForgetPswMap()
        {
            this.ToTable("user_forgetpw");
            this.HasKey(t => t.req_id);
        }
    }
}
