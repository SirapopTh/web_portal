﻿using DataModel.DICOMDB;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VNAMapping.DICOMDB
{
 
    public class patient1Map : EntityTypeConfiguration<patient1>
    {
        public patient1Map()
        {
            this.ToTable("patient1");
            this.HasKey(t => t.ptn_id_id);
        }
    }
}
