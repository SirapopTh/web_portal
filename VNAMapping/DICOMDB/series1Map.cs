﻿using DataModel.DICOMDB;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VNAMapping.DICOMDB
{

    public class series1Map : EntityTypeConfiguration<series1>
    {
        public series1Map()
        {
            this.ToTable("series1");
            this.HasKey(t => t.series_uid);
        }
    }
}
