﻿using DataModel.DICOMDB;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VNAMapping.DICOMDB
{
    public class study1Map : EntityTypeConfiguration<study1>
    {
        public study1Map()
        {
            this.ToTable("study1");
            this.HasKey(t => t.study_uid);
        }
    }
}
