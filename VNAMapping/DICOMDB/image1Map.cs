﻿using DataModel.DICOMDB;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VNAMapping.DICOMDB
{

    public class image1Map : EntityTypeConfiguration<image1>
    {
        public image1Map()
        {
            this.ToTable("image1");
            this.HasKey(t => t.instance_uid);
        }
    }
}
