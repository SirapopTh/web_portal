﻿using DataModel;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VNAMapping
{
    public class UploadMap : EntityTypeConfiguration<Upload>
    {
        public UploadMap()
        {
            this.HasKey(t => t.upload_id);
            this.ToTable("upload");
        }
    }
}
