﻿using BaseData;
using Common;
using DataModel;
using DicomKit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;

namespace WcfService1
{
    /// <summary>
    /// 只有当IIS有访问时，如wcf有被呼叫时，Application_Start才会被执行
    /// </summary>
    public class Global : System.Web.HttpApplication
    {
        private static Timer timer;
        private static readonly object _syncLock = new object();

        protected void Application_Start(object sender, EventArgs e)
        {
            //yys add dicom dll path. 放到bin下面
            //对于64位的dll,调试时iis express要设成64位，在工具-选项-项目和解决方案-web顶目里.
            String _path = String.Concat(System.Environment.GetEnvironmentVariable("PATH"), ";", System.AppDomain.CurrentDomain.RelativeSearchPath);
            System.Environment.SetEnvironmentVariable("PATH", _path, EnvironmentVariableTarget.Process);

            //
            try
            {
                if (!System.IO.Directory.Exists(Server.MapPath("~/Log")))
                    System.IO.Directory.CreateDirectory(Server.MapPath("~/Log"));
            }
            catch//(Exception ex)
            {

            }
            if (timer == null)
            {
                lock (_syncLock)
                {
                    if (timer == null)
                    {
                        timer = new Timer(sotreDcmFiles, null, 0, 300 * 1000);
                    }
                }

            }
        }

        /// <summary>
        /// 对未上传的文件进行上传...这里是对失败的文件重传
        /// </summary>
        /// <param name="state"></param>
        private void sotreDcmFiles(object state)
        {
            bool bautoStore = false;
            if (!bautoStore)
                return;

            VnaDbContext vnaDbContext = new VnaDbContext();
            IVna_FileinfoRepository uploadItemRepository = new Vna_FileinfoRepository(vnaDbContext);

            List<vna_fileinfo> uploadItems = uploadItemRepository.FindList<vna_fileinfo>("select * from vna_fileinfo where status='7'");

            if (uploadItems != null)
            {
                foreach (vna_fileinfo itm in uploadItems)
                {
                    //.......................

                    List<string> lstDcmFiles = new List<string>();

                    lstDcmFiles.Add(itm.vna_filepath);
                    //
                    //start store file to eps..
                    StoreKit storeKit = StoreKit.Instance;
                    string sLocalAe = VNAConfigSection.Instance.epsElement.LocalAE;
                    string epsAe = VNAConfigSection.Instance.epsElement.EpsAE, epsIp = VNAConfigSection.Instance.epsElement.EpsIP;
                    string epsPort = VNAConfigSection.Instance.epsElement.EpsPort;

                    List<string> lstFailed = new List<string>();

                    storeKit.storeSopclassuid = Configs.GetValue("sopclassuid");
                    //storescu时，有问题,因为dicomlib里面的回调函数是全局的...
                    //意味着只能一个dicomCommu的实例的回调会被执行到..
                    int iref = storeKit.DicomStoreU(lstDcmFiles.ToArray(), sLocalAe, epsAe, epsIp, epsPort, "64", ref lstFailed);
                    if (iref == 0)
                    {
                        itm.status = "4"; //store success.
                                          //update db...
                        uploadItemRepository.UpdateVna_Fileinfo(itm);
                        uploadItemRepository.Commit();
                    }
                    else
                        itm.status = "7"; //store failed
                                          //

                    //

                }
            }
        }

        protected void Session_Start(object sender, EventArgs e)
        {
            // Code that runs when a new session is started
            //yys for 奇怪 exception..
            string sessionId = Session.SessionID;
        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {

        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {

        }

        protected void Application_Error(object sender, EventArgs e)
        {

        }

        protected void Session_End(object sender, EventArgs e)
        {

        }

        protected void Application_End(object sender, EventArgs e)
        {

        }
    }
}