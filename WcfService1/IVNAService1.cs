﻿using Common;
using DataModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace VNAWcfService
{/*
    WCF中应用各个标记时所作的序列化处理：

1.          不给任何标记将会做XML映射，所有公有属性/字段都会被序列化
2.          [Serializable]标记会将所有可读写字段序列化
3.        [DataContract]
    和[DataMember] 联合使用来标记被序列化的字段

    所以一个class 如果所有不加dataContrace，那么所有属性都会默认序列化，如果加了DataContrace就要加DataMember来控制哪些要序列化..

    */
    // 注意: 使用“重构”菜单上的“重命名”命令，可以同时更改代码和配置文件中的接口名“IVNAService1”。
    [ServiceContract]
    public interface IVNAService1
    {

        [OperationContract]
        string GetData(int value);

        [OperationContract]
        CompositeType GetDataUsingDataContract(CompositeType composite);
        [OperationContract]
        List<FileStruct> GetUserFiles(string userId);

        //上传文件
        [OperationContract]
        UpFileResult UpLoadFile(UpFile upFile);

        //上传upload table..
        [OperationContract]
        ReturnState UpLoadInfo(VNA_Upload[] uploads);

        [OperationContract]
        ReturnState DeleteUpload(string upload_id);
        //下载文件
        [OperationContract]
        DownFileResult DownLoadFile(DownFile downfile);
        //405 error 参考 https://stackoverflow.com/questions/18004206/protocolexception-unhandled-405-method-not-allowed-with-wcf-bindings-and-endp
        // [WebGet(UriTemplate ="")]//试一下web get ? WebInvoke
        [OperationContract]
        QueryReturn QueryVNA(QueryParam queryParam, Pagination pagination);

        [OperationContract]
        List<QueryDownloadReturn> QueryDownload(string[] download_uids);

        [OperationContract]
        List<string> QueryInstanceUidsOfStudy(string studyuid);
    }

    //如果不使用MessageContract，传入参数或传出参数若有Stream对象，那便只能有一个Stream对象。
    //比如你传文件，传了文件数据，还想传文件名，就有点愁人了。要用MessageContract，把文件名等等一些文件的元数据用MessageHeader标记，
    //文件数据流用MessageBody标记可以成功。

    [MessageContract]
    public class DownFile
    {
       // [MessageHeader]
      //  public string inFileName { get; set; }
        [MessageHeader]
        public string FileId { get; set; }

    }
    /// <summary>
    /// 有MessageContract的operationContract，其它参数类型也要是MessageContract的
    /// </summary>
    [MessageContract]
    public class UpFileResult
    {
        [MessageHeader]
        public bool IsSuccess { get; set; }
        [MessageHeader]
        public string Message { get; set; }
    }

    [MessageContract]
    public class UpFile
    {
        [MessageHeader]
        public string UserId { get; set; }

        [MessageHeader]
        public int UploadId_Id { get; set; } //上传文件所在的uploadid...
        [MessageHeader]
        public DateTime upload_date { get; set; }
        [MessageHeader]
        public string org_filename { get; set; }
        [MessageHeader]
        public string org_filetype { get; set; }
        [MessageHeader]
        public string study_uid { get; set; }
        [MessageHeader]
        public string series_uid { get; set; }
        [MessageHeader]
        public string instance_uid { get; set; }


        [MessageHeader]
        public long FileSize { get; set; }
        [MessageHeader]
        public string FileName { get; set; }
        [MessageBodyMember]
        public Stream FileStream { get; set; }
    }

    [MessageContract]
    public class DownFileResult
    {
        [MessageHeader]
        public long FileSize { get; set; }
        [MessageHeader]
        public string FileName { get; set; }
        [MessageHeader]
        public bool IsSuccess { get; set; }
        [MessageHeader]
        public string Message { get; set; }
        [MessageBodyMember]
        public Stream FileStream { get; set; }
    }

    [DataContract]
    public class FileStruct
    {
        [DataMember]
        public string FileName { get; set; }
        [DataMember]
        public string FileType { get; set; }

        [DataMember]
        public string UploadDate { get; set; }

    }
    [DataContract]
    public class UserFileList
    {
        [DataMember]
        public string UserId;

        [DataMember]
        public List<FileStruct> UserFiles { get; set; }
    }

    // 使用下面示例中说明的数据约定将复合类型添加到服务操作。
    [DataContract]
    public class CompositeType
    {
        bool boolValue = true;
        string stringValue = "Hello ";

        [DataMember]
        public bool BoolValue
        {
            get { return boolValue; }
            set { boolValue = value; }
        }

        [DataMember]
        public string StringValue
        {
            get { return stringValue; }
            set { stringValue = value; }
        }
    }
    [DataContract]
    public class QueryParam
    {
        [DataMember]
        public string medicno { get; set; }
        [DataMember]
        public string nativeid { get; set; }
        [DataMember]
        public string name { get; set; }
        [DataMember]
        public string studydate { get; set; }
        [DataMember]
        public string studydate2 { get; set; }
        [DataMember]
        public string uploaddate { get; set; }
        [DataMember]
        public string uploaddate2 { get; set; }
        [DataMember]
        public string modality { get; set; }

        [DataMember]
        public string refphysician { get; set; }

        [DataMember]
        public string deptid { get; set; }
    }

    /// <summary>
    /// for return parameter.
    /// </summary>
    public class VNA_Upload_Model ///: VNA_Upload //不能直接IUpload ，说序列化的时候有问题..//
    {
        public string upload_sessionid { get; set; }
        public string upload_id { get; set; }
        //    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int upload_id_id { get; set; }

        public int user_id_id { get; set; }
        public string medical_num { get; set; }
        public string native_id { get; set; }
        public string first_name { get; set; }
        public string middle_name { get; set; }
        public string last_name { get; set; }

        public DateTime? create_date { get; set; }

        public DateTime? study_date { get; set; }
        public string ref_physician { get; set; }

        public string report_type { get; set; }
        public string source_dept { get; set; }
        public string source_ofdata { get; set; }
        public string comment { get; set; }
        public string study_uid { get; set; }
        public string series_uid { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string modality { get; set; }

        public string org_filename { get; set; }
    }

    public class QueryDownloadReturn: VNA_Upload_Model
    {
       // public string org_filename { get; set; }
        public string org_filetype { get; set; }
        public string instance_uid { get; set; }

        public DateTime upload_date { get; set; }
    }
    /// <summary>
    /// query 返回的结果
    /// </summary>
    public class QueryReturn
    {
      
        public List<VNA_Upload_Model> VNA_Uploads { get; set; }
        public Pagination Pagination { get; set; }
        public string ViewerRegion { get; set; } //for open from portal
    }
    //  [DataContract]
    //  [Table("upload")]
    /// <summary>
    /// 来自于upload table
    /// </summary>
    ///
    public class UploadReturn :VNA_Upload //QueryParam
    {
       // [DataMember]
        public List<string> study_uids { get; set; }//返回时,考虑到对上传的Upload，在pacs中可能会生成多的file
    }
    public class ReturnState
    {
        public bool IsSuccess { get; set; }
        public string Message { get; set; }
    }
}