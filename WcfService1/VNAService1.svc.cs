﻿using BaseData;
using Common;
using DataModel;
using DataModel.DICOMDB;
using DicomKit;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.SqlClient;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace VNAWcfService
{
    // 注意: 使用“重构”菜单上的“重命名”命令，可以同时更改代码、svc 和配置文件中的类名“VNAService1”。
    // 注意: 为了启动 WCF 测试客户端以测试此服务，请在解决方案资源管理器中选择 VNAService1.svc 或 VNAService1.svc.cs，然后开始调试。
    public class VNAService1 : IVNAService1
    {
        //添加一子目录可用于portal和vna在同一台目录下
        string msFilePath = "~/App_Data/Files/VNA";//Path.Combine(System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath, "App_Data", "Files");

        public Log FileLog
        {
            get { return LogFactory.GetLogger(this.GetType().ToString()); }
        }
        public string GetData(int value)
        {
            return string.Format("You entered: {0}", value);
        }

        public CompositeType GetDataUsingDataContract(CompositeType composite)
        {
            if (composite == null)
            {
                throw new ArgumentNullException("composite");
            }
            if (composite.BoolValue)
            {
                composite.StringValue += "Suffix";
            }
            return composite;
        }
       // [OperationContract]
        public List<FileStruct> GetUserFiles(string userId)
        {
            if (string.IsNullOrEmpty(userId))
            {
                throw new ArgumentException("Argument is empty.");
            }

            UserFileList userFileList = new UserFileList();
            userFileList.UserId = userId;
            List<FileStruct> fileStructs = new List<FileStruct>();

            userFileList.UserFiles = fileStructs;

            fileStructs.Add(new FileStruct() { FileName = "1.txt", FileType = "txt", UploadDate = "2010-10-10" });

            return fileStructs;
        }

       // [OperationContract]
        public UpFileResult UpLoadFile(UpFile upFile)
        {

            UpFileResult result = new UpFileResult();
            result.IsSuccess = false;

            try
            {
                FileLog.Info("start handle " + upFile.FileName);
                string same = Configs.GetValue("VnaPortalSameEps");
                bool sameEps = false;
                if (string.IsNullOrEmpty(same) || same != "1")
                {
                    sameEps = false;
                }
                else
                    sameEps = true;
                string realPath = System.Web.Hosting.HostingEnvironment.MapPath(msFilePath);
                //190726
                if (!Directory.Exists(realPath))
                {
                    try
                    {
                        Directory.CreateDirectory(realPath);
                    }
                    catch(Exception ex)
                    {
                        FileLog.Error(ex.Message);
                    }
                }
                List<UpFile> tmps = new List<UpFile>();
                tmps.Add(upFile);

                UpFile[] fileDatas = tmps.ToArray();
                //string path = System.AppDomain.CurrentDomain.BaseDirectory + @"\service\" + filedata.UserId + "\\";
                if (fileDatas == null || fileDatas.Length == 0)
                    return result;

                // Log log = LogFactory.GetLogger(this.GetType().ToString());
                FileLog.Info("receiveing: "+ upFile.FileName +"\r\n");
            
                realPath = Path.Combine(realPath, upFile.UserId); //

                if (!Directory.Exists(realPath))
                    Directory.CreateDirectory(realPath);

                foreach (UpFile filedata in fileDatas)
                {
                    string tempPath = Path.Combine(realPath, filedata.FileName);
                    if (File.Exists(tempPath)) //path + filedata.FileName
                    {

                        result.Message = filedata.FileName + " is existed.";

                        FileLog.Error(tempPath + " is existed.\r\n");

                        return result;
                    }
                }
                //
                VnaDbContext vnadbContext = new VnaDbContext();
                IVna_FileinfoRepository fileinfoRepository = new Vna_FileinfoRepository(vnadbContext);
                bool bsresult = true;
                List<string> succFiles = new List<string>();

                foreach (UpFile filedata in fileDatas)
                {
                    byte[] buffer = new byte[filedata.FileSize];

                    string sdestFile = Path.Combine(realPath, filedata.FileName);


                     FileStream fs = new FileStream(sdestFile, FileMode.Create, FileAccess.Write);
                   // MemoryStream stream = new MemoryStream();

                    int count = 0;
                    while ((count = filedata.FileStream.Read(buffer, 0, buffer.Length)) > 0)
                    {
                        fs.Write(buffer, 0, count);
                        //stream.Write(buffer,0, count);
                    }
                   // stream.Position = 0;
                    
                    //清空缓冲区
                    fs.Flush();
                    //关闭流
                    fs.Close();
                    
                    //----------------yys for zip----------
                    //using (FileStream decompressedFileStream = new FileStream(sdestFile, FileMode.Create, FileAccess.Write))// File.Create(newFileName)
                    //{
                    //    using (GZipStream decompressionStream = new GZipStream(stream, CompressionMode.Decompress))
                    //    {
                    //        decompressionStream.CopyTo(decompressedFileStream);
                    //        //
                    //    }
                    //}
                    //--------------------------------------//
                    //store index to DB....

                    vna_fileinfo vfileinfo = new vna_fileinfo();
                    vfileinfo.instance_uid = filedata.instance_uid;
                    vfileinfo.org_filename = filedata.org_filename;
                    vfileinfo.org_filetype = filedata.org_filetype;
                    vfileinfo.series_uid = filedata.series_uid;
                    vfileinfo.status = string.Empty;
                    vfileinfo.study_uid = filedata.study_uid;
                    vfileinfo.upload_date = filedata.upload_date;
                    vfileinfo.upload_id_id = filedata.UploadId_Id;
                    vfileinfo.vna_filepath = sdestFile;
                    //
                   // fileinfoRepository.InsertVna_Fileinfo(vfileinfo);

                    //file store to eps.......
                    //看看vna和partal是否连同一台eps
                    if(!sameEps)
                        bsresult = StoreFileToEps(sdestFile);
                    else
                        bsresult = true;
                    //------------------------------------------------//
                    if (!bsresult)
                    {
                        FileLog.Info("store eps failed " + sdestFile + "\r\n");
                        //delete file
                        File.Delete(sdestFile);

                        FileLog.Info("delete failed file" + sdestFile + "\r\n");

                        break;//如果只是store失败？？
                    }
                    else
                    {
                        vfileinfo.status = "4"; //store success.
                                                //update db...,此时还没有写入DB，不能够update....
                        fileinfoRepository.InsertVna_Fileinfo(vfileinfo); //UpdateVna_Fileinfo

                        FileLog.Info("store to eps " + upFile.FileName);
                        FileLog.Info("insert vna_fileinfo " );
                        FileLog.Info(vfileinfo.id.ToString() + "," + vfileinfo.org_filename + "," + vfileinfo.instance_uid + ","
                            + vfileinfo.org_filetype + "," + vfileinfo.series_uid + ","+vfileinfo.status + ","+vfileinfo.study_uid + ","
                            +vfileinfo.upload_date + "," +vfileinfo.upload_id_id.ToString() + ","+vfileinfo.vna_filepath + " \r\n") ;
                        //
                        succFiles.Add(sdestFile);
                    }
             
                    //  vfileinfo.status = "7"; //store failed

                    ///result.IsSuccess = bsresult;
                }
                if (bsresult)
                {
                    FileLog.Info("start commit to db " + upFile.FileName + "\r\n");

                    fileinfoRepository.Commit();
                    FileLog.Info("success to db " + upFile.FileName + "\r\n");

                    foreach (string sfile in succFiles)
                    {
                        FileLog.Info("delete successful file" + sfile);
                        File.Delete(sfile);
                    }
                }
                else
                {
                    //如果store不成功就不改DB..
                }
                //--------------------20190827--------------------
                string sync = Configs.GetValue("VnaSync");
                if (sync == "1")
                {
                    //sync to another vna..

                    WcfService1.ServiceReference1.VNAService1Client client = new WcfService1.ServiceReference1.VNAService1Client();
                    try
                    {
                        string sretstr = string.Empty;
                        bool returnState = client.UpLoadFile(upFile.FileName,upFile.FileSize,upFile.UploadId_Id,
                            upFile.UserId,upFile.instance_uid,upFile.org_filename,upFile.org_filetype,upFile.series_uid,upFile.study_uid,upFile.upload_date,
                            upFile.FileStream,out sretstr);

                        if (!returnState)
                        {
                            FileLog.Error("Sync Failed:" + sretstr );
                        }
                        else
                            FileLog.Info("Sync Success:" + upFile.ToJson());
                    }
                    catch (Exception ex)
                    {
                        FileLog.Error("Sync Error:" + ex.Message);
                    }
                }
                //-----------------------------------//

                result.IsSuccess = bsresult;

                FileLog.Info("success handle " + upFile.FileName);
            }
            catch (Exception ex)
            {
                //
                result.IsSuccess = false;
                
                result.Message = ex.Message;
                if (ex.InnerException != null)
                    result.Message += " " + ex.InnerException.Message + " " + ex.Source + " " + ex.StackTrace + " " + ex.TargetSite.ToString()+"\r\n";

                FileLog.Error(result.Message + "\r\n");
                if (ex.InnerException != null)
                    FileLog.Error(ex.InnerException.Message + " " + ex.InnerException.Source + " " + ex.InnerException.StackTrace + " " + ex.InnerException.TargetSite.ToString() +"\r\n");
                //

            }
            if (!result.IsSuccess)
            {
                string realPath = System.Web.Hosting.HostingEnvironment.MapPath(msFilePath);

                realPath = Path.Combine(realPath, upFile.UserId); //
                string sdestFile = Path.Combine(realPath, upFile.FileName);
                try
                {
                    File.Delete(sdestFile);
                }
                catch(Exception ex)
                {
                    FileLog.Error(ex.Message + " \r\n");
                }

            }
            return result;

        }

        bool StoreFileToEps(string filePath)
        {
            bool bautoStore = true;
            if (!bautoStore)
                return true;

            int iret = storeDcmFiles(filePath);
            
            return iret==0?true:false;
        }
        /// <summary>
        /// 上传文件到eps
        /// </summary>
        /// <param name="state"></param>
        private int storeDcmFiles(string filePath)
        {

         
                    //.......................

                    List<string> lstDcmFiles = new List<string>();

                    lstDcmFiles.Add(filePath);
                    //
                    //start store file to eps..
                    StoreKit storeKit = StoreKit.Instance;
                    string sLocalAe = VNAConfigSection.Instance.epsElement.LocalAE;
                    string epsAe = VNAConfigSection.Instance.epsElement.EpsAE, epsIp = VNAConfigSection.Instance.epsElement.EpsIP;
                    string epsPort = VNAConfigSection.Instance.epsElement.EpsPort;

                    List<string> lstFailed = new List<string>();

                    storeKit.storeSopclassuid = Configs.GetValue("sopclassuid");
                    //storescu时，有问题,因为dicomlib里面的回调函数是全局的...
                    //意味着只能一个dicomCommu的实例的回调会被执行到..
                    int iref = storeKit.DicomStoreU(lstDcmFiles.ToArray(), sLocalAe, epsAe, epsIp, epsPort, "64", ref lstFailed);

            //
            return iref;

        }
        //下载文件
        // [OperationContract]
        public DownFileResult DownLoadFile(DownFile filedata)
        {
            FileLog.Info("DownLoadFile enter");
            DownFileResult result = new DownFileResult();
            if (filedata == null)
            {
                FileLog.Error("param is empty.");
                return result;
            }

            VnaDbContext vnadbContext = new VnaDbContext();
            IVna_FileinfoRepository vna_FileinfoRepository = new Vna_FileinfoRepository(vnadbContext);
            image1Repository image1Repository = new image1Repository(vnadbContext);

            try
            {
                string sUseEpsData = Configs.GetValue("UseEpsData");

                vna_fileinfo vna_Fileinfo = vna_FileinfoRepository.FindEntity<vna_fileinfo>(t => t.instance_uid == filedata.FileId);
                //image1这张表要重新定义...stage_number, view_number 对应不上...
               // image1 image1 = image1Repository.FindEntity<image1>(t => t.instance_uid == filedata.FileId);
                if (vna_Fileinfo == null)
                {
                    if(sUseEpsData != "1")
                    { 
                        FileLog.Error("can not find :" + filedata.FileId);
                        return result;
                    }
                    else
                    {
                        image1 img0 = image1Repository.FindEntity<image1>(t => t.instance_uid == filedata.FileId);
                        //
                        if(img0 == null)
                        {
                            FileLog.Error("can not find :" + filedata.FileId);
                            return result;
                        }
                        else
                        {
                            vna_Fileinfo = new vna_fileinfo() { vna_filepath = img0.image_file,
                                org_filename = Path.GetFileName(img0.image_file)};
                        }
                    }
                }
                // string path = System.AppDomain.CurrentDomain.BaseDirectory + @"\service\" + filedata.FileName;
                string realPath = string.Empty;//190726 System.Web.Hosting.HostingEnvironment.MapPath(msFilePath);
                realPath = vna_Fileinfo.vna_filepath;//.image_file;// Path.Combine(realPath, filedata.FileName);

                if (!File.Exists(realPath))
                {
                    image1 limage1 = image1Repository.FindEntity<image1>(t => t.instance_uid == filedata.FileId);
                    if (limage1 != null && File.Exists(limage1.image_file))
                    {

                        realPath = limage1.image_file;

                    }
                    else
                    {
                        result.IsSuccess = false;
                        result.FileSize = 0;
                        result.Message = "file is not existed.";
                        result.FileStream = new MemoryStream();


                        FileLog.Error("file is not existed:" + realPath);

                        return result;
                    }
                }
                FileLog.Info("start readfile" + realPath);
                //download时，要将文件转换回原本的格式
                // vna_Fileinfo.org_filetype
                string lorgname = vna_Fileinfo.org_filename;
                int itranstate = 0;
                byte[] bytes =  DicomKit.DicomTool.OriginalFileFromDicom(realPath, lorgname, ref itranstate);
                if( bytes == null)
                {
                    FileLog.Error("OriginalFileFromDicom error:" + itranstate.ToString());
                }
                Stream ms = new MemoryStream();
                //FileStream fs = new FileStream(realPath, FileMode.Open, FileAccess.Read);
                //fs.CopyTo(ms);
                ms.Write(bytes, 0, bytes.Length);

                ms.Position = 0;  //重要，不为0的话，客户端读取有问题
                result.IsSuccess = true;
                result.FileSize = ms.Length;
                result.FileStream = ms;  
                result.FileName = Uri.EscapeDataString(lorgname);// Path.GetFileName(realPath);

                FileLog.Info("download file:" + lorgname + " " + Uri.EscapeDataString(lorgname));
                // fs.Flush();
                //fs.Close();
            }
            catch (Exception ex)
            {
                FileLog.Error(ex.Message + ex.StackTrace + ex.Source);
                if(ex.InnerException != null)
                    FileLog.Error(ex.InnerException.Message + ex.InnerException.StackTrace + ex.InnerException.Source);
            }

            return result;
        }
        /// <summary>
        /// 上传upload table
        /// </summary>
        /// <param name="upload"></param>
        /// <returns></returns>
        public ReturnState UpLoadInfo(VNA_Upload[] uploads)
        {
            //
            ReturnState upFileResult = new ReturnState();
            upFileResult.IsSuccess = false;
            if (uploads == null || uploads.Length == 0)
            {
                upFileResult.Message = "Parameter is empty.";
                return upFileResult;
            }
           // Log log = LogFactory.GetLogger(this.GetType().ToString());
            try
            {


                // log.Info("UploadInfo");
                FileLog.Info(uploads.ToJson());
                //insert upload
                VnaDbContext vnadbContext = new VnaDbContext();
                IVNA_UploadRepository uploadRepository = new VNA_UploadRepository(vnadbContext);

                

                foreach (VNA_Upload upload in uploads)
                {
                    //DateTime dateTime = upload.study_date?? DateTime.Now.AddYears(500);//
                   // if (dateTime.Year > 2200)
                     //   upload.study_date = null;

                    
                    if(uploadRepository.GetUpload(upload.upload_id) != null)
                    {
                        FileLog.Info("existed upload_id:"  + upload.upload_id);
                        continue;
                    }
                    
                    FileLog.Info("Start Insert Upload:" + upload.ToJson());

                    uploadRepository.InsertUpload(upload);
                }
                uploadRepository.Commit();

                string sync = Configs.GetValue("VnaSync");
                if(sync=="1")
                {
                    //sync to another vna..
                    //---------------yys-----20190827--------------------
                    WcfService1.ServiceReference1.VNAService1Client client = new WcfService1.ServiceReference1.VNAService1Client();
                    try
                    {
                        WcfService1.ServiceReference1.ReturnState returnState = client.UpLoadInfo(uploads);
                        if(!returnState.IsSuccess)
                        {
                            FileLog.Error("Sync Failed:" + returnState.Message);
                        }
                        else
                            FileLog.Info("Sync Success:" + uploads.ToJson());
                    }
                    catch (Exception ex)
                    {
                        FileLog.Error("Sync Error:" + ex.Message);
                    }
                }

                FileLog.Info("UpLoadInfo Commit");
                upFileResult.IsSuccess = true;

            }
            catch(Exception ex)
            {
                upFileResult.IsSuccess = false;
                string smessage = ex.Message;
                if (ex.InnerException != null)
                    smessage += " " + ex.InnerException.Message;
                upFileResult.Message = smessage;

                FileLog.Error(smessage + "\r\n");
                if (ex.InnerException != null)
                    FileLog.Error(ex.InnerException.StackTrace + "\r\n");
            }
            return upFileResult;
        }
        public ReturnState DeleteUpload(string upload_id)
        {
            ReturnState returnState = new ReturnState();

            try
            {
                VnaDbContext vnadbContext = new VnaDbContext();
                IVNA_UploadRepository uploadRepository = new VNA_UploadRepository(vnadbContext);
                VNA_Upload upload = uploadRepository.GetUpload(upload_id);
                int uid_id = -1;
                if (upload != null)
                {
                    uid_id = upload.upload_id_id;

                    uploadRepository.Delete<VNA_Upload>(t => t.upload_id == upload_id);
                }
                if (uid_id >= 0)
                {
                    IVna_FileinfoRepository vna_FileinfoRepository = new Vna_FileinfoRepository(vnadbContext);
                    //---------------------------------
                    List<vna_fileinfo> vna_Fileinfos = vna_FileinfoRepository.GetVna_FileinfoList().FindAll(t => t.upload_id_id == uid_id);//.FindList<vna_fileinfo>();

                    vna_FileinfoRepository.Delete<vna_fileinfo>(t => t.upload_id_id == uid_id);
                    vna_FileinfoRepository.Commit();
                    //
                    if(vna_Fileinfos != null)
                    {
                        foreach(vna_fileinfo vf in vna_Fileinfos)
                        {
                            File.Delete(vf.vna_filepath);
                        }
                    }
                   
                }
                //
                uploadRepository.Commit();
                //
                //delte file....
                //
                returnState.IsSuccess = true;
                returnState.Message = "success";
            }
            catch(Exception ex)
            {
                returnState.IsSuccess = false;
                string smessage = ex.Message;
                if (ex.InnerException != null)
                    smessage += " " + ex.InnerException.Message;

                returnState.Message = smessage;

                FileLog.Error(smessage + "\r\n");
                if (ex.InnerException != null)
                    FileLog.Error(ex.InnerException.StackTrace + "\r\n");
            }


            //
            return returnState;

        }
        /// <summary>
        /// vna端保存portal上传的upload table的信息到vna_upload表，供查询用
        /// 因为vna可能会增加新的档案，如pacs端生成的，
        /// 所以从vna到pacs的取回档案时，要注意，可能study uid/series uid会不够，可能要直接用patient_id来返回所有影像档案
        /// 还是用study uid,因为如果新增加文件，vna不知道是怎么增加，用patient_id返回就太多了...
        /// </summary>
        /// <param name="queryParam"></param>
        /// <returns></returns>
        public QueryReturn QueryVNA(QueryParam queryParam,Pagination pagination)//用于query download
        {
            FileLog.Info("start QueryVNA " + queryParam.medicno);

            List<VNA_Upload_Model> vNA_Uploads = new List<VNA_Upload_Model>();
            //
            VnaDbContext vnadbContext = new VnaDbContext();//IUploadRepository
            IVNA_UploadRepository uploadRepository = new VNA_UploadRepository(vnadbContext);

            /*
            //添加查询条件
            System.Linq.Expressions.Expression<Func<VNA_Upload, bool>> expression = u => true;
            if (queryParam.medicno != null && !string.IsNullOrEmpty(queryParam.medicno))
                expression = expression.And(u => u.medical_num == queryParam.medicno);
            if (queryParam.nativeid != null && !string.IsNullOrEmpty(queryParam.nativeid))
                expression = expression.And(u => u.native_id == queryParam.nativeid);
            if (queryParam.deptid != null && !string.IsNullOrEmpty(queryParam.deptid))
                expression = expression.And<VNA_Upload>(u => u.source_dept == queryParam.deptid);
            if (queryParam.name != null && !string.IsNullOrEmpty(queryParam.name))
                expression = expression.And(u => u.first_name.Contains(queryParam.name));
            //
            if (queryParam.studydate != null && !string.IsNullOrEmpty(queryParam.studydate))
            {
                string ssdate = queryParam.studydate;
                if (ssdate.Length == 8)
                    ssdate = ssdate.Substring(0, 4) + "-" + ssdate.Substring(4, 2) + "-" + ssdate.Substring(6, 2) +  " 00:00:01";

                // expression.And(u => u.study_date.ToDateString() == ssdate);toDateString ，找不到对应的sql 函数
                expression = expression.And(u => System.Data.Entity.SqlServer.SqlFunctions.DateDiff("day", u.study_date, ssdate) == 0);

            }

            if (queryParam.refphysician != null && !string.IsNullOrEmpty(queryParam.refphysician))
            {

                expression.And(u => u.ref_physician.Contains(queryParam.refphysician));
            }
            if (queryParam.uploaddate != null && !string.IsNullOrEmpty(queryParam.uploaddate))
            {
                string ssdate = queryParam.uploaddate;
                if (ssdate.Length == 8)
                    ssdate = ssdate.Substring(0, 4) + "-" + ssdate.Substring(4, 2) + "-" + ssdate.Substring(6, 2) + " 00:00:01";
                // expression.And(u => u.create_date.ToDateString() == ssdate);
                expression = expression.And(u => System.Data.Entity.SqlServer.SqlFunctions.DateDiff("day", u.create_date, ssdate) == 0);

            }
            //

            if (queryParam.modality != null && !string.IsNullOrEmpty(queryParam.modality))
            {*/
                //series1 表，用sql 来查
                //Func<VNA_Upload, patient1, study1, series1, bool> func = (u, s, d, f) => u.medical_num == s.ptn_id && s.ptn_id_id == d.ptn_id_id && d.study_uid_id == f.study_uid_id;
                System.Linq.Expressions.Expression<Func<VNA_Upload_Model, patient1,study1,series1, bool>> expressfull = (u,s,d,f) => u.medical_num==s.ptn_id && s.ptn_id_id==d.ptn_id_id && d.study_uid_id==f.study_uid_id && f.modality==queryParam.modality;

                //Expression<Func<VNA_Upload, patient1, study1, series1, bool>> ex1 = (u,s,d,f) => u.medical_num == queryParam.medicno;
                string sql = "select u.*,r.modality from VNA_Upload u, patient1 p, study1 s, series1 r ";
                sql += "where u.medical_num=p.ptn_id and p.ptn_id_id=s.ptn_id_id and s.study_uid_id=r.study_uid_id ";

            sql = @"select u.upload_id 
      ,u.upload_id_id             
      ,upload_sessionid          
      ,user_id_id
      ,medical_num
      ,native_id
      ,first_name
      ,middle_name
      ,last_name
      ,create_date
      ,u.study_date
      ,ref_physician
      ,report_type
      ,source_dept
      ,source_ofdata
      ,comment
      ,u.study_uid
      ,'' as series_uid
	  ,max(org_filename) as org_filename
	  ,rtrim( ltrim(replace( s.modalities,'\',' '))) as modality
 from VNA_Upload u,vna_fileinfo v, patient1 p, study1 s, series1 r
               where u.medical_num = p.ptn_id and p.ptn_id_id = s.ptn_id_id and v.upload_id_id=u.upload_id_id and 
                s.study_uid_id = r.study_uid_id and u.study_uid=s.study_uid";
            //加入vna_upload studyuid相等条件..191119
            //  ,stuff( (select ','+a.modality from series1 a,study1 b where a.study_uid_id=b.study_uid_id
         //   and b.study_uid = u.study_uid
      // FOR XML PATH('')),1,1,'') as modality

            string sUseEpsData = Configs.GetValue("UseEpsData");
            string sEpsSql = string.Empty;
            //20200414发现一个问题在portal,vna,eps都在同一台电脑时，如果UseEpsData=1
            //那么未经transtovna的文件在senddownloadlink里会出现，因为此时eps有的是portal的数据
            //要想办法去除掉这部份eps里，但未经transtovna的数据..
            //没有好办法，除非让vna能访问到portal db，在useepsdata时，dicomdb数据再过滤，判断
            //它是不是属于portal未到达vna的数据..
            string same = Configs.GetValue("VnaPortalSameEps");
            if (sUseEpsData=="1")
            {
                //要再查一次eps db，利用现有的epsdb里的数据...
                sEpsSql = @"select '' as upload_id 
              ,0 as upload_id_id             
              ,'' as upload_sessionid          
              ,0 as user_id_id
              ,p.ptn_id as medical_num
              ,'' as native_id
              ,p.ptn_name as first_name
              ,'' as middle_name
              ,'' as last_name
              ,Convert(datetime,Convert(varchar(30),s.study_date),120) as create_date
              ,Convert(datetime,Convert(varchar(30),s.study_date),120) as study_date
              ,s.physician_name as ref_physician
              ,'' as report_type
              ,'' as source_dept
              ,'' as source_ofdata
              ,'' as comment
              ,s.study_uid
              ,'' as series_uid
	          ,'(Local DICOM Files)' as org_filename
	         ,rtrim( ltrim(replace( s.modalities,'\',' '))) as modality
         from  patient1 p, study1 s, series1 r
                       where  p.ptn_id_id = s.ptn_id_id and 
                        s.study_uid_id = r.study_uid_id ";
                //    ,stuff( (select ','+a.modality from series1 a,study1 b where a.study_uid_id=b.study_uid_id
               // and b.study_uid = s.study_uid
               //FOR XML PATH('')),1,1,'') as modality
            }
            //sql += " and r.modality=@modality ";
            List<DbParameter> dbParameters = new List<DbParameter>();
            List<DbParameter> dbEpsParameters = new List<DbParameter>();

            if (!string.IsNullOrEmpty(queryParam.modality))
            {
                
                sql += " and r.modality=@modality ";

                SqlParameter sqlParameter = new SqlParameter("modality", queryParam.modality);
                dbParameters.Add(sqlParameter);
                if (!string.IsNullOrEmpty(sEpsSql))
                {
                    sEpsSql += " and r.modality=@modality ";
                    sqlParameter = new SqlParameter("modality", queryParam.modality);
                    dbEpsParameters.Add(sqlParameter);
                }
            }
            if (!string.IsNullOrEmpty(queryParam.medicno))
                {
                   // Expression<Func<VNA_Upload, patient1, study1, series1, bool>> ex1 = (u, s, d, f) => u.medical_num == queryParam.medicno;
                   // expressfull = (Expression < Func<VNA_Upload, patient1, study1, series1, bool> > )expressfull.AndAlso( ex1);

                    sql += " and u.medical_num=@medicno";

                    SqlParameter sqlParameter = new SqlParameter("medicno", queryParam.medicno);
                        dbParameters.Add(sqlParameter);
                    if (!string.IsNullOrEmpty(sEpsSql))
                    {
                        sEpsSql += " and p.ptn_id=@medicno ";
                        sqlParameter = new SqlParameter("medicno", queryParam.medicno);
                        dbEpsParameters.Add(sqlParameter);
                    }
                }
                if (!string.IsNullOrEmpty(queryParam.nativeid))
                {
                   // Expression<Func<VNA_Upload, patient1, study1, series1, bool>> ex1 = (u, s, d, f) => u.native_id == queryParam.nativeid;
                   // expressfull = (Expression<Func<VNA_Upload, patient1, study1, series1, bool>>)expressfull.AndAlso(ex1);

                    sql += " and u.native_id=@nativeid";
                    
                    SqlParameter sqlParameter = new SqlParameter("nativeid", queryParam.nativeid);
                    dbParameters.Add(sqlParameter);
                }
                if (!string.IsNullOrEmpty(queryParam.deptid))
                {
                    //Expression<Func<VNA_Upload, patient1, study1, series1, bool>> ex1 = (u, s, d, f) => u.source_dept == queryParam.deptid;
                   // expressfull = (Expression<Func<VNA_Upload, patient1, study1, series1, bool>>)expressfull.AndAlso(ex1);

                    sql += " and u.source_dept=@deptid";

                    SqlParameter sqlParameter = new SqlParameter("deptid", queryParam.deptid);
                    dbParameters.Add(sqlParameter);
                }
                if (!string.IsNullOrEmpty(queryParam.name))
                {
                   // Expression<Func<VNA_Upload, patient1, study1, series1, bool>> ex1 = (u, s, d, f) => u.first_name.Contains(queryParam.name);
                   // expressfull = (Expression<Func<VNA_Upload, patient1, study1, series1, bool>>)expressfull.AndAlso(ex1);

                    string name = queryParam.name;
                    string[] namearr = null;
                    if (name.IndexOf("^") >= 0)
                        namearr = name.Split(new char[] { '^' });
                    else
                        namearr = name.Split(new char[] { '^' });
                    if (namearr.Length > 0)
                    {

                        sql = sql + " and first_name like @first_name";

                        SqlParameter sqlParameter = new SqlParameter("first_name", "%" + namearr[0] + "%");
                        dbParameters.Add(sqlParameter);

                    }
                    if (namearr.Length > 1)
                    {

                        sql = sql + "and last_name like @last_name";

                        SqlParameter sqlParameter = new SqlParameter("last_name", "%" + namearr[1] + "%");
                        dbParameters.Add(sqlParameter);

                    }
                    if (!string.IsNullOrEmpty(sEpsSql))
                    {
                        sEpsSql += " and p.ptn_name like @first_name ";
                        SqlParameter sqlParameter = new SqlParameter("first_name", "%" + namearr[0] + "%");
                        dbEpsParameters.Add(sqlParameter);
                    }
            }
            //
            if (!string.IsNullOrEmpty(queryParam.studydate) || !string.IsNullOrEmpty(queryParam.studydate2))
            {
                if (!string.IsNullOrEmpty(queryParam.studydate) && string.IsNullOrEmpty(queryParam.studydate2))
                {
                    string ssdate = queryParam.studydate;
                    string sdate8 = ssdate;
                    if (ssdate.Length == 8)
                        ssdate = ssdate.Substring(0, 4) + "-" + ssdate.Substring(4, 2) + "-" + ssdate.Substring(6, 2) + " 00:00:01";

                    // expression.And(u => u.study_date.ToDateString() == ssdate);toDateString ，找不到对应的sql 函数

                    // Expression<Func<VNA_Upload, patient1, study1, series1, bool>> ex1 = (u, s, d, f) => System.Data.Entity.SqlServer.SqlFunctions.DateDiff("day", u.study_date, ssdate) == 0;
                    // expressfull = (Expression<Func<VNA_Upload, patient1, study1, series1, bool>>)expressfull.AndAlso(ex1);

                    sql += " and ( DateDiff(day, s.study_date, @studydate) <= 0 )";

                    SqlParameter sqlParameter = new SqlParameter("@studydate", ssdate);
                    dbParameters.Add(sqlParameter);
                    if (!string.IsNullOrEmpty(sEpsSql))
                    {
                        sEpsSql += " and s.study_date >= @studydate ";

                        sqlParameter = new SqlParameter("@studydate", sdate8);//ssdate
                        dbEpsParameters.Add(sqlParameter);
                    }
                }
                else if (string.IsNullOrEmpty(queryParam.studydate) && !string.IsNullOrEmpty(queryParam.studydate2))
                {
                    string ssdate = queryParam.studydate2;
                    string edate8 = ssdate;
                    if (ssdate.Length == 8)
                        ssdate = ssdate.Substring(0, 4) + "-" + ssdate.Substring(4, 2) + "-" + ssdate.Substring(6, 2) + " 00:00:01";

                    sql += " and ( DateDiff(day, s.study_date, @studydate) >= 0 )";

                    SqlParameter sqlParameter = new SqlParameter("@studydate", ssdate);
                    dbParameters.Add(sqlParameter);
                    if (!string.IsNullOrEmpty(sEpsSql))
                    {
                        sEpsSql += " and s.study_date <= @studydate ";

                        sqlParameter = new SqlParameter("@studydate", edate8);//ssdate
                        dbEpsParameters.Add(sqlParameter);
                    }
                }
                else
                {
                    //range....
                    string ssdate = queryParam.studydate;
                    string sdate8 = ssdate;
                    if (ssdate.Length == 8)
                        ssdate = ssdate.Substring(0, 4) + "-" + ssdate.Substring(4, 2) + "-" + ssdate.Substring(6, 2) + " 00:00:01";

                    string ssdate2 = queryParam.studydate2;
                    string endate8 = ssdate2;
                    if (ssdate2.Length == 8)
                        ssdate2 = ssdate2.Substring(0, 4) + "-" + ssdate2.Substring(4, 2) + "-" + ssdate2.Substring(6, 2) + " 23:59:59";

                    sql += " and ( ( DateDiff(day, s.study_date, @studydate) <= 0 ) and ( DateDiff(day, s.study_date, @studydate2) >= 0 ) )";

                    SqlParameter sqlParameter = new SqlParameter("@studydate", ssdate);
                    SqlParameter sqlParameter2 = new SqlParameter("@studydate2", ssdate2);
                    dbParameters.Add(sqlParameter);
                    dbParameters.Add(sqlParameter2);
                    if (!string.IsNullOrEmpty(sEpsSql))
                    {
                        sEpsSql += " and s.study_date >= @studydate and s.study_date <= @studydate2 ";

                        sqlParameter = new SqlParameter("@studydate", sdate8);
                        sqlParameter2 = new SqlParameter("@studydate2", endate8);//ssdate2
                        dbEpsParameters.Add(sqlParameter);
                        dbEpsParameters.Add(sqlParameter2);
                    }
                }
            }

                if (!string.IsNullOrEmpty(queryParam.refphysician))
                {
                    //191205 comment
                    //Expression<Func<VNA_Upload_Model, patient1, study1, series1, bool>> ex1 = (u, s, d, f) => u.ref_physician.Contains(queryParam.refphysician);
                   // expressfull = (Expression<Func<VNA_Upload_Model, patient1, study1, series1, bool>>)expressfull.AndAlso(ex1);

                    sql += " and u.ref_physician like @refphysician";

                    SqlParameter sqlParameter = new SqlParameter("@refphysician", "%"+queryParam.refphysician+ "%");
                    dbParameters.Add(sqlParameter);
                }
            if (!string.IsNullOrEmpty(queryParam.uploaddate) || !string.IsNullOrEmpty(queryParam.uploaddate2))
            {
                if (!string.IsNullOrEmpty(queryParam.uploaddate) && string.IsNullOrEmpty(queryParam.uploaddate2))
                {
                    string ssdate = queryParam.uploaddate;
                    string sdate8 = ssdate;
                    if (ssdate.Length == 8)
                        ssdate = ssdate.Substring(0, 4) + "-" + ssdate.Substring(4, 2) + "-" + ssdate.Substring(6, 2) + " 00:00:01";
                    // expression.And(u => u.create_date.ToDateString() == ssdate);

                    // Expression<Func<VNA_Upload, patient1, study1, series1, bool>> ex1 = (u, s, d, f) => System.Data.Entity.SqlServer.SqlFunctions.DateDiff("day", u.create_date, ssdate) == 0;
                    // expressfull = (Expression<Func<VNA_Upload, patient1, study1, series1, bool>>)expressfull.AndAlso(ex1);

                    sql += " and ( DateDiff(day, u.create_date, @uploaddate) <= 0 )";

                    SqlParameter sqlParameter = new SqlParameter("@uploaddate", ssdate);
                    dbParameters.Add(sqlParameter);
                    //没有study date参数下，用uploaddate
                    if (!string.IsNullOrEmpty(sEpsSql)&& string.IsNullOrEmpty(queryParam.studydate) && string.IsNullOrEmpty(queryParam.studydate2))
                    {
                        sEpsSql += " and s.study_date >= @studydate ";

                        sqlParameter = new SqlParameter("@studydate", sdate8);//
                        dbEpsParameters.Add(sqlParameter);
                    }
                }
                else if (string.IsNullOrEmpty(queryParam.uploaddate) && !string.IsNullOrEmpty(queryParam.uploaddate2))
                {
                    string ssdate = queryParam.uploaddate2;
                    string edate8 = ssdate;
                    if (ssdate.Length == 8)
                        ssdate = ssdate.Substring(0, 4) + "-" + ssdate.Substring(4, 2) + "-" + ssdate.Substring(6, 2) + " 00:00:01";
                
                    sql += " and ( DateDiff(day, u.create_date, @uploaddate) >= 0 )";

                    SqlParameter sqlParameter = new SqlParameter("@uploaddate", ssdate);
                    dbParameters.Add(sqlParameter);
                    //没有study date参数下，用uploaddate
                    if (!string.IsNullOrEmpty(sEpsSql) && string.IsNullOrEmpty(queryParam.studydate) && string.IsNullOrEmpty(queryParam.studydate2))
                    {
                        sEpsSql += " and s.study_date <= @studydate ";

                        sqlParameter = new SqlParameter("@studydate", edate8);//
                        dbEpsParameters.Add(sqlParameter);
                    }
                }
                else
                {
                    string ssdate = queryParam.uploaddate;
                    string sdate8 = ssdate;
                    if (ssdate.Length == 8)
                        ssdate = ssdate.Substring(0, 4) + "-" + ssdate.Substring(4, 2) + "-" + ssdate.Substring(6, 2) + " 00:00:01";

                    string ssdate2 = queryParam.uploaddate2;
                    string endate8 = ssdate2;
                    if (ssdate2.Length == 8)
                        ssdate2 = ssdate2.Substring(0, 4) + "-" + ssdate2.Substring(4, 2) + "-" + ssdate2.Substring(6, 2) + " 23:59:59";

                    sql += " and ( ( DateDiff(day, u.create_date, @uploaddate) <= 0 ) and ( DateDiff(day, u.create_date, @uploaddate2) >= 0 ) )";

                    SqlParameter sqlParameter = new SqlParameter("@uploaddate", ssdate);
                    SqlParameter sqlParameter2 = new SqlParameter("@uploaddate2", ssdate2);
                    dbParameters.Add(sqlParameter);
                    dbParameters.Add(sqlParameter2);
                    //没有study date参数下，用uploaddate
                    if (!string.IsNullOrEmpty(sEpsSql) && string.IsNullOrEmpty(queryParam.studydate) && string.IsNullOrEmpty(queryParam.studydate2))
                    {
                        sEpsSql += " and s.study_date >= @studydate and s.study_date <= @studydate2 ";

                        sqlParameter = new SqlParameter("@studydate", sdate8);
                        sqlParameter2 = new SqlParameter("@studydate2", endate8);//ssdate2
                        dbEpsParameters.Add(sqlParameter);
                        dbEpsParameters.Add(sqlParameter2);
                    }
                }
            }
            //
            sql += @" group by  u.upload_id
      ,u.upload_id_id
      ,upload_sessionid
      ,user_id_id
      ,medical_num
      ,native_id
      ,first_name
      ,middle_name
      ,last_name
      ,create_date
      ,u.study_date
      ,ref_physician
      ,report_type
      ,source_dept
      ,source_ofdata
      ,comment
      ,u.study_uid
      ,s.modalities"; //
            //  ,u.series_uid
            // ,r.modality 
            //  , v.org_filename";
            if (!string.IsNullOrEmpty(sEpsSql))
            {
              //  sEpsSql += @"group by  upload_id
              //,upload_id_id
              //,upload_sessionid
              //,user_id_id
              //,medical_num
              //,native_id
              //,first_name
              //,middle_name
              //,last_name
              //,create_date
              //,study_date
              //,ref_physician
              //,report_type
              //,source_dept
              //,source_ofdata
              //,comment
              //,study_uid ";

            }
            try
            {
                FileLog.Info("QueryVNA FindList " + sql);

                
                if (!string.IsNullOrEmpty(sEpsSql))
                {
                    //合并查询，先不分页
                    vNA_Uploads = uploadRepository.FindList<VNA_Upload_Model>(sql, dbParameters.ToArray());
                    List< VNA_Upload_Model> vNA_Uploads0 = uploadRepository.FindList<VNA_Upload_Model>(sEpsSql, dbEpsParameters.ToArray());
                    if(vNA_Uploads0 != null)
                    {
                        if (vNA_Uploads == null)
                            vNA_Uploads = new List<VNA_Upload_Model>();
                        //20200414 可能portal,vna和eps三者在同一台..
                        //要去掉dicomdb中portal来的，未经transtovna的数据..
                        PortalDbContext portalDbContext = null;
                        List<string> unAuditStudy = null;
                        if (same == "1" && vNA_Uploads0.Count>0)
                        {
                            portalDbContext = new PortalDbContext();
                        }
                        foreach(VNA_Upload_Model vmode in vNA_Uploads0)
                        {
                            if(portalDbContext != null)
                            {
                                //查询未经查核的study..
                                string sql0 = "select distinct study_uid from upload u,upload_item i where u.upload_id_id=i.upload_id_id and i.status!='11' and u.study_uid=@study_uid";
                                unAuditStudy = portalDbContext.Database.SqlQuery<string>(sql0, new SqlParameter[] { new SqlParameter("study_uid", vmode.study_uid) }).ToList<string>();
                                //20200414 相同eps情况下，排除未审核到vna的study
                                if (unAuditStudy != null && unAuditStudy.Count > 0)
                                {
                                    //如果属于未审核的study，但是已在portal和vna的eps中
                                    //因为是同一台eps
                                    //if (unAuditStudy.Contains(vmode.study_uid))
                                        continue;
                                }
                            }
                           
                            //除去重复study
                            var vnad = vNA_Uploads.Find(t=> t.study_uid == vmode.study_uid);
                            if (vnad == null)
                            {
                    
                                vNA_Uploads.Add(vmode); 
                            }
                        }
                    }
                    //进行分页...
                    vNA_Uploads = PageList<VNA_Upload_Model>(vNA_Uploads, pagination);
                }
                else
                {
                    vNA_Uploads = uploadRepository.FindList<VNA_Upload_Model>(sql, dbParameters.ToArray(), pagination);
                }
                FileLog.Info("FindList return " + vNA_Uploads!=null?vNA_Uploads.Count.ToString():"null");
            }
            catch(Exception ex)
            {
                FileLog.Error(ex.Message + " " + ex.StackTrace + " " + ex.Source);
            }
                
            /*
            }
            else
            {




                vNA_Uploads = uploadRepository.FindList(expression, pagination);

            }
            */
            QueryReturn returns = new QueryReturn(){ VNA_Uploads=vNA_Uploads , Pagination= pagination,ViewerRegion="" };

            return returns;
        }
        List<TEntity> PageList<TEntity>(List<TEntity> entities, Pagination pagination)
        {
            if (entities == null)
                return null;

            bool isAsc = pagination.sord.ToLower() == "asc" ? true : false;
            string[] _order = pagination.sidx.Split(',');
            MethodCallExpression resultExp = null;
            IQueryable<TEntity> tempData = entities.AsQueryable();// dbcontext.Set<TEntity>().AsQueryable();
            
            //190911
            bool firstTime = true;
            foreach (string item in _order)
            {
                if (string.IsNullOrEmpty(item))
                    continue; //yys

                string _orderPart = item;
                _orderPart = System.Text.RegularExpressions.Regex.Replace(_orderPart, @"\s+", " ");
                string[] _orderArry = _orderPart.Split(' ');
                string _orderField = _orderArry[0];
                bool sort = isAsc;
                if (_orderArry.Length == 2)
                {
                    isAsc = _orderArry[1].ToUpper() == "ASC" ? true : false;
                }
                string method = string.Empty;
                if (firstTime)
                {
                    firstTime = false;

                    method = isAsc ? "OrderBy" : "OrderByDescending";
                }
                else
                {
                    method = isAsc ? "ThenBy" : "ThenByDescending";
                }

                var parameter = Expression.Parameter(typeof(TEntity), "t");
                var property = typeof(TEntity).GetProperty(_orderField);
                var propertyAccess = Expression.MakeMemberAccess(parameter, property);
                var orderByExp = Expression.Lambda(propertyAccess, parameter);
                resultExp = Expression.Call(typeof(Queryable), isAsc ? "OrderBy" : "OrderByDescending", new Type[] { typeof(TEntity), property.PropertyType }, tempData.Expression, Expression.Quote(orderByExp));
                //
                //190911
                //当有orderby多个条件时,呼叫多次...
                resultExp = Expression.Call(typeof(Queryable), method, new Type[] { typeof(TEntity), property.PropertyType }, tempData.Expression, Expression.Quote(orderByExp));
                tempData = tempData.Provider.CreateQuery<TEntity>(resultExp);
            }
           // //if (resultExp != null)
           // tempData = tempData.Provider.CreateQuery<TEntity>(resultExp);

            // tempData = entities.AsQueryable().Provider.CreateQuery<TEntity>(resultExp);
            //
            pagination.records = entities.Count();

            entities = tempData.ToList<TEntity>();

            List<TEntity> entities2 = entities.Skip<TEntity>(pagination.rows * (pagination.page - 1)).Take<TEntity>(pagination.rows).ToList();
            //entities
            return entities2;
        }
        /// <summary>
        /// 用于send downloadlink
        /// </summary>
        /// <param name="download_uids"></param>
        /// <returns></returns>
        public List<QueryDownloadReturn> QueryDownload(string[] download_uids)
        {
            FileLog.Info("start QueryDownload " + download_uids.ToJson());

            //
            VnaDbContext vnadbContext = new VnaDbContext(); //IUploadRepository
            IVNA_UploadRepository uploadRepository = new VNA_UploadRepository(vnadbContext);

            List<QueryDownloadReturn> queryDownloadReturns = null;// new List<QueryDownloadReturn>();

            List<DbParameter> dbParameters = new List<DbParameter>();
            List<DbParameter> dbEpsParameters = new List<DbParameter>();
            string sql = "select u.*,v.org_filename,v.org_filetype,v.upload_date,v.instance_uid from VNA_Upload u, patient1 p, study1 s, vna_fileinfo v ";
            sql += " where u.medical_num=p.ptn_id and p.ptn_id_id=s.ptn_id_id and " +
                "v.study_uid=s.study_uid ";

            sql = @"select u.upload_id
      ,u.upload_id_id
      ,upload_sessionid
      ,user_id_id
      ,medical_num
      ,native_id
      ,first_name
      ,middle_name
      ,last_name
      ,create_date
      ,u.study_date
      ,ref_physician
      ,report_type
      ,source_dept
      ,source_ofdata
      ,comment
      ,u.study_uid
      ,'' as series_uid
      ,max(v.org_filename) as org_filename
      ,max(v.org_filetype) as org_filetype
      ,max(v.upload_date ) as upload_date
      ,'' as instance_uid                  
    from VNA_Upload u,vna_fileinfo v   
where u.upload_id_id=v.upload_id_id ";//max(u.series_uid)max(v.instance_uid)

            string sUseEpsData = Configs.GetValue("UseEpsData");
            string sEpsSql = string.Empty;
            
            if (sUseEpsData == "1")
            {
                sEpsSql = @"select '' as upload_id
                  ,0 as upload_id_id
                  ,'' as upload_sessionid
                  ,0 as user_id_id
                  ,p.ptn_id as medical_num
                  ,'' as native_id
                  ,p.ptn_name as first_name
                  ,'' as middle_name
                  ,'' as last_name
                  ,Convert(datetime,Convert(varchar(30),u.study_date),120) as create_date
                  ,Convert(datetime,Convert(varchar(30),u.study_date),120) as study_date
                  ,physician_name as ref_physician
                  ,'' as report_type
                  ,'' as source_dept
                  ,'' as source_ofdata
                  ,'' as comment
                  ,study_uid
                  ,'' as series_uid
                  ,'(Local DICOM Files)' as org_filename
                  ,'dicom' as org_filetype
                  ,Convert(datetime,Convert(varchar(30),u.study_date),120) as upload_date
                  ,'' as instance_uid
                from patient1 p ,study1 u 
                where p.ptn_id_id=u.ptn_id_id  ";
            }
            StringBuilder stringBuilder = new StringBuilder();
            if (download_uids.Length > 0)
            {
                stringBuilder.Append(" and ");
                stringBuilder.Append("( ");
            }
            for (int ii = 0; ii < download_uids.Length; ++ii)
            {
                if (ii > 0)
                    stringBuilder.Append(" or ");
                string upload_id = download_uids[ii];
                stringBuilder.Append("u.study_uid=@suid" + ii.ToString());

                SqlParameter sqlParameter = new SqlParameter("@suid" + ii.ToString(), download_uids[ii]);
                dbParameters.Add(sqlParameter);
                //
                SqlParameter sqlParameter2 = new SqlParameter("@suid" + ii.ToString(), download_uids[ii]);
                dbEpsParameters.Add(sqlParameter2);
                

            }
            if (download_uids.Length > 0)
                stringBuilder.Append(" )");
            sql = sql + " " + stringBuilder.ToString();
            //
            sql = sql + @"group by 
u.upload_id
      ,u.upload_id_id
      ,upload_sessionid
      ,user_id_id
      ,medical_num
      ,native_id
      ,first_name
      ,middle_name
      ,last_name
      ,create_date
      ,u.study_date
      ,ref_physician
      ,report_type
      ,source_dept
      ,source_ofdata
      ,comment
      ,u.study_uid ";
      //,u.series_uid
      //,v.org_filename
     // ,v.org_filetype
     // ,v.upload_date
     // ,v.instance_uid";
            if(!string.IsNullOrEmpty(sEpsSql))
            {
                sEpsSql = sEpsSql + " " + stringBuilder.ToString();
              //  sEpsSql = sEpsSql + @"group by 
              //  upload_id
              //,upload_id_id
              //,upload_sessionid
              //,user_id_id
              //,medical_num
              //,native_id
              //,first_name
              //,middle_name
              //,last_name
              //,create_date
              //,study_date
              //,ref_physician
              //,report_type
              //,source_dept
              //,source_ofdata
              //,comment
              //,study_uid ";
            }
            try
            {
                FileLog.Info("QueryDownload FindList " + sql);

                if (!string.IsNullOrEmpty(sEpsSql))
                {
                    queryDownloadReturns = uploadRepository.FindList<QueryDownloadReturn>(sql, dbParameters.ToArray());
                    
                    List<QueryDownloadReturn> rets = uploadRepository.FindList<QueryDownloadReturn>(sEpsSql, dbEpsParameters.ToArray());
                    if(rets!=null )
                    {
                        if (queryDownloadReturns == null)
                            queryDownloadReturns = new List<QueryDownloadReturn>();
                        foreach(QueryDownloadReturn qdown in rets)
                        {
                            var vrt = queryDownloadReturns.Find(t => t.study_uid == qdown.study_uid);
                            if (vrt == null)
                                queryDownloadReturns.Add(qdown);
                        }
                    }
                }
                else
                    queryDownloadReturns = uploadRepository.FindList<QueryDownloadReturn>(sql, dbParameters.ToArray());

                FileLog.Info("QueryDownload FindList return " + queryDownloadReturns != null ? queryDownloadReturns.Count.ToString() : "null");
            }
            catch (Exception ex)
            {
                FileLog.Error(ex.Message + " " + ex.StackTrace + " " + ex.Source);
            }
            return queryDownloadReturns;

        }
        public List<string> QueryInstanceUidsOfStudy(string studyuid)
        {
            //
            List<string> lsUids = new List<string>();
            if (string.IsNullOrEmpty(studyuid))
                return lsUids;

            FileLog.Info("QueryInstanceUidsOfStudy :" + studyuid);

            try
            {
                VnaDbContext vnadbContext = new VnaDbContext();
                image1Repository image1Repository = new image1Repository(vnadbContext);
                //
                string sql = @"select distinct m.* from study1 s,series1 r,image1 m where
            s.study_uid_id=r.study_uid_id and r.series_uid_id=m.series_uid_id and s.study_uid=@suid ";
                //
                List<DbParameter> dbParameters = new List<DbParameter>();
                dbParameters.Add(new SqlParameter("@suid", studyuid));
                //
                List<image1> image1s = image1Repository.FindList<image1>(sql, dbParameters.ToArray());
                //
                if (image1s == null || image1s.Count == 0)
                    return lsUids;
                //
                foreach (image1 img1 in image1s)
                {
                    lsUids.Add(img1.instance_uid);
                }
                FileLog.Info("QueryInstanceUidsOfStudy return:" + lsUids.ToJson());
            }
            catch(Exception ex)
            {
                FileLog.Error(ex.Message + ex.StackTrace + ex.Source);
            }
            return lsUids;
        }
    }
}
