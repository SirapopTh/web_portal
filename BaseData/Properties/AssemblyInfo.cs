﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General information about the assembly consists of the following
// control.Change these characteristic values ​​to modify
// Information associated with the assembly。
[assembly: AssemblyTitle("BaseData")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("")]
[assembly: AssemblyProduct("BaseData")]
[assembly: AssemblyCopyright("Copyright ©  2018")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false will make the type in this assembly
// Not visible to COM components.If you need to access the type in this assembly from COM
//Please set this type of ComVisible attribute to true.
[assembly: ComVisible(false)]

//If this project is exposed to COM, the following GUID is used for the type library ID
[assembly: Guid("4974408b-4947-4813-9e5a-b02fb0190a5b")]

// The assembly version information consists of the following four values: 
//
//      Major version
//      Minor version
//      Generation number
//      Amendment No
//
// You can specify all values, or use the "*" preset version number and revision number shown below
//By using "*" as follows:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]
