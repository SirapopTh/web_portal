﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BaseData
{
    public enum DbLogType
    {
        [Description("Other")]
        Other = 0,
        [Description("Login")] //登录
        Login = 1,
        [Description("Exit")]
        Exit = 2,
        [Description("VIsit")]
        Visit = 3,
        [Description("Create")]//新增
        Create = 4,
        [Description("Delete")]//删除
        Delete = 5,
        [Description("Update")]//修改
        Update = 6,
        [Description("Submit")] //提交
        Submit = 7,
        [Description("Exception")] //异常
        Exception = 8,
    }
}
