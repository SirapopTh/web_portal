﻿using Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace BaseData
{
    /// <summary>
    /// Warehousing
    /// </summary>
    public class RepositoryBase : IRepositoryBase, IDisposable
    {
        private VnaDbContext dbcontext;//= new VnaDbContext();
        private DbTransaction dbTransaction { get; set; }

        public RepositoryBase(VnaDbContext vnaDb)
        {
            if (vnaDb != null)
            {
                dbcontext = vnaDb;
                dbcontext.Database.CommandTimeout = 60; //yys
            }
        }
        public IRepositoryBase BeginTrans()
        {
            DbConnection dbConnection = ((IObjectContextAdapter)dbcontext).ObjectContext.Connection;
            if (dbConnection.State == ConnectionState.Closed)
            {
                dbConnection.Open();
            }
            dbTransaction = dbConnection.BeginTransaction();
            return this;
        }
        public int Commit()
        {
            try
            {
                var returnValue = dbcontext.SaveChanges();
                if (dbTransaction != null)
                {
                    dbTransaction.Commit();
                }
                return returnValue;
            }
            catch (Exception)
            {
                if (dbTransaction != null)
                {
                    this.dbTransaction.Rollback();
                }
                throw;
            }
            finally
            {
                // this.Dispose(); //yys Not here dispose transaction and dbcontext
            }
        }
        public void Dispose()
        {
            if (dbTransaction != null)
            {
                this.dbTransaction.Dispose();
            }
            this.dbcontext.Dispose();
        }
        public int Insert<TEntity>(TEntity entity) where TEntity : class
        {
            dbcontext.Entry<TEntity>(entity).State = EntityState.Added;
            
            return 0;// dbTransaction == null ? this.Commit() : 0;
        }
        public int Insert<TEntity>(List<TEntity> entitys) where TEntity : class
        {
           
            foreach (var entity in entitys)
            {
                dbcontext.Entry<TEntity>(entity).State = EntityState.Added;
            }
            return 0;// dbTransaction == null ? this.Commit() : 0;
        }
        public int Update<TEntity>(TEntity entity) where TEntity : class
        {
            dbcontext.Set<TEntity>().Attach(entity);
            PropertyInfo[] props = entity.GetType().GetProperties();
            foreach (PropertyInfo prop in props)
            {
                //yys Ignore only get attributes such as self - increasing fields, no need to modify.
                if (!prop.CanWrite)
                    continue;

                if (prop.GetValue(entity, null) != null) //yys null Also allow update to db ...
                {
                    if (prop.GetValue(entity, null).ToString() == "&nbsp;")
                        dbcontext.Entry(entity).Property(prop.Name).CurrentValue = null;
                    //dbcontext.Entry(entity).Property(prop.Name).IsModified = true; //yys
                }
                dbcontext.Entry(entity).Property(prop.Name).IsModified = true;
            }
            return 0;// dbTransaction == null ? this.Commit() : 0;
        }
        public int Delete<TEntity>(TEntity entity) where TEntity : class
        {
            dbcontext.Set<TEntity>().Attach(entity);
            dbcontext.Entry<TEntity>(entity).State = EntityState.Deleted;
            return 0;// dbTransaction == null ? this.Commit() : 0;
        }
        public int Delete<TEntity>(Expression<Func<TEntity, bool>> predicate) where TEntity : class
        {
            var entitys = dbcontext.Set<TEntity>().Where(predicate).ToList();
            entitys.ForEach(m => dbcontext.Entry<TEntity>(m).State = EntityState.Deleted);
            return 0;// dbTransaction == null ? this.Commit() : 0;
        }
        public TEntity FindEntity<TEntity>(object keyValue) where TEntity : class
        {
            return dbcontext.Set<TEntity>().Find(keyValue);
        }
        public TEntity FindEntity<TEntity>(Expression<Func<TEntity, bool>> predicate) where TEntity : class
        {
            return dbcontext.Set<TEntity>().FirstOrDefault(predicate);
        }
        public IQueryable<TEntity> IQueryable<TEntity>() where TEntity : class
        {
            return dbcontext.Set<TEntity>();
        }
        public IQueryable<TEntity> IQueryable<TEntity>(Expression<Func<TEntity, bool>> predicate) where TEntity : class
        {
            return dbcontext.Set<TEntity>().Where(predicate);
        }
        public List<TEntity> FindList<TEntity>(string strSql) where TEntity : class
        {
            return dbcontext.Database.SqlQuery<TEntity>(strSql).ToList<TEntity>();
        }
        public List<TEntity> FindList<TEntity>(string strSql, DbParameter[] dbParameter) where TEntity : class
        {
            return dbcontext.Database.SqlQuery<TEntity>(strSql, dbParameter).ToList<TEntity>();
        }
        public List<TEntity> FindList<TEntity>(string strSql, DbParameter[] dbParameter, Pagination pagination) where TEntity : class
        {
            List<TEntity> entities = dbcontext.Database.SqlQuery<TEntity>(strSql, dbParameter).ToList<TEntity>();

            bool isAsc = pagination.sord.ToLower() == "asc" ? true : false;
            string[] _order = pagination.sidx.Split(',');
            MethodCallExpression resultExp = null;
            IQueryable<TEntity> tempData = entities.AsQueryable();// dbcontext.Set<TEntity>().AsQueryable();
            
            //190911
            bool firstTime = true;
            foreach (string item in _order)
            {
                if (string.IsNullOrEmpty(item))
                    continue; //yys

                string _orderPart = item;
                _orderPart = Regex.Replace(_orderPart, @"\s+", " ");
                string[] _orderArry = _orderPart.Split(' ');
                string _orderField = _orderArry[0];
                bool sort = isAsc;
                if (_orderArry.Length == 2)
                {
                    isAsc = _orderArry[1].ToUpper() == "ASC" ? true : false;
                }
                //---------
                string method = string.Empty;
                if(firstTime)
                {
                    firstTime = false;

                    method = isAsc ? "OrderBy" : "OrderByDescending";
                }
                else
                {
                    method = isAsc ? "ThenBy":"ThenByDescending";
                }
                var parameter = Expression.Parameter(typeof(TEntity), "t");
                var property = typeof(TEntity).GetProperty(_orderField);
                var propertyAccess = Expression.MakeMemberAccess(parameter, property);
                var orderByExp = Expression.Lambda(propertyAccess, parameter);

                //resultExp = Expression.Call(typeof(Queryable), isAsc ? "OrderBy" : "OrderByDescending", new Type[] { typeof(TEntity), property.PropertyType }, tempData.Expression, Expression.Quote(orderByExp));
                //190911
                // When there are multiple conditions of orderby, call multiple times...
                resultExp = Expression.Call(typeof(Queryable), method, new Type[] { typeof(TEntity), property.PropertyType }, tempData.Expression, Expression.Quote(orderByExp));
                tempData = tempData.Provider.CreateQuery<TEntity>(resultExp);
            }
             //move to up
            //tempData = tempData.Provider.CreateQuery<TEntity>(resultExp);

           // tempData = entities.AsQueryable().Provider.CreateQuery<TEntity>(resultExp);

            //
            pagination.records = entities.Count();

            entities = tempData.ToList<TEntity>();

            List<TEntity> entities2 = entities.Skip<TEntity>(pagination.rows * (pagination.page - 1)).Take<TEntity>(pagination.rows).ToList();
                        //entities
            return entities2;
        }
        public List<TEntity> FindList<TEntity>(Pagination pagination) where TEntity : class, new()
        {
            bool isAsc = pagination.sord.ToLower() == "asc" ? true : false;
            string[] _order = pagination.sidx.Split(',');
            MethodCallExpression resultExp = null;
            var tempData = dbcontext.Set<TEntity>().AsQueryable();
            //190911
            bool firstTime = true;
            foreach (string item in _order)
            {
                if (string.IsNullOrEmpty(item))
                    continue; //yys

                string _orderPart = item;
                _orderPart = Regex.Replace(_orderPart, @"\s+", " ");
                string[] _orderArry = _orderPart.Split(' ');
                string _orderField = _orderArry[0];
                bool sort = isAsc;
                if (_orderArry.Length == 2)
                {
                    isAsc = _orderArry[1].ToUpper() == "ASC" ? true : false;
                }
                //---------
                string method = string.Empty;
                if (firstTime)
                {
                    firstTime = false;

                    method = isAsc ? "OrderBy" : "OrderByDescending";
                }
                else
                {
                    method = isAsc ? "ThenBy" : "ThenByDescending";
                }
                var parameter = Expression.Parameter(typeof(TEntity), "t");
                var property = typeof(TEntity).GetProperty(_orderField);
                var propertyAccess = Expression.MakeMemberAccess(parameter, property);
                var orderByExp = Expression.Lambda(propertyAccess, parameter);
                //resultExp = Expression.Call(typeof(Queryable), isAsc ? "OrderBy" : "OrderByDescending", new Type[] { typeof(TEntity), property.PropertyType }, tempData.Expression, Expression.Quote(orderByExp));
                //-----------------------//
                resultExp = Expression.Call(typeof(Queryable), method, new Type[] { typeof(TEntity), property.PropertyType }, tempData.Expression, Expression.Quote(orderByExp));
                tempData = tempData.Provider.CreateQuery<TEntity>(resultExp);
            }
            ////if (resultExp != null)
            //tempData = tempData.Provider.CreateQuery<TEntity>(resultExp);

            pagination.records = tempData.Count();
            tempData = tempData.Skip<TEntity>(pagination.rows * (pagination.page - 1)).Take<TEntity>(pagination.rows).AsQueryable();
            return tempData.ToList();
        }
        public List<TEntity> FindList<TEntity>(Expression<Func<TEntity, bool>> predicate, Pagination pagination) where TEntity : class, new()
        {
            bool isAsc = pagination.sord.ToLower() == "asc" ? true : false;
            string[] _order = pagination.sidx.Split(',');
            MethodCallExpression resultExp = null;
            var tempData = dbcontext.Set<TEntity>().Where(predicate);

            //190911
            bool firstTime = true;
            foreach (string item in _order)
            {
                if (string.IsNullOrEmpty(item))
                    continue; //yys

                string _orderPart = item;
                _orderPart = Regex.Replace(_orderPart, @"\s+", " ");
                string[] _orderArry = _orderPart.Split(' ');
                string _orderField = _orderArry[0];
                bool sort = isAsc;
                if (_orderArry.Length == 2)
                {
                    isAsc = _orderArry[1].ToUpper() == "ASC" ? true : false;
                }
                //---------
                string method = string.Empty;
                if (firstTime)
                {
                    firstTime = false;

                    method = isAsc ? "OrderBy" : "OrderByDescending";
                }
                else
                {
                    method = isAsc ? "ThenBy" : "ThenByDescending";
                }
                var parameter = Expression.Parameter(typeof(TEntity), "t");
                var property = typeof(TEntity).GetProperty(_orderField);
                var propertyAccess = Expression.MakeMemberAccess(parameter, property);
                var orderByExp = Expression.Lambda(propertyAccess, parameter);
                                                        //isAsc ? "OrderBy" : "OrderByDescending"
                resultExp = Expression.Call(typeof(Queryable), method, new Type[] { typeof(TEntity), property.PropertyType }, tempData.Expression, Expression.Quote(orderByExp));
                tempData = tempData.Provider.CreateQuery<TEntity>(resultExp);
            }

            //tempData = tempData.Provider.CreateQuery<TEntity>(resultExp);

            pagination.records = tempData.Count();
            tempData = tempData.Skip<TEntity>(pagination.rows * (pagination.page - 1)).Take<TEntity>(pagination.rows).AsQueryable();
            return tempData.ToList();
        }
    }
}
