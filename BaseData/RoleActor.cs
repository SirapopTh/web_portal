﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BaseData
{
    public enum Role { Patient,Nurse,Doctor,Manager,Admin,Super=100}

    public enum AuthorityRights {NoneAutho=0, Upload=1,ViewFile=2,SendUpload=4,Monitor=8,SendDownload=16,
        DownloadFromPacs =32,EditPersonal=64,EditGroup=128,DownloadFile=256}
    public class RoleActor
    {
        public static Role GetRole(string srole)
        {
            if (string.IsNullOrEmpty(srole) || srole == ((int)Role.Patient).ToString())
                return Role.Patient;
            if (srole == ((int)Role.Nurse).ToString())
                return Role.Nurse;
            if (srole == ((int)Role.Doctor).ToString())
                return Role.Doctor;
            if (srole == ((int)Role.Manager).ToString())
                return Role.Manager;
            if (srole == ((int)Role.Admin).ToString())
                return Role.Admin;
            if (srole == ((int)Role.Super).ToString())
                return Role.Super;
                
            return Role.Patient;
        }

        public static bool HasAuthorityRights(string sAutho, AuthorityRights authorityRights)
        {
            int i = 0;
            int.TryParse(sAutho, out i);
            //
            if ( (i & (int)authorityRights ) > 0)
                return true;
            return false;
        }
    }
}
