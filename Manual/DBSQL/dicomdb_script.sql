USE [master]
GO
/****** Object:  Database [DICOMDB]    Script Date: 9/3/2020 11:54:46 AM ******/
CREATE DATABASE [DICOMDB]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'DICOMDB', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL11.MSSQLSERVER\MSSQL\DATA\DICOMDB.mdf' , SIZE = 15360KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'DICOMDB_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL11.MSSQLSERVER\MSSQL\DATA\DICOMDB_log.ldf' , SIZE = 7616KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [DICOMDB] SET COMPATIBILITY_LEVEL = 110
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [DICOMDB].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [DICOMDB] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [DICOMDB] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [DICOMDB] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [DICOMDB] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [DICOMDB] SET ARITHABORT OFF 
GO
ALTER DATABASE [DICOMDB] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [DICOMDB] SET AUTO_CREATE_STATISTICS ON 
GO
ALTER DATABASE [DICOMDB] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [DICOMDB] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [DICOMDB] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [DICOMDB] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [DICOMDB] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [DICOMDB] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [DICOMDB] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [DICOMDB] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [DICOMDB] SET  DISABLE_BROKER 
GO
ALTER DATABASE [DICOMDB] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [DICOMDB] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [DICOMDB] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [DICOMDB] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [DICOMDB] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [DICOMDB] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [DICOMDB] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [DICOMDB] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [DICOMDB] SET  MULTI_USER 
GO
ALTER DATABASE [DICOMDB] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [DICOMDB] SET DB_CHAINING OFF 
GO
ALTER DATABASE [DICOMDB] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [DICOMDB] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
EXEC sys.sp_db_vardecimal_storage_format N'DICOMDB', N'ON'
GO
USE [DICOMDB]
GO
/****** Object:  User [dicom]    Script Date: 9/3/2020 11:54:46 AM ******/
CREATE USER [dicom] FOR LOGIN [dicom] WITH DEFAULT_SCHEMA=[dbo]
GO
ALTER ROLE [db_owner] ADD MEMBER [dicom]
GO
/****** Object:  Table [dbo].[admin_group1_log]    Script Date: 9/3/2020 11:54:46 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[admin_group1_log](
	[log_id] [int] IDENTITY(1,1) NOT NULL,
	[log_date] [int] NOT NULL,
	[log_time] [int] NOT NULL,
	[server_ip] [varchar](15) NOT NULL,
	[server_hostname] [varchar](32) NULL,
	[client_ip] [varchar](15) NOT NULL,
	[login_name] [varchar](64) NOT NULL,
	[user_uid] [int] NOT NULL,
	[user_gid] [int] NOT NULL,
	[acckey] [varchar](24) NOT NULL,
	[action] [varchar](8) NOT NULL,
	[new_gid] [int] NULL,
	[new_groupname] [varchar](64) NULL,
	[new_note] [varchar](64) NULL,
	[new_expiretime] [int] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[admin_user1_log]    Script Date: 9/3/2020 11:54:46 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[admin_user1_log](
	[log_id] [int] IDENTITY(1,1) NOT NULL,
	[log_date] [int] NOT NULL,
	[log_time] [int] NOT NULL,
	[server_ip] [varchar](15) NOT NULL,
	[server_hostname] [varchar](32) NULL,
	[client_ip] [varchar](15) NOT NULL,
	[login_name] [varchar](64) NOT NULL,
	[user_uid] [int] NOT NULL,
	[user_gid] [int] NOT NULL,
	[acckey] [varchar](24) NOT NULL,
	[action] [varchar](8) NOT NULL,
	[new_name] [varchar](64) NULL,
	[new_uid] [int] NULL,
	[new_gid] [int] NULL,
	[new_email] [varchar](64) NULL,
	[new_phone] [varchar](16) NULL,
	[new_note] [varchar](64) NULL,
	[new_loginname] [varchar](64) NULL,
	[new_available] [varchar](1) NULL,
	[new_validdate1] [int] NULL,
	[new_validdate2] [int] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[connect1]    Script Date: 9/3/2020 11:54:46 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[connect1](
	[UID] [int] NULL,
	[AccKey] [varchar](24) NULL,
	[RemoteIP] [varchar](15) NULL,
	[ExpireDate] [int] NULL,
	[ExpireTime] [int] NULL,
	[QueryCount] [int] NULL,
	[LoginDate] [int] NULL,
	[LoginTime] [int] NULL,
	[LogoutDate] [int] NULL,
	[LogoutTime] [int] NULL,
	[State] [int] NULL,
	[LoginIP] [varchar](15) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[connect2]    Script Date: 9/3/2020 11:54:46 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[connect2](
	[UUID] [varchar](32) NOT NULL,
	[UserID] [int] NOT NULL,
	[LoginDate0] [int] NOT NULL,
	[LoginTime0] [int] NOT NULL,
	[LoginDate] [int] NOT NULL,
	[LoginTime] [int] NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[deleted_image]    Script Date: 9/3/2020 11:54:46 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[deleted_image](
	[instance_uid] [varchar](64) NOT NULL,
	[series_uid_id] [int] NOT NULL,
	[image_number] [int] NULL,
	[transfer_uid] [varchar](30) NULL,
	[source_ae] [varchar](16) NULL,
	[rcvd_date] [int] NULL,
	[rcvd_time] [int] NULL,
	[file_size] [int] NULL,
	[del_user] [varchar](16) NULL,
	[deleted_date] [int] NULL,
	[deleted_time] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[instance_uid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[deleted_patient]    Script Date: 9/3/2020 11:54:46 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[deleted_patient](
	[ptn_id] [varchar](64) NULL,
	[ptn_name] [varchar](255) NULL,
	[ptn_id_id] [int] NOT NULL,
	[sex] [varchar](1) NULL,
	[birth_date] [int] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[deleted_series]    Script Date: 9/3/2020 11:54:46 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[deleted_series](
	[series_uid] [varchar](64) NOT NULL,
	[series_uid_id] [int] NOT NULL,
	[study_uid_id] [int] NOT NULL,
	[series_number] [int] NULL,
	[modality] [varchar](16) NULL,
	[body_part] [varchar](16) NULL,
PRIMARY KEY CLUSTERED 
(
	[series_uid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[deleted_study]    Script Date: 9/3/2020 11:54:46 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[deleted_study](
	[study_uid] [varchar](64) NOT NULL,
	[study_uid_id] [int] NOT NULL,
	[ptn_id_id] [int] NOT NULL,
	[study_date] [int] NULL,
	[study_time] [numeric](12, 6) NULL,
	[study_id] [varchar](16) NULL,
	[accession_number] [varchar](16) NULL,
	[physician_name] [varchar](64) NULL,
	[study_desc] [varchar](64) NULL,
PRIMARY KEY CLUSTERED 
(
	[study_uid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[dicom_tags]    Script Date: 9/3/2020 11:54:46 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[dicom_tags](
	[lvl] [int] NOT NULL,
	[fieldname] [varchar](32) NOT NULL,
	[tags] [varchar](16) NOT NULL,
	[description] [varchar](64) NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[exportimage_log]    Script Date: 9/3/2020 11:54:46 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[exportimage_log](
	[log_id] [int] IDENTITY(1,1) NOT NULL,
	[log_date] [int] NOT NULL,
	[log_time] [int] NOT NULL,
	[server_ip] [varchar](15) NOT NULL,
	[server_hostname] [varchar](32) NULL,
	[client_ip] [varchar](15) NOT NULL,
	[login_name] [varchar](64) NOT NULL,
	[user_uid] [int] NOT NULL,
	[user_gid] [int] NOT NULL,
	[acckey] [varchar](24) NOT NULL,
	[ptn_id] [varchar](64) NOT NULL,
	[accession_number] [varchar](16) NULL,
	[modality] [varchar](16) NULL,
	[study_uid] [varchar](64) NOT NULL,
	[series_uid] [varchar](64) NOT NULL,
	[instance_uid] [text] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[group1]    Script Date: 9/3/2020 11:54:46 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[group1](
	[GID] [int] NULL,
	[GROUPNAME] [varchar](64) NULL,
	[NOTE] [varchar](64) NULL,
	[EXPIRETIME] [int] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[image1]    Script Date: 9/3/2020 11:54:46 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[image1](
	[instance_uid] [varchar](64) NOT NULL,
	[series_uid_id] [int] NOT NULL,
	[image_number] [int] NULL,
	[image_file] [varchar](255) NOT NULL,
	[transfer_uid] [varchar](30) NULL,
	[class_uid_id] [varchar](64) NOT NULL,
	[backup_label] [varchar](16) NULL,
	[source_ae] [varchar](16) NULL,
	[rcvd_date] [int] NULL,
	[rcvd_time] [int] NULL,
	[file_size] [int] NULL,
	[transaction_uid] [varchar](64) NULL,
	[deleted] [varchar](1) NULL DEFAULT ('F'),
	[key_image] [varchar](1) NULL DEFAULT ('F'),
	[n_rows] [int] NULL,
	[n_columns] [int] NULL,
	[bits_allocated] [int] NULL,
	[n_frames] [int] NULL,
	[pr_label] [varchar](16) NULL,
	[pr_desc] [varchar](64) NULL,
	[pr_date] [int] NULL,
	[pr_time] [numeric](12, 6) NULL,
	[pr_creator] [varchar](64) NULL,
	[completion] [varchar](1) NULL,
	[verification] [varchar](1) NULL,
	[observation_dt] [varchar](26) NULL,
	[v_organization] [varchar](64) NULL,
	[v_datetime] [varchar](26) NULL,
	[v_observer_name] [varchar](64) NULL,
	[titile_value] [varchar](16) NULL,
	[title_designator] [varchar](16) NULL,
	[title_meaning] [varchar](64) NULL,
	[template_id] [varchar](16) NULL,
	[coded_file] [varchar](255) NULL,
	[slice_thickness] [varchar](16) NULL,
PRIMARY KEY CLUSTERED 
(
	[instance_uid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[login_log]    Script Date: 9/3/2020 11:54:46 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[login_log](
	[log_id] [int] IDENTITY(1,1) NOT NULL,
	[login_date] [int] NOT NULL,
	[login_time] [int] NOT NULL,
	[logout_date] [int] NULL,
	[logout_time] [int] NULL,
	[server_ip] [varchar](15) NOT NULL,
	[server_hostname] [varchar](32) NULL,
	[client_ip] [varchar](15) NOT NULL,
	[login_name] [varchar](64) NOT NULL,
	[user_uid] [int] NOT NULL,
	[user_gid] [int] NOT NULL,
	[acckey] [varchar](24) NOT NULL,
	[connect_count] [int] NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[patient1]    Script Date: 9/3/2020 11:54:46 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[patient1](
	[ptn_id] [varchar](64) NULL,
	[ptn_name] [varchar](255) NULL,
	[ptn_id_id] [int] IDENTITY(1,1) NOT NULL,
	[sex] [varchar](1) NULL,
	[birth_date] [int] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[query_log]    Script Date: 9/3/2020 11:54:46 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[query_log](
	[log_id] [int] IDENTITY(1,1) NOT NULL,
	[log_date] [int] NOT NULL,
	[log_time] [int] NOT NULL,
	[start_date] [int] NOT NULL,
	[start_time] [int] NOT NULL,
	[end_date] [int] NOT NULL,
	[end_time] [int] NOT NULL,
	[server_ip] [varchar](15) NOT NULL,
	[server_hostname] [varchar](32) NULL,
	[client_ip] [varchar](15) NOT NULL,
	[login_name] [varchar](64) NOT NULL,
	[user_uid] [int] NOT NULL,
	[user_gid] [int] NOT NULL,
	[acckey] [varchar](24) NULL,
	[query_key] [text] NOT NULL,
	[record_count] [int] NOT NULL,
	[elapse_time] [int] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[rc_eventlog]    Script Date: 9/3/2020 11:54:46 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[rc_eventlog](
	[username] [varchar](64) NULL,
	[rc_aetitle] [varchar](64) NULL,
	[rc_ip] [varchar](15) NULL,
	[rc_event] [varchar](256) NULL,
	[event_date] [int] NULL,
	[event_time] [int] NULL,
	[event_result_flag] [int] NULL,
	[event_result] [varchar](255) NULL,
	[client_ip] [varchar](15) NULL,
	[ptn_id] [varchar](64) NULL,
	[ptn_name] [varchar](64) NULL,
	[study_date] [int] NULL,
	[study_time] [int] NULL,
	[accession_number] [varchar](16) NULL,
	[modality] [varchar](16) NULL,
	[study_uid] [varchar](64) NULL,
	[series_uid] [varchar](64) NULL,
	[instance_uid] [varchar](64) NULL,
	[del_reason] [varchar](64) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[rc_log]    Script Date: 9/3/2020 11:54:46 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[rc_log](
	[ptn_id] [varchar](64) NULL,
	[ptn_name] [varchar](255) NULL,
	[study_uid] [varchar](64) NOT NULL,
	[study_date] [int] NULL,
	[study_time] [numeric](12, 6) NULL,
	[accession_number] [varchar](16) NULL,
	[series_uid] [varchar](64) NOT NULL,
	[series_number] [int] NULL,
	[modality] [varchar](16) NULL,
	[instance_uid] [varchar](64) NOT NULL,
	[image_number] [int] NULL,
	[server_ae] [varchar](16) NOT NULL,
	[command_user] [varchar](16) NOT NULL,
	[client_ip] [varchar](15) NOT NULL,
	[command_type] [varchar](15) NOT NULL,
	[command_date] [varchar](8) NOT NULL,
	[command_time] [varchar](6) NOT NULL,
	[reason] [varchar](64) NULL,
	[image_file] [varchar](255) NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[rc_srvlist]    Script Date: 9/3/2020 11:54:46 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[rc_srvlist](
	[rc_aetitle] [varchar](64) NULL,
	[rc_ip] [varchar](15) NULL,
	[rc_port] [int] NULL,
	[rc_username] [varchar](50) NULL,
	[rc_password] [varchar](50) NULL,
	[rc_priority] [int] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[rc_user]    Script Date: 9/3/2020 11:54:46 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[rc_user](
	[rc_user] [varchar](16) NOT NULL,
	[rc_pass] [varchar](16) NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[report_status1]    Script Date: 9/3/2020 11:54:46 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[report_status1](
	[RID] [int] IDENTITY(1,1) NOT NULL,
	[StudyUID] [varchar](64) NULL,
	[AccessionNum] [varchar](64) NULL,
	[InstanceUID] [varchar](64) NULL,
	[RevisionNo] [int] NULL,
	[RequestedUserID] [varchar](64) NULL,
	[RequestedDateTime] [varchar](14) NULL,
	[ReadDateTime] [varchar](14) NULL,
	[ReadByUserID] [varchar](64) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[series1]    Script Date: 9/3/2020 11:54:46 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[series1](
	[series_uid] [varchar](64) NOT NULL,
	[series_uid_id] [int] IDENTITY(1,1) NOT NULL,
	[study_uid_id] [int] NOT NULL,
	[series_number] [int] NULL,
	[modality] [varchar](16) NULL,
	[body_part] [varchar](16) NULL,
	[series_date] [int] NULL,
	[series_time] [numeric](12, 6) NULL,
	[series_desc] [varchar](64) NULL,
	[req_proc_id] [varchar](80) NULL,
	[sched_proc_id] [varchar](80) NULL,
	[pps_date] [int] NULL,
	[pps_time] [numeric](12, 6) NULL,
	[protocol_name] [varchar](64) NULL,
	[operator_name] [varchar](64) NULL,
	[model_name] [varchar](64) NULL,
	[institution] [varchar](64) NULL,
PRIMARY KEY CLUSTERED 
(
	[series_uid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[study1]    Script Date: 9/3/2020 11:54:46 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[study1](
	[study_uid] [varchar](64) NOT NULL,
	[study_uid_id] [int] IDENTITY(1,1) NOT NULL,
	[ptn_id_id] [int] NOT NULL,
	[study_date] [int] NULL,
	[study_time] [numeric](12, 6) NULL,
	[study_id] [varchar](16) NULL,
	[accession_number] [varchar](16) NULL,
	[physician_name] [varchar](64) NULL,
	[study_desc] [varchar](60) NULL,
	[user_group] [int] NULL CONSTRAINT [study1_usergroup_default]  DEFAULT ((16777215)),
	[status_viewimage_useruid] [int] NULL,
	[status_viewimage_username] [varchar](64) NULL,
	[status_viewimage_date] [int] NULL,
	[status_viewimage_time] [int] NULL,
	[request_service] [varchar](64) NULL,
	[request_physician] [varchar](64) NULL,
	[report_read] [varchar](1) NULL CONSTRAINT [DF__study1__report_r__47DBAE45]  DEFAULT ('U'),
	[modalities] [varchar](64) NULL,
	[trigger_dttm] [varchar](14) NULL CONSTRAINT [DF_study1_trigger_dttm]  DEFAULT ('A'),
	[hospital] [varchar](64) NULL,
	[hospital_code] [varchar](16) NULL,
	[hospital_name] [varchar](64) NULL,
 CONSTRAINT [PK__study1__6D9F6C96CE00B8C0] PRIMARY KEY CLUSTERED 
(
	[study_uid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TEMP_PACS]    Script Date: 9/3/2020 11:54:46 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TEMP_PACS](
	[TMP_ID] [int] IDENTITY(1,1) NOT NULL,
	[ACCESSION_NUMBER] [varchar](50) NOT NULL,
	[Date_Achrive] [varchar](50) NULL,
	[Study_Date] [varchar](50) NULL,
	[Study_Time] [varchar](50) NULL,
	[HN] [varchar](50) NULL,
	[NAME] [varchar](200) NULL,
	[Modality] [varchar](50) NULL,
	[Study_Desc] [varchar](255) NULL,
	[Total_Images] [varchar](50) NULL,
	[Recieve_DATE] [varchar](50) NULL,
	[FROM_AE] [varchar](50) NULL,
 CONSTRAINT [PK_TEMP_PACS] PRIMARY KEY CLUSTERED 
(
	[TMP_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[user1]    Script Date: 9/3/2020 11:54:46 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[user1](
	[NAME] [varchar](64) NULL,
	[UID] [int] IDENTITY(1,1) NOT NULL,
	[GID] [int] NULL,
	[EMAIL] [varchar](64) NULL,
	[PHONE] [varchar](16) NULL,
	[NOTE] [varchar](64) NULL,
	[LOGINPW] [varchar](16) NULL,
	[LOGINNAME] [varchar](64) NULL,
	[AVAILABLE] [varchar](1) NULL CONSTRAINT [DF_user1_AVAILABLE]  DEFAULT ('T'),
	[VALIDDATE1] [int] NULL CONSTRAINT [DF_user1_VALIDDATE1]  DEFAULT ((0)),
	[VALIDDATE2] [int] NULL CONSTRAINT [DF_user1_VALIDDATE2]  DEFAULT ((0))
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[viewimage_log]    Script Date: 9/3/2020 11:54:46 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[viewimage_log](
	[log_id] [int] IDENTITY(1,1) NOT NULL,
	[log_date] [int] NOT NULL,
	[log_time] [int] NOT NULL,
	[start_date] [int] NOT NULL,
	[start_time] [int] NOT NULL,
	[end_date] [int] NOT NULL,
	[end_time] [int] NOT NULL,
	[server_ip] [varchar](15) NOT NULL,
	[server_hostname] [varchar](32) NULL,
	[client_ip] [varchar](15) NOT NULL,
	[login_name] [varchar](64) NOT NULL,
	[user_uid] [int] NOT NULL,
	[user_gid] [int] NOT NULL,
	[acckey] [varchar](24) NOT NULL,
	[ptn_id] [varchar](64) NOT NULL,
	[accession_number] [varchar](16) NULL,
	[modality] [varchar](16) NULL,
	[study_uid] [varchar](64) NOT NULL,
	[series_uid] [varchar](64) NOT NULL,
	[image_count] [int] NOT NULL,
	[image_totalbyte] [int] NOT NULL,
	[elapse_time] [int] NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[vna_fileinfo]    Script Date: 9/3/2020 11:54:46 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[vna_fileinfo](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[upload_id_id] [int] NOT NULL,
	[upload_date] [datetime] NULL,
	[org_filename] [varchar](120) NULL,
	[org_filetype] [varchar](30) NULL,
	[study_uid] [varchar](64) NULL,
	[series_uid] [varchar](64) NULL,
	[instance_uid] [varchar](64) NULL,
	[vna_filepath] [varchar](384) NULL,
	[status] [varchar](2) NULL,
 CONSTRAINT [PK_vna_fileinfo] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[vna_upload]    Script Date: 9/3/2020 11:54:46 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[vna_upload](
	[upload_id] [varchar](40) NOT NULL,
	[upload_id_id] [int] NOT NULL,
	[upload_sessionid] [varchar](40) NULL,
	[user_id_id] [int] NULL,
	[medical_num] [varchar](50) NULL,
	[native_id] [varchar](50) NULL,
	[first_name] [nvarchar](50) NULL,
	[middle_name] [nvarchar](50) NULL,
	[last_name] [nvarchar](50) NULL,
	[create_date] [datetime] NULL,
	[study_date] [datetime] NULL,
	[ref_physician] [nvarchar](50) NULL,
	[report_type] [varchar](20) NULL,
	[source_dept] [varchar](10) NULL,
	[source_ofdata] [varchar](10) NULL,
	[comment] [nvarchar](50) NULL,
	[study_uid] [varchar](64) NULL,
	[series_uid] [varchar](64) NULL,
 CONSTRAINT [PK_vna_upload] PRIMARY KEY CLUSTERED 
(
	[upload_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[wado_log]    Script Date: 9/3/2020 11:54:46 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[wado_log](
	[log_id] [int] IDENTITY(1,1) NOT NULL,
	[log_date] [int] NOT NULL,
	[log_time] [int] NOT NULL,
	[server_ip] [varchar](15) NOT NULL,
	[server_hostname] [varchar](32) NULL,
	[client_ip] [varchar](15) NOT NULL,
	[login_name] [varchar](64) NOT NULL,
	[user_uid] [int] NOT NULL,
	[user_gid] [int] NOT NULL,
	[acckey] [varchar](24) NULL,
	[object_uid] [varchar](64) NOT NULL,
	[param] [text] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[worklist]    Script Date: 9/3/2020 11:54:46 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[worklist](
	[study_instance_uid] [varchar](64) NOT NULL,
	[patient_id] [varchar](64) NOT NULL,
	[patient_name] [varchar](64) NOT NULL,
	[patient_birth] [varchar](8) NULL,
	[patient_sex] [varchar](1) NULL,
	[patient_size] [numeric](4, 3) NULL,
	[patient_weight] [numeric](4, 1) NULL,
	[medical_alert] [varchar](64) NULL,
	[contrast_allergy] [varchar](64) NULL,
	[occupation] [varchar](16) NULL,
	[pregnancy_status] [smallint] NULL,
	[modality] [varchar](16) NOT NULL,
	[station_aet] [varchar](16) NULL,
	[sched_start_date] [varchar](8) NOT NULL,
	[sched_start_time] [varchar](16) NOT NULL,
	[perform_physician] [varchar](64) NULL,
	[request_physician] [varchar](64) NULL,
	[refer_physician] [varchar](64) NULL,
	[accession_num] [varchar](16) NULL,
	[sched_proc_id] [varchar](16) NOT NULL,
	[sched_proc_desc] [varchar](64) NOT NULL,
	[req_proc_id] [varchar](16) NOT NULL,
	[req_proc_desc] [varchar](64) NOT NULL,
	[station_name] [varchar](16) NULL,
	[req_proc_location] [varchar](16) NULL,
	[sched_proc_loc] [varchar](16) NULL,
	[pre_medication] [varchar](64) NULL,
	[admission_id] [varchar](64) NULL,
	[special_needs] [varchar](64) NULL,
	[patient_location] [varchar](64) NULL,
	[patient_state] [varchar](64) NULL,
	[req_proc_priority] [varchar](16) NULL,
	[patient_transport] [varchar](64) NULL,
	[confidentiality] [varchar](64) NULL,
	[perfrmd_aet] [varchar](16) NULL,
	[perfrmd_start_date] [varchar](8) NULL,
	[perfrmd_start_time] [varchar](16) NULL,
	[perfrmd_status] [varchar](16) NULL,
	[mpps_sop_uid] [varchar](64) NULL,
	[perfrmd_proc_id] [varchar](16) NULL,
	[perfrmd_end_date] [varchar](8) NULL,
	[perfrmd_end_time] [varchar](16) NULL,
	[character_set] [varchar](32) NULL,
	[service] [varchar](64) NULL,
	[residence] [varchar](64) NULL,
	[code_value] [varchar](255) NULL,
	[code_designator] [varchar](255) NULL,
	[code_meaning] [varchar](255) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  View [dbo].[View2]    Script Date: 9/3/2020 11:54:46 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[View2] AS Select Convert(datetime,Convert(varchar(10),dbo.study1.study_date,103)) As study_date, dbo.study1.study_time, dbo.study1.accession_number, dbo.series1.sched_proc_id, dbo.study1.study_desc, dbo.series1.modality, dbo.study1.study_uid, dbo.image1.file_size / 1000 As image_size From dbo.study1 Inner Join dbo.series1 On dbo.study1.study_uid_id = dbo.series1.study_uid_id Inner Join dbo.image1 On dbo.series1.series_uid_id = dbo.image1.series_uid_id Where dbo.series1.modality <> 'REPORT'

GO
/****** Object:  View [dbo].[viewTele]    Script Date: 9/3/2020 11:54:46 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[viewTele] AS Select Convert(datetime,Convert(varchar(10),dbo.study1.study_date,103)) As study_date, dbo.study1.study_time, dbo.study1.accession_number, dbo.series1.sched_proc_id, dbo.study1.study_desc, dbo.study1.trigger_dttm, dbo.series1.modality, dbo.study1.physician_name, dbo.study1.study_id, dbo.study1.hospital, dbo.study1.study_uid From dbo.study1 Inner Join dbo.series1 On dbo.study1.study_uid_id = dbo.series1.study_uid_id Where dbo.series1.modality <> 'REPORT'

GO
/****** Object:  Index [del_image_idx]    Script Date: 9/3/2020 11:54:46 AM ******/
CREATE NONCLUSTERED INDEX [del_image_idx] ON [dbo].[deleted_image]
(
	[series_uid_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [del_series_idx]    Script Date: 9/3/2020 11:54:46 AM ******/
CREATE NONCLUSTERED INDEX [del_series_idx] ON [dbo].[deleted_series]
(
	[study_uid_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [del_study_idx]    Script Date: 9/3/2020 11:54:46 AM ******/
CREATE NONCLUSTERED INDEX [del_study_idx] ON [dbo].[deleted_study]
(
	[ptn_id_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_dicom_tags_lvl]    Script Date: 9/3/2020 11:54:46 AM ******/
CREATE NONCLUSTERED INDEX [IX_dicom_tags_lvl] ON [dbo].[dicom_tags]
(
	[lvl] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_dicom_tags_tags]    Script Date: 9/3/2020 11:54:46 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_dicom_tags_tags] ON [dbo].[dicom_tags]
(
	[tags] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [image_backup]    Script Date: 9/3/2020 11:54:46 AM ******/
CREATE NONCLUSTERED INDEX [image_backup] ON [dbo].[image1]
(
	[deleted] ASC,
	[backup_label] ASC,
	[rcvd_date] ASC,
	[rcvd_time] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [image_delete]    Script Date: 9/3/2020 11:54:46 AM ******/
CREATE NONCLUSTERED INDEX [image_delete] ON [dbo].[image1]
(
	[deleted] ASC,
	[rcvd_date] ASC,
	[rcvd_time] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [image_index]    Script Date: 9/3/2020 11:54:46 AM ******/
CREATE NONCLUSTERED INDEX [image_index] ON [dbo].[image1]
(
	[series_uid_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [patient_idx1]    Script Date: 9/3/2020 11:54:46 AM ******/
CREATE NONCLUSTERED INDEX [patient_idx1] ON [dbo].[patient1]
(
	[ptn_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [patient_idx2]    Script Date: 9/3/2020 11:54:46 AM ******/
CREATE NONCLUSTERED INDEX [patient_idx2] ON [dbo].[patient1]
(
	[ptn_id] ASC,
	[ptn_name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [patient_idx3]    Script Date: 9/3/2020 11:54:46 AM ******/
CREATE NONCLUSTERED INDEX [patient_idx3] ON [dbo].[patient1]
(
	[ptn_id_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [series_idx1]    Script Date: 9/3/2020 11:54:46 AM ******/
CREATE NONCLUSTERED INDEX [series_idx1] ON [dbo].[series1]
(
	[modality] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [series_idx2]    Script Date: 9/3/2020 11:54:46 AM ******/
CREATE NONCLUSTERED INDEX [series_idx2] ON [dbo].[series1]
(
	[series_uid_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [series_index]    Script Date: 9/3/2020 11:54:46 AM ******/
CREATE NONCLUSTERED INDEX [series_index] ON [dbo].[series1]
(
	[study_uid_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [study_delete]    Script Date: 9/3/2020 11:54:46 AM ******/
CREATE NONCLUSTERED INDEX [study_delete] ON [dbo].[study1]
(
	[study_date] ASC,
	[study_time] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [study_idx1]    Script Date: 9/3/2020 11:54:46 AM ******/
CREATE NONCLUSTERED INDEX [study_idx1] ON [dbo].[study1]
(
	[study_date] ASC,
	[study_time] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [study_idx2]    Script Date: 9/3/2020 11:54:46 AM ******/
CREATE NONCLUSTERED INDEX [study_idx2] ON [dbo].[study1]
(
	[accession_number] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [study_idx3]    Script Date: 9/3/2020 11:54:46 AM ******/
CREATE NONCLUSTERED INDEX [study_idx3] ON [dbo].[study1]
(
	[study_uid_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [study_index]    Script Date: 9/3/2020 11:54:46 AM ******/
CREATE NONCLUSTERED INDEX [study_index] ON [dbo].[study1]
(
	[ptn_id_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [sched_start]    Script Date: 9/3/2020 11:54:46 AM ******/
CREATE NONCLUSTERED INDEX [sched_start] ON [dbo].[worklist]
(
	[sched_start_date] ASC,
	[sched_start_time] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[41] 4[43] 2[5] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "patient1"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 126
               Right = 198
            End
            DisplayFlags = 280
            TopColumn = 1
         End
         Begin Table = "study1"
            Begin Extent = 
               Top = 6
               Left = 236
               Bottom = 126
               Right = 464
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "series1"
            Begin Extent = 
               Top = 6
               Left = 502
               Bottom = 126
               Right = 665
            End
            DisplayFlags = 280
            TopColumn = 9
         End
         Begin Table = "image1"
            Begin Extent = 
               Top = 6
               Left = 703
               Bottom = 141
               Right = 889
            End
            DisplayFlags = 280
            TopColumn = 8
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 17
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 75
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 117' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'viewTele'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N'0
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'viewTele'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'viewTele'
GO
USE [master]
GO
ALTER DATABASE [DICOMDB] SET  READ_WRITE 
GO
