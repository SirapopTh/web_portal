﻿using System.Web;
using System.Web.Optimization;

namespace CDSS.OAP.NSW.UI.Web
{
    public class BundleConfig
    {
        // For more information on Bundling, visit http://go.microsoft.com/fwlink/?LinkId=254725
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                         "~/Scripts/jquery-3.3.1.js",
                         "~/Scripts/jquery-3.3.1.min.js"
                         ));

            bundles.Add(new ScriptBundle("~/bundles/jqueryui").Include(
                        "~/Scripts/jquery-ui*")
                        );

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.unobtrusive*",
                        "~/Scripts/jquery.validate*"));


            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                "~/Scripts/popper.min.js",
                "~/Scripts/bootstrap/bootstrap.js",
                "~/Scripts/bootstrap/bootstrap.min.js",
                "~/Scripts/bootstrap/bootstrap-toggle.min.js",
                "~/Scripts/popper.min.js"
            ));

            bundles.Add(new ScriptBundle("~/bundles/kendo").Include(
                "~/Scripts/kendo/kendo.all.min.js",
                "~/Scripts/kendo/kendo.core.min.js",
                "~/Scripts/kendo/kendo.metro.min.js",
                "~/Scripts/kendo/kendo.toolbar.min.js",
                "~/Scripts/kendo/kendo.upload.min.js",
                "~/Scripts/kendo/kendo.aspnetmvc.min.js",
                "~/Scripts/kendo/cdss.kendo.datepicker.js",
                "~/Scripts/kendo/cdss.kendo.datetimepicker.js",
                "~/Scripts/kendo/cdss.kendo.calendar.js",
                "~/Scripts/kendo/kendo.popup.min.js",
                "~/Scripts/kendo/cultures/kendo.culture.th-TH.min.js",
                "~/Scripts/kendo/jszip.min.js"
             ));

            bundles.Add(new StyleBundle("~/Content/kendo/css").Include(
                 "~/Content/kendo/kendo.common-bootstrap.min.css",
                 "~/Content/kendo/cdss.default.blue.min.css"
             ));

            bundles.Add(new StyleBundle("~/Content/bootstrap").Include(
           "~/Content/bootstrap/bootstrap.css",
           "~/Content/bootstrap/bootstrap-glyphicons.css",
                "~/Content/bootstrap/css/bootstrap-toggle.min.css"

           //"~/Scripts/jQueryCalendarThai/jquery-ui-1.11.4.custom.css"
           ));
            bundles.Add(new StyleBundle("~/Content/").Include(
            "~/Content/_LayoutLogOn.css"

          //"~/Scripts/jQueryCalendarThai/jquery-ui-1.11.4.custom.css"
          ));

            //bundles.Add(new ScriptBundle("~/bundles/cdss").Include(
            //     "~/Scripts/cdss.js",
            //     "~/Scripts/jQueryCalendarThai/jquery-ui-1.11.4.custom.js",
            //    "~/Scripts/cdss.datetime.th.js"
            //));

            bundles.Add(new StyleBundle("~/Content/cdss").Include(
                "~/Scripts/jQueryCalendarThai/jquery-ui-1.11.4.custom.css",
                "~/Content/jquery.datetimepicker.css"
            ));

            bundles.Add(new ScriptBundle("~/bundles/cdss").Include(
                // "~/Scripts/cdss.js",
                //"~/Scripts/cdss.datetime.th.js",
                //"~/Scripts/cdss.userprofile.multiform.js",
                //"~/Scripts/cdss.master.nameprefix.js",
                //"~/Scripts/cdss.master.country.js",
                //"~/Scripts/cdss.master.agencytype.js",
                //"~/Scripts/cdss.master.formgroup.js",
                //"~/Scripts/cdss.master.profiletaxno.js",
                //"~/Scripts/cdss.master.vserviceappprofile.js",
                //"~/Scripts/cdss.master.province.js",
                //"~/Scripts/cdss.master.quantity.js",
                //"~/Scripts/cdss.master.weight.js",
                //"~/Scripts/cdss.master.element.js",
                //"~/Scripts/cdss.master.formtype.js",
                //"~/Scripts/cdss.master.subprovince.js",
                //"~/Scripts/cdss.master.district.js",
                "~/Scripts/sweetalert.js",
                //"~/Scripts/cdss.ThaiCitizenIdCard.js",
                "~/Scripts/sweetalert.min.js",
                "~/Scripts/jQueryCalendarThai/jquery-ui-1.11.4.custom.js",
                "~/Scripts/cdss.datetime.th.js",
                "~/Scripts/cdss-onchange.js"
                 //"~/Scripts/cdss.window.js"
                 // "~/Scripts/jquery.string.format.js",
                 // "~/Scripts/jquery.bootstrap.loading.js"

             ));

            bundles.Add(new StyleBundle("~/Content/fontAwesome").Include(
          "~/Content/dist/css/font-awesome.css",
           "~/Content/dist/css/ionicons.min.css"
           ));

            bundles.Add(new StyleBundle("~/Content/theme/css").Include(
       "~/Content/dist/css/sweetalert.css"));

            bundles.IgnoreList.Clear();

        }
    }
}