﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using Common;
using DataModel;

namespace VNA.Controllers
{
    public class RegisterController : Controller //ControllerBase，不需要登录信息
    {
       // BaseData.VnaDbContext vnaDbContext = new BaseData.VnaDbContext();
        

        private Common.MailHelper mailHelper = new Common.MailHelper();

        // GET: Register
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult SignUp()
        {
            return View();

        }
        
        [HttpPost]
      //  [HandlerAjaxOnly]
        public ActionResult CreateAccount(string user_email, string password)
        {
            if(string.IsNullOrEmpty(user_email.Trim()) || string.IsNullOrEmpty(password))
            {
                return Json(new { state = "0", message = "mail or password is empty." });
            }
            BaseData.VnaDbContext vnaDbContext = new BaseData.VnaDbContext();

            IUserRegRepository userRegRepository = new UserRegRepository(vnaDbContext);

           // string snewpass = Md5.Md5Hash(password).ToLower(); //保存Md5的hasp...

            UserReg ur = userRegRepository.GetUser(user_email);
            if( ur != null && ur.is_verified )
            {
                //已经verified..
                return Json(new { state = "0", message = "user is existed." });
            }
            UserReg userReg = new UserReg();
            userReg.user_email = user_email;
            userReg.regid = Common.Common.GuId();
            userReg.is_verified = false;
            userReg.reg_date = DateTime.Now;
            //.........................
            try
            {


                userRegRepository.BeginTrans();

                bool bf = userRegRepository.CreateUserReg(userReg);
                if (bf)
                {
                    //save to users..
                    IUserRepository usrep = new UserRepository(vnaDbContext);

                    Users uss = new Users();
                    uss.user_id = user_email;
                    uss.user_name = "";
                    uss.email = user_email;
                    uss.is_verified = false;
                    uss.user_password = password ;// snewpass;
                    uss.delete_mark = false;
                    uss.is_initial = false;

                    usrep.CreateUser(uss);


                    userRegRepository.Commit();
                }
                //发送激活邮件..
                if (bf)
                {
                    sendActivateMail(user_email,userReg.regid);
                }
                string success = "The account hs been created successfully,\nPlease receive mail to activate it.";
                if (bf)
                    return Json(new { state = "1", message = success });
               
                
            }
            catch (Exception ex)
            {
                return Json(new { state = "0", message = ex.Message });
            }
            return Json(new { state = "0", message = "user created error." });
        }
      //  [HttpPost]
      ////  [HandlerAjaxOnly]
      //  public ActionResult CreateAccountU(UserReg userReg)
      //  {
      //      bool bf = userRegRepository.CreateUserReg(userReg);

      //      if (bf)
      //          return Json(new { state = "1", message = "success" });

      //      return Json( new { state = "0", message = "user created error." });
      //  }
        [HttpPost]
        public ActionResult CheckUser(string user_email)
        {
            BaseData.VnaDbContext vnaDbContext = new BaseData.VnaDbContext();
            IUserRegRepository userRegRepository = new UserRegRepository(vnaDbContext);
            UserReg ur = userRegRepository.GetUser(user_email);

            if (ur != null)
                return Json(new { state = "1", message = "user is existed." });

            return Json(new { state = "0", message = "user is not existed." });
        }
        [HttpGet]
        public ActionResult Activate(string regid)
        {
            //
            BaseData.VnaDbContext vnaDbContext = new BaseData.VnaDbContext();
            IUserRegRepository userRegRepository = new UserRegRepository(vnaDbContext);
            IUserRepository ussRepository = new UserRepository(vnaDbContext);

            UserReg ur = userRegRepository.GetUserByRegid(regid);
            if(ur != null && !ur.is_verified )
            {
                //using (var db = new BaseData.RepositoryBase(vnaDbContext).BeginTrans())
                userRegRepository.BeginTrans();

                {
                    userRegRepository.ValidateUserReg(ur);
                    //insert..user...
                    Users uss = ussRepository.GetUser(ur.user_email);
                    uss.is_verified = true;
                    uss.delete_mark = false;
                    //
                    ussRepository.Update(uss);

                    userRegRepository.Commit();

                    ViewBag.logUser = uss.user_id;
                }
            }
            else
            {
                
                return Content("user has been activated.");// Json(new { state = "0", message = "user has been activated." },JsonRequestBehavior.AllowGet);
            }
            //
            //string smessage = "User is activated, <a href='" + Url.Action("Login", "User") + "'> Login </a>.";
            // smessage = Server.HtmlEncode(smessage); 
            

            return View();// "User is activated,click the Link to login");// Json(new { state = "1", message = "user is activated." },JsonRequestBehavior.AllowGet);
        }

        void sendActivateMail(string smail,string regid)
        {
            VNAConfigSection vna = VNAConfigSection.Instance;
            string smtpHost =  vna.emailElement.SmtpHost;
            string sloginId = vna.emailElement.SmtpLoginId;
            string sloginPass = vna.emailElement.SmtpLoginPasswd;
            string senderName = vna.emailElement.SenderName;
            string sfrommail = vna.emailElement.SenderAddr;

            mailHelper.MailName = senderName;// "VNA Administrator";
            mailHelper.MailPassword = sloginPass;// "t#s%b7";
            mailHelper.MailServer = smtpHost;// "ebmtech.com";
            mailHelper.MailUserName = sloginId;// "sbfh";
           
            //
            StringBuilder contentBuilder = new StringBuilder();
            contentBuilder.Append("Please click the link to ");
            string surl = Url.Action("Activate", "Register");//http://localhost:53414/Register/Activate?regid=
            //surl = Configs.GetValue("SitePrefix") + surl;
            surl = vna.SiteElement.Home + surl;
            if(!surl.ToLower().Contains("http"))
            {
                surl = "http://" + surl;
            }
           // Url.RequestContext.HttpContext.Request.Url.Scheme;
            contentBuilder.Append("<a href='"+surl + "?regid=" + regid + "'>activate</a> your VNA Account.");
            string sbody = contentBuilder.ToString();

            string stitle = "VNA account activate";
            int port = 25;
            //
            mailHelper.SendByThread(smail, stitle,sfrommail, sbody, port);
        }
    }
}