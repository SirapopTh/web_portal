﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Common;
using DataModel;
using BaseData;
using System.Text;
using System.Data.Common;
using System.Data.SqlClient;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.ComponentModel.DataAnnotations;
using System.Net.Mail;
using System.Speech.Synthesis;
using System.Threading.Tasks;
using System.Web.Configuration;

namespace VNA.Controllers
{
    //mvc 知识点 https://www.cnblogs.com/DotNetStu/p/7412404.html
    public class UserController : Controller // ControllerBase// controllerBase有自动检查是否已登录过...
    {
        public Log FileLog
        {
            get { return LogFactory.GetLogger(this.GetType().ToString()); }
        }
        // private IUserRegRepository userRegRepository = new UserRegRepository();
        private Common.MailHelper mailHelper = new Common.MailHelper();
        // GET: User
        public  ActionResult Index()//override
        {
            return View();
        }
        public ActionResult Login(string user_id)
        {
            if (!string.IsNullOrEmpty(user_id))
                ViewBag.loginUser = user_id;
            //System.Web.Routing.RouteData routeData = Request.RequestContext.RouteData;
            string sreturnurl = Request.QueryString["ReturnUrl"];
            ViewBag.ReturnUrl = sreturnurl;

            string sUseLdap =  Configs.GetValue("UseLdap");//"0";//
            ViewBag.UseLdap = sUseLdap;

            return View();
        }
        [HttpGet]
        public ActionResult GetAuthCode()
        {
            return File(new VerifyCode().GetVerifyCode(), @"image/Gif");
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult> GetVerifySound()
        {
            Task<FileContentResult> task = Task.Run(() =>
            {
                string svercode = Session["verifycodeOrg"] as string;
                if(!string.IsNullOrEmpty(svercode))
                {
                    StringBuilder stemp = new StringBuilder();
                    for(int ii =0;ii < svercode.Length;++ii)
                    {
                        
                        stemp.Append(svercode[ii]);
                        stemp.Append(' ');//
                    }
                    svercode = stemp.ToString();
                }
                using (SpeechSynthesizer speechSynthesizer = new SpeechSynthesizer())
                {
                  
                    //foreach (InstalledVoice voice1 in speechSynthesizer.GetInstalledVoices())
                    //{
                    //    VoiceInfo info = voice1.VoiceInfo;
                    //    Console.WriteLine(" Voice Name: " + info.Name);
                    //}
                    var speakers = speechSynthesizer.GetInstalledVoices();
               
                    var voice = speakers.First(x => x.VoiceInfo.Gender == VoiceGender.Female && x.VoiceInfo.Culture.Name.Contains("en"));
                    speechSynthesizer.SelectVoice(voice.VoiceInfo.Name);

                    // s.Volume = 60;//音量
                    speechSynthesizer.Rate = -2;//语速
                    //s.SetOutputToDefaultAudioDevice();
                    //s.SetOutputToWaveFile(fullFileName);
                    using (MemoryStream stream = new MemoryStream())
                    {
                       
                        speechSynthesizer.SetOutputToWaveStream(stream);
                        speechSynthesizer.Speak(svercode);
                        var bytes = stream.GetBuffer();
                        return File(bytes, "audio/x-wav");
                    }
                }
            });
            
            return await task;
        }
        /// <summary>
        /// Convert text to audio.. Text to Speech
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        byte[] SpeakText(string text)
        {
            try
            {
                var task = Task.Run(() =>
                {
       
                    using (SpeechSynthesizer s = new SpeechSynthesizer())
                    {
                        // s.Volume = 60;//Speed ​​of speech
                        // s.Rate = -2;//volume
                        //s.SetOutputToDefaultAudioDevice();
                        //s.SetOutputToWaveFile(fullFileName);//"D:\\Record.wav"
                        using (MemoryStream ms = new MemoryStream())
                        {
                            s.SetOutputToWaveStream(ms);
                            s.Speak(text);

                            return ms.GetBuffer();
                        }
                    }
                });
                Task.WaitAll(task);
            }
            catch (AggregateException ae)
            {
                var sb = new StringBuilder();
                foreach (var item in ae.InnerExceptions)
                {
                    var t = item.GetType();
                    sb.AppendLine(t.Namespace + "." + t.Name + " " + item.Message);
                }

                FileLog.Error(sb);
            }
            return null;
        }
        public ActionResult Logout()
        {
            //
            OperatorModel operatorModel = OperatorProvider.Provider.GetCurrent();
            if (operatorModel != null)
            {
                VnaDbContext vnaDbContext = new VnaDbContext();
                ILogEntityRepository logRepo = new LogEntityRepository(vnaDbContext);
                logRepo.BeginTrans();
                LogEntity logEntity = new LogEntity
                {
                    module_name = "System",
                    log_type = DbLogType.Exit.ToString(),
                    account =operatorModel.UserId, // 
                    user_name = operatorModel.UserName,
                    result = true,
                    description = "safety exit..",
                    //idno = Common.Common.GuId();
                };

                logRepo.InsertLog(logEntity);

                logRepo.Commit();
            }
           // new LogApp().WriteDbLog(logEntity);

            Session.Abandon();
            Session.Clear();
            OperatorProvider.Provider.RemoveCurrent();

            return RedirectToAction("Login", "User");
        }
        [HttpPost]
        public ActionResult CheckLogin(string user_email, string password,string code)
        {
            LogEntity logEntity = new LogEntity();
            logEntity.module_name = "Login";// "system login";
            logEntity.log_type = BaseData.DbLogType.Login.ToString();

            BaseData.VnaDbContext vnaDbContext = new BaseData.VnaDbContext();

            try
            {
                bool IsCaptcha = WebConfigurationManager.AppSettings["Captcha"].ToString() == "1" ? true : false;

                if (IsCaptcha)
                {
                        string svnacode = Session["vna_session_verifycode"] as string;
                        if (string.IsNullOrEmpty(svnacode))
                        {
                            HttpCookie httpCookie = Request.Cookies["vna_session_verifycode"];//Session["vna_session_verifycode"].ToString()
                            if (httpCookie != null)
                                svnacode = httpCookie.Value;
                        }
                        if (svnacode.IsEmpty() || Md5.md5(code.ToLower(), 16) != svnacode)
                        {
                            return Content(new AjaxResult { state = ResultType.error, message = "verify code error" }.ToJson());
                        }
                }
                

                string slogpass = password.ToLower();

                bool isLdap = false;
                string sUseLdap = Configs.GetValue("UseLdap");
                IUserRepository userRepository = new UserRepository(vnaDbContext);

                Users user = null; //If the login account is email, check the email field, otherwise check the user_id, such as from AD ...
                if (user_email.Contains("@"))
                    user = userRepository.GetUser(user_email);
                else
                    user = userRepository.FindEntity<Users>(t => t.user_id.Equals(user_email, StringComparison.OrdinalIgnoreCase));

                ILogEntityRepository logRepo = new LogEntityRepository(vnaDbContext);
                if (user == null)
                {
                    return Content(new AjaxResult { state = ResultType.error, message = "Login failed,user not found." }.ToJson());
                }
                //
                int isource = user.source.GetValueOrDefault();

                    if(isource == 1 && sUseLdap == "1")//
                    {
                        VNAConfigSection vna = VNAConfigSection.Instance;

                    //The password here is in clear text
                        string sdomainurl = vna.LdapElement.LDAPUrl;
                        string sdomain = vna.LdapElement.Domain;
                        bool bl = Common.LdapHelper.validateUserByBind(user_email,sdomain, password,sdomainurl);
                        if (bl)
                            isLdap = true;
                        else
                            user = null; //AD verification fails...
                    }

                if (user != null  && !user.delete_mark && user.is_verified)///
                {
                    string dbPassword = user.user_password.Trim().ToLower();// Md5.md5(user.user_password.Trim(),32).ToLower();//32 DESEncrypt.Encrypt(user.user_password), 32);
                    Session["style"] = user.user_theme == true? "1" : null;
                    if (!isLdap && sUseLdap=="1")
                    {
                        slogpass = Md5.Md5Hash(password).ToLower();
                    }
                    if (!isLdap && !dbPassword.Equals( slogpass))
                    {
                        logEntity.account = user_email;
                        logEntity.result = false;
                        logEntity.description = "login failed " ;
                        ILogEntityRepository logRepo0 = new LogEntityRepository(vnaDbContext);
                        logRepo0.BeginTrans();
                        logRepo0.InsertLog(logEntity);

                        logRepo0.Commit();

                        return Content(new AjaxResult { state = ResultType.error, message = "password is wrong" }.ToJson());
                    }

                    OperatorModel operatorModel = new OperatorModel();
                    operatorModel.UserId = user.user_id;
                    operatorModel.UserCode = user.user_id_id.ToString();// user_id;
                    operatorModel.UserName = user.user_name;
                    
                    operatorModel.DepartmentId = user.department_id;
                    operatorModel.RoleId = user.role_id;
                    operatorModel.LoginIPAddress = Net.Ip;
                    operatorModel.UserTheme = user.user_theme;
                    //20200413 No address
                    operatorModel.LoginIPAddressName =  string.Empty;// Net.GetLocation(operatorModel.LoginIPAddress);
                    operatorModel.LoginTime = DateTime.Now;
                    operatorModel.LoginToken = DESEncrypt.Encrypt(Guid.NewGuid().ToString());
                    operatorModel.Authority = user.user_authority.ToString();
                    operatorModel.headImageFile = user.header_icon;

                    operatorModel.IsLDAP = isLdap;

                    if (user.user_id == "admin")
                    {
                        operatorModel.IsSystem = true;
                    }
                    else
                    {
                        operatorModel.IsSystem = false;
                    }

                    OperatorProvider.Provider.AddCurrent(operatorModel);
                    logEntity.account = user.user_id;
                    logEntity.user_name = user.user_name;
                    logEntity.result = true;
                    logEntity.description = "Login success";
                   // logEntity.idno = Common.Common.GuId();
                    // new LogApp().WriteDbLog(logEntity);
                    logRepo.BeginTrans();
                    logRepo.InsertLog(logEntity);

                    logRepo.Commit();

                    return Content(new AjaxResult { state = ResultType.success, message = "Login success" }.ToJson());//.ToString()
                }
                else if(user == null)
                    return Content(new AjaxResult { state = ResultType.error, message = "Login failed,user not found." }.ToJson());
                else if( !user.is_verified || user.delete_mark)
                    return Content(new AjaxResult { state = ResultType.error, message = "Login failed,please active or enable you account." }.ToJson());//.ToString()

            }
            catch (Exception ex)
            {
                logEntity.account = user_email;
               // logEntity.user_name = username;
                logEntity.result = false;
                logEntity.description = "login failed ，" + ex.Message;
               // logEntity.idno = Common.Common.GuId();
                // new LogApp().WriteDbLog(logEntity);
                ILogEntityRepository logRepo = new LogEntityRepository(vnaDbContext);
                logRepo.BeginTrans();
                logRepo.InsertLog(logEntity);

                logRepo.Commit();

                return Content(new AjaxResult { state = ResultType.error, message = ex.Message }.ToJson());//.ToString()
            }

            return Content(new AjaxResult { state = ResultType.error, message ="error" }.ToJson());

            // return Json(new { state = "0", message = "success." });

        }
        //Coming from HanderLogin...
        public ActionResult ChangePassword()
        {
            //Convenient to return when the user modifies the information...
            string sreturnurl = Request.QueryString["ReturnUrl"];
            ViewBag.ReturnUrl = sreturnurl;

            string suserid = Request.QueryString["userid"];
            ViewBag.user_email = suserid;


            return View();
        }
        public ActionResult ForgetPassword()
        {
            return View();
        }
        // Send password reset email
        
        [HttpPost]
        public ActionResult _SendResetPassword(string user_email)
        {
            var obj1 = new { state = "0", message = "false." };

            VnaDbContext vnaDbContext = new VnaDbContext();

            IUserRepository usr = new UserRepository(vnaDbContext);
            Users user = usr.GetUser(user_email);
            if (user == null || user.delete_mark)
            {
                return Json(new { state = "0", message = "email not found" });
            }
            //
            //insert forgetpassword.
            ForgetPasswd forgetPasswd = new ForgetPasswd();
            forgetPasswd.req_id = Common.Common.GuId();
            forgetPasswd.req_date = DateTime.Now;
            forgetPasswd.is_open = false;
            forgetPasswd.user_email = user_email;
            //
            IForgetPswRepository forgetPswRepository = new ForgetPasswdRepository(vnaDbContext);
            //
            forgetPswRepository.BeginTrans();

            forgetPswRepository.Insert(forgetPasswd);

            forgetPswRepository.Commit();

            //send mail..
            SendResetPaswordMail(user_email, forgetPasswd.req_id);

            var obj2 = new { state = "1", message = "Please receive email to reset password." };
           
            return Json(obj2);// Content();// obj1;
        }

        void SendResetPaswordMail(string user_email,string reqid)
        {
            VNAConfigSection vna = VNAConfigSection.Instance;
            string smtpHost = vna.emailElement.SmtpHost;
            string sloginId = vna.emailElement.SmtpLoginId;
            string sloginPass = vna.emailElement.SmtpLoginPasswd;
            string senderName = vna.emailElement.SenderName;
            string sfrommail = vna.emailElement.SenderAddr;

            mailHelper.MailName = senderName;// "VNA Administrator";
            mailHelper.MailPassword = sloginPass;// "t#s%b7";
            mailHelper.MailServer = smtpHost;// "ebmtech.com";
            mailHelper.MailUserName = sloginId;// "sbfh";
            //
            StringBuilder contentBuilder = new StringBuilder();
            contentBuilder.Append("Please click the link to ");
            string surl = Url.Action("ResetPassword", "User");//http://localhost:53414/User/ResetPassword?user_email=
            //surl =  Configs.GetValue("SitePrefix") + surl;
            surl = vna.SiteElement.Home + surl;
            if (!surl.ToLower().Contains("http"))
            {
                surl = "http://" + surl;
            }

            contentBuilder.Append("<a href='"+surl+ "?user_email=" + user_email+"&reqid="+reqid + "'>Reset</a> your VNA account password.");
            string sbody = contentBuilder.ToString();

            string stitle = "VNA password reset";
            int port = 25;
            //
            mailHelper.SendByThread(user_email, stitle,sfrommail, sbody, port);
        }

        // Password reset page
        public ActionResult ResetPassword(string user_email,string reqid)
        {
            ViewBag.user_email = user_email;
            ViewBag.reqid = reqid;

            return View();
        }
        [HttpPost]
        //Change password...
        public ActionResult SubmitChangePassword(string user_email, string password, string oldpass)
        {
            VnaDbContext vnaDbContext = new VnaDbContext();
            //
            UserRepository usr = new UserRepository(vnaDbContext);
            Users user = usr.GetUser(user_email);
            if(user==null || user.delete_mark )
                return Json(new { state = "0", message = "user is not existed." });

            string dbPassword = user.user_password.Trim().ToLower();
            if(!dbPassword.Equals(oldpass))
            {
                return Json(new { state = "0", message = "old password is wrong." });
            }
            //
            user.is_initial = true;
            user.user_password = password;
            //
            usr.UpdateUser(user);
            usr.Commit();
            //
            return Json(new { state = "1", message = "password is changed." });
        }

        [HttpPost]
        public ActionResult SubmitResetPassword(string user_email,string password,string reqid)
        {
            VnaDbContext vnaDbContext = new VnaDbContext();
            //

            IForgetPswRepository forgetPswRepository = new ForgetPasswdRepository(vnaDbContext);
            //
            ForgetPasswd fp = forgetPswRepository.GetForgetPsw(reqid);
            var obj1 = Json(new { state = "0", message = "false." });
            if (fp == null || fp.is_open)
            {
                return obj1;
            }
            //
            // check user..
            IUserRepository usr = new UserRepository(vnaDbContext);
            Users user = usr.GetUser(user_email);
            if (user == null || user.delete_mark)
                return obj1;
            user.user_password = password;

            usr.UpdateUser(user);
            //
            usr.Commit();
            //
            fp.is_open = true;
            fp.open_date = DateTime.Now;
            //
            forgetPswRepository.UpdateForgetPsw(fp);
            //
            forgetPswRepository.Commit();

            return Json(new { state = "1", message = "The password is reset." });
        }
        /*
                public ActionResult SignUp()
                {
                    return View();

                }

                [HttpPost]
                [HandlerAjaxOnly]
                [ValidateAntiForgeryToken]
                public ActionResult CreateUser(string user_email,string password)
                {
                    UserReg userReg = new UserReg();
                    userReg.user_email = user_email;
                    userReg.regid = Common.Common.GuId();
                    userReg.is_verified = false;
                    userReg.reg_date = DateTime.Now;

                    bool bf = userRegRepository.CreateUserReg(userReg);

                    if (bf)
                        return Success("success");

                    return Error("create User error");
                }
                [HttpPost]
                [HandlerAjaxOnly]
                [ValidateAntiForgeryToken]
                public ActionResult CreateUser(UserReg userReg)
                {
                    bool bf = userRegRepository.CreateUserReg(userReg);

                    if (bf)
                        return Success("success");

                    return Error("create User error");
                }
                */


        [HandlerLogin]
        public ActionResult EditGroupFunc()
        {
            OperatorModel operatorProvider = OperatorProvider.Provider.GetCurrent();

            VnaDbContext vnaDbContext = new VnaDbContext();
            IRoleRepository roleRepository = new RoleRepository(vnaDbContext);
            List<DataModel.Role> roles = roleRepository.GetRoleList();
            if(roles != null)
            {
                foreach(DataModel.Role role in roles)
                {
                    if (role.role_id.Trim() == ((int)BaseData.Role.Patient).ToString())//"0"
                    {
                        ViewBag.r1 = role;
                        //FileLog.Info(role.ToJson());
                    }
                    else if (role.role_id.Trim() == ((int)BaseData.Role.Doctor).ToString())//1
                    {
                        ViewBag.r2 = role;
                        //FileLog.Info(role.ToJson());
                    }
                    else if (role.role_id.Trim() == ((int)BaseData.Role.Nurse).ToString())
                    {
                        ViewBag.r3 = role;
                        //FileLog.Info(role.ToJson());
                    }
                    else if (role.role_id.Trim() == ((int)BaseData.Role.Manager).ToString())
                    {
                        ViewBag.r4 = role;

                        //FileLog.Info(role.ToJson());
                    }
                }
            }

       
            string sAutho = operatorProvider.Authority;

            bool bUploadRight = RoleActor.HasAuthorityRights(sAutho, AuthorityRights.EditGroup);
            if (!bUploadRight)
            {
                //
                //FileLog.Info(sAutho + operatorProvider.RoleId);
                if (sAutho == "0" || string.IsNullOrEmpty(sAutho))
                {
                    //IRoleRepository roleRepository = new RoleRepository(vnaDbContext);
                    DataModel.Role role = roleRepository.GetRole(operatorProvider.RoleId);
                    //
                    if (role != null)
                    {
                        
                            bUploadRight = role.editgroup_enable;
                    }
                    else
                    {
                        //If not ... additionally restricted admin permissions ...
                        BaseData.Role rolea = BaseData.RoleActor.GetRole(operatorProvider.RoleId);
                        if (rolea == BaseData.Role.Super || rolea == BaseData.Role.Admin)
                            bUploadRight = true;
                        //
                       // FileLog.Info("bUploadRight" + bUploadRight.ToString() + operatorProvider.RoleId);
                    }

                }
                else
                {

                }
                if (!bUploadRight)
                    return Content("You have no permission.");
            }


            return View();
        }
        [HttpPost]
        public ActionResult SubmitGroupFunc(int type,string uploadFiles,string viewFiles,string sendFiles,string monitorFiles,
            string sendDown,string Download,string Downpacs)
        {
            VnaDbContext vnaDbContext = new VnaDbContext();
            IRoleRepository roleRepository = new RoleRepository(vnaDbContext);

            DataModel.Role role = roleRepository.GetRole(type.ToString());
            bool bnew = false;
            if (role == null)
            {
                role = new DataModel.Role();
                bnew = true;
                role.role_id = type.ToString();
            }
            role.upload_enable = (uploadFiles.ToLower() == "true"? true:false);
            role.view_enable = (viewFiles.ToLower() == "true" ? true : false);
            role.send_enable = (sendFiles.ToLower() == "true" ? true : false);
            role.monitor_enable = (monitorFiles.ToLower() == "true" ? true : false);
            role.senddown_enable = (sendDown.ToLower() == "true" ? true : false);
            role.download_enable = (Download.ToLower() == "true" ? true : false);
            role.downloadpacs_enable = (Downpacs.ToLower() == "true" ? true : false);
            //
            if (bnew)
                roleRepository.InsertRole(role);
            else
                roleRepository.UpdateRole(role);

            roleRepository.Commit();

            var obj = new { state = "1", message = "success." };

            return Json(obj);
        }

        [HandlerLogin]
        public ActionResult EditPersonalFunc(string para1,string para2)
        {
            VnaDbContext vnaDbContext = new VnaDbContext();
            IDepartmentRepository departmentRepository = new DepartmentRepository(vnaDbContext);
            //List<Department> departments = departmentRepository.FindList<Department>("select * from department");

            OperatorModel operatorProvider = OperatorProvider.Provider.GetCurrent();
            string sAutho = operatorProvider.Authority;

            bool bUploadRight = RoleActor.HasAuthorityRights(sAutho, AuthorityRights.EditPersonal);
            if (!bUploadRight)
            {
                //
                if (sAutho == "0" || string.IsNullOrEmpty(sAutho))
                {
                    IRoleRepository roleRepository = new RoleRepository(vnaDbContext);
                    DataModel.Role role = roleRepository.GetRole(operatorProvider.RoleId);
                    //
                    if (role != null)
                    {
                        
                       bUploadRight = role.editperson_enable;
                    }
                    else
                    {
                        // If not ... additionally restricted admin permissions ...
                        BaseData.Role rolea = BaseData.RoleActor.GetRole(operatorProvider.RoleId);

                        if (rolea == BaseData.Role.Super || rolea == BaseData.Role.Admin)
                            bUploadRight = true;
                       
                    }
                }
                if (!bUploadRight)
                    return Content("You have no permission.");
            }

            if (string.IsNullOrEmpty(para1))
            {
                List<Department> departments = departmentRepository.GetDepartmentList();

                ViewBag.departments = departments;
                if(!string.IsNullOrEmpty(para2))
                {
                    //This time is user_id_id
                                        //string[] uids = para2.Split(new char[] { ',' });
                                        //
                                        ViewBag.userid_ids = para2;// uids;
                }
                return View();
            }
            else if( Uri.UnescapeDataString(para1) == "1")
            {
                if (string.IsNullOrEmpty(para2))
                    return View();

                string suserid_id_id = Uri.UnescapeDataString(para2);
                //
                ViewBag.user_id_id = suserid_id_id;
                IUserRepository userRepository = new UserRepository(vnaDbContext);
                int iuserid_id = 0;
                int.TryParse(suserid_id_id, out iuserid_id);

                Users users = userRepository.FindEntity<Users>(t => t.user_id_id == iuserid_id);
                RoleRepository roleRepository = new RoleRepository(vnaDbContext);
                string sroleid = users.role_id;
                if (string.IsNullOrEmpty(sroleid))
                    sroleid = ((int)BaseData.Role.Patient).ToString();

                DataModel.Role role = roleRepository.GetRole(sroleid);
                int iAuthority = users.user_authority;
                if(role == null  )
                {
                    role = new DataModel.Role();
                    if(RoleActor.GetRole(users.role_id) == BaseData.Role.Super)
                    {
                        iAuthority = 0x7fffffff;
                    }
                    role.role_id = users.role_id;
                    
                    role.upload_enable = RoleActor.HasAuthorityRights(iAuthority.ToString(), AuthorityRights.Upload);
                    role.view_enable = RoleActor.HasAuthorityRights(iAuthority.ToString(), AuthorityRights.ViewFile);
                    role.send_enable = RoleActor.HasAuthorityRights(iAuthority.ToString(), AuthorityRights.SendUpload);
                    role.monitor_enable = RoleActor.HasAuthorityRights(iAuthority.ToString(), AuthorityRights.Monitor);
                    role.senddown_enable = RoleActor.HasAuthorityRights(iAuthority.ToString(), AuthorityRights.SendDownload);
                    role.download_enable = RoleActor.HasAuthorityRights(iAuthority.ToString(), AuthorityRights.DownloadFile);
                    role.downloadpacs_enable = RoleActor.HasAuthorityRights(iAuthority.ToString(), AuthorityRights.DownloadFromPacs);
                    //Does not work for admin ...
                    role.editgroup_enable = false;
                    role.editperson_enable = false;
                }
                else if(iAuthority != 0 )
                {
                    role.upload_enable = RoleActor.HasAuthorityRights(iAuthority.ToString(), AuthorityRights.Upload);
                    role.view_enable = RoleActor.HasAuthorityRights(iAuthority.ToString(), AuthorityRights.ViewFile);
                    role.send_enable = RoleActor.HasAuthorityRights(iAuthority.ToString(), AuthorityRights.SendUpload);
                    role.monitor_enable = RoleActor.HasAuthorityRights(iAuthority.ToString(), AuthorityRights.Monitor);
                    role.senddown_enable = RoleActor.HasAuthorityRights(iAuthority.ToString(), AuthorityRights.SendDownload);
                    role.download_enable = RoleActor.HasAuthorityRights(iAuthority.ToString(), AuthorityRights.DownloadFile);
                    role.downloadpacs_enable = RoleActor.HasAuthorityRights(iAuthority.ToString(), AuthorityRights.DownloadFromPacs);
                    //
                    role.editgroup_enable = false;
                    role.editperson_enable = false;
                }
                ViewBag.role = role;// (role != null ? role.role_name : "");
               

                Department department = departmentRepository.GetDepartment(users.department_id);
                ViewBag.deptname = (department != null ? department.department_name : "");

                List<Department> departments = departmentRepository.GetDepartmentList();
                ViewBag.deptlist = (departments == null ? (new List<Department>()):departments);

                IUserAuthorDeptRepository userAuthorDeptRepository = new UserAuthorDeptRepository(vnaDbContext);
                List<UserAuthorDept> userAuthorDepts = userAuthorDeptRepository.GetUserAuthorDept(iuserid_id);
                ViewBag.userAuthorDepts = userAuthorDepts;

                return View("EditPersonalFunc1",users);
            }
            return View("EditPersonalFunc2");
        }
        [HttpPost]
        public ActionResult SubmitUserAuthority(string user_id_id,bool upload,bool view,bool send,bool monitor,
            bool senddown,bool download,bool downpacs)
        {
            //
            VnaDbContext vnaDbContext = new VnaDbContext();
            IUserRepository userRepository = new UserRepository(vnaDbContext);
            //
            int iuserid_id = 0;
            int.TryParse(user_id_id, out iuserid_id);

            Users users = userRepository.FindEntity<Users>(t => t.user_id_id == iuserid_id);
            if(users == null)
            {
                var obj = new { state = "0", message = "user is not existed." };
                return Json(obj);
            }
            int iautho = 0;
            if (upload)
                iautho |= (int)AuthorityRights.Upload;// 1;
            if (view)
                iautho |= (int)AuthorityRights.ViewFile;// 2;
            if (send)
                iautho |= (int)AuthorityRights.SendUpload;// 4;
            if (monitor)
                iautho |= (int)AuthorityRights.Monitor;// 8;
            if (senddown)
                iautho |= (int)AuthorityRights.SendDownload;// 16;
            if (download)
                iautho |= (int)AuthorityRights.DownloadFile;// 
            if (downpacs)
                iautho |= (int)AuthorityRights.DownloadFromPacs;// 

            users.user_authority = iautho;
            //
            userRepository.UpdateUser(users);
            userRepository.Commit();
            //
            OperatorModel operatorProvider = OperatorProvider.Provider.GetCurrent();
            if (operatorProvider != null && operatorProvider.UserCode == user_id_id)
            {
                operatorProvider.Authority = iautho.ToString();
                OperatorProvider.Provider.AddCurrent(operatorProvider);
            }
            var obj1 = new { state = "1", message = "success." };
            return Json(obj1);
        }
        [HttpPost]
        public ActionResult SubmitUserAuthorDept(string user_id_id, string[] deptids)
        {
           
            //..............
            VnaDbContext vnaDbContext = new VnaDbContext();
            IUserRepository userRepository = new UserRepository(vnaDbContext);

            int iuser_id_id = 0;
            int.TryParse(user_id_id, out iuser_id_id);
            Users users = userRepository.FindEntity<Users>(t => t.user_id_id == iuser_id_id);
            if (users == null || users.delete_mark)
                return Json(new { state = "0", message = "user not validl" });
            //...............//
            //insert.. UserAuthorDepartment..(user_id_id,author_dept_id)
            IUserAuthorDeptRepository userAuthorDeptRepository = new UserAuthorDeptRepository(vnaDbContext);
            userAuthorDeptRepository.DeleteUserAuthorDept(iuser_id_id);
            if(deptids != null )
            {
                for(int ii = 0; ii < deptids.Length;++ii)
                {
                    int dept_id_id = 0;
                    int.TryParse(deptids[ii], out dept_id_id);
                    //
                    UserAuthorDept userAuthorDept = new UserAuthorDept();
                    userAuthorDept.user_id_id = iuser_id_id;
                    userAuthorDept.dept_id_id = dept_id_id;

                    userAuthorDeptRepository.InsertUserAuthorDept(userAuthorDept);
                }
                //
                userAuthorDeptRepository.Commit();
            }
            var obj1 = new { state = "1", message = "success." };
            return Json(obj1);
        }
        [HttpPost]
        public ActionResult GetUsers(string userid_ids, int pageLimit, int pageIndex, string sortBy, string order)
        {
            var obj = new { state = "1", message = "success." };

            VnaDbContext vnaDbContext = new VnaDbContext();
            IUserRepository userRepository = new UserRepository(vnaDbContext);
            //

            Pagination pagination = new Pagination();
            pagination.rows = pageLimit;
            if (pageIndex <= 0)
                pageIndex = 1;
            pagination.page = pageIndex;

            if (string.IsNullOrEmpty(order))
                order = "asc";

            if (string.IsNullOrEmpty(sortBy) || sortBy == "1")
            {
                sortBy = "user_name";
                //order = "asc";
            }
            else if (sortBy == "2")
                sortBy = "id_number";
            else if (sortBy == "3")
                sortBy = ""; //medical_recno

            pagination.sord = order;// "asc";
            pagination.sidx = sortBy;// "";

            bool bf = false;
            string stxt = "select * from users ";


            StringBuilder sql = new StringBuilder(stxt);// 
            List<DbParameter> dbParameters = new List<DbParameter>();

            if (userid_ids!=null && userid_ids.Length >0)
            {
                string[] stpuserids = userid_ids.Split(new char[] { ',' });
                if (!bf)
                    sql.Append(" where user_id_id in (");
                
                for(int ii=0; ii < stpuserids.Length;++ii)
                {
                    if (ii > 0)
                        sql.Append(",");
                    string s = $"{stpuserids[ii]}";
                    sql.Append(s);
                }
                sql.Append(")");

                bf = true;
            }
            else
            {
                var obje0 = new { state = "0", message = "No Condition." };
                //
                return Json(obje0);
            }

            IDepartmentRepository departmentRepository = new DepartmentRepository(vnaDbContext);
            List<Department> departments = departmentRepository.GetDepartmentList();//.FindList<Department>("select * from department");

            List<Users> userRegs = userRepository.FindList<Users>(sql.ToString(), dbParameters.ToArray(), pagination);
            if (userRegs != null && departments != null)
            {
                //In order to return the department name ...
                foreach (Users user in userRegs)
                {
                    //Handle a role
                    string group = string.Empty;
                    BaseData.Role rl = RoleActor.GetRole(user.role_id);
                    if (rl == BaseData.Role.Patient)
                        group = "Patient";
                    else if (rl == BaseData.Role.Doctor)
                        group = "Physician";
                    else if (rl == BaseData.Role.Nurse)
                        group = "Nurse";
                    else if (rl == BaseData.Role.Manager)
                        group = "Manager";
                    user.role_id = group;

                    string sdeptid = user.department_id;
                    if (string.IsNullOrEmpty(sdeptid))
                        continue;

                    Department ldept = departments.Find(t => t.department_id == user.department_id);
                    if (ldept != null)
                        user.department_id = user.department_id + "$" + ldept.department_name;

                }
            }

            if (userRegs != null)
            {
                var uploadret = new { pageCount = pagination.total, datas = userRegs };
                var objs = new { state = "1", message = "success.", data = uploadret };

                return Json(objs);
            }

            var obje = new { state = "0", message = "Error." };
            //
            return Json(obje);
        }
        [HttpPost]
        public ActionResult QueryUser(string group, string appdate, string name, string nativeid, string dept, string newapp,
             int pageLimit, int pageIndex, string sortBy, string order)
        {
            var obj = new { state = "1", message = "success." };

            VnaDbContext vnaDbContext = new VnaDbContext();
            IUserRepository userRepository = new UserRepository(vnaDbContext);
            //

            Pagination pagination = new Pagination();
            pagination.rows = pageLimit;
            if (pageIndex <= 0)
                pageIndex = 1;
            pagination.page = pageIndex;

            if (string.IsNullOrEmpty(order))
                order = "asc";

            if (string.IsNullOrEmpty(sortBy) || sortBy == "1")
            {
                sortBy = "user_name";
                //order = "asc";
            }
            else if (sortBy == "2")
                sortBy = "id_number";
            else if (sortBy == "3")
                sortBy = "create_date"; // medical_recno

            pagination.sord = order;// "asc";
            pagination.sidx = sortBy;// "";

            //
            bool bf = false;
            string stxt = "select * from users ";


            StringBuilder sql = new StringBuilder(stxt);// 
            List<DbParameter> dbParameters = new List<DbParameter>();
            /*if (!string.IsNullOrEmpty(medicno))
            {
                if (!bf)
                    sql.Append(" where ");

                sql.Append("medical_recno= @medical_recno");

                SqlParameter sqlParameter = new SqlParameter("medical_recno", medicno);
                dbParameters.Add(sqlParameter);
                bf = true;
            }*/
            if (!string.IsNullOrEmpty(nativeid))
            {
                if (!bf)
                    sql.Append(" where ");
                if (bf)
                    sql.Append(" and ");

                sql.Append("id_number=@id_number");
                SqlParameter sqlParameter = new SqlParameter("id_number", nativeid);
                dbParameters.Add(sqlParameter);
                bf = true;
            }
            if (!string.IsNullOrEmpty(name))
            {
                if (!bf)
                    sql.Append(" where ");
                if (bf)
                    sql.Append(" and ");

                sql.Append("user_name like @user_name");
                SqlParameter sqlParameter = new SqlParameter("user_name", "%" + name + "%");
                dbParameters.Add(sqlParameter);
                bf = true;
            }
            if (!string.IsNullOrEmpty(appdate))
            {
                if (!bf)
                    sql.Append(" where ");
                if (bf)
                    sql.Append(" and ");

                sql.Append("CONVERT(varchar(100), create_date, 112)=@createdate");//)yyyymmdd的格式
                SqlParameter sqlParameter = new SqlParameter("createdate", appdate);
                dbParameters.Add(sqlParameter);
                bf = true;
            }
            if (!string.IsNullOrEmpty(dept))
            {
                if (!bf)
                    sql.Append(" where ");
                if (bf)
                    sql.Append(" and ");

                sql.Append("department_id =@department_id");
                SqlParameter sqlParameter = new SqlParameter("department_id", dept);
                dbParameters.Add(sqlParameter);
                bf = true;
            }
            if (!string.IsNullOrEmpty(group))
            {
                if (!bf)
                    sql.Append(" where ");
                if (bf)
                    sql.Append(" and ");

                BaseData.Role lrole = BaseData.Role.Patient;
                if (group == "P")
                    lrole = BaseData.Role.Patient;
                else if (group == "Y")
                    lrole = BaseData.Role.Doctor;
                else if (group == "N")
                    lrole = BaseData.Role.Nurse;
                else if (group == "M")
                    lrole = BaseData.Role.Manager;
                if (lrole == BaseData.Role.Patient)
                {
                    sql.Append(" ( role_id =@role_id or role_id is NULL)");
                }
                else
                    sql.Append(" role_id =@role_id ");
                SqlParameter sqlParameter = new SqlParameter("role_id", ((int)lrole).ToString());
                dbParameters.Add(sqlParameter);
                //
               

                bf = true;
            }
            //
            IDepartmentRepository departmentRepository = new DepartmentRepository(vnaDbContext);
            List<Department> departments = departmentRepository.GetDepartmentList();//.FindList<Department>("select * from department");

            List<Users> userRegs = userRepository.FindList<Users>(sql.ToString(), dbParameters.ToArray(), pagination);
            if (userRegs != null && departments != null)
            {
                //为了返回department name...
                foreach (Users user in userRegs)
                {
                    string gp = string.Empty;
                    BaseData.Role rl = RoleActor.GetRole(user.role_id);
                    if (rl == BaseData.Role.Patient)
                        gp = "Patient";
                    else if (rl == BaseData.Role.Doctor)
                        gp = "Physician";
                    else if (rl == BaseData.Role.Nurse)
                        gp = "Nurse";
                    else if (rl == BaseData.Role.Manager)
                        gp = "Manager";
                    user.role_id = gp;

                    string sdeptid = user.department_id;
                    if (string.IsNullOrEmpty(sdeptid))
                        continue;

                    Department ldept = departments.Find(t => t.department_id == user.department_id);
                    if (ldept != null)
                        user.department_id = user.department_id + "$" + ldept.department_name;

                }
            }

            if (userRegs != null)
            {
                var uploadret = new { pageCount = pagination.total, datas = userRegs };
                var objs = new { state = "1", message = "success.",data=uploadret };

                return Json(objs);
            }

            var obje = new { state = "0", message = "Error." };
            //
            return Json(obje);
        }
        [HandlerLogin]
        public ActionResult CreateIndividualAcount(string fromad)
        {
            OperatorModel operatorProvider = OperatorProvider.Provider.GetCurrent();
            BaseData.Role rolea = BaseData.RoleActor.GetRole(operatorProvider.RoleId);
            if (operatorProvider!=null &&( rolea == BaseData.Role.Super || rolea == BaseData.Role.Admin))
            {
                if (fromad=="1" &&Session["LDAP"] != null && Session["AdUser"] != null && Session["AdUserPass"] != null)
                {
                    //此时转向create account from AD

                    ViewBag.MultiSelect = "0";
                    ViewBag.vnagroup = Session["VnaGroup"] as string;

                    return View("UserFromAD");
                }

                VnaDbContext vnaDbContext = new VnaDbContext();
                IDepartmentRepository ideptrep = new DepartmentRepository(vnaDbContext);
                List<Department> departments = ideptrep.GetDepartmentList();

                ViewBag.departments = departments;

           
                return View();
            }
            return Content("You have no permission.");
           
        }
        [HandlerLogin]
        public ActionResult CreateAcountFromAD(string s)
        {
            OperatorModel operatorProvider = OperatorProvider.Provider.GetCurrent();
            BaseData.Role rolea = BaseData.RoleActor.GetRole(operatorProvider.RoleId);
            if (operatorProvider != null && (rolea == BaseData.Role.Super || rolea == BaseData.Role.Admin))
            {
                ViewBag.MultiSelect = s;
                ViewBag.vnagroup = Session["VnaGroup"] as string;

                return View("UserFromAD");
            }
            return Content("You have no permission.");
        }
        /// <summary>
        /// 生成随机的几位字符
        /// </summary>
        /// <returns></returns>
        string randPassword(int codeLen)
        {
            //
            char[] character = { '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'd', 'e', 'f', 'h', 'k', 'm', 'n', 'r', 'x', 'y', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'J', 'K', 'L', 'M', 'N', 'P', 'R', 'S', 'T', 'W', 'X', 'Y' };
            Random rnd = new Random();
            string chkCode = string.Empty;

            //生成验证码字符串 
            for (int i = 0; i < codeLen; i++)
            {
                chkCode += character[rnd.Next(character.Length)];
            }

            return chkCode;
        }
        [HttpPost]
        public ActionResult SubmitCreateAccount(string user_id,string user_name,string id_number,string medical_recno,string gender,
            string birthday,string email, string mobile_phone,string address,string department_id,string role_id)
        {
            //
            VnaDbContext vnaDbContext = new VnaDbContext();
            IUserRepository userRepository = new UserRepository(vnaDbContext);
            Users users = userRepository.GetUser(user_id);
            if (users != null)
            {
                var obj = new { state = "0", message = "User: " + user_id +" is existed" };
                return Json(obj);
            }
            users = userRepository.FindEntity<Users>(t => t.id_number == id_number);
            if (users != null)
            {
                var obj = new { state = "0", message = "ID NO: " + id_number + " is existed" };
                return Json(obj);
            }
            BaseData.Role lrole = BaseData.Role.Patient;
            if (role_id == "P" || string.IsNullOrEmpty(role_id))
                lrole = BaseData.Role.Patient;
            else if (role_id == "Y")
                lrole = BaseData.Role.Doctor;
            else if (role_id == "N")
                lrole = BaseData.Role.Nurse;
            else if (role_id == "M")
                lrole = BaseData.Role.Manager;

            int irole = (int)lrole;

            users = new Users();
            string siniPass = randPassword(8);
            users.user_password = Md5.Md5Hash(siniPass);// default passs "111111"
            users.user_id = user_id;
            users.user_name = !string.IsNullOrEmpty(user_name)?user_name.Trim():string.Empty;
            users.birthday = birthday;
            users.address = address;
            users.department_id = department_id;
            users.email = email;
            users.gender = gender;
            users.id_number = id_number;
            users.medical_recno = medical_recno;
            users.mobile_phone = mobile_phone;
            users.role_id = irole.ToString();
            users.is_verified = true; //patch...
            users.is_initial = false;
            users.create_date = DateTime.Now;

            userRepository.Insert(users);

            userRepository.Commit();

            SendUserAccountCreatedMail(email, siniPass);

            var obj1 = new { state = "1", message = "Success." };
            //
            return Json(obj1);
        }
        /// <summary>
        /// After the account is created...send an email, tell the user name and password, and reset the password for the first login
        /// </summary>
        /// <param name="user_email"></param>
        /// <param name="inipass"></param>
        void SendUserAccountCreatedMail(string user_email,string inipass)
        {
            VNAConfigSection vna = VNAConfigSection.Instance;
            string smtpHost = vna.emailElement.SmtpHost;
            string sloginId = vna.emailElement.SmtpLoginId;
            string sloginPass = vna.emailElement.SmtpLoginPasswd;
            string senderName = vna.emailElement.SenderName;
            string sfrommail = vna.emailElement.SenderAddr;

            mailHelper.MailName = senderName;// "VNA Administrator";
            mailHelper.MailPassword = sloginPass;// "t#s%b7";
            mailHelper.MailServer = smtpHost;// "ebmtech.com";
            mailHelper.MailUserName = sloginId;// "sbfh";
            //
            StringBuilder contentBuilder = new StringBuilder();
            contentBuilder.Append("Welcome to the EBM--eDC \r\n\r\n <br /> <br />");
            contentBuilder.Append("An account has been created for you by System Admin.\r\n <br />");
            contentBuilder.Append("Your account has been created with the following information:\r\n <br />");
            contentBuilder.Append($"Account ID: {user_email}\r\n <br />");
            contentBuilder.Append($"Password: {inipass}\r\n\r\n <br />");

            contentBuilder.Append($"To verify your account, please click on the follow link: ");
            string surl = Url.Action("Login", "User");//
          
            surl = vna.SiteElement.Home + surl;
            if (!surl.ToLower().Contains("http"))
            {
                surl = "http://" + surl;
            }

            contentBuilder.Append("<a href='" + surl + "'>Activate Account</a> <br />");
            string sbody = contentBuilder.ToString();

            string stitle = "VNA user account created";
            int port = 25;
            //
            mailHelper.SendByThread(user_email, stitle, sfrommail, sbody, port);
        }
        /// <summary>
        /// Used when the user account is not the same as email, such as from ldap
        /// </summary>
        /// <param name="useraccount"></param>
        /// <param name="user_email"></param>
        /// <param name="inipass"></param>
        void SendUserAccountCreatedMail(string useraccount, string user_email, string inipass)
        {
            VNAConfigSection vna = VNAConfigSection.Instance;
            string smtpHost = vna.emailElement.SmtpHost;
            string sloginId = vna.emailElement.SmtpLoginId;
            string sloginPass = vna.emailElement.SmtpLoginPasswd;
            string senderName = vna.emailElement.SenderName;
            string sfrommail = vna.emailElement.SenderAddr;

            mailHelper.MailName = senderName;// "VNA Administrator";
            mailHelper.MailPassword = sloginPass;// "t#s%b7";
            mailHelper.MailServer = smtpHost;// "ebmtech.com";
            mailHelper.MailUserName = sloginId;// "sbfh";
            //
            StringBuilder contentBuilder = new StringBuilder();
            contentBuilder.Append("Welcome to the EBM--eDC \r\n\r\n <br /> <br />");
            contentBuilder.Append("An account has been created for you by System Admin.\r\n <br />");
            contentBuilder.Append("Your account has been created with the following information:\r\n <br />");
            contentBuilder.Append($"Account ID: {useraccount}\r\n <br />");
            contentBuilder.Append($"Password: {inipass}\r\n\r\n <br />");

            contentBuilder.Append($"To verify your account, please click on the follow link: ");
            string surl = Url.Action("Login", "User");//

            surl = vna.SiteElement.Home + surl;
            if (!surl.ToLower().Contains("http"))
            {
                surl = "http://" + surl;
            }

            contentBuilder.Append("<a href='" + surl + "'>Activate Account</a> <br />");
            string sbody = contentBuilder.ToString();

            string stitle = "VNA user account created";
            int port = 25;
            //
            mailHelper.SendByThread(user_email, stitle, sfrommail, sbody, port);
        }
        [HandlerLogin]
        public ActionResult CreateGroupAcount(string fromad)
        {
            
            OperatorModel operatorProvider = OperatorProvider.Provider.GetCurrent();
            BaseData.Role rolea = BaseData.RoleActor.GetRole(operatorProvider.RoleId);
            if (operatorProvider != null && (rolea == BaseData.Role.Super || rolea == BaseData.Role.Admin))
            {
                if (fromad=="1" && Session["LDAP"] != null && Session["AdUser"] != null && Session["AdUserPass"] != null)
                {
                    //此时转向create account from AD
                    
                    //VNAConfigSection vna = VNAConfigSection.Instance;

                    //string sdomainurl = vna.LdapElement.LDAPUrl;
                    //string sdomain = vna.LdapElement.Domain;

                    //List<LDAPUser> lstUsers = LdapHelper.GetLDAPUsers(sdomain,sdomainurl, Session["AdUser"] as string, Session["AdUserPass"] as string);
                    //for(int ii =0;ii < 6;++ii)
                    //{
                    //    LDAPUser user = new LDAPUser() { Email = ii.ToString(), FirstName = ii.ToString() };
                    //    lstUsers.Add(user);
                    //}
                    //Session["LDAPUsers"] = lstUsers;

                    ViewBag.MultiSelect = "1";
                    //ViewBag.LDAPUsers = lstUsers;
                    ViewBag.vnagroup = Session["VnaGroup"] as string;

                    return View("UserFromAD");
                }
                return View();
            }
            return Content("You have no permission.");
        }
        [HttpPost]
        public ActionResult UploadAccount(HttpPostedFileBase file)
        {
            string strFileName = string.Empty;

            List<int> ierres = new List<int>();

            if (file != null)
            {
                var fileName = file.FileName;
                fileName = System.IO.Path.GetFileName(fileName);

                string sext = System.IO.Path.GetExtension(fileName);
                strFileName = Guid.NewGuid().ToString() + sext;
                var filePath = Server.MapPath(string.Format("~/App_Data/Temp/{0}", strFileName));

                file.SaveAs( filePath);



                if (sext.ToLower().Equals(".csv"))
                {
                    FileStream fs = new FileStream(filePath, FileMode.Open, FileAccess.Read, FileShare.None);
                    StreamReader sr = new StreamReader(fs);// System.Text.Encoding.UTF8);
                    string str = "";

                    EmailAddressAttribute emailAddressAttribute = new EmailAddressAttribute();
                    int ir = 0;
                    while (str != null)
                    {
                        ir++;
                        str = sr.ReadLine();

                        string[] datas = str.Split(',');
                        //
                        if (datas.Length < 8 || string.IsNullOrEmpty(datas[NameIdx].ToString()) || string.IsNullOrEmpty(datas[NativeIdx].ToString())
                            || string.IsNullOrEmpty(datas[MrnIdx].ToString()) || string.IsNullOrEmpty(datas[EmailIdx].ToString()))
                        {
                            ierres.Add(ir);
                            continue;
                        }
                        //
                        string semail = datas[EmailIdx];
                        //
                        if (string.IsNullOrEmpty(semail) || !emailAddressAttribute.IsValid(semail))
                            ierres.Add(ir);
                    }
                    sr.Close();
                    //

                }
                else if (sext.ToLower().Equals(".xls"))
                {
                    DataSet ds = GetExcelData(filePath);
                    //
                    if (ds == null)
                    {
                        return Json(new { state = "0", message = "this xls is not supported", File = Path.GetFileName(filePath) });
                    }
                    int ir = 0;
                    EmailAddressAttribute emailAddressAttribute = new EmailAddressAttribute();
                    //foreach(DataTable dt in ds.Tables)
                    DataTable dt = ds.Tables[0];
                    {
                       
                        foreach (DataRow dataRow in dt.Rows)
                        {
                            ir++;

                            if (string.IsNullOrEmpty(dataRow[NameIdx].ToString())
                                || string.IsNullOrEmpty(dataRow[NativeIdx].ToString()) ||
                                string.IsNullOrEmpty(dataRow[MrnIdx].ToString()) || string.IsNullOrEmpty(dataRow[EmailIdx].ToString()))
                            {
                                ierres.Add(ir);
                                continue;
                            }

                            string semail = dataRow[EmailIdx].ToString();
                            //
                            if (string.IsNullOrEmpty(semail) || !emailAddressAttribute.IsValid(semail))
                                ierres.Add(ir);
                        }
                    }
                }
                else if (sext.ToLower().Equals(".xlsx"))
                {
                    DataSet ds = GetExcelData(filePath);
                    if (ds == null)
                    {
                        return Json(new { state = "0", message = "this xlsx is not supported", File = Path.GetFileName(filePath) });
                    }
                    int ir = 0;
                    EmailAddressAttribute emailAddressAttribute = new EmailAddressAttribute();
                    //foreach(DataTable dt in ds.Tables)
                    DataTable dt = ds.Tables[0];
                    {
                       
                        foreach (DataRow dataRow in dt.Rows)
                        {
                            ir++;

                            if (string.IsNullOrEmpty(dataRow[NameIdx].ToString())
                                || string.IsNullOrEmpty(dataRow[NativeIdx].ToString()) ||
                                string.IsNullOrEmpty(dataRow[MrnIdx].ToString()) || string.IsNullOrEmpty(dataRow[EmailIdx].ToString()))
                            {
                                ierres.Add(ir);
                                continue;
                            }
                            string semail = dataRow[EmailIdx].ToString();
                            //
                            if (string.IsNullOrEmpty(semail) || !emailAddressAttribute.IsValid(semail))
                                ierres.Add(ir);
                        }
                    }
                }
                else
                    return Json(new { state = "0", message = "not supported :" + file.FileName,File="" });
            }
            /*
            if(Request.Files.Count > 0 )
            {
                var myfile = Request.Files["file"];
                //
                string filenme = myfile.FileName;
            }
            */
            if (ierres.Count > 0)
            {
                var obj0 = new { state = "0", message = "something not valid in file "+ file.FileName + " Line:"+ierres[0].ToString(), File = "" };
                return Json(obj0);
            }

            var obj = new { state = "1", message = "success", File = strFileName };

            return Json(obj);
        }
        const int NameIdx = 0;
        const int NativeIdx = 1;
        const int MrnIdx = 2;
        const int GenderIdx = 3;
        const int BirthIdx = 4;
        const int AddressIdx = 5;
        const int PhoneIdx = 6;
        const int EmailIdx = 7;
        const int DeptIdx = 8;
        [HttpPost]
        public ActionResult ClearFile(string sFile)
        {
            var obj1 = new { state = "0", message = "File not existed." };
            var filePath = Server.MapPath(string.Format("~/App_Data/Temp/{0}", sFile));
            if (!System.IO.File.Exists(filePath))
                return Json(obj1);
            //
            try
            {
                System.IO.File.Delete(filePath);
            }
            catch(Exception ex)
            {
                FileLog.Info(ex.Message);
            }

            return Json(new { state = "1", message = "success" });
        }
        [HttpPost]
        public ActionResult GetAccountsFileRec(string sFile, int pageLimit, int pageIndex, string sortBy, string order)
        {
            var obj1 = new { state = "0", message = "File not existed." };

            if (string.IsNullOrEmpty(sFile))
                return Json( obj1);

            var filePath = Server.MapPath(string.Format("~/App_Data/Temp/{0}", sFile));
            if (!System.IO.File.Exists(filePath))
                return Json(obj1);

            string sext = Path.GetExtension(filePath);

            List<Users> ierres = new List<Users>();
            int iTotal = 0;

            if (sext.ToLower().Equals(".csv"))
            {
                string[] recs = System.IO.File.ReadAllLines(filePath);
                int iskip = pageLimit * (pageIndex - 1);
                iTotal = recs.Length;

                if(recs != null && iskip < recs.Length)
                {
                    for(int ii = iskip,jj=0; ii < recs.Length&&jj < pageLimit; ++ii)
                    {
                        Users users = new Users();
                        users.user_name = recs[NameIdx];
                        users.user_id = recs[EmailIdx];
                        users.id_number = recs[NativeIdx];
                        users.medical_recno = recs[MrnIdx];
                        users.mobile_phone = recs[PhoneIdx];
                        users.address = recs[AddressIdx];
                        users.email = recs[EmailIdx];
                        users.gender = recs[GenderIdx];
                        users.birthday = recs[BirthIdx];
                        users.department_id = recs[DeptIdx];
                        users.create_date = DateTime.Now;

                        ierres.Add(users);
                    }
                }
                //


            }
            else if (sext.ToLower().Equals(".xls"))
            {
                DataSet ds = GetExcelData(filePath);
                //
                if (ds == null)
                {
                    return Json(new { state = "0", message = "xls is supported", File = "" });
                }
                //int ir = 0;
                //foreach(DataTable dt in ds.Tables)
                DataTable dt = ds.Tables[0];
                {
                    //ir++;
                    int iskip = pageLimit * (pageIndex - 1);
                    iTotal = dt.Rows.Count;

                    if (dt != null && iskip < dt.Rows.Count)
                    {

                        for (int ii = iskip, jj = 0; ii < dt.Rows.Count && jj < pageLimit; ++ii)
                        {
                            //
                            DataRow dataRow = dt.Rows[ii];
                            //
                            if (dt.Columns.Count < 8 || string.IsNullOrEmpty(dataRow[NameIdx] as string)
                            || string.IsNullOrEmpty(dataRow[NativeIdx] as string) ||
                            string.IsNullOrEmpty(dataRow[MrnIdx] as string)
                            || string.IsNullOrEmpty(dataRow[EmailIdx] as string))
                            {
                                //ierres.Add(ir);
                            }
                            //
                            Users users = new Users();
                            users.user_name = dataRow[NameIdx].ToString();
                            users.user_id = dataRow[EmailIdx].ToString();
                            users.id_number = dataRow[NativeIdx].ToString();
                            users.medical_recno = dataRow[MrnIdx].ToString();
                            users.mobile_phone = dataRow[PhoneIdx].ToString();
                            users.address = dataRow[AddressIdx].ToString();
                            users.email = dataRow[EmailIdx].ToString();
                            users.gender = dataRow[GenderIdx].ToString();
                            users.birthday = dataRow[BirthIdx].ToString();
                            users.department_id = dataRow[DeptIdx].ToString();
                            users.create_date = DateTime.Now;

                            ierres.Add(users);
                        }
                            
                    }
                }
            }
            else if (sext.ToLower().Equals(".xlsx"))
            {
                DataSet ds = GetExcelData(filePath);
                if (ds == null)
                {
                    return Json(new { state = "0", message = "xls is supported", File = "" });
                }
              //  int ir = 0;
                //foreach(DataTable dt in ds.Tables)
                DataTable dt = ds.Tables[0];
                {
                    int iskip = pageLimit * (pageIndex - 1);
                    iTotal = dt.Rows.Count;

                    if (dt != null && iskip < dt.Rows.Count)
                    {

                        for (int ii = iskip, jj = 0; ii < dt.Rows.Count && jj < pageLimit; ++ii)
                        {
                            //
                            DataRow dataRow = dt.Rows[ii];
                            //
                            if (dt.Columns.Count < 8 || string.IsNullOrEmpty(dataRow[NameIdx] as string)
                            || string.IsNullOrEmpty(dataRow[NativeIdx] as string) ||
                            string.IsNullOrEmpty(dataRow[MrnIdx] as string)
                            || string.IsNullOrEmpty(dataRow[EmailIdx] as string))
                            {
                                //ierres.Add(ir);
                            }
                            //
                            Users users = new Users();
                            users.user_name = dataRow[NameIdx].ToString();
                            users.user_id = dataRow[EmailIdx].ToString();
                            users.id_number = dataRow[NativeIdx].ToString();
                            users.medical_recno = dataRow[MrnIdx].ToString();
                            users.mobile_phone = dataRow[PhoneIdx].ToString();
                            users.address = dataRow[AddressIdx].ToString();
                            users.email = dataRow[EmailIdx].ToString();
                            users.gender = dataRow[GenderIdx].ToString();
                            users.birthday = dataRow[BirthIdx].ToString();
                            users.department_id = dataRow[DeptIdx].ToString();
                            users.create_date = DateTime.Now;

                            ierres.Add(users);
                        }

                    }
                }
            }
            else
                return Json(new { state = "0", message = "not supported :" + sFile });
            
            if(ierres != null)
            ierres.Sort((l, r) => {
                int ir = 0;
                if (sortBy == "1")
                {
                    ir = string.Compare(l.user_name, r.user_name);

                }
                else if (sortBy == "2")
                {
                    ir = string.Compare(l.id_number, r.id_number);
                }
                else if (sortBy == "3")
                {
                    ir = string.Compare(l.medical_recno, r.medical_recno);
                }
                else if (sortBy == "4")
                {
                    ir = string.Compare(l.email, r.email);
                }
                if (string.IsNullOrEmpty(order) || order == "asc")
                {

                }
                else
                    ir = -ir;

                return ir;
            });

            int ipagecount = iTotal / pageLimit;
            if (ierres.Count % pageLimit > 0)
                ipagecount++;

            var retobj = new { state = "1", message = "success", users = ierres,pageCount=ipagecount};
            return Json(retobj);
     
        }
        [HttpPost]
        public ActionResult SubmitCreateGroupAccounts(string group,string fileName)
        {
            //
            BaseData.Role lrole = BaseData.Role.Patient;
            if (group == "P")
                lrole = BaseData.Role.Patient;
            else if (group == "Y")
                lrole = BaseData.Role.Doctor;
            else if (group == "N")
                lrole = BaseData.Role.Nurse;
            else if (group == "M")
                lrole = BaseData.Role.Manager;

            //
            var obj1 = new { state = "0", message = "File not existed." };

            if (string.IsNullOrEmpty(fileName))
                return Json(obj1);

            var filePath = Server.MapPath(string.Format("~/App_Data/Temp/{0}", fileName));
            if (!System.IO.File.Exists(filePath))
                return Json(obj1);

            string sext = Path.GetExtension(filePath);

            List<Users> ierres = new List<Users>();
            List<string> lstDeptNames = new List<string>();

            if (sext.ToLower().Equals(".csv"))
            {
                string[] recs = System.IO.File.ReadAllLines(filePath);
                int iskip = 0;// pageLimit * (pageIndex - 1);

                if (recs != null && iskip < recs.Length)
                {
                    for (int ii = iskip ; ii < recs.Length ; ++ii)
                    {
                        Users users = new Users();
                        string siniPass = randPassword(8);
                        users.user_password = Md5.Md5Hash(siniPass);// default passs 111111
                        users.user_name = !string.IsNullOrEmpty(recs[NameIdx]) ? recs[NameIdx].Trim() : string.Empty;
                        users.user_id = recs[EmailIdx];
                        users.id_number = recs[NativeIdx];
                        users.medical_recno = recs[MrnIdx];
                        users.mobile_phone = recs[PhoneIdx];
                        users.address = recs[AddressIdx];
                        users.email = recs[EmailIdx];
                        users.gender = recs[GenderIdx];
                        users.birthday = recs[BirthIdx];
                        users.department_id = recs[DeptIdx];
                        users.role_id = ((int)lrole).ToString();
                        users.is_verified = true;
                        users.is_initial = false;
                        users.create_date = DateTime.Now;

                        if (!lstDeptNames.Contains(recs[DeptIdx]))
                            lstDeptNames.Add(recs[DeptIdx]);
                        //The password is used to send letters and change back to plain text
                        Users us0 = new Users()
                        {
                            user_id = users.user_id,
                            email = users.email,
                            user_password = siniPass,
                            user_name = users.user_name,
                            id_number=users.id_number,
                            medical_recno = users.medical_recno,
                            department_id = users.department_id
                        };
                        ierres.Add(us0);
                    }
                }
                //


            }
            else if (sext.ToLower().Equals(".xls"))
            {
                DataSet ds = GetExcelData(filePath);
                //
                if (ds == null)
                {
                    return Json(new { state = "0", message = "xls is supported", File = "" });
                }
                //int ir = 0;
                //foreach(DataTable dt in ds.Tables)
                DataTable dt = ds.Tables[0];
                {
                    //ir++;
                    int iskip = 0;// pageLimit * (pageIndex - 1);

                    if (dt != null && iskip < dt.Rows.Count)
                    {

                        for (int ii = iskip ; ii < dt.Rows.Count ; ++ii)
                        {
                            //
                            DataRow dataRow = dt.Rows[ii];
                            //
                            if (dt.Columns.Count < 8 || string.IsNullOrEmpty(dataRow[NameIdx] as string)
                            || string.IsNullOrEmpty(dataRow[NativeIdx] as string) ||
                            string.IsNullOrEmpty(dataRow[MrnIdx] as string)
                            || string.IsNullOrEmpty(dataRow[EmailIdx] as string))
                            {
                                //ierres.Add(ir);
                            }
                            //
                            Users users = new Users();
                            string siniPass = randPassword(8);
                            string susername = !string.IsNullOrEmpty(dataRow[NameIdx].ToString()) ? dataRow[NameIdx].ToString().Trim() : string.Empty;
                            users.user_name = susername;
                            users.user_password = Md5.Md5Hash(siniPass);// default passs
                            users.user_id = dataRow[EmailIdx].ToString();
                            users.id_number = dataRow[NativeIdx].ToString();
                            users.medical_recno = dataRow[MrnIdx].ToString();
                            users.mobile_phone = dataRow[PhoneIdx].ToString();
                            users.address = dataRow[AddressIdx].ToString();
                            users.email = dataRow[EmailIdx].ToString();
                            users.gender = dataRow[GenderIdx].ToString();
                            users.birthday = dataRow[BirthIdx].ToString();
                            users.department_id = dataRow[DeptIdx].ToString();
                            users.role_id = ((int)lrole).ToString();
                            users.is_verified = true;
                            users.is_initial = false;
                            users.create_date = DateTime.Now;

                            if (!lstDeptNames.Contains(dataRow[DeptIdx].ToString()))
                                lstDeptNames.Add(dataRow[DeptIdx].ToString());

                            //The password is used to send letters and change back to plain text
                            Users us0 = new Users()
                            {
                                user_id = users.user_id,
                                email = users.email,
                                user_password = siniPass,
                                user_name = users.user_name,
                                id_number = users.id_number,
                                medical_recno = users.medical_recno,
                                department_id = users.department_id
                            };
                            ierres.Add(us0);
                        }

                    }
                }
            }
            else if (sext.ToLower().Equals(".xlsx"))
            {
                DataSet ds = GetExcelData(filePath);
                if (ds == null)
                {
                    return Json(new { state = "0", message = "xls is supported", File = "" });
                }
                //  int ir = 0;
                //foreach(DataTable dt in ds.Tables)
                DataTable dt = ds.Tables[0];
                {
                    int iskip = 0;// pageLimit * (pageIndex - 1);

                    if (dt != null && iskip < dt.Rows.Count)
                    {

                        for (int ii = iskip; ii < dt.Rows.Count ; ++ii)
                        {
                            //
                            DataRow dataRow = dt.Rows[ii];
                            //
                            if (dt.Columns.Count < 8 || string.IsNullOrEmpty(dataRow[NameIdx] as string)
                            || string.IsNullOrEmpty(dataRow[NativeIdx] as string) ||
                            string.IsNullOrEmpty(dataRow[MrnIdx] as string)
                            || string.IsNullOrEmpty(dataRow[EmailIdx] as string))
                            {
                                //ierres.Add(ir);
                            }
                            //
                            Users users = new Users();
                            string siniPass = randPassword(8);
                            string susername = !string.IsNullOrEmpty(dataRow[NameIdx].ToString()) ? dataRow[NameIdx].ToString().Trim() : string.Empty;

                            users.user_name = susername;// dataRow[NameIdx].ToString();
                            users.user_password = Md5.Md5Hash(siniPass);// default passs

                            users.user_id = dataRow[EmailIdx].ToString();
                            users.id_number = dataRow[NativeIdx].ToString();
                            users.medical_recno = dataRow[MrnIdx].ToString();
                            users.mobile_phone = dataRow[PhoneIdx].ToString();
                            users.address = dataRow[AddressIdx].ToString();
                            users.email = dataRow[EmailIdx].ToString();
                            users.gender = dataRow[GenderIdx].ToString();
                            users.birthday = dataRow[BirthIdx].ToString();
                            users.department_id = dataRow[DeptIdx].ToString();
                            users.role_id = ((int)lrole).ToString();
                            users.is_verified = true;
                            users.is_initial = false;
                            users.create_date = DateTime.Now;

                            if (!lstDeptNames.Contains(dataRow[DeptIdx].ToString()))
                                lstDeptNames.Add(dataRow[DeptIdx].ToString());

                            //The password is used to send letters and change back to plain text
                            Users us0 = new Users()
                            {
                                user_id = users.user_id,
                                email = users.email,
                                user_password = siniPass,
                                user_name =users.user_name,
                                id_number = users.id_number,
                                medical_recno = users.medical_recno,
                                department_id = users.department_id
                            };
                            ierres.Add(us0);
                        }

                    }
                }
            }
            else
                return Json(new { state = "0", message = "not supported :" + fileName });

            if(ierres.Count > 0)
            {
                VnaDbContext vnaDbContext = new VnaDbContext();
                IDepartmentRepository departmentRepository = new DepartmentRepository(vnaDbContext);
                IUserRepository userRepository = new UserRepository(vnaDbContext);
                //check account...
                List<string> errAcounts = new List<string>();
                for(int kk=0;kk <ierres.Count;++kk)
                {
                    Users lusr = ierres[kk];
                    if (userRepository.FindEntity<Users>(t => t.email == lusr.email) != null)
                        errAcounts.Add(lusr.email);
                }
                if( errAcounts.Count >0 )
                {
                    return Json(new { state = "0", message = "Email existed." ,emails=errAcounts });
                }
                //
                List<Department> departments = departmentRepository.GetDepartmentList();
                int dcount = 0;
                if (departments != null)
                    dcount = departments.Count;
                Dictionary<string, string> dicNameIDs = new Dictionary<string, string>();
                bool binsert = false;
                foreach(string sdname in lstDeptNames)
                {
                    Department ldept = departmentRepository.FindEntity<Department>(t => t.department_name == sdname);
                    if(ldept != null)
                    {
                        dicNameIDs.Add(sdname, ldept.department_id);
                    }
                    else
                    {
                        string sdeptid = "D" + dcount.ToString();
                        dcount++;
                        //
                        dicNameIDs.Add(sdname, sdeptid);

                        Department department = new Department();
                        department.department_id = sdeptid;
                        department.department_name = sdname;

                        departmentRepository.InsertDepartment(department);
                        binsert = true;
                    }
                }
                //
                if (binsert)
                    departmentRepository.Commit();

                
                int ic = ierres.Count;
                try
                {
                    for (int ii = 0; ii < ic; ++ii)
                    {
                        if (dicNameIDs.ContainsKey(ierres[ii].department_id))
                            ierres[ii].department_id = dicNameIDs[ierres[ii].department_id];
                        else
                            ierres[ii].department_id = string.Empty;

                        userRepository.Insert(ierres[ii]);

                    }
                    userRepository.Commit();

                    //
                    for (int ii = 0; ii < ic; ++ii)
                    {
                        SendUserAccountCreatedMail(ierres[ii].email, ierres[ii].user_password);
                    }
                }
                catch(Exception ex)
                {
                    string serr = ex.Message + ex.StackTrace;
                    if (ex.InnerException != null)
                        serr += ex.InnerException.Message;
                    if (ex.InnerException.InnerException != null)
                        serr += ex.InnerException.InnerException.Message;

                    FileLog.Error(serr);

                    return Json(new { state = "0", message = ex.Message+ (ex.InnerException!=null?ex.InnerException.Message:" ") });
                }
            }
            return Json(new { state = "1", message = "success" });
        }
        /// <summary>
        /// The only thing to note is that if the operating system of the target machine is 64-bit.
        ///The project needs to be compiled to x86 instead of simply using the default Any CPU.
        /// </summary>
        /// <param name="strExcelFileName"></param>
        /// <returns></returns>
    private string GetOleDbConnectionString(string strExcelFileName)
        {
            // Office 2007 And the following versions..
            string strJETConnString =
              String.Format("Provider=Microsoft.Jet.OLEDB.4.0;Data Source={0};Extended Properties='Excel 8.0;HDR=Yes;IMEX=1;'", strExcelFileName);
            // xlsx extension use.
            string strASEConnXlsxString =
              String.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties=\"Excel 12.0 Xml;HDR=YES;IMEX=1;\"", strExcelFileName);
            // xls Extension use
            string strACEConnXlsString =
              String.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties=\"Excel 8.0;HDR=YES\"", strExcelFileName);
            //other
            string strOtherConnXlsString =
              String.Format("Provider=Microsoft.Jet.OLEDB.4.0;Data Source={0};Extended Properties='Excel 8.0;HDR=Yes;IMEX=1;'", strExcelFileName);

            //Try using ACE. If no errors occur, use the ACE driver.
            try
            {
                System.Data.OleDb.OleDbConnection cn = new System.Data.OleDb.OleDbConnection(strACEConnXlsString);
                cn.Open();
                cn.Close();
                // use ACE
                return strACEConnXlsString;
            }
            catch (Exception ex)
            {
                // Failed to start ACE.
                Console.WriteLine(ex.Message);
            }

            // Try using Jet. If no errors occur, use the Jet driver.
            try
            {
                System.Data.OleDb.OleDbConnection cn = new System.Data.OleDb.OleDbConnection(strJETConnString);
                cn.Open();
                cn.Close();
                // use Jet
                return strJETConnString;
            }
            catch (Exception ex)
            {
                // Failed to start Jet.
                Console.WriteLine(ex.Message);
            }

            // Try using Jet. If no errors occur, use the Jet driver.
            try
            {
                System.Data.OleDb.OleDbConnection cn = new System.Data.OleDb.OleDbConnection(strASEConnXlsxString);
                cn.Open();
                cn.Close();
                // use Jet
                return strASEConnXlsxString;
            }
            catch (Exception ex)
            {
                // Failed to start Jet.
                Console.WriteLine(ex.Message);
            }
            // If both ACE and JET fail, JET is used by default.
            return strOtherConnXlsString;
        }

        /// <summary>
        /// To obtain Excel data, you need to install the Microsoft Access 2010 database engine redistributable package
        /// https://www.microsoft.com/zh-CN/download/details.aspx?id=13255
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        private DataSet GetExcelData(string strFilePath)
        {
            try
            {
                //Get connection string
                // @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + filePath + ";Extended Properties=Excel 12.0;HDR=YES;";
                string strConn = GetOleDbConnectionString(strFilePath);

                DataSet ds = new DataSet();
                using (OleDbConnection conn = new OleDbConnection(strConn))
                {
                    //Open connection
                    conn.Open();
                    System.Data.DataTable dt = conn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, new object[] { null, null, null, "TABLE" });

                    // Get all worksheets in Excel workbook
                    System.Data.DataTable schemaTable = conn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                    OleDbDataAdapter sqlada = new OleDbDataAdapter();

                    foreach (DataRow dr in schemaTable.Rows)
                    {
                        try
                        {
                            string strSql = "Select * From [" + dr[2].ToString().Trim() + "]";
                            if (strSql.Contains("$"))
                            {
                                OleDbCommand objCmd = new OleDbCommand(strSql, conn);
                                sqlada.SelectCommand = objCmd;
                                sqlada.Fill(ds, dr[2].ToString().Trim());
                            }
                        }
                        catch { }
                    }
                    //Close connection
                    conn.Close();
                }
                return ds;
            }
            catch (Exception ex)
            {
                FileLog.Info(ex.Message + ex.StackTrace);

                return null;
            }
        }
        public DataSet ImportFromExcel(string file)
        {
            // Create new dataset
            DataSet ds = new DataSet();

            // -- Start of Constructing OLEDB connection string to Excel file
            Dictionary<string, string> props = new Dictionary<string, string>();

            // For Excel 2007/2010
            if (file.EndsWith(".xlsx"))
            {
                props["Provider"] = "Microsoft.ACE.OLEDB.12.0;";
                props["Extended Properties"] = "Excel 12.0 XML";
            }
            // For Excel 2003 and older
            else if (file.EndsWith(".xls"))
            {
                props["Provider"] = "Microsoft.Jet.OLEDB.4.0";
                props["Extended Properties"] = "Excel 8.0";
            }
            else
                return null;

            props["Data Source"] = file;

            StringBuilder sb = new StringBuilder();

            foreach (KeyValuePair<string, string> prop in props)
            {
                sb.Append(prop.Key);
                sb.Append('=');
                sb.Append(prop.Value);
                sb.Append(';');
            }

            string connectionString = sb.ToString();
            // -- End of Constructing OLEDB connection string to Excel file

            // Connecting to Excel File
            using (OleDbConnection conn = new OleDbConnection(connectionString))
            {
                conn.Open();
                OleDbCommand cmd = new OleDbCommand();
                cmd.Connection = conn;

                DataTable dtSheet = conn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);

                foreach (DataRow dr in dtSheet.Rows)
                {
                    string sheetName = dr["TABLE_NAME"].ToString();

                    // you can choose the colums you want.
                    cmd.CommandText = "SELECT * FROM [" + sheetName + "]";

                    DataTable dt = new DataTable();
                    dt.TableName = sheetName.Replace("$", string.Empty);

                    OleDbDataAdapter da = new OleDbDataAdapter(cmd);
                    da.Fill(dt);

                    // Add table into DataSet
                    ds.Tables.Add(dt);
                }

                cmd = null;
                conn.Close();
            }

            return ds;
        }
        /// <summary>
        /// Log in to AD and set the session to get user information from AD.
        /// </summary>
        /// <param name="aduser"></param>
        /// <param name="adpass"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult CheckADAccount(string aduser,string adpass,string vnagroup)
        {
            VNAConfigSection vna = VNAConfigSection.Instance;
            string sdomainurl = vna.LdapElement.LDAPUrl;
            string sdomain = vna.LdapElement.Domain;
            bool bl = Common.LdapHelper.validateUserByBind(aduser, sdomain, adpass, sdomainurl);
            if (bl)
            {
     
            }
           // bl = true;
            Session["LDAP"] = bl?"1":null;
            Session["AdUser"] = bl?aduser:null;
            Session["AdUserPass"] = bl?adpass:null;
            Session["VnaGroup"] = vnagroup;

            var obj = new { state = bl?"1":"0", message = bl?"Success.":"Login failed." };
            //
            return Json(obj);
        }
        [HttpPost]
        public ActionResult GetADAccounts(string userName,int pageLimit, int pageIndex, string sortBy, string order)
        {
            int iTotal = 0;
            List<LDAPUser> users = null;

            if (Session["LDAPUsers"] == null)
            {
                VNAConfigSection vna = VNAConfigSection.Instance;

                string sdomainurl = vna.LdapElement.LDAPUrl;
                string sdomain = vna.LdapElement.Domain;

                List<LDAPUser> lstUsers = null;
                if(string.IsNullOrEmpty(userName))
                    lstUsers = LdapHelper.GetLDAPUsers(sdomain, sdomainurl, Session["AdUser"] as string, Session["AdUserPass"] as string);
                else
                {
                    lstUsers = LdapHelper.GetLDAPUsersFilter(sdomain, sdomainurl, Session["AdUser"] as string, Session["AdUserPass"] as string,userName,string.Empty,string.Empty);

                }
                //for (int ii = 0; ii < 6; ++ii)
                //{
                //    LDAPUser user = new LDAPUser() { Email = "a"+ii.ToString() + "@ebmtech.com", UserAccount="u"+ ii.ToString(), FirstName = ii.ToString(),LastName=string.Empty };
                //    lstUsers.Add(user);
                //}
                if (lstUsers!=null && lstUsers.Count>0)
                    Session["LDAPUsers"] = lstUsers;

                users = Session["LDAPUsers"] as List<LDAPUser>;
            }
            else
            {
                //
                List<LDAPUser> tmpusers = Session["LDAPUsers"] as List<LDAPUser>;
                if (tmpusers.Count > 0 && !string.IsNullOrEmpty(userName))
                {
                    users = tmpusers.FindAll(t => t.UserAccount.IndexOf(userName, StringComparison.OrdinalIgnoreCase)>=0);//Contains

                }
                else
                    users = tmpusers;
            }
            //string sortField = "UserAccount";
            
            if (users == null)
                users = new List<LDAPUser>();
            else
            {
          
            }
            users.Sort((l, r) => {
                int ir = 0;
                if (sortBy == "1")
                {
                    ir = string.Compare(l.UserAccount, r.UserAccount);
      
                }
                else if (sortBy == "2")
                {
                    ir = string.Compare(l.FirstName, r.FirstName);
                }
                else if (sortBy == "3")
                {
                    ir = string.Compare(l.LastName, r.LastName);
                }
                else if (sortBy == "4")
                {
                    ir = string.Compare(l.Email, r.Email);
                }
                if (string.IsNullOrEmpty(order) || order == "asc")
                {

                }
                else
                    ir = -ir;

                return ir;
            });
            iTotal = users.Count;

            int ipagecount = iTotal / pageLimit;
            if (users.Count % pageLimit > 0)
                ipagecount++;

            int iskip = pageLimit * (pageIndex - 1);
            List<LDAPUser> pageusrs = new List<LDAPUser>();
            if (users != null && iskip < users.Count)
            {
                for (int ii = iskip, jj = 0; ii < users.Count && jj < pageLimit; ++ii)
                {
                    pageusrs.Add(users[ii]);
                }
            }

            var retobj = new { state = "1", message = "success", users = pageusrs, pageCount = ipagecount };
            return Json(retobj);
        }
        
        [HttpPost]
        public ActionResult SubmitCreateGroupAccountsFromAD(string group,string[] users)
        {
            string smessage = string.Empty, state = "0";
            List<string> errusr = new List<string>();
            List<string> sucusr = new List<string>();

            BaseData.Role lrole = BaseData.Role.Patient;
            if (group == "P")
                lrole = BaseData.Role.Patient;
            else if (group == "Y")
                lrole = BaseData.Role.Doctor;
            else if (group == "N")
                lrole = BaseData.Role.Nurse;
            else if (group == "M")
                lrole = BaseData.Role.Manager;
            else
                lrole = BaseData.Role.Doctor;

            List<LDAPUser> usersto = new List<LDAPUser>();
            if(Session["LDAPUsers"] == null)
            {
                smessage = "Not valid create Accounts.";
                if(users != null)
                    errusr.AddRange(users);
            }
            else if(users != null)
            {
                foreach (string account in users)
                {
                    bool bf = false;
                    foreach (LDAPUser usr in Session["LDAPUsers"] as List<LDAPUser>)
                    {
                        if (usr.UserAccount == account )
                        {
                            if (IsValidMail(usr.Email))
                            {
                                usersto.Add(usr);
                                bf = true;
                            }
                            else
                                bf = false;
                            break;
                        }
                     
                    }
                    if(!bf)
                    {
                        errusr.Add(account);
                    }
                }

                //
                
                if (usersto.Count > 0)
                {
                    VnaDbContext vnaDbContext = new VnaDbContext();
                    IDepartmentRepository departmentRepository = new DepartmentRepository(vnaDbContext);
                    IUserRepository userRepository = new UserRepository(vnaDbContext);

                    List<Users> tosubmits = new List<Users>();
                    //userRepository.BeginTrans();

                    foreach (LDAPUser ldapusr in usersto)
                    {
                        if (userRepository.FindEntity<Users>(t => t.email == ldapusr.Email)!=null)
                        {
                            if (string.IsNullOrEmpty(smessage))
                                smessage += "existed mail: ";
                            errusr.Add(ldapusr.UserAccount);

                            continue;
                        }

                        Users tmpusr = new Users();
                        string siniPass = randPassword(8);
                        tmpusr.user_password = Md5.Md5Hash(siniPass);// default passs 111111
                        string susername = ldapusr.FirstName + ' ' + ldapusr.LastName;
                        susername = !string.IsNullOrEmpty(susername) ? susername.Trim() : string.Empty;

                        tmpusr.user_name = susername;
                        tmpusr.user_id = ldapusr.UserAccount;
                        //tmpusr.id_number = ;
                        //tmpusr.medical_recno = ;
                        //tmpusr.mobile_phone = ;
                        //tmpusr.address = ;
                        tmpusr.email = ldapusr.Email;
                        //users.gender = ;
                        //users.birthday = ;
                        //users.department_id = ;
                        tmpusr.role_id = ((int)lrole).ToString();
                        tmpusr.is_verified = true;
                        tmpusr.is_initial = true;// false;
                        tmpusr.delete_mark = false;
                        tmpusr.source = 1;
                        tmpusr.create_date = DateTime.Now;
                        //
                        try
                        {
                            userRepository.Insert<Users>(tmpusr);

                            userRepository.Commit(); //此时会返回自增的变量

                            //密码用于寄信，改回明文

                            Users us0 = new Users() { user_id = tmpusr.user_id, email = tmpusr.email,
                                user_password = siniPass,
                                user_name = tmpusr.user_name
                            };
                            tosubmits.Add(us0);
                            sucusr.Add(tmpusr.user_id_id.ToString());
                        }
                        catch (Exception ex)
                        {
                            smessage = ex.Message;
                            errusr.Add(ldapusr.UserAccount);
                        }
                        
                    }
                    try
                    {
                        
                        //
                        state = errusr.Count > 0?"0":"1";

                        // Partial failure at 1...
                        //
                        //send mail ,, form AR no longer send emails, just log in with AD
                        for (int ii = 0; ii < tosubmits.Count; ++ii)
                        {
                            //SendUserAccountCreatedMail(tosubmits[ii].user_id,tosubmits[ii].email, tosubmits[ii].user_password);
                        }
                    }
                    catch (Exception ex)
                    {
                        smessage += ex.Message;
                        ////transaction失败所有的都提交失败...
                        //for (int ii = 0; ii < tosubmits.Count; ++ii)
                        //{
                        //    errusr.Add(tosubmits[ii].user_id);
                        //}
                        //sucusr.Clear();
                    }
                }
                else
                {
                    smessage = "no valid account";
                }

            }
            else
            {
                smessage = "no selected user";
                state = "0";
            }
            if(errusr.Count>0)
            {
                if (!string.IsNullOrEmpty(smessage))
                    smessage += ":";
                foreach (string su in errusr)
                    smessage += su+" ";

            }
            if(sucusr.Count >0 && errusr.Count>0)
            {//Partial success
                smessage += "successful: " + sucusr.Count;
            }
           
            var obj = new { state = state, message = smessage, emails = errusr.ToArray(),succs=sucusr.ToArray() };

            return Json(obj);
        }
        /// <summary>
        ///Verify EMail is legal
        /// </summary>
        /// <param name="email">Email to be verified</param>
        public bool IsValidMail(string emailaddress)
        {
            try
            {
                MailAddress m = new MailAddress(emailaddress);

                return true;
            }
            catch (FormatException)
            {
                return false;
            }
        }
        [HandlerLogin]
        public ActionResult Profiles(string user_id_id)
        {
            OperatorModel operatorProvider = OperatorProvider.Provider.GetCurrent();
            //
            bool beditable = true;
            bool bAdmin = false;
            bool beditSelf = true;
            if (operatorProvider != null)
            {
                string suserid = operatorProvider.UserId;
                string susercode = operatorProvider.UserCode;
                string sUserEditPhoneAddr = Configs.GetValue("UserPhoneAddrEdit");

                if ( !string.IsNullOrEmpty(user_id_id) && user_id_id!= susercode )
                {
                    //Not the same person, then only the admin or manager roles can be modified ..
                    BaseData.Role role = RoleActor.GetRole(operatorProvider.RoleId);
                    if(role != BaseData.Role.Admin && role != BaseData.Role.Super) //&& role != BaseData.Role.Manager 
                    {
                        return Content("You has no permission.");
                    }
                    //
                    beditable = (role == BaseData.Role.Admin || role == BaseData.Role.Super);
                    bAdmin = beditable;
                    beditSelf = false;
                }
                else
                { //Edit yourself
                    BaseData.Role role = RoleActor.GetRole(operatorProvider.RoleId);
                    bAdmin = (role == BaseData.Role.Admin || role == BaseData.Role.Super);
                    beditSelf = true;
                }

                VnaDbContext vnaDbContext = new VnaDbContext();
                IUserRepository userRepository = new UserRepository(vnaDbContext);

                if (string.IsNullOrEmpty(user_id_id))
                    user_id_id = susercode;

                int iuserid_id = 0;
                int.TryParse(user_id_id, out iuserid_id);
                Users users = userRepository.FindEntity<Users>(t => t.user_id_id==iuserid_id );//suserid
                if (users == null)
                    return Content("user_id_id not existed.");
                users.birthday = string.IsNullOrEmpty(users.birthday) ? "" : users.birthday.Trim();

                IDepartmentRepository ideptrep = new DepartmentRepository(vnaDbContext);
                List<Department> departments = ideptrep.GetDepartmentList();

                ViewBag.departments = departments;
                string sroleid = operatorProvider.RoleId;
                ViewBag.roleid = sroleid;
                BaseData.Role lrole = RoleActor.GetRole(users.role_id);
                string sroleName = lrole.ToString();

                ViewBag.roleName = sroleName;

                ViewBag.editable = beditable.ToString();
                ViewBag.Admin = bAdmin.ToString();
                ViewBag.UserEditPhoneAddr = sUserEditPhoneAddr;
                ViewBag.beditSelf = beditSelf;

                string srocode = "P";
                if (lrole == BaseData.Role.Patient)
                {
                    srocode = "P";
                }
                else if (lrole == BaseData.Role.Manager)
                {
                    srocode = "M";
                }
                else if (lrole == BaseData.Role.Doctor)
                {
                    srocode = "Y";
                }
                else if (lrole == BaseData.Role.Nurse)
                {
                    srocode = "N";
                }
                else if(lrole == BaseData.Role.Admin)
                {
                    srocode = "A";
                }

                ViewBag.RoleCode = srocode;

                //Convenient to return when the user modifies the information...
                string sreturnurl = Request.QueryString["ReturnUrl"];
                ViewBag.ReturnUrl = sreturnurl;

                return View(users);
            }
            else
                return RedirectToAction("Login","User");// View();
        }

        [HttpPost]
        public ActionResult ModifyProfile(UserModifier user)
        {
            var obj = new { state = "0", message = "false" };
            if (user == null)
                return Json(obj);


            VnaDbContext vnaDbContext = new VnaDbContext();
            IUserRepository userRepository = new UserRepository(vnaDbContext);
            Users users = userRepository.GetUser(user.user_id);
            if (users == null)
                return Json(obj);

            string group = user.role_id;
            BaseData.Role lrole = BaseData.Role.Patient;
            if (group == "P")
                lrole = BaseData.Role.Patient;
            else if (group == "Y")
                lrole = BaseData.Role.Doctor;
            else if (group == "N")
                lrole = BaseData.Role.Nurse;
            else if (group == "M")
                lrole = BaseData.Role.Manager;
            else if (group == "A")
                lrole = BaseData.Role.Admin;
            try
            { 
                users.role_id = ((int)lrole).ToString(); //Allow change Allow change

                users.user_name = !string.IsNullOrEmpty(user.user_name)?user.user_name.Trim():string.Empty;
                users.birthday = user.birthday;
                users.address = user.address;
                users.department_id = user.department_id;
                users.email = user.email!=null?user.email.Trim():null;
                users.gender = user.gender!=null?user.gender.Trim():null;
                users.id_number = user.id_number!=null?user.id_number.Trim():null;
                users.medical_recno = user.medical_recno;
                users.mobile_phone = user.mobile_phone;

            
                userRepository.UpdateUser(users);

                userRepository.Commit();

                obj = new { state = "1", message = "Modified." };

                //Reset current user
                if (OperatorProvider.Provider.GetCurrent() != null
                    && OperatorProvider.Provider.GetCurrent().UserCode == user.user_id)
                {
                    OperatorModel operatorModel = new OperatorModel();
                    operatorModel.UserId = users.user_id;
                    operatorModel.UserCode = users.user_id_id.ToString();// user_id;
                    operatorModel.UserName = users.user_name;

                    operatorModel.DepartmentId = users.department_id;
                    operatorModel.RoleId = users.role_id;

                    OperatorProvider.Provider.AddCurrent(operatorModel);

                }
            }
            catch(Exception ex)
            {
                string serr = ex.Message;
                if (ex.InnerException != null)
                    serr += " " + ex.InnerException.Message;

                FileLog.Info(serr);

                obj = new { state = "0", message = "Failed." };
            }
     
            return Json(obj);
        }
        [HttpPost]
        public ActionResult UploadIcon()
        {
            string blogimage = Request["image"];
            string user_id_id = Request["userid_id"];
            string ftype = Request["filetype"];

            VnaDbContext vnaDbContext = new VnaDbContext();
            IUserRepository userRepository = new UserRepository(vnaDbContext);
            int iuser_id_id = 0;
            int.TryParse(user_id_id, out iuser_id_id);
            Users users = userRepository.FindEntity<Users>(t => t.user_id_id == iuser_id_id);
            if (users == null)
                return Json(new { state = "0", success = false, message = "user is not existed." });

            try
            {
                // var file = Request.Files[0]; //data:image/bmp;base64,
                string sbase64 = blogimage;
                if (sbase64.Contains("base64,"))
                    sbase64 = sbase64.Substring(sbase64.IndexOf("base64,") + 7);
                byte[] bts = Convert.FromBase64String(sbase64);
                if (ftype.Length >= 4)
                    ftype = ftype.Substring(ftype.Length - 3);

                var iconFile = IconFilePath();
                string fileName = Guid.NewGuid().ToString() +"."+ ftype;// Path.GetFileName(file.FileName);// Original file name

                                iconFile = iconFile + "\\" + fileName;
                //file.SaveAs(iconFile);
                System.IO.File.WriteAllBytes(iconFile,bts);
                users.header_icon = iconFile;
                userRepository.UpdateUser(users);
                vnaDbContext.SaveChanges();
                //information update
                OperatorModel operatorProvider = OperatorProvider.Provider.GetCurrent();
                if(operatorProvider!= null)
                {
                    operatorProvider.headImageFile = iconFile; //This has not been saved 
                    OperatorProvider.Provider.AddCurrent(operatorProvider); //This way.. 
                }
                return Json(new {state="1", success = true });
            }
            catch (Exception ex)
            {
                FileLog.Error( ex.Message);
                return Json(new { state = "0", success=false,message = "Failed." });
            }
            //var obj = new { state = "0", success = false, message = "Failed." };

            //return Json(obj);
        }

        //创建文件夹 สร้างโฟลเดอร์
        public string IconFilePath()
        {
            //文件上传后的保存路径 บันทึกเส้นทางหลังจากอัพโหลดไฟล์
            string filePath = Server.MapPath("~/App_Data/IconFiles");
            try
            {
                if (!Directory.Exists(filePath))
                {
                    Directory.CreateDirectory(filePath);
                }
            }
            catch(Exception e)
            {
                FileLog.Error(e.Message);
            }
            return filePath;
        }
        [HttpPost]
        public ActionResult GetUser(string user_id_id)
        {
            if (string.IsNullOrEmpty(user_id_id))
                return Json(new { state = "0", message = "user_id_id is empty." });
            //
            VnaDbContext vnaDbContext = new VnaDbContext();
            IUserRepository userRepository = new UserRepository(vnaDbContext);
            int iuser_id_id = 0;
            int.TryParse(user_id_id, out iuser_id_id);

            Users users = userRepository.FindEntity<Users>(t => t.user_id_id == iuser_id_id);
            if( users == null)
                return Json(new { state = "0", message = "user is not existed." });

            BaseData.Role lrole = BaseData.RoleActor.GetRole(users.role_id);
            string srocode = "P";
            if (lrole == BaseData.Role.Patient)
            {
                srocode = "P";
            }
            else if (lrole == BaseData.Role.Manager)
            {
                srocode = "M";
            }
            else if (lrole == BaseData.Role.Doctor)
            {
                srocode = "Y";
            }
            else if (lrole == BaseData.Role.Nurse)
            {
                srocode = "N";
            }
            else if (lrole == BaseData.Role.Admin)
            {
                srocode = "A";
            }
            users.role_id = srocode;

            return Json(new { state = "1", message = "success.", data = users });
        }

        [HttpGet]
        public ActionResult SwitchStyle(string Return)
        {
            OperatorModel operatorProvider = OperatorProvider.Provider.GetCurrent();
            if (Session["style"] == null)
            {
                Session["style"] = "1";
            }
            else
            {
                Session["style"] = null;
            }

            VnaDbContext vnaDbContext = new VnaDbContext();
            IUserRepository userRepository = new UserRepository(vnaDbContext);
            Users users = userRepository.FindEntity<Users>(t => t.user_id == operatorProvider.UserId);
            users.user_theme = Session["style"] == null ? false : true;
            userRepository.Update(users);
            userRepository.Commit();

            if (!string.IsNullOrEmpty(Return))
                return Redirect(Return);

            return RedirectToAction("Index","Home");
        }
       public class UserModifier
        {
            public string user_id { get; set; }
            public string user_name { get; set; }
            public string id_number { get; set; }
            public string medical_recno { get; set; }
            public string gender { get; set; }
            public string birthday { get; set; }
            public string email { get; set; }
            public string mobile_phone { get; set; }
            public string address { get; set; }
            public string department_id { get; set; }
            public string role_id { get; set; }
        }
    }
}