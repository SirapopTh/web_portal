﻿using BaseData;
using Common;
using DataModel;
using DicomKit;
using Ionic.Zip;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using VNA.Models;

namespace VNA.Controllers
{
    public class HomeController : ControllerBase//Controller
    {
        string msFilePath = "~/App_Data/Files";
        string msTempPath = "~/App_Data/Temp";

        private Common.MailHelper mailHelper = new Common.MailHelper();
        public override ActionResult Index()
        {
            return View();
        }

        
        public class UserStudy
        {
            public string user_id_id { get; set; }
            public string study_uid { get; set; }
            public string medno { get; set; }
        }
        /// <summary>
        /// Send study to user to download...
        /// </summary>
        /// <param name="sendtoUserStudy">userid_id/studyuid </param>
        /// <param name="expire"></param>
        /// <param name="creator"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult SendDownloadLinkID(List<UserStudy> sendtoUserStudy, string expire, string creator,string receivemail)
        {
            if (sendtoUserStudy == null )//|| userids.Length == 0)
                return Error("useris is empty.");

            FileLog.Info("SendDownloadLinkID:" + sendtoUserStudy.ToJson());

            VnaDbContext vnaDbContext = new VnaDbContext();
            IUserRepository userRepository = new UserRepository(vnaDbContext);
         
            bool bRsna = false;// true;
            if (bRsna && !string.IsNullOrEmpty(receivemail))
            {

            }

            IDownloadRepository uplinkRepository = new DownloadRepository(vnaDbContext);
            int creator_id = 0;
            int.TryParse(creator, out creator_id);
            IUploadRepository uploadRepository = new UploadRepository(vnaDbContext);

            DateTime dtexpire = DateTime.Now.AddDays(7);

            //The expiration time ends with d or m, indicating days or months..
            if (string.IsNullOrEmpty(expire))
                expire = "7d";
            expire = expire.ToLower();
            if (expire.Last() == 'd')
            {
                expire = expire.Substring(0, expire.Length - 1);
                int iexpire = 7;
                int.TryParse(expire, out iexpire);
                //
                dtexpire = DateTime.Now.AddDays(iexpire);
            }
            else if (expire.Last() == 'm')
            {
                expire = expire.Substring(0, expire.Length - 1);
                int iexpire = 7;
                int.TryParse(expire, out iexpire);
                //
                dtexpire = DateTime.Now.AddMonths(iexpire);
            }
            else if (expire.Last() == 'h')
            {
                expire = expire.Substring(0, expire.Length - 1);
                int iexpire = 7;
                int.TryParse(expire, out iexpire);
                //
                dtexpire = DateTime.Now.AddHours(iexpire);
            }
            string sExcept = string.Empty;
            int sucCount = 0, failCount = 0;
            Dictionary<int, string> dicUserDownID = new Dictionary<int, string>();

            VNAServiceReference1.VNAService1Client client = new VNAServiceReference1.VNAService1Client();
            try
            {
                //To judge, for the same person, downloaduid only generates one, that is, only one link ..
                // sort....Sorting is not allowed here, because user_id_id may not be a patient
                // sendtoUserStudy.Sort((left,right) => { return string.Compare(left.user_id_id, right.user_id_id); });
                string reqid = string.Empty;
                if (bRsna)
                {
                    reqid = Common.Common.GuId();

                    for (int i = 0; i < sendtoUserStudy.Count; ++i)
                    {
                        //bool bnew = false;

                        int iuserid = 0;
                        string suserid_id = sendtoUserStudy[i].user_id_id;
                        int.TryParse(suserid_id, out iuserid);

                        //Find the corresponding patient based on study_uid and user_id_id and send it back to the patient ...
                        string studyuid = sendtoUserStudy[i].study_uid;

                        string lstudyuid = sendtoUserStudy[i].study_uid;

                        FileLog.Info("start QueryDownload:" + lstudyuid);

                        VNAServiceReference1.QueryDownloadReturn[] queryDownns = client.QueryDownload(new string[] { lstudyuid });
                        VNAServiceReference1.QueryDownloadReturn queryDownload = queryDownns != null && queryDownns.Length > 0 ? queryDownns[0] : null;

                        FileLog.Info("end QueryDownload:" + lstudyuid);

                        Download uplink = new Download()
                        {
                            create_date = DateTime.Now,
                            download_uid = reqid,
                            medical_recno = string.Empty,
                            modality_instudy = string.Empty,
                            native_id = string.Empty,
                            patient_name = string.Empty,
                            ref_physician = string.Empty,
                            source_dept = string.Empty,
                            study_date = string.Empty,
                            study_uid = lstudyuid,
                            user_id_id = iuserid,
                            creator_id = creator_id,
                            expire_time = dtexpire,
                            is_valid = true,
                        };
                        if (queryDownload != null)
                        {
                            uplink.medical_recno = queryDownload.medical_num;
                            uplink.modality_instudy = queryDownload.modality;
                            uplink.native_id = queryDownload.native_id;
                            uplink.patient_name = queryDownload.first_name + (!string.IsNullOrEmpty(queryDownload.last_name) ? " " + queryDownload.last_name : "");
                            uplink.ref_physician = queryDownload.ref_physician;
                            uplink.source_dept = queryDownload.source_dept;
                            uplink.study_date = queryDownload.study_date == null ? "" : queryDownload.study_date.Value.ToString("yyyyMMdd");
                            uplink.study_file = queryDownload.org_filename;
                        }
                        FileLog.Info("InsertDownload " + uplink.ToJson());

                        uplinkRepository.InsertDownload(uplink);

                        uplinkRepository.Commit();
                        //
                        
                    }
                    //.... When the users are different, start sending
                    if (sendtoUserStudy.Count > 0)
                    {
                        string susermail = receivemail;

                        FileLog.Info("SendDownloadLinkID mail:" + susermail + " " + reqid);

                        SendDownloadlinkMail(susermail, reqid, true, string.Empty);
                    }

                    client.Close();

                    return Success("success");
                }

                // string lastuser = string.Empty;

                // Note that if the study was originally in pacs, not uploaded, then send downloadlink processing
                for (int i = 0; i < sendtoUserStudy.Count; ++i)
                {
                    bool bnew = false;
             
                    int iuserid = 0;
                    string suserid_id = sendtoUserStudy[i].user_id_id;
                    int.TryParse( suserid_id, out iuserid);

                    //Find the corresponding patient based on study_uid and user_id_id and send it back to the patient ...
                    string studyuid = sendtoUserStudy[i].study_uid;
                    Upload curupd = null;// new Upload();// 
                    curupd = uploadRepository.FindEntity<Upload>(t => (t.study_uid == studyuid && t.user_id_id == iuserid));
                    //190927 If you want users to download files that are not from upload, use the following sentence...
                    //new Upload();// curupd.medical_num = sendtoUserStudy[i].medno; //Change to bring medical num
                    if (curupd == null)
                    {
                        //191126 Allow downloading existing images from pacs, as long as the user exists
                        curupd = new Upload();
                        curupd.medical_num = sendtoUserStudy[i].medno;
                        //191126 Allow to continue..
                        //FileLog.Info("can not find upload study:" + sendtoUserStudy[i].study_uid);
                        //if (string.IsNullOrEmpty(sExcept))
                        //    sExcept = sendtoUserStudy[i].medno;
                        //else
                        //    sExcept += "," + sendtoUserStudy[i].medno;
                        //failCount++;
                        //continue;
                    }
                    FileLog.Info("get upload study:" + sendtoUserStudy[i].study_uid);
                    //
                    //yys Change to use medical number to get from users ...
                    Users users = userRepository.FindEntity<Users>(t => t.medical_recno== curupd.medical_num); //t.user_id_id == iuserid
                    if (users == null)
                    {
                        FileLog.Info("can not find users:" + curupd.medical_num);
                        if (string.IsNullOrEmpty(sExcept))
                            sExcept = curupd.medical_num;
                        else
                            sExcept += "," + curupd.medical_num;
                        failCount++;
                        continue;
                    }
                    FileLog.Info("get user:" + curupd.medical_num);
                    //sendtoUserStudy[i].user_id_id lastuser != users.user_id_id.ToString()|| string.IsNullOrEmpty(reqid)
                    if (!dicUserDownID.ContainsKey(users.user_id_id))
                    {
                        reqid = Common.Common.GuId();
                        bnew = true;

                        dicUserDownID.Add(users.user_id_id, reqid);
                    }
                    else
                        reqid = dicUserDownID[users.user_id_id];

                    string lstudyuid = sendtoUserStudy[i].study_uid;

                    FileLog.Info("start QueryDownload:" + lstudyuid);

                    VNAServiceReference1.QueryDownloadReturn[] queryDownns= client.QueryDownload(new string[] { lstudyuid });
                    VNAServiceReference1.QueryDownloadReturn queryDownload = queryDownns != null && queryDownns.Length > 0 ? queryDownns[0] : null;

                    FileLog.Info("end QueryDownload:" + lstudyuid);

                    Download uplink = new Download() { create_date=DateTime.Now, download_uid= reqid, medical_recno= string.Empty, modality_instudy=string.Empty,
                         native_id=string.Empty, patient_name=string.Empty, ref_physician=string.Empty, source_dept=string.Empty,
                         study_date=string.Empty, study_uid = lstudyuid , user_id_id=iuserid ,creator_id = creator_id, expire_time = dtexpire, is_valid = true,
                        };
                    if(queryDownload != null)
                    {
                        uplink.medical_recno = queryDownload.medical_num;
                        uplink.modality_instudy = queryDownload.modality;
                        uplink.native_id = queryDownload.native_id;
                        uplink.patient_name = queryDownload.first_name + (!string.IsNullOrEmpty(queryDownload.last_name)?" "+queryDownload.last_name : "");
                        uplink.ref_physician = queryDownload.ref_physician;
                        uplink.source_dept = queryDownload.source_dept;
                        uplink.study_date = queryDownload.study_date==null?"":queryDownload.study_date.Value.ToString("yyyyMMdd");
                        uplink.study_file = queryDownload.org_filename;
                    }
                    FileLog.Info("InsertDownload " + uplink.ToJson());

                    uplinkRepository.InsertDownload(uplink);

                    uplinkRepository.Commit();
                    //
                    //....When the users are different, start sending
                    string susermail = users.email;
                    if (bnew)
                    {
                        FileLog.Info("SendDownloadLinkID mail:" + susermail + " " + reqid);

                        SendDownloadlinkMail(susermail, reqid, users.is_initial, users.user_password);
                        sucCount++;

                    }
                    //lastuser = users.user_id_id.ToString();// sendtoUserStudy[i].user_id_id;
                }

                FileLog.Info("SendDownloadLinkID finish:" + sendtoUserStudy.ToJson());


                client.Close();

                if(!string.IsNullOrEmpty(sExcept) || sucCount==0)
                {
                    string sSucc = string.Empty;

                    if (sucCount > 0 )
                        sSucc = "Successful:" + sucCount.ToString() +"\r\n";

                    return Error(sSucc + "Send failed:"+ failCount.ToString()+" ,cannot find patient account medical number:"+sExcept+" in VNA");
                }
            }
            catch (Exception ex)
            {
                FileLog.Error(ex.Message + " " + ex.StackTrace + " " + ex.Source);
                return Error(ex.Message);
            }
            return Success("success");
        }

        #region Tab SendUploadLink

        public ActionResult SendUploadLink()
        {
            OperatorModel operatorProvider = OperatorProvider.Provider.GetCurrent();
            if (operatorProvider == null)
                return Error("no login", JsonRequestBehavior.AllowGet);

            string userid = operatorProvider.UserId;

            VnaDbContext vnaDbContext = new VnaDbContext();
            IDepartmentRepository departmentRepository = new DepartmentRepository(vnaDbContext);

            List<Department> departments = departmentRepository.GetDepartmentList();

            string sAutho = operatorProvider.Authority;

            bool bUploadRight = RoleActor.HasAuthorityRights(sAutho, AuthorityRights.SendUpload);
            if (!bUploadRight)
            {
                //
                if (sAutho == "0" || string.IsNullOrEmpty(sAutho))
                {
                    IRoleRepository roleRepository = new RoleRepository(vnaDbContext);
                    DataModel.Role role = roleRepository.GetRole(operatorProvider.RoleId);
                    //
                    if (role != null)
                        bUploadRight = role.send_enable;
                    else
                    {
                        BaseData.Role rolea = BaseData.RoleActor.GetRole(operatorProvider.RoleId);
                        if (rolea == BaseData.Role.Super)
                            bUploadRight = true;

                    }
                }
                if (!bUploadRight)
                    return View("NoRight");
            }

            ViewBag.departments = departments;
            ViewBag.user_id_id = operatorProvider.UserCode;

            return View();
        }

        [HttpPost]
        public ActionResult GetPatients(SearchModel model)
        {
            VnaDbContext vnaDbContext = new VnaDbContext();
            IUserRepository userRepository = new UserRepository(vnaDbContext);
            //

            Pagination pagination = new Pagination();
            pagination.rows = model.pageLimit;
            if (model.pageIndex <= 0)
                model.pageIndex = 1;
            pagination.page = model.pageIndex;

            if (string.IsNullOrEmpty(model.Order))
                model.Order = "asc";

            if (string.IsNullOrEmpty(model.SortBy) || model.SortBy == "1")
            {
                model.SortBy = "medical_recno";
                //order = "asc";
            }
            else if (model.SortBy == "2")
                model.SortBy = "id_number";
            else if (model.SortBy == "3")
                model.SortBy = "user_name";

            pagination.sord = model.Order;// "asc";
            pagination.sidx = model.SortBy;// "";

            bool bf = false;
            string stxt = "select * from users ";


            StringBuilder sql = new StringBuilder(stxt);// 
            List<DbParameter> dbParameters = new List<DbParameter>();
            if (!string.IsNullOrEmpty(model.MedicalNo))
            {
                if (!bf)
                    sql.Append(" where ");

                sql.Append("medical_recno= @medical_recno");

                SqlParameter sqlParameter = new SqlParameter("medical_recno", model.MedicalNo);
                dbParameters.Add(sqlParameter);
                bf = true;
            }
            if (!string.IsNullOrEmpty(model.NativeID))
            {
                if (!bf)
                    sql.Append(" where ");
                if (bf)
                    sql.Append(" and ");

                sql.Append("id_number=@id_number");
                SqlParameter sqlParameter = new SqlParameter("id_number", model.NativeID);
                dbParameters.Add(sqlParameter);
                bf = true;
            }
            if (!string.IsNullOrEmpty(model.Name))
            {
                if (!bf)
                    sql.Append(" where ");
                if (bf)
                    sql.Append(" and ");

                sql.Append("user_name like @user_name");
                SqlParameter sqlParameter = new SqlParameter("user_name", "%" + model.Name + "%");
                dbParameters.Add(sqlParameter);
                bf = true;
            }
            if (!string.IsNullOrEmpty(model.Dept))
            {
                if (!bf)
                    sql.Append(" where ");
                if (bf)
                    sql.Append(" and ");

                sql.Append("department_id =@department_id");
                SqlParameter sqlParameter = new SqlParameter("department_id", model.Dept);
                dbParameters.Add(sqlParameter);
                bf = true;
            }
            //
            IDepartmentRepository departmentRepository = new DepartmentRepository(vnaDbContext);
            List<Department> departments = departmentRepository.GetDepartmentList();//.FindList<Department>("select * from department");

            List<Users> userRegs = userRepository.FindList<Users>(sql.ToString(), dbParameters.ToArray(), pagination);
            if (userRegs != null && departments != null)
            {
                //为了返回department name...
                foreach (Users user in userRegs)
                {
                    string sdeptid = user.department_id;
                    if (string.IsNullOrEmpty(sdeptid))
                        continue;

                    Department ldept = departments.Find(t => t.department_id == user.department_id);
                    if (ldept != null)
                        user.department_id = user.department_id + "$" + ldept.department_name;

                }
            }

            if (userRegs != null)
            {
                var uploadret = new { pageCount = pagination.total, datas = userRegs };
                return Success("success", uploadret);//uploadItems
                //return PartialView("_SendUploadLink", userRegs);
            }
            return Error("error");
            //return PartialView("_SendUploadLink", userRegs);
        }

        [HttpPost]
        public ActionResult SendUploadLinkID(string[] userids, string expire,string creator)
        {
            if (userids == null || userids.Length == 0)
                return Error("useris is empty.");
          
            List<string> lstUsers = new List<string>();
            lstUsers.AddRange(userids);
            
            VnaDbContext vnaDbContext = new VnaDbContext();
            IUserRepository userRepository = new UserRepository(vnaDbContext);

     

            Dictionary<string, bool> dicInitial = new Dictionary<string, bool>();
            Dictionary<string, string> dicpass = new Dictionary<string, string>();

            //verify..
           
                for (int i = lstUsers.Count - 1; i >= 0; --i)
                {
                    Users users = userRepository.GetValidUser(lstUsers[i]);
                    if (users == null || users.delete_mark)
                    {
                        lstUsers.RemoveAt(i); //not valid..
                        continue;
                    }
                    //
                    dicInitial.Add(users.user_id, users.is_initial);
                    dicpass.Add(users.user_id, users.user_password);
                }
                if (lstUsers.Count == 0)
                    return Error("no valid user");

            

            IUplinkRepository uplinkRepository = new UplinkRepository(vnaDbContext);
            int creator_id = 0;
            int.TryParse(creator, out creator_id);

            DateTime dtexpire = DateTime.Now.AddDays(7);

            //Expiry time ends with d or m, indicating days or months..
            if (string.IsNullOrEmpty(expire))
                expire = "7d";
            expire = expire.ToLower();
            if( expire.Last()=='d')
            {
                expire = expire.Substring(0, expire.Length - 1);
                int iexpire = 7;
                int.TryParse(expire, out iexpire);
                //
                dtexpire = DateTime.Now.AddDays(iexpire);
            }
            else if(expire.Last() =='m')
            {
                expire = expire.Substring(0, expire.Length - 1);
                int iexpire = 7;
                int.TryParse(expire, out iexpire);
                //
                dtexpire = DateTime.Now.AddMonths(iexpire);
            }
            else if (expire.Last() == 'h')
            {
                expire = expire.Substring(0, expire.Length - 1);
                int iexpire = 7;
                int.TryParse(expire, out iexpire);
                //
                dtexpire = DateTime.Now.AddHours(iexpire);
            }

            try
            {
              
                for (int i = 0; i < lstUsers.Count; ++i)
                {
                    string reqid = Common.Common.GuId();

                    Uplink uplink = new Uplink() { creator_id = creator_id, expire_time =dtexpire, is_valid = true, uplink_uid = reqid, user_id = lstUsers[i]};

                    uplinkRepository.InsertUplink(uplink);

                    uplinkRepository.Commit();
                    //
                    SendUplinkMail(lstUsers[i], reqid, dicInitial[lstUsers[i]], dicpass[lstUsers[i]]);
                }
            }
            catch(Exception ex)
            {
                return Error(ex.Message);
            }
            return Success("success");
        }
        #endregion

        void SendDownloadlinkMail(string user_email, string reqid, bool binitial, string spass)
        {

            StringBuilder contentBuilder = new StringBuilder();
            contentBuilder.Append("Please click the link to ");
            string surl = Url.Action("DownloadFiles", "Home");//http://localhost:53414/User/ResetPassword?user_email=

            //surl =  Configs.GetValue("SitePrefix") + surl;
            VNAConfigSection vna = VNAConfigSection.Instance;
            surl = vna.SiteElement.Home + surl;
            if (!surl.ToLower().Contains("http"))
            {
                surl = "http://" + surl;
            }

            contentBuilder.Append("<a href='" + surl + "?DownloadLinkUid=" + reqid + "'>Download</a> your VNA files.");

            if (!binitial)
            {
                //The first time the user does not know the password .. ,, but the password here is encrypted, so the user cannot use it。。。。
                //191127先不发送了...
                contentBuilder.Append("</br> ");
                //contentBuilder.Append(" User account:" + user_email + " Password:" + spass + "");
            }
            string sbody = contentBuilder.ToString();

            string stitle = "Download File";

            SendMailInfo(user_email, stitle, sbody);
        }
        /// <summary>
        /// Send upload link email
        /// Click on the address in the email before the patient can upload the file
        /// </summary>
        /// <param name="user_email"></param>
        void SendUplinkMail(string user_email,string reqid,bool binitial,string spass)
        {

            StringBuilder contentBuilder = new StringBuilder();
            contentBuilder.Append("Please click the link to ");
            string surl = Url.Action("UploadFiles", "Home");//http://localhost:53414/User/ResetPassword?user_email=

            //surl =  Configs.GetValue("SitePrefix") + surl;
            VNAConfigSection vna = VNAConfigSection.Instance;
            surl = vna.SiteElement.Home + surl;
            if (!surl.ToLower().Contains("http"))
            {
                surl = "http://" + surl;
            }

            contentBuilder.Append("<a href='" + surl + "?UpLinkUid=" + reqid + "'>Upload</a> your VNA files.");
            
            if( !binitial)
            {
                //contentBuilder.Append("</br> ");
                //contentBuilder.Append(" User account:" + user_email + "Initial password:" +"111111"  +"");
            }
            string sbody = contentBuilder.ToString();

            string stitle = "Upload File";

            SendMailInfo(user_email, stitle, sbody);
        }
        void SendMailInfo(string user_email,string stitle,string sbody)
        {
            VNAConfigSection vna = VNAConfigSection.Instance;
            string smtpHost = vna.emailElement.SmtpHost;
            string sloginId = vna.emailElement.SmtpLoginId;
            string sloginPass = vna.emailElement.SmtpLoginPasswd;
            string senderName = vna.emailElement.SenderName;
            string sfrommail = vna.emailElement.SenderAddr;

            mailHelper.MailName = senderName;// "VNA Administrator";
            mailHelper.MailPassword = sloginPass;// "t#s%b7";
            mailHelper.MailServer = smtpHost;// "ebmtech.com";
            mailHelper.MailUserName = sloginId;// "sbfh";
            //
          
            int port = 25;
            //
            mailHelper.SendByThread(user_email, stitle, sfrommail, sbody, port);
        }


    
        public ActionResult UploadFiles(string UpLinkUid)
        {
            OperatorModel operatorProvider = OperatorProvider.Provider.GetCurrent();
            if (operatorProvider != null)
            {
                VnaDbContext vnaDbContext = new VnaDbContext();
                IDepartmentRepository ideptrep = new DepartmentRepository(vnaDbContext);
                List<Department> departments = ideptrep.GetDepartmentList();
                int iuserid_id = 0;
                int.TryParse(operatorProvider.UserCode, out iuserid_id);
                IUserAuthorDeptRepository userAuthorDeptRepository = new UserAuthorDeptRepository(vnaDbContext);
                List<UserAuthorDept> userAuthorDepts = userAuthorDeptRepository.GetUserAuthorDept(iuserid_id);
                if(userAuthorDepts != null && userAuthorDepts.Count >0 && 
                    departments!=null && departments.Count>0)
                {
                    int ic = departments.Count;
                    for(int ii=ic-1; ii>=0;--ii)
                    {
                        Department ld = departments[ii];
                        UserAuthorDept ud = userAuthorDepts.Find(t => t.dept_id_id == ld.id);
                        if(ud == null)
                        {
                            departments.RemoveAt(ii);
                            continue;
                        }
                    }
                }
                ViewBag.departments = departments;

                if (RoleActor.GetRole(operatorProvider.RoleId) == BaseData.Role.Patient)
                {
                    if (!string.IsNullOrEmpty(UpLinkUid))
                        Session["UpLinkUid"] = UpLinkUid;

                    string curLinkuid = Session["UpLinkUid"] as string;

                    if (string.IsNullOrEmpty(curLinkuid))
                    {
                        return View("NoRight");// Error("The link is not valid.") ;
                    }
                    
                    //check linkuid is not expire..
                    IUplinkRepository uplinkRepository = new UplinkRepository(vnaDbContext);
                    Uplink uplink = uplinkRepository.FindEntity<Uplink>(t => t.uplink_uid == curLinkuid);

                    if(uplink == null || !uplink.is_valid || uplink.expire_time < DateTime.Now)
                    {
                        if (uplink != null && uplink.is_valid)
                        {
                            uplink.is_valid = false;

                            uplinkRepository.UpdateUplink(uplink);
                            uplinkRepository.Commit();
                        }

                        return View("NoRight");
                    }
                    string sAutho = operatorProvider.Authority;

                    bool bUploadRight = RoleActor.HasAuthorityRights(sAutho, AuthorityRights.Upload);
                    if(!bUploadRight)
                    {
                        //
                        if (sAutho == "0" || string.IsNullOrEmpty(sAutho))
                        {
                            IRoleRepository roleRepository = new RoleRepository(vnaDbContext);
                            DataModel.Role role = roleRepository.GetRole(operatorProvider.RoleId);
                            //
                            if (role != null)
                                bUploadRight = role.upload_enable;
                        }
                        if(!bUploadRight)
                        return View("NoRight"); //Content("You have no permission.");
                    }
                   // VnaDbContext vnaDbContext = new VnaDbContext();
                    IUserRepository userRepository = new UserRepository(vnaDbContext);
                    Users users = userRepository.GetUser(operatorProvider.UserId);

                    FileLog.Debug("open PatientUpload:" + users.ToJson());
                    return View("PatientUpload", users);
                }
                else if (RoleActor.GetRole(operatorProvider.RoleId) == BaseData.Role.Nurse)
                {
                    ViewBag.roleid = ((int)RoleActor.GetRole(operatorProvider.RoleId)).ToString();// "1";
                    //ViewBag.department = operatorProvider.DepartmentId;

                    string sAutho = operatorProvider.Authority;

                    bool bUploadRight = RoleActor.HasAuthorityRights(sAutho, AuthorityRights.Upload);
                    if (!bUploadRight)
                    {
                        //
                        if (sAutho == "0" || string.IsNullOrEmpty(sAutho))
                        {
                            IRoleRepository roleRepository = new RoleRepository(vnaDbContext);
                            DataModel.Role role = roleRepository.GetRole(operatorProvider.RoleId);
                            //
                            if (role != null)
                                bUploadRight = role.upload_enable;
                        }
                        if (!bUploadRight)
                            return View("NoRight"); //Content("You have no permission.");
                    }
                    IUserRepository userRepository = new UserRepository(vnaDbContext);
                    Users users = userRepository.GetUser(operatorProvider.UserId);

                    FileLog.Debug("open UploadFiles:" + users.ToJson());

                    return View();//View("PatientUpload", users);
                }
                else if(RoleActor.GetRole(operatorProvider.RoleId) == BaseData.Role.Doctor)
                {
                    ViewBag.roleid = ((int)RoleActor.GetRole(operatorProvider.RoleId)).ToString();// "2";
                    //ViewBag.department = operatorProvider.DepartmentId;

                    string sAutho = operatorProvider.Authority;

                    bool bUploadRight = RoleActor.HasAuthorityRights(sAutho, AuthorityRights.Upload);
                    if (!bUploadRight)
                    {
                        //
                        if (sAutho == "0" || string.IsNullOrEmpty(sAutho))
                        {
                            IRoleRepository roleRepository = new RoleRepository(vnaDbContext);
                            DataModel.Role role = roleRepository.GetRole(operatorProvider.RoleId);
                            //
                            if (role != null)
                                bUploadRight = role.upload_enable;
                        }
                        if (!bUploadRight)
                            return View("NoRight"); //Content("You have no permission.");
                    }
                    IUserRepository userRepository = new UserRepository(vnaDbContext);
                    Users users = userRepository.GetUser(operatorProvider.UserId);

                    return View();//View("PatientUpload", users);

                }
                else if (RoleActor.GetRole(operatorProvider.RoleId) == BaseData.Role.Manager)
                {
                    ViewBag.roleid = ((int)RoleActor.GetRole(operatorProvider.RoleId)).ToString();// "3";
                    //ViewBag.department = operatorProvider.DepartmentId;
                    string sAutho = operatorProvider.Authority;

                    bool bUploadRight = RoleActor.HasAuthorityRights(sAutho, AuthorityRights.Upload);
                    if (!bUploadRight)
                    {
                        //
                        if (sAutho == "0" || string.IsNullOrEmpty(sAutho))
                        {
                            IRoleRepository roleRepository = new RoleRepository(vnaDbContext);
                            DataModel.Role role = roleRepository.GetRole(operatorProvider.RoleId);
                            //
                            if (role != null)
                                bUploadRight = role.upload_enable;
                        }
                        if (!bUploadRight)
                            return View("NoRight"); //Content("You have no permission.");
                    }

                    IUserRepository userRepository = new UserRepository(vnaDbContext);
                    Users users = userRepository.GetUser(operatorProvider.UserId);

                    return View();//View("PatientUpload", users);
                }
                else if (RoleActor.GetRole(operatorProvider.RoleId) == BaseData.Role.Admin)
                {
                    ViewBag.roleid = ((int)RoleActor.GetRole(operatorProvider.RoleId)).ToString();//"100";
                    //ViewBag.department = operatorProvider.DepartmentId;
                    string sAutho = operatorProvider.Authority;

                    bool bUploadRight = RoleActor.HasAuthorityRights(sAutho, AuthorityRights.Upload);
                    if (!bUploadRight)
                    {
                        //
                        if (sAutho == "0" || string.IsNullOrEmpty(sAutho))
                        {
                            IRoleRepository roleRepository = new RoleRepository(vnaDbContext);
                            DataModel.Role role = roleRepository.GetRole(operatorProvider.RoleId);
                            //
                            if (role != null)
                                bUploadRight = role.upload_enable;
                            else
                            {
                                //If not ... additionally restricted admin permissions ...
                                BaseData.Role rolea = BaseData.RoleActor.GetRole(operatorProvider.RoleId);
                                if (rolea == BaseData.Role.Super || rolea == BaseData.Role.Admin)
                                    bUploadRight = true;

                            }
                        }
                        if (!bUploadRight)
                            return View("NoRight"); //Content("You have no permission.");
                    }

                    IUserRepository userRepository = new UserRepository(vnaDbContext);
                    Users users = userRepository.GetUser(operatorProvider.UserId);

                    return View();// View("PatientUpload", users);
                }
                else if (RoleActor.GetRole(operatorProvider.RoleId) == BaseData.Role.Super)
                {
                    ViewBag.roleid = ((int)RoleActor.GetRole(operatorProvider.RoleId)).ToString();//"100";
                                                                                                  //ViewBag.department = operatorProvider.DepartmentId;

                    IUserRepository userRepository = new UserRepository(vnaDbContext);
                    Users users = userRepository.GetUser(operatorProvider.UserId);
                    return View();//
                }
            }

            return View();
        }
        
        public ActionResult DownloadFiles(string DownloadLinkUid, string s, string o)//sortBy,order
        {
            bool bRsna = false;// true;
            string sortBy = s;
            string order = o;

            OperatorModel operatorProvider = OperatorProvider.Provider.GetCurrent();
            if (!bRsna && operatorProvider == null)
                return View("NoRight");

            if (!string.IsNullOrEmpty(DownloadLinkUid))
                Session["DownloadLinkUid"] = DownloadLinkUid;

            string curLinkuid = Session["DownloadLinkUid"] as string;

            if (string.IsNullOrEmpty(curLinkuid))
            {
                return View("NoRight");// Error("The link is not valid.") ;
            }

            VnaDbContext vnaDbContext = new VnaDbContext();
            //check linkuid is not expire..
            IDownloadRepository uplinkRepository = new DownloadRepository(vnaDbContext);
            Download uplink = null;// uplinkRepository.FindEntity<Download>(t => t.download_uid == curLinkuid);
            //
            List<Download> downloads = uplinkRepository.FindList<Download>("select * from download where download_uid=@uid", new DbParameter[] { new SqlParameter("uid", curLinkuid) });
            if (downloads != null && downloads.Count > 0)
                uplink = downloads[0];

            if (downloads == null)
                return View("NoRight");

            if (!bRsna)
            {


                //一 One download_uid corresponds to multiple download ...
                if (uplink == null || !uplink.is_valid || uplink.expire_time < DateTime.Now)
                {
                    if (uplink != null && uplink.is_valid)
                    {
                        if (downloads != null)
                        {
                            foreach (Download down in downloads)
                            {
                                down.is_valid = false;

                                uplinkRepository.UpdateDownload(down);
                            }
                        }
                        uplinkRepository.Commit();
                    }

                    return View("NoRight");
                }

                string sAutho = operatorProvider.Authority;

                bool bUploadRight = RoleActor.HasAuthorityRights(sAutho, AuthorityRights.DownloadFile);
                if (!bUploadRight)
                {
                    //
                    if (sAutho == "0" || string.IsNullOrEmpty(sAutho))
                    {
                        IRoleRepository roleRepository = new RoleRepository(vnaDbContext);
                        DataModel.Role role = roleRepository.GetRole(operatorProvider.RoleId);
                        //
                        if (role != null)
                            bUploadRight = role.download_enable;
                        else
                        {
                            //If not ... additional restrictions on super's permissions ...
                            BaseData.Role rolea = BaseData.RoleActor.GetRole(operatorProvider.RoleId);
                            if (rolea == BaseData.Role.Super)
                                bUploadRight = true;

                        }
                    }
                    if (!bUploadRight)
                        return View("NoRight"); //Content("You have no permission.");
                }

                //
            }

            List<string> uids = new List<string>();
            foreach (Download down in downloads)
                uids.Add(down.study_uid);

            FileLog.Info("call QueryDownload");
            //Direct query backend vna service..
            VNAServiceReference1.VNAService1Client vNAService1Client = new VNAServiceReference1.VNAService1Client();
            VNAServiceReference1.QueryDownloadReturn[] queryDownloadReturns = vNAService1Client.QueryDownload(uids.ToArray());
            //
            FileLog.Info(" QueryDownload return :" + queryDownloadReturns==null?"null" : queryDownloadReturns.ToJson());

            if (queryDownloadReturns == null)
                queryDownloadReturns = new VNAServiceReference1.QueryDownloadReturn[] { };
            if (string.IsNullOrEmpty(order))
                order = "asc";
            if (order=="asc"||order=="a")
            {

                if (string.IsNullOrEmpty(sortBy) || sortBy == "1")
                {
                    //sortBy = "upload_date";
                    queryDownloadReturns = queryDownloadReturns.OrderBy(n => n.upload_date).ToArray();
                }
                else if (sortBy == "2")
                    queryDownloadReturns = queryDownloadReturns.OrderBy(n => n.medical_num).ToArray(); //sortBy = "medical_num";
                else if (sortBy == "3")
                    queryDownloadReturns = queryDownloadReturns.OrderBy(n => n.org_filename).ToArray(); //sortBy = "org_filename";
                else if (sortBy == "4")
                    queryDownloadReturns = queryDownloadReturns.OrderBy(n => n.ref_physician).ToArray(); //sortBy = "ref_physician";
                
            }
            else
            {
                if (string.IsNullOrEmpty(sortBy) || sortBy == "1")
                {
                    queryDownloadReturns = queryDownloadReturns.OrderByDescending(n => n.upload_date).ToArray();
                }
                else if (sortBy == "2")
                    queryDownloadReturns = queryDownloadReturns.OrderByDescending(n => n.medical_num).ToArray(); //
                else if (sortBy == "3")
                    queryDownloadReturns = queryDownloadReturns.OrderByDescending(n => n.org_filename).ToArray(); //
                else if (sortBy == "4")
                    queryDownloadReturns = queryDownloadReturns.OrderByDescending(n => n.ref_physician).ToArray(); //

            }
            // ViewBag.queryReturn = queryDownloadReturns;
            ViewBag.curUid = DownloadLinkUid;

            return View(queryDownloadReturns);
        }
        //https://stackoverflow.com/questions/16287706/how-to-upload-large-files-using-mvc-4

        /// <summary>
        /// 上传header信息...
        /// </summary>
        /// <param name="upload"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult UploadId(List<UploadModel> uploads)
        {
            var obj = new { state = "0", message = "false", upload_id = "" };
            if (uploads == null)
                return Json(obj);

            //Return a guid.. as uploadid..
            //Use guid as a directory name ..

            //First save it in a session without writing the database first.
                        string sUpSessionid = Session["uploadId"] as string;

            if (string.IsNullOrEmpty(sUpSessionid))
            {
                string sguid = Common.Common.GuId();
                Session["uploadId"] = sguid;
                sUpSessionid = sguid;
            }
            List<string> strFlies = new List<string>();
            if (uploads != null)
            {
                foreach (UploadModel up in uploads)
                {
                    up.upload_id = sUpSessionid;
                    strFlies.AddRange(up.upload_files);
                }
            }
            List<UploadModel> uploadModels = Session[sUpSessionid] as List<UploadModel>;
            if (uploadModels != null)
                uploadModels.AddRange(uploads);
            else
                uploadModels = uploads;

            Session[sUpSessionid] = uploadModels;
            UploadReturns uploadReturns = new UploadReturns();
            uploadReturns.state = "1";
            uploadReturns.message = "success";
            uploadReturns.upload_id = sUpSessionid;
            uploadReturns.upload_files = strFlies;
            obj = new { state = "1", message = "success", upload_id = sUpSessionid };

            return Json(obj);//obj uploadReturns
        }
        public class UploadReturns
        {
            public string state { get; set; }
            public string message { get; set; }
            public string upload_id { get; set; }
            public List<string> upload_files { get; set; }
        }
        [HttpPost]
        public ActionResult MultiUpload(string chunkNumber, string fileName,string uploadId)
        {
            var obj = new { state = "0", message = "false" };
            if (string.IsNullOrEmpty(uploadId) || string.IsNullOrEmpty(chunkNumber) || string.IsNullOrEmpty(fileName))
                return Json(obj);

            //...........................
            //fileName = Server.UrlDecode(fileName);
            fileName = Uri.UnescapeDataString(fileName);

            // Remove full path
            int ildx = fileName.LastIndexOf("\\");
            if(ildx >=0 )
            {
                fileName = fileName.Substring(ildx + 1);
            }
          //  var chunkNumber = id;
            var chunks = Request.InputStream;
            string path = Server.MapPath(msTempPath);// msFilePath + "/Temp");

            OperatorModel opuser = OperatorProvider.Provider.GetCurrent();
            string suserid = opuser.UserId;
            path = Path.Combine(path, suserid);
            path = Path.Combine(path, uploadId);

            if (!Directory.Exists(path))
                Directory.CreateDirectory(path);


            string newpath = Path.Combine(path, fileName + chunkNumber);
            using (FileStream fs = System.IO.File.Create(newpath))
            {
                byte[] bytes = new byte[3757000];
                int bytesRead;
                while ((bytesRead = Request.InputStream.Read(bytes, 0, bytes.Length)) > 0)
                {
                    fs.Write(bytes, 0, bytesRead);
                }
            }
            return Json(obj);
        }

        [HttpPost]
        public ActionResult UploadComplete(string fileName, string complete,string uploadId)
        {
            var obj = new { state = "0", message = "false" };
            if (string.IsNullOrEmpty(uploadId) || string.IsNullOrEmpty(fileName))
                return Json(obj);

            //fileName = Server.UrlDecode(fileName);
            fileName = Uri.UnescapeDataString(fileName);
            //Remove full path
            int ildx = fileName.LastIndexOf("\\");
            if (ildx >= 0)
            {
                fileName = fileName.Substring(ildx + 1);
            }

            string tempPath = Server.MapPath(msTempPath);// msFilePath + "/Temp");
            string realPath = Server.MapPath(msFilePath);

            try
            {
                FileLog.Info("uploadComplete: " + fileName +"\r\n");

                OperatorModel opuser = OperatorProvider.Provider.GetCurrent();
                string suserid = opuser.UserId;
                tempPath = Path.Combine(tempPath, suserid);
                tempPath = Path.Combine(tempPath, uploadId);

                realPath = Path.Combine(realPath, suserid);
                realPath = Path.Combine(realPath, uploadId);

                if (!Directory.Exists(realPath))
                    Directory.CreateDirectory(realPath);

                string newPath = Path.Combine(tempPath, fileName);
                if (complete == "1")
                {
                    string[] filePaths = Directory.GetFiles(tempPath).Where(p => p.Contains(fileName)).OrderBy(p => Int32.Parse(p.Replace(fileName, "$").Split('$')[1])).ToArray();
                    foreach (string filePath in filePaths)
                    {
                        MergeFiles(newPath, filePath);
                    }
                }
                System.IO.File.Move(Path.Combine(tempPath, fileName), Path.Combine(realPath, fileName));
                //
                //Judge whether all are uploaded, but this judgment is not safe, because the user may cancel the file after selecting it, and then cancel
                //When canceling, the file list in the session is not canceled ...
                //So you need to change the method. When the upload is successful, convert the dicom and store eps instead of waiting for all uploads ...
                //This is not easy to have out of sync ...
                if (complete == "1")
                FinishFileUploaded(uploadId, fileName);
                /*
                List<string> lstFinished = Session[uploadId + "Finished"] as List<string>;
                if (lstFinished == null)
                {
                    lstFinished = new List<string>();
                    //
                    Session[uploadId + "Finished"] = lstFinished;
                }
                lstFinished.Add(fileName);

                List<UploadModel> uploadModels = Session[uploadId] as List<UploadModel>;
                if(uploadModels != null)
                {
               

                    bool empty = true;

                    foreach(UploadModel model in uploadModels)
                    {
                        List<string> lisfiles = model.upload_files;

                        if (!empty)
                            break;

                        if(lisfiles != null)
                        {
                            for(int ii = 0; ii < lisfiles.Count;++ii )
                            {

                                string sfs = lisfiles[ii];
                                int idxx = sfs.LastIndexOf("\\");
                                if (idxx >= 0)
                                {
                                    sfs = sfs.Substring(ildx + 1);
                                }
                                //
                                if( !lstFinished.Contains(sfs))
                                {

                                    empty = false;
                                    break;
                                }
                            }
                        }
                 
                    }

                    if(empty)
                    {
                        //all upload

                        AllUploaded(uploadId);
                    }
                }
                */
            }
            catch(Exception ex)
            {
                return Error(ex.Message);
            }


            return Success("success");// Json(new {state="1",message ="success" });
        }
        /// <summary>
        /// Wait for external call ... If you know when the session is over, it doesn't matter if you don't call, because after each file is successful, the file name will be deleted
        /// </summary>
        /// <param name="upload_sessionId"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult FinishUploadSession(string upload_sessionId)
        {
            if( Session["uploadId"] as string == upload_sessionId )
                Session["uploadId"] = null;

            Session[upload_sessionId] = null;
            // Session[uploadId + "Finished"] = null;
            return Success("success");
        }
       // [HttpPost]
        //public 
        ActionResult AllUploaded( string uploadId)
        {
            OperatorModel opuser = OperatorProvider.Provider.GetCurrent();
            
            if (opuser == null)
                return Error("not login.");
            string suserid = opuser.UserId;

            try
            {
                FileLog.Info("AllUploaded: " + uploadId + "\r\n");

                List<UploadModel> uploadModels = Session[uploadId] as List<UploadModel>;
                if (uploadModels != null && uploadModels.Count > 0)
                {
                    //
                    VnaDbContext vnaDbContext = new VnaDbContext();
                    IUserRepository userRepository = new UserRepository(vnaDbContext);
                    Users users = userRepository.GetValidUser(suserid);
                    //
                    string realPath = Server.MapPath(msFilePath);
                    realPath = Path.Combine(realPath, suserid);
                    realPath = Path.Combine(realPath, uploadId);



                    foreach (UploadModel model in uploadModels)
                    {
                        string studyuid = string.Empty;
                        string seriesuid = string.Empty;

                        studyuid = DicomKit.DicomTool.GenerateStudyInstanceUID("300", "100");
                        seriesuid = DicomKit.DicomTool.GenerateStudyInstanceUID("400", "100");
                        //
                        IUploadRepository uploadRepository = new UploadRepository(vnaDbContext);
                        //
                        Upload upload = new Upload();
                        upload.upload_id = Common.Common.GuId();
                        upload.user_id_id = users.user_id_id;
                        upload.upload_sessionid = uploadId;

                        upload.study_uid = studyuid; //Here first write study uid, series uid to DB
                        upload.series_uid = seriesuid;

                        upload.comment = model.comment;
                        upload.create_date = DateTime.Now;
                        upload.first_name = model.first_name;
                        upload.last_name = model.last_name;
                        upload.medical_num = model.medical_num;
                        upload.middle_name = model.middle_name;
                        upload.native_id = model.native_id;
                        upload.ref_physician = model.ref_physician;
                        upload.report_type = model.report_type;

                        upload.source_dept = model.source_dept; //Department from which the file came
                                                                //upload.study_uid;

                        uploadRepository.InsertUpload(upload);
                        uploadRepository.Commit();  //save The self-increasing variable in the upload will be automatically assigned to the value in the DB
                                                    //refresh data from db，get identity field..
                                                    // DbPropertyValues dbPropval = vnaDbContext.Entry<Upload>(upload).GetDatabaseValues();
                                                    // upload.upload_id_id = dbPropval.GetValue<int>("upload_id_id");
                                                    // upload = uploadRepository.GetUpload(upload.upload_id);

                        //
                        IUploadItemRepository uploadItemRepository = new UploadItemRepository(vnaDbContext);
                        //事务放这里..Business is here
                                                uploadItemRepository.BeginTrans();


                        List<string> strfile = model.upload_files;
                        foreach (string sfile in strfile)
                        {
                            UploadItem uploadItem = new UploadItem();
                            //
                            string lfile = sfile;
                            int ildx = lfile.LastIndexOf("\\");
                            if (ildx >= 0)
                            {
                                lfile = lfile.Substring(ildx + 1);
                            }

                            uploadItem.upload_item_id = Common.Common.GuId();
                            // uploadItem.upload_id = upload.upload_id;
                            uploadItem.upload_id_id = upload.upload_id_id;
                            uploadItem.dest_file_name = Path.Combine(realPath, lfile);
                            uploadItem.instance_uid = "";
                            uploadItem.src_file_name = lfile;
                            uploadItem.status = "";
                            uploadItem.upload_at = DateTime.Now;

                            uploadItemRepository.InsertUpload(uploadItem);
                        }

                        //
                        uploadItemRepository.Commit();
                    }

                    //
                    FileLog.Info("TransferDicomToEps: " + uploadId + "\r\n");

                    //To dicom
                    TransferDicomToEps(uploadId);


                    Session["uploadId"] = null;
                    Session[uploadId] = null;
                    Session[uploadId + "Finished"] = null;
                }


            }
            catch (Exception ex)
            {
              
                //FileLog.Error(ex.Source + ex.StackTrace + ex.Message);

                return Error(ex.Message);
            }
            return Success("success");
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="uploadId">session_uid</param>
        /// <param name="fileName"></param>
        /// <returns></returns>
        ActionResult FinishFileUploaded(string uploadId,string fileName)
        {
            OperatorModel opuser = OperatorProvider.Provider.GetCurrent();

            if (opuser == null)
                return Error("not login.");
            string suserid = opuser.UserId;

            try
            {
                FileLog.Info("FinishFileUploaded: " + uploadId + " " + fileName);

                List<UploadModel> uploadModels = Session[uploadId] as List<UploadModel>;
                if (uploadModels != null && uploadModels.Count > 0)
                {
                    //
                    VnaDbContext vnaDbContext = new VnaDbContext();
                    IUserRepository userRepository = new UserRepository(vnaDbContext);
                    Users users = userRepository.GetValidUser(suserid);
                    //
                    string realPath = Server.MapPath(msFilePath);
                    realPath = Path.Combine(realPath, suserid);
                    realPath = Path.Combine(realPath, uploadId);

                    UploadModel uploadModel = null;

                    foreach (UploadModel mod in uploadModels)
                    {
                        if (mod != null && mod.upload_files != null &&
                            mod.upload_files.Contains(fileName))
                        {
                            uploadModel = mod;
                            //

                            break;
                        }
                    }
                    if (uploadModel == null)
                    {
                        FileLog.Info("model is null :" + uploadId + " " + fileName + "");
                        return Error("model is null");
                    }
                    UploadModel model = uploadModel;


                    string studyuid = string.Empty;
                    string seriesuid = string.Empty;
                    //If it is a diocm file. . .There is no need to create the studyyuid etc... otherwise the study uid in the upload will not be equal to the study1.
                    string srealFile = Path.Combine(realPath, fileName);
                    bool isdcmFile = DicomKit.DicomTool.IsDicomFile(srealFile);
                    if (isdcmFile)
                    {
                        DicomKit.DicomTool.GetDicomTagInfo(srealFile, 0x0020, 0x000D, ref studyuid);
                        DicomKit.DicomTool.GetDicomTagInfo(srealFile, 0x0020, 0x000E, ref seriesuid);
                    }
                    else
                    {
                        studyuid = DicomKit.DicomTool.GenerateStudyInstanceUID("300", "100");
                        seriesuid = DicomKit.DicomTool.GenerateStudyInstanceUID("400", "100");
                    }
                    //
                    IUploadRepository uploadRepository = new UploadRepository(vnaDbContext);
                    // For the dicom file, if it is the same study, there is only one record in the upload
                    //If there is one record for each file in the same study, the upload_id and upload_sessionid of the same study should be the same
                    //This is to display only one record of the same study when displaying the list ... otherwise the list is too long, which is not conducive to operation ...
                    Upload upload = null;
                    upload = uploadRepository.FindEntity<Upload>(t => t.study_uid == studyuid);
                    if (upload == null)
                    {
                        upload = new Upload();
                        upload.upload_id = Common.Common.GuId();
                        upload.user_id_id = users.user_id_id;
                        upload.upload_sessionid = uploadId;

                        upload.study_uid = studyuid; //Here first write study uid, series uid to DB
                        upload.series_uid = seriesuid;

                        upload.comment = model.comment;
                        upload.create_date = DateTime.Now;
                        upload.first_name = model.first_name;
                        upload.last_name = model.last_name;
                        upload.medical_num = model.medical_num;
                        upload.middle_name = model.middle_name;
                        upload.native_id = model.native_id;
                        upload.ref_physician = model.ref_physician;
                        upload.report_type = model.report_type;

                        upload.source_dept = model.source_dept; //Department from which the file came
                                                                //upload.study_uid;
                        FileLog.Info("start InsertUpload" + upload.ToJson());

                        uploadRepository.InsertUpload(upload);
                        uploadRepository.Commit();  //sAfter saving, the self-increasing variable in upload will be automatically assigned to the value in DB
                        //refresh data from db，get identity field..
                        // DbPropertyValues dbPropval = vnaDbContext.Entry<Upload>(upload).GetDatabaseValues();
                        // upload.upload_id_id = dbPropval.GetValue<int>("upload_id_id");
                        // upload = uploadRepository.GetUpload(upload.upload_id);

                    }
                    //
                    IUploadItemRepository uploadItemRepository = new UploadItemRepository(vnaDbContext);
                    // Business is here
                                        uploadItemRepository.BeginTrans();


                    //List<string> strfile = model.upload_files;
                    //foreach (string sfile in strfile)
                    string sfile = fileName;

                    UploadItem uploadItem = new UploadItem();
                    //
                    string lfile = sfile;
                    int ildx = lfile.LastIndexOf("\\");
                    if (ildx >= 0)
                    {
                        lfile = lfile.Substring(ildx + 1);
                    }

                    uploadItem.upload_item_id = Common.Common.GuId();
                    // uploadItem.upload_id = upload.upload_id;
                    uploadItem.upload_id_id = upload.upload_id_id;
                    uploadItem.dest_file_name = Path.Combine(realPath, lfile);
                    uploadItem.instance_uid = "";
                    uploadItem.src_file_name = lfile;
                    uploadItem.status = "";
                    uploadItem.upload_at = DateTime.Now;

                    FileLog.Info("start InsertUploadItem" + uploadItem.ToJson());

                    uploadItemRepository.InsertUpload(uploadItem);


                    //
                    uploadItemRepository.Commit();

                    FileLog.Info("insert uploadItem :" + uploadId + " " + fileName + "");

                    //Delete the uploaded file name...
                    model.upload_files.Remove(fileName);
                    for (int ii = uploadModels.Count - 1; ii >= 0; ii--)
                    {
                        UploadModel mod = uploadModels[ii];
                        if (mod != null && mod.upload_files != null &&
                            mod.upload_files.Count == 0)
                        {
                            uploadModels.RemoveAt(ii);
                        }
                    }
                    //
                    FileLog.Info("TransferDicomToEps: " + uploadId + " " + fileName);

                    //To dicom
                    // TransferDicomToEps(uploadId);
                    //一次转一个item...
                    TransferFiletoDicomToEps(upload.upload_id, uploadItem.upload_item_id);

                    // See if all uploaded...
                    // Session["uploadId"] = null;
                    // Session[uploadId] = null;
                    // Session[uploadId + "Finished"] = null;
                }
                else
                    FileLog.Error("Session[uploadId] unnormal empty");


            }
            catch (Exception ex)
            {

                FileLog.Error(ex.Source + ex.StackTrace + ex.Message);

                return Error(ex.Message);
            }
            return Success("success");
        }
        private static void MergeFiles(string file1, string file2)
        {
            FileStream fs1 = null;
            FileStream fs2 = null;
            try
            {
                fs1 = System.IO.File.Open(file1, FileMode.Append);
                fs2 = System.IO.File.Open(file2, FileMode.Open);
                byte[] fs2Content = new byte[fs2.Length];
                fs2.Read(fs2Content, 0, (int)fs2.Length);
                fs1.Write(fs2Content, 0, (int)fs2.Length);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message + " : " + ex.StackTrace);
            }
            finally
            {
                if (fs1 != null) fs1.Close();
                if (fs2 != null) fs2.Close();
                System.IO.File.Delete(file2);
            }
        }
        [HttpPost]
        public ActionResult RequestOTP()
        {
            OperatorModel opuser = OperatorProvider.Provider.GetCurrent();
            if (opuser == null)
                return Error("not login");

            string user_id = opuser.UserId;
            //
            VnaDbContext vnaDbContext = new VnaDbContext();
            IUserRepository userRepository = new UserRepository(vnaDbContext);
            Users users = userRepository.GetValidUser(user_id);
            //
            if (users.delete_mark)
                return Error("user is deleted.");
            string smail = users.email;
            //
            string sbody = string.Empty;
            sbody = "The VNA Request OTP Code is: ";
            Random random = new Random();
            int icode = random.Next(1000, 10000);
            sbody += icode.ToString();
            //...............
            Session["RequestOTP"] = icode.ToString();

            SendMailInfo(smail, "VNA Request OTP", sbody);

            //
            return Success("Success, please receive email to get OTP Code");

        }
        [HttpPost]
        public ActionResult VerifyOTP(string OTPCode)
        {
            if (string.IsNullOrEmpty(OTPCode))
                return Error("OTPCode is empty");
            //
            string sotpcode = Session["RequestOTP"] as string;
            //
            if (sotpcode != OTPCode)
                return Error("OTPCode is wrong");

            return Success("success");
        }
        /// <summary>
        /// 一次转一个文件.
        /// </summary>
        /// <param name="uploadId"> upload_ID</param>
        /// <param name="upload_item_id"></param>
        void TransferFiletoDicomToEps(string uploadId,string upload_item_id)
        {
            new Thread(new ThreadStart(delegate ()
            {
                try
                {
                    VnaDbContext vnaDbContext = new VnaDbContext();
                    IUploadRepository uploadRepository = new UploadRepository(vnaDbContext);

                    Upload upload = uploadRepository.GetUpload(uploadId);
                    if (upload == null)
                        return;
                    List<string> lstDcmFiles = new List<string>();

                    //foreach (Upload upload in uploads)
                    {


                        string studyuid = string.Empty;
                        string seriesuid = string.Empty;
                        //Written in advance..
                        studyuid = upload.study_uid;// DicomKit.DicomTool.GenerateStudyInstanceUID("300", "100");
                        seriesuid = upload.series_uid;// DicomKit.DicomTool.GenerateStudyInstanceUID("400", "100");

                        int upload_id_id = upload.upload_id_id;

                        IUploadItemRepository uploadItemRepository = new UploadItemRepository(vnaDbContext);
                        string sql = "select * from upload_item where (status is NULL or status='') and upload_id_id=@idid and upload_item_id=@upload_item_id";
                        List<UploadItem> uploadItems = uploadItemRepository.FindList<UploadItem>(sql, new SqlParameter[] { new SqlParameter("@idid", upload_id_id),
                        new SqlParameter("@upload_item_id", upload_item_id)});

                        FileLog.Info(sql);
                        //patient name->  "Last name^First name^Middle name^Prefix^Suffix"
                        if (uploadItems != null)
                        {
                            int iidx = 0;
                            foreach (UploadItem itm in uploadItems)
                            {
                                iidx++;
                                //
                                string sfile = itm.dest_file_name;
                                //
                                string pid = upload.medical_num;// native_id;
                                string pname = string.Empty;
                                if (!string.IsNullOrEmpty(upload.last_name))
                                {
                                    pname = upload.last_name.TrimStart().TrimEnd();
                                }
                                if (!string.IsNullOrEmpty(upload.first_name))
                                {
                                    if (!string.IsNullOrEmpty(pname))
                                        pname = pname + "^";
                                    pname = pname + upload.first_name.TrimStart().TrimEnd();
                                }
                                //
                                string studydate = string.Empty;
                                string studytime = string.Empty;
                                if (upload.create_date != null)
                                {
                                    studydate = ((DateTime)upload.create_date).ToString("yyyyMMdd");
                                    studytime = ((DateTime)upload.create_date).ToString("HHmmss");
                                }
                                string accessno = "";// upload.medical_num;

                                string studydesc = upload.comment;
                                string refphysician = upload.ref_physician;

                                string instanceuid = DicomKit.DicomTool.GenerateStudyInstanceUID("1000", "100");
                                string sopClassUid = Configs.GetValue("sopclassuid");//private...

                                string studyID = string.Empty;
                                string modality = "DOC"; //OT
                                string instanceNumber = iidx.ToString();
                                string seriesNumber = "1";
                                string pixelspacing = string.Empty;
                                string patientBirthDate = string.Empty;
                                string patientSex = string.Empty;

                                string dcmfile = Path.Combine(Path.GetDirectoryName(sfile), Path.GetFileNameWithoutExtension(sfile)) + ".dcm";

                                FileLog.Info("createImgDcm:" + sfile +":"+dcmfile+":"+studyuid + ":" +pid + ":" +pname + ":" +patientBirthDate
                                    + ":" +patientSex + ":" +studydate + ":" +studytime + ":" +accessno + ":" +studyID + ":" +studydesc + ":" +modality
                                    + ":" +instanceNumber + ":" +seriesuid + ":" +instanceuid + ":" +sopClassUid + ":" +seriesNumber + ":" +
                                    pixelspacing + ":" +refphysician);

                                bool isdcmFile = DicomKit.DicomTool.IsDicomFile(sfile);
                                if (isdcmFile)
                                {
                                    DicomKit.DicomTool.GetDicomTagInfo(sfile, 0x0008, 0x0018, ref instanceuid);
                                }
                                int res = DicomKit.DicomTool.CreateImgDCM(sfile, dcmfile, studyuid, pid, pname, patientBirthDate, patientSex, studydate, studytime, accessno,
                                    studyID, studydesc, modality, instanceNumber, seriesuid, instanceuid, sopClassUid, seriesNumber, pixelspacing, refphysician);


                                if (res == 0)
                                {
                                    if (!System.IO.File.Exists(dcmfile))
                                    {
                                        res = -1; //

                                        FileLog.Info(sfile + " to dcm failed,but return 0 ");
                                    }
                                }
                                if (res == 0)
                                {
                                    
                                    
                                    itm.instance_uid = instanceuid;/////Assignment... Note whether the source file is dicom

                                                                       //成功
                                                                       itm.dest_file_name = dcmfile;
                                    itm.status = Upload_Status.toDcmSuccess;// "1";

                                    FileLog.Info(sfile + " to dcm success ");

                                }
                                else
                                {
                                    itm.status = Upload_Status.toDcmFailed;// "3";//to dicom fail..
                                                                           //  
                                    FileLog.Info(sfile + " to dcm failed ");
                                }


                                //If you want to upload to eps..&&.bStoreToEps
                                if (res == 0  )
                                {
                                    lstDcmFiles.Add(dcmfile);
                                    //
                                    //start store file to eps..
                                    StoreKit storeKit = StoreKit.Instance;
                                    string sLocalAe = VNAConfigSection.Instance.epsElement.LocalAE;
                                    string epsAe = VNAConfigSection.Instance.epsElement.EpsAE, epsIp = VNAConfigSection.Instance.epsElement.EpsIP;
                                    string epsPort = VNAConfigSection.Instance.epsElement.EpsPort;
                                    List<string> lstFailed = new List<string>();

                                    storeKit.storeSopclassuid = Configs.GetValue("sopclassuid");
                                    //There are problems with storescu, because the callback function in dicomlib is global ...
                                    //It means that the callback of only one instance of dicomCommu will be executed ..
                                    int iref = storeKit.DicomStoreU(lstDcmFiles.ToArray(), sLocalAe, epsAe, epsIp, epsPort, "64", ref lstFailed);
                                    if (iref == 0)
                                    {
                                        itm.status = Upload_Status.storeSuccess;// "4"; //store success.

                                        FileLog.Info(sfile + " store to eps success \r\n");
                                        //190703 yys Delete list if successful
                                                                                lstDcmFiles.Clear();
                                    }
                                    else
                                    {
                                        itm.status = Upload_Status.storeFailed;// "7"; //store failed

                                        FileLog.Info(sfile + " store to eps failed \r\n");
                                        //190703 If it fails, put it again next time ..
                                        lstDcmFiles.Clear();
                                        lstDcmFiles.AddRange(lstFailed);
                                    }
                                    //
                                }
                                //update db...
                                uploadItemRepository.UpdateUpload(itm);
                                uploadItemRepository.Commit();

                                FileLog.Info("update " + sfile + " to db success \r\n");
                            }
                        }
                    }
                    ////all file handled..


                }
                catch (Exception ex)
                {
                    //throw;
                    FileLog.Error(ex.Message + ex.StackTrace + ex.Source);
                    if(ex.InnerException!=null)
                        FileLog.Error(ex.InnerException.Message + ex.InnerException.StackTrace + ex.InnerException.Source);
                }

            })).Start();
        }
        /// <summary>
        ///Convert the original dicom to eps, such as download from pacs, at this time, do not modify the dicom file, because
        ///  manager download is the patient's image ...
        /// </summary>
        /// <param name="uploadId"></param>
        /// <param name="upload_item_id"></param>
        void TransferOrigDicomFiletoEpsNoThread(string uploadId,string upload_item_id)
        {
            try
            {
                VnaDbContext vnaDbContext = new VnaDbContext();
                IUploadRepository uploadRepository = new UploadRepository(vnaDbContext);

                Upload upload = uploadRepository.GetUpload(uploadId);
                if (upload == null)
                    return;
                List<string> lstDcmFiles = new List<string>();

                FileLog.Info("TransferOrigDicomFiletoEpsNoThread " + uploadId + " " + upload_item_id);

                //foreach (Upload upload in uploads)
                {


                    string studyuid = string.Empty;
                    string seriesuid = string.Empty;
                    //Written in advance
                    studyuid = upload.study_uid;// DicomKit.DicomTool.GenerateStudyInstanceUID("300", "100");
                    seriesuid = upload.series_uid;// DicomKit.DicomTool.GenerateStudyInstanceUID("400", "100");

                    int upload_id_id = upload.upload_id_id;

                    IUploadItemRepository uploadItemRepository = new UploadItemRepository(vnaDbContext);
                    string sql = "select * from upload_item where (status is NULL or status='') and upload_id_id=@idid and upload_item_id=@upload_item_id";
                    List<UploadItem> uploadItems = uploadItemRepository.FindList<UploadItem>(sql, new SqlParameter[] { new SqlParameter("@idid", upload_id_id),
                        new SqlParameter("@upload_item_id", upload_item_id)});

                    FileLog.Info(sql);
                    //patient name->  "Last name^First name^Middle name^Prefix^Suffix"
                    if (uploadItems != null)
                    {
                        int iidx = 0;
                        foreach (UploadItem itm in uploadItems)
                        {
                            iidx++;
                            //
                            string sfile = itm.dest_file_name;
                            //
                            string pid = upload.medical_num;// native_id;
                            string pname = string.Empty;
                            if (!string.IsNullOrEmpty(upload.last_name))
                            {
                                pname = upload.last_name.TrimStart().TrimEnd();
                            }
                            if (!string.IsNullOrEmpty(upload.first_name))
                            {
                                if (!string.IsNullOrEmpty(pname))
                                    pname = pname + "^";
                                pname = pname + upload.first_name.TrimStart().TrimEnd();
                            }
                            //
                            string studydate = string.Empty;
                            string studytime = string.Empty;
                            if (upload.create_date != null)
                            {
                                studydate = ((DateTime)upload.create_date).ToString("yyyyMMdd");
                                studytime = ((DateTime)upload.create_date).ToString("HHmmss");
                            }
                           // string accessno = "";// upload.medical_num;

                            string studydesc = upload.comment;
                            string refphysician = upload.ref_physician;
                            int res = 0;
                            string dcmfile = sfile;

                            /*
                            string instanceuid = DicomKit.DicomTool.GenerateStudyInstanceUID("1000", "100");
                            string sopClassUid = Configs.GetValue("sopclassuid");//私有的...

                            string studyID = string.Empty;
                            string modality = "DOC"; //OT
                            string instanceNumber = iidx.ToString();
                            string seriesNumber = "1";
                            string pixelspacing = string.Empty;
                            string patientBirthDate = string.Empty;
                            string patientSex = string.Empty;

                            string dcmfile = Path.Combine(Path.GetDirectoryName(sfile), Path.GetFileNameWithoutExtension(sfile)) + ".dcm";

                            FileLog.Info("createImgDcm:" + sfile + ":" + dcmfile + ":" + studyuid + ":" + pid + ":" + pname + ":" + patientBirthDate
                                + ":" + patientSex + ":" + studydate + ":" + studytime + ":" + accessno + ":" + studyID + ":" + studydesc + ":" + modality
                                + ":" + instanceNumber + ":" + seriesuid + ":" + instanceuid + ":" + sopClassUid + ":" + seriesNumber + ":" +
                                pixelspacing + ":" + refphysician);

                            bool isdcmFile = DicomKit.DicomTool.IsDicomFile(sfile);
                            if (isdcmFile)
                            {
                                DicomKit.DicomTool.GetDicomTagInfo(sfile, 0x0008, 0x0018, ref instanceuid);
                            }
                            int res = DicomKit.DicomTool.CreateImgDCM(sfile, dcmfile, studyuid, pid, pname, patientBirthDate, patientSex, studydate, studytime, accessno,
                                studyID, studydesc, modality, instanceNumber, seriesuid, instanceuid, sopClassUid, seriesNumber, pixelspacing, refphysician);


                            if (res == 0)
                            {
                                if (!System.IO.File.Exists(dcmfile))
                                {
                                    res = -1; //

                                    FileLog.Info(sfile + " to dcm failed,but return 0 ");
                                }
                            }
                            if (res == 0)
                            {


                                itm.instance_uid = instanceuid;/////赋值...注意源文件是否dicom

                                //成功
                                itm.dest_file_name = dcmfile;
                                itm.status = Upload_Status.toDcmSuccess;// "1";

                                FileLog.Info(sfile + " to dcm success ");

                            }
                            else
                            {
                                itm.status = Upload_Status.toDcmFailed;// "3";//to dicom fail..
                                                                       //  
                                FileLog.Info(sfile + " to dcm failed ");
                            }
                            */

                            //如果要上传到eps..&&.bStoreToEps
                            if (res == 0)
                            {
                                lstDcmFiles.Add(dcmfile);
                                //
                                //start store file to eps..
                                StoreKit storeKit = StoreKit.Instance;
                                string sLocalAe = VNAConfigSection.Instance.epsElement.LocalAE;
                                string epsAe = VNAConfigSection.Instance.epsElement.EpsAE, epsIp = VNAConfigSection.Instance.epsElement.EpsIP;
                                string epsPort = VNAConfigSection.Instance.epsElement.EpsPort;
                                List<string> lstFailed = new List<string>();

                                storeKit.storeSopclassuid = Configs.GetValue("sopclassuid");
                                //storescu时，有问题,因为dicomlib里面的回调函数是全局的...
                                //意味着只能一个dicomCommu的实例的回调会被执行到..
                                int iref = storeKit.DicomStoreU(lstDcmFiles.ToArray(), sLocalAe, epsAe, epsIp, epsPort, "64", ref lstFailed);
                                if (iref == 0)
                                {
                                    itm.status = Upload_Status.storeSuccess;// "4"; //store success.

                                    FileLog.Info(sfile + " store to eps success \r\n");

                                    //190703 yys 如果成功要删除list
                                    lstDcmFiles.Clear();
                                }
                                else
                                {
                                    itm.status = Upload_Status.storeFailed;// "7"; //store failed

                                    FileLog.Info(sfile + " store to eps failed \r\n");

                                    //190703 如果失败，放在下次再来一次..
                                    lstDcmFiles.Clear();
                                    lstDcmFiles.AddRange(lstFailed);
                                }
                                //
                            }
                            //update db...
                            uploadItemRepository.UpdateUpload(itm);
                            uploadItemRepository.Commit();

                            FileLog.Info("update " + sfile + " to db success \r\n");
                        }
                    }
                }
                ////all file handled..


            }
            catch (Exception ex)
            {
                //throw;
                FileLog.Error(ex.Message + ex.StackTrace + ex.Source);
                if (ex.InnerException != null)
                    FileLog.Error(ex.InnerException.Message + ex.InnerException.StackTrace + ex.InnerException.Source);
            }
        }
    /// <summary>
    /// 将文件转成dicom 并上传到eps...
    /// 放在一个thread中..
    /// </summary>
    /// <param name="uploadId">session id</param>
        void TransferDicomToEps( string uploadId)
        {
            bool bTransferDcmTo = true;
            bool bStoreToEps = true;

            if (!bTransferDcmTo)
                return;
            
            //upload_item.stats=null或空时，为原始状态，1为转成dicom ,3为转失败, 4为上传到eps,7为上传失败...
            new Thread(new ThreadStart(delegate ()
            {
                try
                {
                    VnaDbContext vnaDbContext = new VnaDbContext();
                    IUploadRepository uploadRepository = new UploadRepository(vnaDbContext);

                    List<Upload> uploads = uploadRepository.GetUploads(uploadId);
                    if (uploads == null)
                        return;
                    List<string> lstDcmFiles = new List<string>();

                    foreach (Upload upload in uploads)
                    {


                        string studyuid = string.Empty;
                        string seriesuid = string.Empty;
                        //已提前写入..
                        studyuid = upload.study_uid;// DicomKit.DicomTool.GenerateStudyInstanceUID("300", "100");
                        seriesuid = upload.series_uid;// DicomKit.DicomTool.GenerateStudyInstanceUID("400", "100");

                        int upload_id_id = upload.upload_id_id;

                        IUploadItemRepository uploadItemRepository = new UploadItemRepository(vnaDbContext);
                        string sql = "select * from upload_item where (status is NULL or status='') and upload_id_id=@idid";
                        List<UploadItem> uploadItems = uploadItemRepository.FindList<UploadItem>(sql, new SqlParameter[] { new SqlParameter(@"idid", upload_id_id) });
                        //patient name->  "Last name^First name^Middle name^Prefix^Suffix"
                        if (uploadItems != null)
                        {
                            int iidx = 0;
                            foreach (UploadItem itm in uploadItems)
                            {
                                iidx++;
                                //
                                string sfile = itm.dest_file_name;
                                //
                                string pid = upload.medical_num;// native_id;
                                string pname = string.Empty;
                                if (!string.IsNullOrEmpty(upload.last_name))
                                {
                                    pname = upload.last_name.TrimStart().TrimEnd();
                                }
                                if (!string.IsNullOrEmpty(upload.first_name))
                                {
                                    if (!string.IsNullOrEmpty(pname))
                                        pname = pname + "^";
                                    pname = pname + upload.first_name.TrimStart().TrimEnd();
                                }
                                //
                                string studydate = string.Empty;
                                string studytime = string.Empty;
                                if (upload.create_date != null)
                                {
                                    studydate = ((DateTime)upload.create_date).ToString("yyyyMMdd");
                                    studytime = ((DateTime)upload.create_date).ToString("HHmmss");
                                }
                                string accessno = "";// upload.medical_num;

                                string studydesc = upload.comment;
                                string refphysician = upload.ref_physician;

                                string instanceuid = DicomKit.DicomTool.GenerateStudyInstanceUID("1000", "100");
                                string sopClassUid = Configs.GetValue("sopclassuid");//私有的...

                                string studyID = string.Empty;
                                string modality = "DOC"; //OT
                                string instanceNumber = iidx.ToString();
                                string seriesNumber = "1";
                                string pixelspacing = string.Empty;
                                string patientBirthDate = string.Empty;
                                string patientSex = string.Empty;

                                string dcmfile = Path.Combine(Path.GetDirectoryName(sfile), Path.GetFileNameWithoutExtension(sfile)) + ".dcm";

                                int res = DicomKit.DicomTool.CreateImgDCM(sfile, dcmfile, studyuid, pid, pname, patientBirthDate, patientSex, studydate, studytime, accessno,
                                    studyID, studydesc, modality, instanceNumber, seriesuid,instanceuid,sopClassUid, seriesNumber, pixelspacing, refphysician);

                                itm.instance_uid = instanceuid;

                                if(res ==0 )
                                {
                                    if(!System.IO.File.Exists(dcmfile) )
                                    {
                                        res = -1; //

                                        FileLog.Info(sfile + " to dcm failed,but return 0 ");
                                    }
                                }
                                if(res == 0 )
                                {
                                    //成功
                                    itm.dest_file_name = dcmfile;
                                    itm.status = Upload_Status.toDcmSuccess;// "1";

                                    FileLog.Info(sfile + " to dcm success ");
                                    
                                }
                                else
                                {
                                    itm.status = Upload_Status.toDcmFailed;// "3";//to dicom fail..
                                                                           //  
                                    FileLog.Info(sfile + " to dcm failed ");
                                }

                                
                                //如果要上传到eps...
                                if (res == 0 && bStoreToEps )
                                {
                                    lstDcmFiles.Add(dcmfile);
                                    //
                                    //start store file to eps..
                                    StoreKit storeKit = StoreKit.Instance;
                                    string sLocalAe = VNAConfigSection.Instance.epsElement.LocalAE;
                                    string epsAe = VNAConfigSection.Instance.epsElement.EpsAE, epsIp = VNAConfigSection.Instance.epsElement.EpsIP;
                                    string epsPort = VNAConfigSection.Instance.epsElement.EpsPort;
                                    List<string> lstFailed = new List<string>();

                                    storeKit.storeSopclassuid = Configs.GetValue("sopclassuid");
                                    //storescu时，有问题,因为dicomlib里面的回调函数是全局的...
                                    //意味着只能一个dicomCommu的实例的回调会被执行到..
                                    int iref = storeKit.DicomStoreU(lstDcmFiles.ToArray(), sLocalAe, epsAe, epsIp, epsPort, "64", ref lstFailed);
                                    if (iref == 0)
                                    {
                                        itm.status = Upload_Status.storeSuccess;// "4"; //store success.

                                        FileLog.Info(sfile + " store to eps success \r\n");
                                        //190703 yys 如果成功要删除list
                                        lstDcmFiles.Clear();
                                    }
                                    else
                                    {
                                        itm.status = Upload_Status.storeFailed;// "7"; //store failed

                                        FileLog.Info(sfile + " store to eps failed \r\n");

                                        //190703 如果失败，放在下次再来一次..
                                        lstDcmFiles.Clear();
                                        lstDcmFiles.AddRange(lstFailed);
                                    }
                                    //
                                }
                                //update db...
                                uploadItemRepository.UpdateUpload(itm);
                                uploadItemRepository.Commit();

                                FileLog.Info("update " + sfile + " to db success \r\n");
                            }
                        }
                    }
                    ////all file handled..
                   

                }
                catch (Exception)
                {
                    throw;
                }

            })).Start();
        }
        public ActionResult ViewFiles()
        {
            VnaDbContext vnaDbContext = new VnaDbContext();

            OperatorModel operatorProvider = OperatorProvider.Provider.GetCurrent();
            if (operatorProvider != null)
            {
               // operatorProvider.RoleId = "1";

                string viewName = string.Empty;

                IDepartmentRepository ideptrep = new DepartmentRepository(vnaDbContext);
                List<Department> departments = ideptrep.GetDepartmentList();

                ViewBag.departments = departments;

                switch (RoleActor.GetRole(operatorProvider.RoleId))
                {
                    case BaseData.Role.Nurse:
                        viewName = "ViewFilesNurse";
                        
                        break;
                    case BaseData.Role.Doctor:
                        viewName = "ViewFilesNurse";
                        break;
                    case BaseData.Role.Manager:
                        viewName = "ViewFilesNurse";
                        break;
                    case BaseData.Role.Admin:
                        viewName = "ViewFilesNurse";
                        break;
                    case BaseData.Role.Super:
                        viewName = "ViewFilesNurse";
                        break;
                    default:
                        break;
                }
                //not patient...
                if (!string.IsNullOrEmpty(viewName))
                {
                    string sAutho0 = operatorProvider.Authority;
                    //对nurse等，view和download放在同一个页面中了..
                    bool bUploadRight0 = RoleActor.HasAuthorityRights(sAutho0, AuthorityRights.ViewFile); //ViewAll
                    bool bdown = RoleActor.HasAuthorityRights(sAutho0, AuthorityRights.DownloadFile);

                    if (!bUploadRight0 && !bdown)
                    {
                        //
                        //看一下，当authority为0或空时，group是否有权限
                        if (sAutho0 == "0" || string.IsNullOrEmpty(sAutho0))
                        {
                            IRoleRepository roleRepository = new RoleRepository(vnaDbContext);
                            DataModel.Role role = roleRepository.GetRole(operatorProvider.RoleId);
                            //
                            if (role != null)
                            {
                                bUploadRight0 = role.view_enable;
                                //
                                bdown = role.download_enable;
                            }
                            else
                            {
                                //如果没有...额外限制了super的权限...
                                BaseData.Role rolea = BaseData.RoleActor.GetRole(operatorProvider.RoleId);
                                if (rolea == BaseData.Role.Super)
                                    bUploadRight0 = true;

                            }
                        }
                        if(!bUploadRight0 && !bdown)
                            return View("NoRight");
                    }

                    ViewBag.viewEnable = bUploadRight0.ToString();
                    ViewBag.downEnable = bdown.ToString();

                    return View(viewName);
                }
                //
                ////if patient....
                string sAutho = operatorProvider.Authority;
                bool bUploadRight = RoleActor.HasAuthorityRights(sAutho, AuthorityRights.ViewFile);
                if (!bUploadRight)
                {
                    //
                    //看一下，当authority为0或空时，group是否有权限
                    if (sAutho == "0" || string.IsNullOrEmpty(sAutho))
                    {
                        IRoleRepository roleRepository = new RoleRepository(vnaDbContext);
                        DataModel.Role role = roleRepository.GetRole(operatorProvider.RoleId);
                        //
                        if (role != null)
                            bUploadRight = role.view_enable;//bug... monitor_enable;
                        else
                        {
                            //如果没有...额外限制了super的权限...
                            BaseData.Role rolea = BaseData.RoleActor.GetRole(operatorProvider.RoleId);
                            if (rolea == BaseData.Role.Super)
                                bUploadRight = true;

                        }
                    }
                    if (!bUploadRight)
                        return View("NoRight");
                }

                //
            }
  
            return View();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <param name="fileName"></param>
        /// <param name="pageLimit">每页几条</param>
        /// <param name="pageIndex">第几页</param>
        /// <returns></returns>
        [HandlerAjaxOnly]
        [HttpPost]
        public ActionResult GetPatientFiles(string startDate,string endDate,string fileName,int pageLimit,int pageIndex,string sortBy,string order)
        {
            VnaDbContext vnaDbContext = new VnaDbContext();
            IUploadItemRepository uploadItemRepository = new UploadItemRepository(vnaDbContext);

            OperatorModel opuser = OperatorProvider.Provider.GetCurrent();

            if (string.IsNullOrEmpty(order))
                order = "asc";

            Pagination pagination = new Pagination();
            pagination.rows = pageLimit;
            if (pageIndex <= 0)
                pageIndex = 1;
            pagination.page = pageIndex;
            if (string.IsNullOrEmpty(sortBy) || sortBy == "1")
            {
                sortBy = "upload_at";
                //order = "asc";
            }
            else if (sortBy == "2")
                sortBy = "src_file_name";

            pagination.sord = order;// "asc";
            pagination.sidx = sortBy;// "upload_at";

            List<UploadItem> uploadItems = null;
            //
            if (string.IsNullOrEmpty(startDate) && string.IsNullOrEmpty(endDate) && string.IsNullOrEmpty(fileName))
            {
                //get all
                string stxt = "select upload_item.* from upload_item,upload where upload_item.upload_id_id=upload.upload_id_id and upload.user_id_id=@user_id_id ";

                StringBuilder sql = new StringBuilder(stxt);// "select * from upload_item where ");
                List<DbParameter> dbParameters = new List<DbParameter>();
                int iuser_id_id = 0;
                int.TryParse(opuser.UserCode, out iuser_id_id);
                SqlParameter sqlParam = new SqlParameter("user_id_id", iuser_id_id);
                dbParameters.Add(sqlParam);

                uploadItems = uploadItemRepository.FindList<UploadItem>(stxt,dbParameters.ToArray(),pagination);
            }
            else
            {
                bool bf = true;
                string stxt = "select upload_item.* from upload_item,upload where upload_item.upload_id_id=upload.upload_id_id and upload.user_id_id=@user_id_id ";

                StringBuilder sql = new StringBuilder(stxt);// "select * from upload_item where ");
                List<DbParameter> dbParameters = new List<DbParameter>();
                int iuser_id_id = 0;
                int.TryParse(opuser.UserCode, out iuser_id_id);
                SqlParameter sqlParam = new SqlParameter("user_id_id", iuser_id_id);
                dbParameters.Add(sqlParam);

                if (!string.IsNullOrEmpty(startDate)&& startDate.Length==8)
                {
                    DateTime ds = Convert.ToDateTime(startDate.Substring(0,4)+"-"+startDate.Substring(4,2)+"-"+startDate.Substring(6,2));
                    if (bf)
                        sql.Append(" and ");

                    sql.Append("upload_at>= @ds");

                    SqlParameter sqlParameter = new SqlParameter("ds", ds) ;
                    dbParameters.Add(sqlParameter);
                    bf = true;

                }
                if (!string.IsNullOrEmpty(endDate)&&endDate.Length==8)
                {
                    DateTime es = Convert.ToDateTime(endDate.Substring(0, 4) + "-" + endDate.Substring(4, 2) + "-" + endDate.Substring(6, 2) +" 23:59:59");
                    if (bf)
                        sql.Append(" and ");

                    sql.Append("upload_at <= @es");

                    SqlParameter sqlParameter = new SqlParameter("es", es);
                    dbParameters.Add(sqlParameter);
                    bf = true;
                }
                if(!string.IsNullOrEmpty(fileName))
                {
                    if (bf)
                        sql.Append(" and ");
                    sql.Append("src_file_name like @filename");

                    SqlParameter sqlParameter = new SqlParameter("filename","%"+fileName +"%");
                    dbParameters.Add(sqlParameter);
                    bf = true;
                }
                //
                string sqlText = sql.ToString();

                uploadItems = uploadItemRepository.FindList<UploadItem>(sqlText, dbParameters.ToArray(), pagination);

            }

            if (uploadItems != null)
            {
                var uploadret = new { pageCount = pagination.total, datas = uploadItems };
                return Success("success", uploadret);//uploadItems
            }
            return Error("error");
        }

        //返回当前用户的上传文件,，或是所有用户....
        //getType=''||null,getType='1',GetType=''
        [HandlerAjaxOnly]
        [HttpPost]
        public ActionResult GetFiles(string medic_no,string native_id,string name,string studydate,string report_type,string upload_date,
            string ref_physician, string department, int pageLimit, int pageIndex, string sortBy, string order,string getType)
        {
            VnaDbContext vnaDbContext = new VnaDbContext();
            IUploadRepository uploadRepository = new UploadRepository(vnaDbContext);


            OperatorModel opuser = OperatorProvider.Provider.GetCurrent();

            if (string.IsNullOrEmpty(order))
                order = "asc";

            Pagination pagination = new Pagination();
            pagination.rows = pageLimit;
            if (pageIndex <= 0)
                pageIndex = 1;
            pagination.page = pageIndex;
            if (string.IsNullOrEmpty(sortBy) || sortBy == "1")
            {
                sortBy = "create_date";
                //order = "asc";
            }
            else if (sortBy == "2")
                sortBy = "study_date";
            else if (sortBy == "3")
                sortBy = "medical_num";
            else if (sortBy == "4")
                sortBy = "native_id";
            else if (sortBy == "5")
                sortBy = "last_name,first_name";
            else if (sortBy == "6")
                sortBy = "ref_physician";

            pagination.sord = order;// "";
            pagination.sidx = sortBy;// ""; 

            //select DISTINCT upload.* from upload,users where (upload.user_id_id=13 or (upload.user_id_id !=13 and users.user_id_id = upload.user_id_id
            //and((users.department_id = '') or(users.department_id is NULL)) ) ) and medical_num='124'
            //string deptid = opuser.DepartmentId;
            //if (string.IsNullOrEmpty(deptid))
               // deptid = string.Empty;

            string stxt = string.Empty;// "select DISTINCT upload.* from upload,users where ( upload.user_id_id=@user_id_id or (upload.user_id_id !=@user_id_id1 and users.user_id_id = upload.user_id_id ";
                           //stxt += " and ((users.department_id = @dept_id ) or ( users.department_id is NULL )) )) ";
       
            stxt = "select DISTINCT upload.* from upload,users where  upload.user_id_id=users.user_id_id ";

            StringBuilder sql = new StringBuilder(stxt);// "select * from upload_item where ");
            List<DbParameter> dbParameters = new List<DbParameter>();
            SqlParameter sqlParam = null;

            if ( string.IsNullOrEmpty(getType))
            {
                sql.Append(" and (upload.user_id_id = @user_id_id ) ");

                int iuser_id_id = 0;
                int.TryParse(opuser.UserCode, out iuser_id_id);
                sqlParam = new SqlParameter("user_id_id", iuser_id_id);
                dbParameters.Add(sqlParam);
            }
            //在下面的department...
            //if (!string.IsNullOrEmpty(deptid))
            //{
            //    sql.Append(" and (upload.source_dept = @dept_id ) ");

            //    sqlParam = new SqlParameter("dept_id", deptid);
            //    dbParameters.Add(sqlParam);
            //}

            bool bf = true;

            if(!string.IsNullOrEmpty(medic_no))
            {
                if (bf)
                    sql.Append(" and ");

                sql.Append("medical_num = @medical_num");

                SqlParameter sqlParameter = new SqlParameter("medical_num", medic_no);
                dbParameters.Add(sqlParameter);
                bf = true;
            }
            if (!string.IsNullOrEmpty(native_id))
            {
                if (bf)
                    sql.Append(" and ");

                sql.Append("native_id = @native_id");

                SqlParameter sqlParameter = new SqlParameter("native_id", native_id);
                dbParameters.Add(sqlParameter);
                bf = true;
            }
            if (!string.IsNullOrEmpty(name))
            {
                string[] namearr = null;
                if (name.IndexOf("^") >= 0)
                    namearr = name.Split(new char[] { '^' });
                else
                    namearr = name.Split(new char[] { '^' });
                if(namearr.Length > 0)
                {
                    if (bf)
                        sql.Append(" and ");

                    sql.Append("first_name like @first_name");

                    SqlParameter sqlParameter = new SqlParameter("first_name", "%" + namearr[0] + "%");
                    dbParameters.Add(sqlParameter);

                    bf = true;
                }
                if (namearr.Length > 1)
                {
                    if (bf)
                        sql.Append(" and ");

                    sql.Append("last_name like @last_name");

                    SqlParameter sqlParameter = new SqlParameter("last_name", "%" + namearr[1] + "%");
                    dbParameters.Add(sqlParameter);
                    bf = true;
                }
            }
            //if (!string.IsNullOrEmpty(studydate))
            //{
            //    if (bf)
            //        sql.Append(" and ");

            //    sql.Append("study_date>= @studydate");

            //    SqlParameter sqlParameter = new SqlParameter("studydate", studydate);
            //    dbParameters.Add(sqlParameter);
            //    bf = true;
            //}
            if (!string.IsNullOrEmpty(report_type))
            {
                if (bf)
                    sql.Append(" and ");

                sql.Append("report_type>= @report_type");

                SqlParameter sqlParameter = new SqlParameter("report_type", report_type);
                dbParameters.Add(sqlParameter);
                bf = true;
            }
            if (!string.IsNullOrEmpty(ref_physician))
            {
                if (bf)
                    sql.Append(" and ");

                sql.Append("ref_physician like @ref_physician");

                SqlParameter sqlParameter = new SqlParameter("ref_physician", "%"+ ref_physician + "%");
                dbParameters.Add(sqlParameter);
                bf = true;
            }
            if (!string.IsNullOrEmpty(department))
            {
                if (bf)
                    sql.Append(" and ");

                sql.Append("source_dept = @department");

                SqlParameter sqlParameter = new SqlParameter("department", department);
                dbParameters.Add(sqlParameter);
                bf = true;
            }
            if (!string.IsNullOrEmpty(studydate) && studydate.Length == 8)
            {
                DateTime ds = Convert.ToDateTime(studydate.Substring(0, 4) + "-" + studydate.Substring(4, 2) + "-" + studydate.Substring(6, 2));
                if (bf)
                    sql.Append(" and ");

                sql.Append("study_date>= @ds");

                SqlParameter sqlParameter = new SqlParameter("ds", ds);
                dbParameters.Add(sqlParameter);
                bf = true;

            }
            if (!string.IsNullOrEmpty(upload_date) && upload_date.Length == 8)
            {
                DateTime ds = Convert.ToDateTime(upload_date.Substring(0, 4) + "-" + upload_date.Substring(4, 2) + "-" + upload_date.Substring(6, 2));
                DateTime ds1 = ds.AddDays(1);//计算当天的时间

                if (bf)
                    sql.Append(" and ");

                sql.Append("( upload.create_date>= @us and upload.create_date < @us1)"); //

                SqlParameter sqlParameter = new SqlParameter("us", ds);
                dbParameters.Add(sqlParameter);
                SqlParameter sqlParameter2 = new SqlParameter("us1", ds1);
                dbParameters.Add(sqlParameter2);

                bf = true;

            }

            string sqlText = sql.ToString();

            List<Upload> uploadItems = uploadRepository.FindList<Upload>(sqlText, dbParameters.ToArray(), pagination);

            if (uploadItems != null)
            {
                var uploadret = new { pageCount = pagination.total, datas = uploadItems };
                return Success("success", uploadret);//uploadItems
            }

            return Error("error");
        }
      
        /// <summary>
        /// 查询返回行合条件的上传记录...
        /// 以后加入验证...是否有权限..
        /// </summary>
        /// <param name="medic_no"></param>
        /// <param name="native_id"></param>
        /// <param name="name"></param>
        /// <param name="studydate"></param>
        /// <param name="report_type"></param>
        /// <param name="upload_date"></param>
        /// <param name="ref_physician"></param>
        /// <param name="department"></param>
        /// <param name="pageLimit"></param>
        /// <param name="pageIndex"></param>
        /// <param name="sortBy"></param>
        /// <param name="order"></param>
        /// <returns></returns>
        [HandlerAjaxOnly]
        [HttpPost]
        public ActionResult GetFilesInward(string medic_no, string native_id, string name, string studydate, string report_type, string upload_date, string upload_date2,
         string ref_physician, string department,string status, int pageLimit, int pageIndex, string sortBy, string order)
        {
            VnaDbContext vnaDbContext = new VnaDbContext();
            IUploadRepository uploadRepository = new UploadRepository(vnaDbContext);


            //OperatorModel opuser = OperatorProvider.Provider.GetCurrent();

            if (string.IsNullOrEmpty(order))
                order = "asc";

            Pagination pagination = new Pagination();
            pagination.rows = pageLimit;
            if (pageIndex <= 0)
                pageIndex = 1;
            pagination.page = pageIndex;
            if (string.IsNullOrEmpty(sortBy) || sortBy == "1")
            {
                sortBy = "user_name";
                // order = "asc";
            }
            else if (sortBy == "2")
                sortBy = "native_id";
            else if (sortBy == "3")
                sortBy = "medical_num";
            else if (sortBy == "4")
                sortBy = "create_date";
            else if (sortBy == "5")
                sortBy = "email";// "last_name,first_name";
            else if (sortBy == "6")
                sortBy = "src_file_name";// "ref_physician";


            pagination.sord = order;// "";
            pagination.sidx = sortBy;// ""; 


            //select DISTINCT upload.* from upload,users where (upload.user_id_id=13 or (upload.user_id_id !=13 and users.user_id_id = upload.user_id_id
            //and((users.department_id = '') or(users.department_id is NULL)) ) ) and medical_num='124'

            // string deptid = opuser.DepartmentId;
            // if (string.IsNullOrEmpty(deptid))
            //     deptid = string.Empty;
            //这里一个文件一条记录，考虑到上传转dicom，到eps这中间是要有时间差的...可能同一study有的文件存在失败的可能...
            //select DISTINCT upload.*,users.user_name ,users.email,upload_item.src_file_name from ...
            string stxt = @"select  upload_id
           ,upload.upload_id_id
           ,upload_sessionid
           ,upload.user_id_id
           ,medical_num
           ,native_id
           ,first_name
           ,middle_name
           ,last_name
           ,upload.create_date
           ,study_date
           ,ref_physician
           ,report_type
           ,source_dept
           ,source_ofdata
           ,comment
           ,study_uid
           ,users.user_name
           ,users.email 
           ,max(upload_item.src_file_name ) as src_file_name
            from upload,upload_item,users where  upload.user_id_id=users.user_id_id  
            and upload.upload_id_id=upload_item.upload_id_id ";

            StringBuilder sql = new StringBuilder(stxt);// "select * from upload_item where ");
            List<DbParameter> dbParameters = new List<DbParameter>();
            
            //在以下用department....
            //if (!string.IsNullOrEmpty(deptid))
            //{
            //    sql.Append(" and (users.department_id = @dept_id ) ");

            //    sqlParam = new SqlParameter("dept_id", deptid);
            //    dbParameters.Add(sqlParam);
            //}
            //int iuser_id_id = 0;
           // int.TryParse(opuser.UserCode, out iuser_id_id);        

            bool bf = true;

            if (!string.IsNullOrEmpty(medic_no))
            {
                if (bf)
                    sql.Append(" and ");

                sql.Append("medical_num = @medical_num");

                SqlParameter sqlParameter = new SqlParameter("medical_num", medic_no);
                dbParameters.Add(sqlParameter);
                bf = true;
            }
            if (!string.IsNullOrEmpty(native_id))
            {
                if (bf)
                    sql.Append(" and ");

                sql.Append("native_id = @native_id");

                SqlParameter sqlParameter = new SqlParameter("native_id", native_id);
                dbParameters.Add(sqlParameter);
                bf = true;
            }
            if (!string.IsNullOrEmpty(name))
            {
                string[] namearr = null;
                if (name.IndexOf("^") >= 0)
                    namearr = name.Split(new char[] { '^' });
                else
                    namearr = name.Split(new char[] { '^' });
                if (namearr.Length > 0)
                {
                    if (bf)
                        sql.Append(" and ");

                    sql.Append("first_name like @first_name");

                    SqlParameter sqlParameter = new SqlParameter("first_name", "%" + namearr[0] + "%");
                    dbParameters.Add(sqlParameter);
                 
                    bf = true;
                }
                if (namearr.Length > 1)
                {
                    if (bf)
                        sql.Append(" and ");

                    sql.Append("last_name like @last_name");

                    SqlParameter sqlParameter = new SqlParameter("last_name", "%" + namearr[1] + "%");
                    dbParameters.Add(sqlParameter);

                    bf = true;
                }
            }
            //if (!string.IsNullOrEmpty(studydate))
            //{
            //    if (bf)
            //        sql.Append(" and ");

            //    sql.Append("study_date>= @studydate");

            //    SqlParameter sqlParameter = new SqlParameter("studydate", studydate);
            //    dbParameters.Add(sqlParameter);
            //    bf = true;
            //}
            if (!string.IsNullOrEmpty(report_type))
            {
                if (bf)
                    sql.Append(" and ");

                sql.Append("report_type>= @report_type");

                SqlParameter sqlParameter = new SqlParameter("report_type", report_type);
                dbParameters.Add(sqlParameter);
                bf = true;
            }
            if (!string.IsNullOrEmpty(ref_physician))
            {
                if (bf)
                    sql.Append(" and ");

                sql.Append("ref_physician like @ref_physician");

                SqlParameter sqlParameter = new SqlParameter("ref_physician", "%" + ref_physician + "%");
                dbParameters.Add(sqlParameter);
                bf = true;
            }
            if (!string.IsNullOrEmpty(department))
            {
                if (bf)
                    sql.Append(" and ");

                sql.Append("source_dept = @department");

                SqlParameter sqlParameter = new SqlParameter("department", department);
                dbParameters.Add(sqlParameter);
                bf = true;
            }
            if (!string.IsNullOrEmpty(studydate) && studydate.Length == 8)
            {
                DateTime ds = Convert.ToDateTime(studydate.Substring(0, 4) + "-" + studydate.Substring(4, 2) + "-" + studydate.Substring(6, 2));
                if (bf)
                    sql.Append(" and ");

                sql.Append("study_date>= @ds");

                SqlParameter sqlParameter = new SqlParameter("ds", ds);
                dbParameters.Add(sqlParameter);
                bf = true;

            }
            if (!string.IsNullOrEmpty(upload_date) && upload_date.Length == 8 && string.IsNullOrEmpty(upload_date2))
            {
                DateTime ds = Convert.ToDateTime(upload_date.Substring(0, 4) + "-" + upload_date.Substring(4, 2) + "-" + upload_date.Substring(6, 2));
                if (bf)
                    sql.Append(" and ");

                sql.Append("upload.create_date>= @us");

                SqlParameter sqlParameter = new SqlParameter("us", ds);
                dbParameters.Add(sqlParameter);
                bf = true;

            }
            else if(string.IsNullOrEmpty(upload_date) && !string.IsNullOrEmpty(upload_date2))
            {
                DateTime ds = Convert.ToDateTime(upload_date2.Substring(0, 4) + "-" + upload_date2.Substring(4, 2) + "-" + upload_date2.Substring(6, 2) + " 23:59:59");
                if (bf)
                    sql.Append(" and ");

                sql.Append("upload.create_date<= @us");

                SqlParameter sqlParameter = new SqlParameter("us", ds);
                dbParameters.Add(sqlParameter);
                bf = true;
            }
            else if(!string.IsNullOrEmpty(upload_date) && !string.IsNullOrEmpty(upload_date2))
            {
                DateTime ds = Convert.ToDateTime(upload_date.Substring(0, 4) + "-" + upload_date.Substring(4, 2) + "-" + upload_date.Substring(6, 2));

                DateTime ds2 = Convert.ToDateTime(upload_date2.Substring(0, 4) + "-" + upload_date2.Substring(4, 2) + "-" + upload_date2.Substring(6, 2) + " 23:59:59");
                if (bf)
                    sql.Append(" and ");

                sql.Append("( upload.create_date<= @us2 and upload.create_date >=@us )");

                SqlParameter sqlParameter = new SqlParameter("us", ds);
                dbParameters.Add(sqlParameter);
                SqlParameter sqlParameter2 = new SqlParameter("us2", ds2);
                dbParameters.Add(sqlParameter2);

                bf = true;
            }
            //加入状态....
            if (status != null) //只要不为null就表示要处理status...
            {
                if (bf)
                    sql.Append(" and ");
                SqlParameter sqlParameter = null;
                if (string.IsNullOrEmpty(status) || status == "1")
                {
                    sql.Append("( status=@status  )"); //or status is NULL
                    sqlParameter = new SqlParameter("@status", Upload_Status.storeSuccess);
                }
                else
                {
                    sql.Append("status=@status");
                    string statusvalue = string.Empty;
                    if (status == "2")
                        statusvalue = Upload_Status.audited;
                    else
                        statusvalue = status;

                    sqlParameter = new SqlParameter("@status", statusvalue);
                }
                dbParameters.Add(sqlParameter);
                bf = true;
            }
            string sqlText = sql.ToString();
            string sgroup = @"
                     group by
                       upload_id
                       ,upload.upload_id_id
                       ,upload_sessionid
                       ,upload.user_id_id
                       ,medical_num
                       ,native_id
                       ,first_name
                       ,middle_name
                       ,last_name
                       ,upload.create_date
                       ,study_date
                       ,ref_physician
                       ,report_type
                       ,source_dept
                       ,source_ofdata
                       ,comment
                       ,study_uid
                       , users.user_name
                       ,users.email ";
            sqlText = sqlText + sgroup;

            List<Upload_User_Item> uploadItems = uploadRepository.FindList<Upload_User_Item>(sqlText, dbParameters.ToArray(), pagination);

            if (uploadItems != null)
            {
                var uploadret = new { pageCount = pagination.total, datas = uploadItems };
                return Success("success", uploadret);//uploadItems
            }

            return Error("error");
        }

        [HttpPost]
        public ActionResult GetFilesOutward(string medic_no,string native_id,string name,string send_date, string send_date2, string ref_physician,string department, int pageLimit, int pageIndex, string sortBy, string order)
        {
            VnaDbContext vnaDbContext = new VnaDbContext();
            IDownloadRepository downloadRepository = new DownloadRepository(vnaDbContext);


            //OperatorModel opuser = OperatorProvider.Provider.GetCurrent();

            if (string.IsNullOrEmpty(order))
                order = "asc";

            Pagination pagination = new Pagination();
            pagination.rows = pageLimit;
            if (pageIndex <= 0)
                pageIndex = 1;
            pagination.page = pageIndex;
            if (string.IsNullOrEmpty(sortBy) || sortBy == "1")
            {
                sortBy = "native_id";//  "";
                // order = "asc";
            }
            else if (sortBy == "2")
                sortBy = "medical_recno";
            else if (sortBy == "3")
                sortBy = "patient_name";
            else if (sortBy == "4")
                sortBy = "create_date";
            else if (sortBy == "5")
                sortBy = "ref_physician";// "last_name,first_name";
            else if (sortBy == "6")
                sortBy = "source_dept";// "ref_physician";
            else if (sortBy == "7")
                sortBy = "creator_id";
            else if (sortBy == "8")
                sortBy = "study_file";

            pagination.sord = order;// "";
            pagination.sidx = sortBy;// ""; 

            //
            System.Linq.Expressions.Expression<Func<Download, bool>> expression = u => true;// (u,m) => u.user_id_id==m.user_id_id;
            //
            if(!string.IsNullOrEmpty(medic_no))
            {
                //System.Linq.Expressions.Expression<Func<Download, Users, bool>> ex = (u, m) => u.medical_recno == medic_no;
                expression = expression.And(u => u.medical_recno == medic_no);// (System.Linq.Expressions.Expression < Func<Download, Users, bool> > )expression.AndAlso( ex);
            }
            if(!string.IsNullOrEmpty(native_id))
            {
                // System.Linq.Expressions.Expression<Func<Download, Users, bool>> ex = (u, m) => u.id_number == native_id;
                expression = expression.And(u => u.native_id == native_id);// (System.Linq.Expressions.Expression < Func<Download, Users, bool> >) expression.AndAlso(ex);// u => u.id_number == native_id);
            }
            if(!string.IsNullOrEmpty(name))
            {
                //System.Linq.Expressions.Expression<Func<Download, Users, bool>> ex = (u, m) => u.patient_name.Contains(name);
                expression = expression.And(u => u.patient_name.Contains(name));// (System.Linq.Expressions.Expression < Func<Download, Users, bool> > )expression.AndAlso(ex);// u => u.patient_name.Contains(name));
            }
            if (!string.IsNullOrEmpty(send_date) && send_date.Length == 8 && string.IsNullOrEmpty(send_date2))
            {
                //System.Linq.Expressions.Expression<Func<Download, Users, bool>> ex = (u, m) => u.create_date.ToString("yyyyMMdd") == send_date;

                //date.ToString("yyyyMMdd")== send_date :  linq to entities中没有函数来换sql的，会出错
                //并且不能直接用yyyymmdd
                var syear = send_date.Substring(0, 4);
                var smon = send_date.Substring(4, 2);
                var sday = send_date.Substring(6, 2);
                var newdate = syear + "-" + smon + "-" + sday + " 00:00:01";
                //== 0 to >=0
                expression = expression.And(u => System.Data.Entity.SqlServer.SqlFunctions.DateDiff("day", u.create_date, newdate) <= 0);
                //System.Data.Entity.SqlServer.SqlFunctions.DateName("dd", u.create_date) == send_date);// (System.Linq.Expressions.Expression<Func<Download, Users, bool>>)expression.AndAlso(ex);// u => u.create_date.ToString("yyyyMMdd") == send_date);
            }
            else if (string.IsNullOrEmpty(send_date) && !string.IsNullOrEmpty(send_date2))
            {
                var syear = send_date2.Substring(0, 4);
                var smon = send_date2.Substring(4, 2);
                var sday = send_date2.Substring(6, 2);
                var newdate = syear + "-" + smon + "-" + sday + " 23:59:59";

                expression = expression.And(u => System.Data.Entity.SqlServer.SqlFunctions.DateDiff("day", u.create_date, newdate) >= 0);

            }
            else if (!string.IsNullOrEmpty(send_date) && !string.IsNullOrEmpty(send_date2))
            {
                var syear = send_date.Substring(0, 4);
                var smon = send_date.Substring(4, 2);
                var sday = send_date.Substring(6, 2);
                var newdate = syear + "-" + smon + "-" + sday + " 00:00:01";

                var syear2 = send_date2.Substring(0, 4);
                var smon2 = send_date2.Substring(4, 2);
                var sday2 = send_date2.Substring(6, 2);
                var newdate2 = syear2 + "-" + smon2 + "-" + sday2 + " 23:59:59";

                expression = expression.And(u => System.Data.Entity.SqlServer.SqlFunctions.DateDiff("day", u.create_date, newdate) <= 0 &&
                System.Data.Entity.SqlServer.SqlFunctions.DateDiff("day", u.create_date, newdate2) >=0 );

            }
            if (!string.IsNullOrEmpty(ref_physician))
            {
                //System.Linq.Expressions.Expression<Func<Download, Users, bool>> ex = (u, m) => u.ref_physician.Contains(ref_physician);
                expression =  expression.And(u => u.ref_physician.Contains(ref_physician));// (System.Linq.Expressions.Expression < Func<Download, Users, bool> > )expression.AndAlso(ex);// u => u.ref_physician == ref_physician);
            }
            if(!string.IsNullOrEmpty(department))
            {
                //System.Linq.Expressions.Expression<Func<Download, Users, bool>> ex = (u, m) => u.source_dept == department;
                expression = expression.And( u => u.source_dept == department);// (System.Linq.Expressions.Expression<Func<Download, Users, bool>>)expression.AndAlso(ex);// u => u.source_dept == department);
            }
            //
            try
            {


                //System.Linq.Expressions.Expression nwexpression = expression;
                List<Download> downloads = downloadRepository.FindList<Download>(expression, pagination);
                //
                List<Download_User> download_Users = new List<Download_User>();
                if (downloads != null)
                {

                    IUserRepository userRepository = new UserRepository(vnaDbContext);
                    IDepartmentRepository departmentRepository = new DepartmentRepository(vnaDbContext);

                    foreach (Download down in downloads)
                    {
                        //赋值creator和department name.
                        Download_User download_User = new Download_User()
                        {
                            create_date = down.create_date,
                            creator_id = down.creator_id,
                            download_uid = down.download_uid,
                            expire_time = down.expire_time,
                            id = down.id,
                            native_id = down.native_id,
                            is_valid = down.is_valid,
                            medical_recno = down.medical_recno,
                            modality_instudy = down.modality_instudy,
                            patient_name = down.patient_name,
                            ref_physician = down.ref_physician,
                            source_dept = down.source_dept,
                            study_date = down.study_date,
                            study_uid = down.study_uid,
                            user_id_id = down.user_id_id,
                            study_file = down.study_file
                        };
                        Users usr = userRepository.FindEntity<Users>(t => t.user_id_id == down.creator_id);
                        if (usr != null)
                            download_User.user_name = usr.user_name;
                        usr = userRepository.FindEntity<Users>(t => t.user_id_id == down.user_id_id);
                        if (usr != null)
                            download_User.receiver_name = usr.user_name;
                        //
                        if (!string.IsNullOrEmpty(down.source_dept))
                        {
                            Department ldepart = departmentRepository.GetDepartment(down.source_dept);
                            if (ldepart != null)
                                download_User.source_dept = down.source_dept + "$" + ldepart.department_name;
                        }
                        //
                        download_Users.Add(download_User);
                    }


                }

                var uploadret = new { pageCount = pagination.total, datas = download_Users };
                return Success("success", uploadret);//
            }
            catch (Exception ex)
            {
                FileLog.Error(ex.Message + " " + ex.StackTrace);
                if(ex.InnerException!=null)
                    FileLog.Error(ex.InnerException.Message + " " + ex.InnerException.StackTrace);
            }

            return Error("error");
        }

        [HttpPost]
        public ActionResult DeleteFile(string item_id)
        {
            if (string.IsNullOrEmpty(item_id))
                return Error("id is empty.");

            VnaDbContext vnaDbContext = new VnaDbContext();
            IUploadItemRepository uploadItemRepository = new UploadItemRepository(vnaDbContext);

            uploadItemRepository.DeleteUpload(item_id);
            uploadItemRepository.Commit();

            return Success("success");
        }

        [HttpPost]
        //每一文件最好有一个，特别码，用来区分相同文件名的，以允许上传相同文件...
        public ActionResult DeleteSessionFile(string fileName)
        {
            string sUpSessionid = Session["uploadId"] as string;

            if (!string.IsNullOrEmpty(sUpSessionid))
            {
                List<UploadModel> uploadModels = Session[sUpSessionid] as List<UploadModel>;
                if (uploadModels != null)
                {
                   
                    foreach (UploadModel model in uploadModels)
                    {
                        if (model == null)
                            continue;
                        List<string> lstfiles = model.upload_files;
                        lstfiles.Remove(fileName);
                    }
                   
                }
            }
            return Success("success");
        }
        public ActionResult CheckFile(string fileName)
        {
            /* //不准确，当选 中了文件有取消时....session中还存有
            //1.先判断当前session是否存在同名文件
            string sUpSessionid = Session["uploadId"] as string;

            if (!string.IsNullOrEmpty(sUpSessionid))
            {
                List<UploadModel> uploadModels = Session[sUpSessionid] as List<UploadModel>;
                if(uploadModels != null)
                {
                    bool bf = false;
                    foreach(UploadModel model in uploadModels)
                    {
                        if (model == null)
                            continue;
                        List<string> lstfiles = model.upload_files;
                        if(lstfiles !=null && lstfiles.Contains(fileName) )
                        {
                            bf = true;
                            break;
                        }
                    }
                    if(bf)
                    {
                        return Error(fileName + " file is existed in session.");
                    }
                }
            }
            */
            VnaDbContext vnaDbContext = new VnaDbContext();
            IUploadItemRepository uploadItemRepository = new UploadItemRepository(vnaDbContext);

            OperatorModel opuser = OperatorProvider.Provider.GetCurrent();
            int userid = 0;
            int.TryParse(opuser.UserCode, out userid);

            List<UploadItem> uploadItems = uploadItemRepository.FindList<UploadItem>("select upload_item.* from upload_item,upload where upload.user_id_id=@user_id_id and upload.upload_id_id=upload_item.upload_id_id and upload_item.src_file_name=@filename", new SqlParameter[] {new SqlParameter("@user_id_id",userid) ,new SqlParameter("@filename", fileName) });
            if(uploadItems== null || uploadItems.Count == 0)
                return Success("success");

            return Error(fileName +  " file is existed table.");
        }
        public ActionResult OpenFile(string item_id)
        {
            VnaDbContext vnaDbContext = new VnaDbContext();
            IUploadItemRepository uploadItemRepository = new UploadItemRepository(vnaDbContext);

            UploadItem uploadItem = uploadItemRepository.FindEntity<UploadItem>(item_id);
            if (uploadItem == null)
                return Content("file isn't existed.");

            IUploadRepository uploadRepository = new UploadRepository(vnaDbContext);
            string stxt = "select upload.* from upload,upload_item  where upload.upload_id_id=upload_item.upload_id_id and upload_item.upload_item_id=@itemid";

            string studyuid = string.Empty, seriesuid = string.Empty;
            List<Upload> uploads = uploadRepository.FindList<Upload>(stxt, new SqlParameter[] { new SqlParameter("itemid", item_id) });
            if(uploads != null && uploads.Count >0 )
            {
                studyuid = uploads[0].study_uid;
                seriesuid = uploads[0].series_uid;
            }

            string sinsuid = uploadItem.instance_uid;
            if (string.IsNullOrEmpty(sinsuid))
                sinsuid = string.Empty;
            if (string.IsNullOrEmpty(studyuid))
                studyuid = string.Empty;
            if (string.IsNullOrEmpty(seriesuid))
                seriesuid = string.Empty;

            VNAConfigSection vNAConfigSection = VNAConfigSection.Instance;
            SiteElement siteConfigElement = vNAConfigSection.SiteElement;

            string shomeurl = siteConfigElement.Home.TrimEnd('/');
            string simagehomeurl = siteConfigElement.ImageHome.TrimEnd('/');
            if (string.IsNullOrEmpty(simagehomeurl))
                simagehomeurl = shomeurl;
            //&STUDYUID=" + sStudyUid +"&SERIESUID="+ sSeriesUid ;
            // "&INSTANCEUID=" + sInstanceUid;
            string suser = siteConfigElement.UniUser;
            string spwd = siteConfigElement.UniPwd;

            String uniwebUrl = String.Format("{0}/DicomWeb/DicomWeb.dll/OpenImage?STUDYUID={1}&SERIESUID={2}&INSTANCEUID={3}", simagehomeurl,studyuid,seriesuid,sinsuid);
            string html5Url = siteConfigElement.Html5Home;// string.Empty;
            //html5Url = String.Format("{0}/html5/ShowImage.html?shareID={1}&sharePassword={2}", siteConfigElement.HomeUrl.TrimEnd('/'), share.share_id, share.passwd);

            string htmlPrefix = siteConfigElement.UrlPath;// "UW360";// "InvokeImageDisplay";// "html5/index.html";
            //yys for multi study share.
            html5Url = String.Format("{0}/{4}?STUDYUID={1}&SERIESUID={2}&INSTANCEUID={3}", html5Url, studyuid, seriesuid, sinsuid,htmlPrefix);

            string h5user = siteConfigElement.H5User;
            string h5pass = siteConfigElement.H5Pwd;
            //if(string.IsNullOrEmpty(h5user) && string.IsNullOrEmpty(h5pass))
            //{
            //    h5user = suser;
            //    h5pass = spwd;
            //}
            if (!string.IsNullOrEmpty(suser))
            {
                uniwebUrl = uniwebUrl + "&User=" + suser + "&Password=" + spwd;

            }
            if (!string.IsNullOrEmpty(h5user))
            { 

                html5Url = html5Url + "&User=" + h5user + "&Password=" + h5pass;
            }

            ViewerRegionElement viewerregion = vNAConfigSection.viewerRegionElement;
            string sidovieweregion = viewerregion.IDOViewerRegion;
            if (string.IsNullOrEmpty(sidovieweregion))
                sidovieweregion = "iDoCloud";

            ViewBag.UniwebUrl = uniwebUrl;
            ViewBag.Html5Url = html5Url;
            ViewBag.IDOViewerRegon = sidovieweregion;
            ViewBag.IDOUrl = String.Format("idoviewer://{0}/ShareQuery?STUDYUID={1}&SERIESUID={2}&INSTANCEUID={3}", sidovieweregion,studyuid,seriesuid, sinsuid);
            //20200206 漏掉了
            if (Session["SelectViewer"] != null)
            {
                string selected = Session["SelectViewer"] as string;
                string sredrict = string.Empty;
                if (selected == "0")
                    sredrict = ViewBag.IDOUrl as string;
                else if (selected == "1")
                    sredrict = uniwebUrl;
                else if (selected == "2")
                    sredrict = html5Url;
                if (!string.IsNullOrEmpty(sredrict))
                    return Redirect(sredrict);
            }

            return View();

        }
        public ActionResult OpenUpload(string upload_id)
        {
            VnaDbContext vnaDbContext = new VnaDbContext();
            IUploadRepository uploadItemRepository = new UploadRepository(vnaDbContext);

            Upload uploadItem = uploadItemRepository.FindEntity<Upload>(upload_id);
            if (uploadItem == null)
                return Content("file isn't existed.");

            string sinsuid = uploadItem.study_uid;
            if (string.IsNullOrEmpty(sinsuid))
                sinsuid = string.Empty;

            string seriesuid = uploadItem.series_uid;
            if (string.IsNullOrEmpty(seriesuid))
                seriesuid = string.Empty;

            return openStudy(sinsuid);
        }
        ActionResult openStudy(string studyuid)
        {
            VNAConfigSection vNAConfigSection = VNAConfigSection.Instance;
            SiteElement siteConfigElement = vNAConfigSection.SiteElement;
            ViewerRegionElement viewerregion = vNAConfigSection.viewerRegionElement;

            string shomeurl = siteConfigElement.Home.TrimEnd('/');
            string simagehomeurl = siteConfigElement.ImageHome.TrimEnd('/');
            if (string.IsNullOrEmpty(simagehomeurl))
                simagehomeurl = shomeurl;

            string sidovieweregion = viewerregion.IDOViewerRegion;
            if (string.IsNullOrEmpty(sidovieweregion))
                sidovieweregion = "iDoCloud";

            string suser = siteConfigElement.UniUser;
            string spwd = siteConfigElement.UniPwd;

            /*
            //&STUDYUID=" + sStudyUid +"&SERIESUID="+ sSeriesUid ;
            // "&INSTANCEUID=" + sInstanceUid;
            //&SERIESUID={2}
            String uniwebUrl = String.Format("{0}/DicomWeb/DicomWeb.dll/OpenImage?STUDYUID={1}", simagehomeurl, studyuid);
            string html5Url = string.Empty;
            //html5Url = String.Format("{0}/html5/ShowImage.html?shareID={1}&sharePassword={2}", siteConfigElement.HomeUrl.TrimEnd('/'), share.share_id, share.passwd);
            //yys for multi study share.
            html5Url = String.Format("{0}/html5/index.html?STUDYUID={1}", simagehomeurl, studyuid);//&SERIESUID={2} ,seriesuid

           
            ViewBag.UniwebUrl = uniwebUrl;
            ViewBag.Html5Url = html5Url;
            ViewBag.IDOViewerRegon = sidovieweregion;
            ViewBag.IDOUrl = String.Format("idoviewer://{0}/ShareQuery?STUDYUID={1}", sidovieweregion, studyuid);//&SERIESUID={2} seriesuid

            return View("OpenFile");
            */
            return OpenStuduyWith(studyuid, simagehomeurl, sidovieweregion,suser,spwd);
        }
        [HttpPost]
        public ActionResult SelectViewer(string id)
        {
            Session["SelectViewer"] = id;

            return Success("success");
        }
        ActionResult OpenStuduyWith(string studyuid ,string simagehomeurl, string sidovieweregion,string suser,string spwd)
        {
            VNAConfigSection vNAConfigSection = VNAConfigSection.Instance;
            SiteElement siteConfigElement = vNAConfigSection.SiteElement;

            String uniwebUrl = String.Format("{0}/DicomWeb/DicomWeb.dll/OpenImage?STUDYUID={1}", simagehomeurl, studyuid);
            string html5Url = siteConfigElement.Html5Home;// string.Empty;
            //html5Url = String.Format("{0}/html5/ShowImage.html?shareID={1}&sharePassword={2}", siteConfigElement.HomeUrl.TrimEnd('/'), share.share_id, share.passwd);
            //yys for multi study share.
            string htmlPrefix = siteConfigElement.UrlPath;// "UW360";// "InvokeImageDisplay";// "html5/index.html";
            html5Url = String.Format("{0}/{2}?STUDYUID={1}", html5Url, studyuid,htmlPrefix);//&SERIESUID={2} ,seriesuid

            string h5user = siteConfigElement.H5User;
            string h5pass = siteConfigElement.H5Pwd;
            //if (string.IsNullOrEmpty(h5user) && string.IsNullOrEmpty(h5pass))
            //{
            //    h5user = suser;
            //    h5pass = spwd;
            //}

            if (!string.IsNullOrEmpty(suser))
            {
                uniwebUrl = uniwebUrl + "&User=" + suser + "&Password=" + spwd;

            }
            if (!string.IsNullOrEmpty(h5user))
            {
                html5Url = html5Url + "&User=" + h5user + "&Password=" + h5pass;
            }
            ViewBag.UniwebUrl = uniwebUrl;
            ViewBag.Html5Url = html5Url;
            ViewBag.IDOViewerRegon = sidovieweregion;
            ViewBag.IDOUrl = String.Format("idoviewer://{0}/ShareQuery?STUDYUID={1}", sidovieweregion, studyuid);//&SERIESUID={2} seriesuid

            FileLog.Info("OpenStuduyWith:" + html5Url + " " + uniwebUrl +" "+ ViewBag.IDOUrl as string);

            if(Session["SelectViewer"] !=null )
            {
                string selected = Session["SelectViewer"] as string;
                string sredrict = string.Empty;
                if (selected == "0")
                    sredrict = ViewBag.IDOUrl as string;
                else if (selected == "1")
                    sredrict = uniwebUrl;
                else if (selected == "2")
                    sredrict = html5Url;
                if (!string.IsNullOrEmpty(sredrict))
                    return Redirect(sredrict);
            }
            return View("OpenFile");
        }
        public ActionResult OpenVNAUpload(string studyuid)
        {
            VNAConfigSection vNAConfigSection = VNAConfigSection.Instance;
            SiteElement siteConfigElement = vNAConfigSection.SiteElementV; //from portal to vna...
            ViewerRegionElement viewerregion = vNAConfigSection.viewerRegionElement;

            string shomeurl = siteConfigElement.Home.TrimEnd('/');
            string simagehomeurl = siteConfigElement.ImageHome.TrimEnd('/');
            if (string.IsNullOrEmpty(simagehomeurl))
                simagehomeurl = shomeurl;

            //ido 可能有点问题，因为在VNA。。。。。
            string sidovieweregion = viewerregion.IDOViewerRegion;
            if (string.IsNullOrEmpty(sidovieweregion))
                sidovieweregion = "iDoCloud";

            string suser = siteConfigElement.UniUser;
            string spwd = siteConfigElement.UniPwd;

            return OpenStuduyWith(studyuid, simagehomeurl, sidovieweregion, suser, spwd);
        }
        /// <summary>
        /// 一个download可能多个stdudy....，这里只打开了一个study
        /// </summary>
        /// <param name="download_uid"></param>
        /// <returns></returns>
        public ActionResult OpenDownload(string download_uid)
        {
            VnaDbContext vnaDbContext = new VnaDbContext();
            IDownloadRepository downloadRepository = new DownloadRepository(vnaDbContext);

            Download download = downloadRepository.FindEntity<Download>(t => t.download_uid == download_uid);
            string studyuid = string.Empty;
            if (download != null)
                studyuid = download.study_uid;
            //
            VNAConfigSection vNAConfigSection = VNAConfigSection.Instance;
            SiteElement siteConfigElement = vNAConfigSection.SiteElementV; //from portal to vna...
            ViewerRegionElement viewerregion = vNAConfigSection.viewerRegionElement;

            string shomeurl = siteConfigElement.Home.TrimEnd('/');
            string simagehomeurl = siteConfigElement.ImageHome.TrimEnd('/');
            if (string.IsNullOrEmpty(simagehomeurl))
                simagehomeurl = shomeurl;

            string sidovieweregion = viewerregion.IDOViewerRegion;
            if (string.IsNullOrEmpty(sidovieweregion))
                sidovieweregion = "iDoCloud";

            string suser = siteConfigElement.UniUser;
            string spwd = siteConfigElement.UniPwd;

            return OpenStuduyWith(studyuid,simagehomeurl,sidovieweregion,suser,spwd);
        }
        public ActionResult MonitorFiles()
        {
            OperatorModel operatorProvider = OperatorProvider.Provider.GetCurrent();
            if (operatorProvider == null)
                return Error("no login", JsonRequestBehavior.AllowGet);


            int iuserid_id = 0;
            int.TryParse( operatorProvider.UserCode,out iuserid_id);

            VnaDbContext vnaDbContext = new VnaDbContext();
            IUserAuthorDeptRepository userAuthorDeptRepository = new UserAuthorDeptRepository(vnaDbContext);
            List<UserAuthorDept> userAuthorDepts = userAuthorDeptRepository.GetUserAuthorDept(iuserid_id);
            ViewBag.userAuthorDepts = userAuthorDepts;

            IDepartmentRepository departmentRepository = new DepartmentRepository(vnaDbContext);
            List<Department> departments = departmentRepository.GetDepartmentList();

            string sAutho = operatorProvider.Authority;
            bool bUploadRight = RoleActor.HasAuthorityRights(sAutho, AuthorityRights.Monitor);
            if (!bUploadRight)
            {
                //看一下，当authority为0或空时，group是否有权限
                if (sAutho == "0"||string.IsNullOrEmpty(sAutho))
                {
                    IRoleRepository roleRepository = new RoleRepository(vnaDbContext);
                    DataModel.Role role = roleRepository.GetRole(operatorProvider.RoleId);
                    //
                    if(role != null)
                        bUploadRight = role.monitor_enable;
                    else
                    {
                        //如果没有...额外限制了super的权限...
                        BaseData.Role rolea = BaseData.RoleActor.GetRole(operatorProvider.RoleId);
                        if (rolea == BaseData.Role.Super)
                            bUploadRight = true;

                    }
                }
                if(!bUploadRight)
                    return View("NoRight");
            }

            ViewBag.departments = departments;

            return View();
        }
        [HttpPost]
        public ActionResult QueryDownload(string medicno,string nativeid,string name,string studydate, string studydate2, 
            string modalities,string uploaddate, string uploaddate2,
           string refphysician, string dept,int pageLimit,int pageIndex,string sortBy, string order)
        {
            //
            if (string.IsNullOrEmpty(order))
                order = "asc";

            Pagination pagination = new Pagination();
            pagination.rows = pageLimit;
            if (pageIndex <= 0)
                pageIndex = 1;
            pagination.page = pageIndex;
            if (string.IsNullOrEmpty(sortBy) || sortBy == "1")
            {
                sortBy = "medical_num";
                //order = "asc";
            }
            else if (sortBy == "2")
                sortBy = "native_id";
            else if (sortBy == "3")
                sortBy = "first_name,last_name";
            else if (sortBy == "4")
                sortBy = "modality"; //modalities
            else if (sortBy == "5")
                sortBy = "study_date";
            else if (sortBy == "6")
                sortBy = "create_date";
            else if (sortBy == "7")
                sortBy = "ref_physician";
            else if (sortBy == "8")
                sortBy = "source_dept";

            pagination.sord = order;// "";
            pagination.sidx = sortBy;// ""; 

            FileLog.Info("call QueryVNA ");
            //
            VNAServiceReference1.VNAService1Client vNAService1Client = new VNAServiceReference1.VNAService1Client();
            try
            {


                VNAServiceReference1.QueryParam queryParam = new VNAServiceReference1.QueryParam()
                {
                    deptid = dept,
                    medicno = medicno,
                    modality = modalities,
                    name = name,
                    nativeid = nativeid,
                    refphysician = refphysician,
                    studydate = studydate,
                    studydate2=studydate2,
                    uploaddate = uploaddate,
                    uploaddate2=uploaddate2
                };
                VNAServiceReference1.QueryReturn queryReturn = vNAService1Client.QueryVNA(queryParam, pagination);
                FileLog.Info("QueryVNA return " + queryReturn.ToJson());

                VNAServiceReference1.VNA_Upload_Model[] uploads = null;
                if (queryReturn != null)
                {
                    uploads = queryReturn.VNA_Uploads;
                    pagination = queryReturn.Pagination; 
                }
                vNAService1Client.Close(); //close.....
                //if (uploads != null)
                {
                    var uploadret = new { pageCount = pagination.total, datas = uploads, ViewerRegion = queryReturn.ViewerRegion };
                    return Success("success", uploadret);
                }
            }
            catch (Exception ex)
            {
                FileLog.Error(ex.Message + " " + ex.StackTrace + "" + ex.Source );
                if(ex.InnerException!= null)
                    FileLog.Error(ex.InnerException.Message + " " + ex.InnerException.StackTrace + "" + ex.InnerException.Source);
            }

            return Error("something wrong.");
        }

        public ActionResult SendDownloadLink()
        {
            OperatorModel operatorProvider = OperatorProvider.Provider.GetCurrent();
            if (operatorProvider == null)
                return Error("no login", JsonRequestBehavior.AllowGet);

            string userid = operatorProvider.UserId;


            VnaDbContext vnaDbContext = new VnaDbContext();
            IDepartmentRepository departmentRepository = new DepartmentRepository(vnaDbContext);
            //List<Department> departments = departmentRepository.FindList<Department>("select * from department");

            List<Department> departments = departmentRepository.GetDepartmentList();

            ViewBag.departments = departments;
            ViewBag.user_id_id = operatorProvider.UserCode;

            string sAutho = operatorProvider.Authority;

            bool bUploadRight = RoleActor.HasAuthorityRights(sAutho, AuthorityRights.SendDownload);
            if (!bUploadRight)
            {
                //
                if (sAutho == "0" || string.IsNullOrEmpty(sAutho))
                {
                    IRoleRepository roleRepository = new RoleRepository(vnaDbContext);
                    DataModel.Role role = roleRepository.GetRole(operatorProvider.RoleId);
                    //
                    if (role != null)
                        bUploadRight = role.senddown_enable;
                    else
                    {
                        //如果没有...额外限制了super的权限...
                        BaseData.Role rolea = BaseData.RoleActor.GetRole(operatorProvider.RoleId);
                        if (rolea == BaseData.Role.Super)
                            bUploadRight = true;

                    }
                }
                if (!bUploadRight)
                    return View("NoRight");// Content("You have no permission.");
            }
            return View();
        }
        [HttpPost]
        public ActionResult TransInfoToVNA(string[] upload_ids)
        {
            if (upload_ids == null)
                return Error("parameter is empty.");
            //
            try{
                VnaDbContext vnaDbContext = new VnaDbContext();
                IUploadRepository uploadRepository = new UploadRepository(vnaDbContext);
                IUploadItemRepository uploadItemRepository = new UploadItemRepository(vnaDbContext);

                List<DbParameter> dbParameters = new List<DbParameter>();
                List<DbParameter> dbParameters2 = new List<DbParameter>();

                Dictionary<int, string> keyValuePairs = new Dictionary<int, string>();
                Dictionary<int, string> keyValueUpId = new Dictionary<int, string>();

                string sql = string.Empty;
                sql = "select distinct upload.* from upload,upload_item where upload.upload_id_id=upload_item.upload_id_id and status='4' ";//and (status='' or status is NULL )
                                                                                                                                            //sql = "select DISTINCT upload.*,users.user_name,users.email ,upload_item.src_file_name from upload,upload_item,users where  upload.user_id_id=users.user_id_id  ";
                                                                                                                                            //sql += " and upload.upload_id_id=upload_item.upload_id_id ";

                StringBuilder stringBuilder = new StringBuilder();
                if (upload_ids.Length > 0)
                {
                    stringBuilder.Append(" and ");
                    stringBuilder.Append("( ");
                }
                for (int ii = 0; ii < upload_ids.Length; ++ii)
                {
                    if (ii > 0)
                        stringBuilder.Append(" or ");
                    string upload_id = upload_ids[ii];
                    stringBuilder.Append("upload.upload_id=@upload" + ii.ToString());

                    SqlParameter sqlParameter = new SqlParameter("@upload" + ii.ToString(), upload_ids[ii]);
                    dbParameters.Add(sqlParameter);
                    //
                    sqlParameter = new SqlParameter("@upload" + ii.ToString(), upload_ids[ii]);
                    dbParameters2.Add(sqlParameter);


                }
                if (upload_ids.Length > 0)
                    stringBuilder.Append(" )");
                sql = sql + " " + stringBuilder.ToString();

                List<Upload> uploads = uploadRepository.FindList<Upload>(sql, dbParameters.ToArray());
                List<VNA_Upload> vNA_Uploads = new List<VNA_Upload>();
                if (uploads != null)
                {
                    foreach (Upload up in uploads)
                    {
                        VNA_Upload vNA_Upload = new VNA_Upload()
                        {
                            comment = (string.IsNullOrEmpty(up.comment) ? string.Empty : up.comment),
                            create_date = up.create_date,
                            first_name = (string.IsNullOrEmpty(up.first_name) ? string.Empty : up.first_name),
                            last_name = (string.IsNullOrEmpty(up.last_name) ? string.Empty : up.last_name),
                            medical_num = (string.IsNullOrEmpty(up.medical_num) ? string.Empty : up.medical_num),
                            middle_name = (string.IsNullOrEmpty(up.middle_name) ? string.Empty : up.middle_name),

                            native_id = (string.IsNullOrEmpty(up.native_id) ? string.Empty : up.native_id),
                            ref_physician = (string.IsNullOrEmpty(up.ref_physician) ? string.Empty : up.ref_physician),
                            report_type = (string.IsNullOrEmpty(up.report_type) ? string.Empty : up.report_type),
                            series_uid = (string.IsNullOrEmpty(up.series_uid) ? string.Empty : up.series_uid),
                            source_dept = (string.IsNullOrEmpty(up.source_dept) ? string.Empty : up.source_dept),
                            source_ofdata = (string.IsNullOrEmpty(up.source_ofdata) ? string.Empty : up.source_ofdata),
                            study_date = up.study_date,//?? DateTime.Now.AddYears(500), //?? 运算符..给一个非法的年数
                            study_uid = (string.IsNullOrEmpty(up.study_uid) ? string.Empty : up.study_uid),
                            upload_id = up.upload_id,
                            upload_id_id = up.upload_id_id,
                            upload_sessionid = up.upload_sessionid,
                            user_id_id = up.user_id_id
                        };
                        vNA_Uploads.Add(vNA_Upload);

                        keyValuePairs.Add(up.upload_id_id, up.study_uid);
                        keyValueUpId.Add(up.upload_id_id, up.upload_id);
                    }
                }
                //upload info to vna
                VNAServiceReference1.VNAService1Client vNAService1Client = new VNAServiceReference1.VNAService1Client();
                //vNAService1Client.Endpoint.Binding.SendTimeout = new TimeSpan(0, 2, 0);//OpenTimeout
                //web.config中也可以设timeout
                if (vNA_Uploads != null)
                {
                    FileLog.Info("Try to UploadInfo  " + vNA_Uploads.ToJson() + "\r\n");

                    VNAServiceReference1.ReturnState returnState = vNAService1Client.UpLoadInfo(vNA_Uploads.ToArray());

                    FileLog.Info("UploadInfo  " + returnState.IsSuccess.ToString() + "\r\n");

                    if (!returnState.IsSuccess)
                        return Error(returnState.Message);
                }
                //upload与upload_item是一对一的关系....这样，就可以如此处理...
                //
                string sql2 = "select upload_item.* from upload,upload_item where upload.upload_id_id=upload_item.upload_id_id ";//and (status=='' or status is NULL )

                sql2 = sql2 + " " + stringBuilder.ToString();

                bool bhasWrong = false, bhasRight = false;

                FileLog.Info("UploadInfo  SQL:" + sql2 + "\r\n");

                List<UploadItem> uploadItems = uploadItemRepository.FindList<UploadItem>(sql2, dbParameters2.ToArray());
                if (uploadItems != null)
                {
                    //upload file info to vna
                    bool bReturn = true;
                    string message = string.Empty;
                    foreach (UploadItem itm in uploadItems)
                    {
                        string fileName = itm.src_file_name;
                        string orgtype = fileName.Substring(fileName.LastIndexOf(".") + 1);


                        if (System.IO.File.Exists(itm.dest_file_name))
                        {
                            FileStream fileStream = new FileStream(itm.dest_file_name, FileMode.Open, FileAccess.Read);
                            //string sdate = DateTime.Now.ToString("yyyyMMdd");
                            string lstuduid = string.Empty;
                            if (keyValuePairs.ContainsKey(itm.upload_id_id))
                                lstuduid = keyValuePairs[itm.upload_id_id];
                            //yys for zip file...
                            //MemoryStream compressedFileStream = new MemoryStream();

                            //System.IO.Compression.GZipStream compressionStream = new System.IO.Compression.GZipStream(compressedFileStream,
                            //     System.IO.Compression.CompressionMode.Compress);

                            //fileStream.CopyTo(compressionStream);
                            //compressedFileStream.Position = 0;
                            //-------------------------------------------------------//

                            string dicomfile = Path.GetFileName(itm.dest_file_name);

                            FileLog.Info("start Upload  File:" + itm.dest_file_name + "\r\n");
                            //fileStream compressedFileStream
                            bReturn = vNAService1Client.UpLoadFile(dicomfile, fileStream.Length, itm.upload_id_id, itm.upload_id_id.ToString(), itm.instance_uid, fileName, orgtype, "",
                                lstuduid, itm.upload_at.Value, fileStream, out message); //fileStream

                            FileLog.Info("UpLoadFile  " + dicomfile + " : " + bReturn.ToString() + " " + message + "\r\n");
                            fileStream.Close();
                            fileStream.Dispose();

                            if (!bReturn)
                            {
                                //wrtie log.
                                //if (keyValueUpId.ContainsKey(itm.upload_id_id))
                                //{
                                //    string luploadid = keyValueUpId[itm.upload_id_id];

                                //    VNAServiceReference1.ReturnState rst = vNAService1Client.DeleteUpload(luploadid);

                                //    FileLog.Info("DeleteUpload  " + rst.IsSuccess.ToString() + " " + rst.Message);
                                //}
                                // break;

                            }

                        }
                        else
                        {
                            //write log...
                            //file is not existed..
                            bReturn = false;
                            message = itm.dest_file_name + " is not existed,please check...\r\n";

                            FileLog.Info(message);

                            // break;
                        }

                        if (bReturn)
                        {

                            try
                            {
                                FileLog.Info("updating audit:" + itm.ToJson());
                                //update state..
                                itm.status = Upload_Status.audited;
                                uploadItemRepository.UpdateUpload(itm);
                                uploadItemRepository.Commit();

                                bhasRight = true;

                            }
                            catch (Exception ex)
                            {
                                FileLog.Error(ex.Message + " " + ex.StackTrace + "" + ex.Source + "\r\n");

                                bReturn = false;
                            }
                            //181213....
                            if (bReturn)
                            {
                                //上传到vna后，最后，最好将本地的文件删除包括eps里面的
                                try
                                {
                                    System.IO.File.Delete(itm.dest_file_name);
                                    //eps的先不删除，免得当在view files里查看影像时，不能显示...
                                }
                                catch (Exception ex)
                                {
                                    FileLog.Info("delete dest_file_name" + ex.Message);
                                }
                            }

                        }

                        if (!bReturn)
                        {
                            bhasWrong = true;

                            if (keyValueUpId.ContainsKey(itm.upload_id_id))
                            {
                                string luploadid = keyValueUpId[itm.upload_id_id];

                                VNAServiceReference1.ReturnState rst = vNAService1Client.DeleteUpload(luploadid);

                                FileLog.Info("DeleteUpload " + rst.IsSuccess.ToString() + " " + rst.Message);
                            }
                        }
                    }
                    if (bhasWrong && bhasRight)//!bReturn)
                        return Success("partial success");// 
                    else if (!bhasWrong && bhasRight)
                        return Success("success");
                    else if (bhasWrong && !bhasRight)
                        return Error("UploadFile failed:" + message);


                }
                else
                {
                    FileLog.Info("find no upload_item :" + sql2);
                }

            }
            catch (Exception ex)
            {
                FileLog.Error(ex.Message + " " + ex.StackTrace + "" + ex.Source);
                if (ex.InnerException != null)
                    FileLog.Error(ex.InnerException.Message + " " + ex.InnerException.StackTrace + "" + ex.InnerException.Source);

                return Error("TransToVna:" + ex.Message);
            }


            return Success("success");
        }
        public class DownParam
        {
            public string uid { get; set; }
            public string filename { get; set; }
        }
       // [HttpPost]
       /// <summary>
       /// download study
       /// </summary>
       /// <param name="uids">study uids</param>
       /// <returns></returns>
        public ActionResult DownloadActualStudy(string studyuid)
        {
            if (string.IsNullOrEmpty(studyuid) )
                return Error("parameter is null");

            FileLog.Info("start QueryInstanceUidsOfStudy:" + studyuid);
            //1.get instance uid from vna service..
            //2.download files..
            VNAServiceReference1.VNAService1Client vNAService1Client = new VNAServiceReference1.VNAService1Client();
            string[] uids = vNAService1Client.QueryInstanceUidsOfStudy(studyuid);
            //
            FileLog.Info("return QueryInstanceUidsOfStudy:" + uids.ToJson());
            vNAService1Client.Close();

            return DownloadActualFiles(uids);

        }
        public ActionResult DownloadActualStudyEs(string[] studyuids)
        {
            if (studyuids==null || studyuids.Length ==0)
                return Error("parameter is null");

            FileLog.Info("start QueryInstanceUidsOfStudy:" + studyuids.ToJson());
            //List<string> uidss = new List<string>();
            //1.get instance uid from vna service..
            //2.download files..
            VNAServiceReference1.VNAService1Client vNAService1Client = new VNAServiceReference1.VNAService1Client();

            var outputStream = new MemoryStream();

            VnaDbContext vnaDbContext = new VnaDbContext();
            //IUploadRepository uploadRepository = new UploadRepository(vnaDbContext);
            IDownloadItemRepository downloadItemRepository = new Download_ItemRepository(vnaDbContext);

         
            try
            {
                using (var zip = new ZipFile(Encoding.Default)) //指定unicode，避免中文乱码
                {
                    for (int i = 0; i < studyuids.Length; ++i)
                    {
                        string[] uids = vNAService1Client.QueryInstanceUidsOfStudy(studyuids[i]);

                        //if (uids != null)
                        //    uidss.AddRange(uids);
                        //
                        FileLog.Info("return QueryInstanceUidsOfStudy:" + studyuids[i] + " :" + uids != null ? uids.ToJson() : "null");
                        
                        //添加目录
                        zip.AddDirectoryByName(studyuids[i]);

                        foreach (string uid in uids) //(DownParam down in downParams)
                        {
                            bool bf = false;
                            string message = string.Empty;
                            Stream fileStream = null;
                            FileLog.Info("start download " + uid);// + down.ToJson());
                            string filename = string.Empty;
                            long filesize = 0;
                            filename = vNAService1Client.DownLoadFile(uid, out filesize, out bf, out message, out fileStream);


                            if (bf)
                            {
                                FileLog.Info("download file:" + filename + " " + Uri.UnescapeDataString(filename));
                                filename = Uri.UnescapeDataString(filename);


                                FileLog.Info("end download " + uid);// down.ToJson());
                                                                    //
                                //文件加在目录名下面..
                                zip.AddEntry(studyuids[i]+"\\"+ filename, fileStream);

                                //save download_item..update.. 暂时不写？
                                Download_Item download_Item = new Download_Item();
                                download_Item.download_id = 0;
                                download_Item.file_instanceid = uid;
                                download_Item.file_path = string.Empty;
                                download_Item.src_filename = filename;
                                download_Item.src_filetype = MimeMapping.GetMimeMapping(filename);
                                //
                                downloadItemRepository.InsertDownloadItem(download_Item);
                                //
                                FileLog.Info("InsertDownloadItem " + download_Item.ToJson());

                            }
                            else
                                FileLog.Info("failed download " + uid);// down.ToJson());
                        }

                    }
                    downloadItemRepository.Commit();

                    zip.Save(outputStream);
                }

                outputStream.Position = 0;

                vNAService1Client.Close();

                return File(outputStream, "application/zip", "downloads.zip");
            }
            catch (Exception ex)
            {
                FileLog.Error(ex.Message + ex.StackTrace + ex.Source);
                if (ex.InnerException != null)
                    FileLog.Error(ex.InnerException.Message + ex.InnerException.StackTrace + ex.InnerException.Source);
            }

            return Error("error");

        }

        /// <summary>
        /// download file
        /// </summary>
        /// <param name="uids">instance uid</param>
        /// <returns></returns>
        public ActionResult DownloadActualFiles(string[] uids)//List<DownParam> downParams
        {
            if (uids == null)
                return Error("parameter is null");
            //
            FileLog.Info("start DownloadActualFiles:" + uids.ToJson());

            VNAServiceReference1.VNAService1Client client = new VNAServiceReference1.VNAService1Client();

            var outputStream = new MemoryStream();

            VnaDbContext vnaDbContext = new VnaDbContext();
            //IUploadRepository uploadRepository = new UploadRepository(vnaDbContext);
            IDownloadItemRepository downloadItemRepository = new Download_ItemRepository(vnaDbContext);

            //using (var zip = new ZipFile())
            //{
            //    zip.AddEntry("file1.txt", "content1");
            //    zip.AddEntry("file2.txt", "content2");
            //    zip.Save(outputStream);
            //}
            try
            {
                using (var zip = new ZipFile(Encoding.Default)) //指定unicode，避免中文乱码
                {

                    foreach (string uid in uids) //(DownParam down in downParams)
                    {
                        bool bf = false;
                        string message = string.Empty;
                        Stream fileStream = null;
                        FileLog.Info("start download " + uid);// + down.ToJson());
                        string filename = string.Empty;
                        long filesize = 0;
                        filename = client.DownLoadFile(uid, out filesize, out bf, out message, out fileStream);

                        
                        if (bf)
                        {
                            FileLog.Info("download file:" + filename + " " + Uri.UnescapeDataString(filename));
                            filename = Uri.UnescapeDataString(filename);
                            

                            FileLog.Info("end download " + uid);// down.ToJson());
                                                                //
                           
                            zip.AddEntry(filename,fileStream);

                            //save download_item..update.. 暂时不写？
                            Download_Item download_Item = new Download_Item();
                            download_Item.download_id = 0;
                            download_Item.file_instanceid = uid;
                            download_Item.file_path = string.Empty;
                            download_Item.src_filename = filename;
                            download_Item.src_filetype = MimeMapping.GetMimeMapping(filename);
                            //
                            downloadItemRepository.InsertDownloadItem(download_Item);
                            //
                            FileLog.Info("InsertDownloadItem " + download_Item.ToJson());

                        }
                        else
                            FileLog.Info("failed download " + uid);// down.ToJson());
                    }

                    downloadItemRepository.Commit();

                    zip.Save(outputStream);
                }

                outputStream.Position = 0;

                client.Close();

                return File(outputStream, "application/zip", "downloads.zip");
            }
            catch (Exception ex)
            {
                FileLog.Error(ex.Message + ex.StackTrace + ex.Source);
                if(ex.InnerException!=null)
                FileLog.Error(ex.InnerException.Message + ex.InnerException.StackTrace + ex.InnerException.Source);
            }

            return Error("error");
        }

        public ActionResult DownloadActualFile(string uid,string filename)
        {
        
            //
            VNAServiceReference1.VNAService1Client client = new VNAServiceReference1.VNAService1Client();

         //   var outputStream = new MemoryStream();

            VnaDbContext vnaDbContext = new VnaDbContext();
            //IUploadRepository uploadRepository = new UploadRepository(vnaDbContext);
            IDownloadItemRepository downloadItemRepository = new Download_ItemRepository(vnaDbContext);

            //using (var zip = new ZipFile())
            //{
            //    zip.AddEntry("file1.txt", "content1");
            //    zip.AddEntry("file2.txt", "content2");
            //    zip.Save(outputStream);
            //}
            //using (var zip = new ZipFile())
            //{

                
                
                    bool bf = false;
                    string message = string.Empty;
                    Stream fileStream = null;
                    FileLog.Info("start download " + uid);
                    string lfilename = string.Empty;
                    long filesize = 0;
                    client.DownLoadFile(uid, out filesize, out bf, out message, out fileStream);
                    if (bf)
                    {
                        FileLog.Info("end download " + uid);
                        //

                       // zip.AddEntry(lfilename, fileStream);

                        //save download_item..update.. 暂时不写？
                        Download_Item download_Item = new Download_Item();
                        download_Item.download_id = 0;
                        download_Item.file_instanceid = uid;
                        download_Item.file_path = string.Empty;
                        download_Item.src_filename = lfilename;
                        download_Item.src_filetype = MimeMapping.GetMimeMapping(lfilename);
                        //
                        downloadItemRepository.InsertDownloadItem(download_Item);
                        //


                    }
                    else
                        FileLog.Info("failed download " + uid);
                

                downloadItemRepository.Commit();

            return File(fileStream, MimeMapping.GetMimeMapping(lfilename), lfilename);
              //  zip.Save(outputStream);
           // }

           // outputStream.Position = 0;

            //return File(outputStream, "application/zip", "downloads.zip");
        }

        public ActionResult DownloadFromPacs()
        {
            int servercount = VNAConfigSection.Instance.ServerList.Count;
            Dictionary<string,string> lstservers = new Dictionary<string, string>();
       
            for (int ii = 0; ii < servercount; ++ii)
            {
                ServerElement serverElement0 = VNAConfigSection.Instance.ServerList[ii];
                lstservers.Add(serverElement0.AE,serverElement0.DisplayName);
            }

            ViewBag.servers = lstservers;

            OperatorModel operatorProvider = OperatorProvider.Provider.GetCurrent();
            if (operatorProvider == null)
                return Error("no login", JsonRequestBehavior.AllowGet);

            VnaDbContext vnaDbContext = new VnaDbContext();
            string sAutho = operatorProvider.Authority;

            bool bUploadRight = RoleActor.HasAuthorityRights(sAutho, AuthorityRights.DownloadFromPacs);
            if (!bUploadRight)
            {
                //
                if (sAutho == "0" || string.IsNullOrEmpty(sAutho))
                {
                    IRoleRepository roleRepository = new RoleRepository(vnaDbContext);
                    DataModel.Role role = roleRepository.GetRole(operatorProvider.RoleId);
                    //
                    if (role != null)
                        bUploadRight = role.downloadpacs_enable;
                    else
                    {
                        //If not ... additionally restricts super's permissions...
                        BaseData.Role rolea = BaseData.RoleActor.GetRole(operatorProvider.RoleId);
                        if (rolea == BaseData.Role.Super)
                            bUploadRight = true;

                    }
                }
                if (!bUploadRight)
                    return View("NoRight"); //Content("You have no permission.");
            }

            return View();
        }
        //Consider not putting all parameters in the function list，
        //Instead, use Request to get the parameters in the url...
        [HttpPost] //string serverIp,int iPort,
        public ActionResult QueryDicomServer(string server, string medicno,string name,string studydateStart,string studydateEnd,
            string modalities, int pageLimit, int pageIndex, string sortBy, string order)
        {
          
            return Success("success");

        }
        /// <summary>
        /// DcmCommu,To become a global, when multiple users are concurrent .... otherwise there is no way to deal with...
        /// </summary>
        /// <param name="studyuids"></param>
        /// <param name="serverAE"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult TransDicomMoveToVNA(string[] studyuids,int[] studyFileCounts, string serverAE)
        {
            if (studyuids == null || studyuids.Length == 0)
                return Error("studyuids is empty");
            if (string.IsNullOrEmpty(serverAE))
                return Error("server is empty");
            //

            for (int i = 0; i < studyuids.Length; ++i)
            {
                var json = TransDicomMoveToVNAForStudy(studyuids[i], studyFileCounts[i], serverAE);
                if (json != null)
                {
                    FileLog.Info("TransDicomMoveToVNAForStudy : study " + json.ToJson());
                    
                    //AjaxResult ajaxresult = Newtonsoft.Json.JsonConvert.DeserializeObject(json.ToJson(), typeof(AjaxResult)) as AjaxResult;
                    //if (ajaxresult != null&& ajaxresult.state.ToString()!="success" && ajaxresult.state.ToString() !="1")
                    //{
                    //    FileLog.Info("TransDicomMoveToVNAForStudy : study " + studyuids[i] + " failed." + ajaxresult.message);
                    //}
                }
            }
            //
            return Success("finished");
        }
        public ActionResult TransDicomMoveToVNAForStudy(string studyuid,int fileCounts,string serverAE)
        {
            //
      


            return Success("success");
        }
        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}