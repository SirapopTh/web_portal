﻿
function showLoadingDialog() {
    var loadingDialog = $('#loadingDialog');
    loadingDialog.modal('show');
}

function hideLoadingDialog() {
    var loadingDialog = $('#loadingDialog');
    loadingDialog.modal('hide');
}

function convertUTCDateToLocalDate(date) {
    var newDate = new Date(date.getTime() + date.getTimezoneOffset() * 60 * 1000);

    var offset = date.getTimezoneOffset() / 60;
    var hours = date.getHours();

    newDate.setHours(hours - offset);

    return newDate;
}