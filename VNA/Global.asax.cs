﻿using BaseData;
using Common;
using DataModel;
using DicomKit;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using VNA.Models;

namespace VNA
{
    public class MvcApplication : System.Web.HttpApplication
    {
        private static Timer timer;
        private static readonly object _syncLock = new object();
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            //yys add dicom dll path. 放到bin下面
            //对于64位的dll,调试时iis express要设成64位，在工具-选项-项目和解决方案-web顶目里.
            String _path = String.Concat(System.Environment.GetEnvironmentVariable("PATH"), ";", System.AppDomain.CurrentDomain.RelativeSearchPath);
            System.Environment.SetEnvironmentVariable("PATH", _path, EnvironmentVariableTarget.Process);

            //
            try
            {
                if(!System.IO.Directory.Exists(Server.MapPath("~/Log")))
                    System.IO.Directory.CreateDirectory(Server.MapPath("~/Log"));
            }
            catch//(Exception ex)
            {

            }
            if (timer == null)
            {
                lock (_syncLock)
                {
                    if (timer == null)
                    {
                        timer = new Timer(sotreDcmFiles, null, 0, 300 * 1000);
                    }
                }

            }
            //
            //string sLocalAe = VNAConfigSection.Instance.epsElement.LocalAE;
            //int ilocalPort = 1288;
            //int.TryParse(VNAConfigSection.Instance.epsElement.LocalPort,out ilocalPort);

            //SyncQueryUtil.ListenServer(sLocalAe, ilocalPort);
        }
        /// <summary>
        /// 对未上传的文件进行上传...
        /// </summary>
        /// <param name="state"></param>
        private void sotreDcmFiles(object state)
        {
            bool bautoStore = true;
            if (!bautoStore)
                return;

            VnaDbContext vnaDbContext = new VnaDbContext();
            IUploadItemRepository uploadItemRepository = new UploadItemRepository(vnaDbContext);
            IUploadRepository uploadRepository = new UploadRepository(vnaDbContext);

            /*
            //---------------------------------//
            string sql = "select * from upload_item where (status is NULL or status='') "; //='3' failed
            List<UploadItem> uploadItems = uploadItemRepository.FindList<UploadItem>(sql, new SqlParameter[] { });
            //patient name->  "Last name^First name^Middle name^Prefix^Suffix"
            if (uploadItems != null)
            {
                int iidx = 0;
                foreach (UploadItem itm in uploadItems)
                {
                    iidx++;
                    //
                    string sfile = itm.dest_file_name;
                    //
                    Upload upload = uploadRepository.FindEntity<Upload>(t => t.upload_id_id == itm.upload_id_id);
                    if (upload == null)
                        continue;

                    string studyuid = string.Empty;
                    string seriesuid = string.Empty;
                    //已提前写入..
                    studyuid = upload.study_uid;// DicomKit.DicomTool.GenerateStudyInstanceUID("300", "100");
                    seriesuid = upload.series_uid;// DicomKit.DicomTool.GenerateStudyInstanceUID("400", "100");

                    string pid = upload.native_id;
                    string pname = string.Empty;
                    if (!string.IsNullOrEmpty(upload.last_name))
                    {
                        pname = upload.last_name.TrimStart().TrimEnd();
                    }
                    if (!string.IsNullOrEmpty(upload.first_name))
                    {
                        if (!string.IsNullOrEmpty(pname))
                            pname = pname + "^";
                        pname = pname + upload.first_name.TrimStart().TrimEnd();
                    }
                    //
                    string studydate = string.Empty;
                    string studytime = string.Empty;
                    if (upload.create_date != null)
                    {
                        studydate = ((DateTime)upload.create_date).ToString("yyyyMMdd");
                        studytime = ((DateTime)upload.create_date).ToString("HHmmss");
                    }
                    string accessno = upload.medical_num;

                    string studydesc = upload.comment;
                    string refphysician = upload.ref_physician;

                    string instanceuid = DicomKit.DicomTool.GenerateStudyInstanceUID("1000", "100");
                    string sopClassUid = Configs.GetValue("sopclassuid");//私有的...

                    string studyID = string.Empty;
                    string modality = "DOC"; //OT
                    string instanceNumber = iidx.ToString();
                    string seriesNumber = "1";
                    string pixelspacing = string.Empty;
                    string patientBirthDate = string.Empty;
                    string patientSex = string.Empty;

                    string dcmfile = Path.Combine(Path.GetDirectoryName(sfile), Path.GetFileNameWithoutExtension(sfile)) + ".dcm";

                    int res = DicomKit.DicomTool.CreateImgDCM(sfile, dcmfile, studyuid, pid, pname, patientBirthDate, patientSex, studydate, studytime, accessno,
                        studyID, studydesc, modality, instanceNumber, seriesuid, instanceuid, sopClassUid, seriesNumber, pixelspacing, refphysician);

                    itm.instance_uid = instanceuid;

                    if (res == 0)
                    {
                        //成功
                        itm.dest_file_name = dcmfile;
                        itm.status = Upload_Status.toDcmSuccess;// "1";
                    }
                    else
                    {
                        itm.status = Upload_Status.toDcmFailed;// "3";//to dicom fail..
                                                               //  
                    }

                    
                    //如果要上传到eps...
                    if (res == 0) //bStoreToEps
                    {
                        List<string> lstDcmFiles = new List<string>();
                        lstDcmFiles.Add(dcmfile);
                        //
                        //start store file to eps..
                        StoreKit storeKit = StoreKit.Instance;
                        string sLocalAe = VNAConfigSection.Instance.epsElement.LocalAE;
                        string epsAe = VNAConfigSection.Instance.epsElement.EpsAE, epsIp = VNAConfigSection.Instance.epsElement.EpsIP;
                        string epsPort = VNAConfigSection.Instance.epsElement.EpsPort;
                        List<string> lstFailed = new List<string>();

                        storeKit.storeSopclassuid = Configs.GetValue("sopclassuid");
                        //storescu时，有问题,因为dicomlib里面的回调函数是全局的...
                        //意味着只能一个dicomCommu的实例的回调会被执行到..
                        int iref = storeKit.DicomStoreU(lstDcmFiles.ToArray(), sLocalAe, epsAe, epsIp, epsPort, "64", ref lstFailed);
                        if (iref == 0)
                        {
                            itm.status = Upload_Status.storeSuccess;// "4"; //store success.
                        }
                        else
                            itm.status = Upload_Status.storeFailed;// "7"; //store failed
                                                                   //
                    }
                    //update db...
                    uploadItemRepository.UpdateUpload(itm);
                    uploadItemRepository.Commit();
                }
            }
            */
            //----------------------------------------------//
            //
            List<UploadItem> uploadItems = uploadItemRepository.FindList<UploadItem>("select * from upload_item where status='7'");

            if (uploadItems != null)
            {
                foreach (UploadItem itm in uploadItems)
                {
                    //.......................

                    List<string> lstDcmFiles = new List<string>();

                        lstDcmFiles.Add(itm.dest_file_name);
                        //
                        //start store file to eps..
                        StoreKit storeKit = StoreKit.Instance;
                        string sLocalAe = VNAConfigSection.Instance.epsElement.LocalAE;
                        string epsAe = VNAConfigSection.Instance.epsElement.EpsAE, epsIp = VNAConfigSection.Instance.epsElement.EpsIP;
                        string epsPort = VNAConfigSection.Instance.epsElement.EpsPort;

                        List<string> lstFailed = new List<string>();

                        storeKit.storeSopclassuid = Configs.GetValue("sopclassuid");
                        //storescu时，有问题,因为dicomlib里面的回调函数是全局的...
                        //意味着只能一个dicomCommu的实例的回调会被执行到..
                        int iref = storeKit.DicomStoreU(lstDcmFiles.ToArray(), sLocalAe, epsAe, epsIp, epsPort, "64", ref lstFailed);
                        if (iref == 0)
                        {
                            itm.status = "4"; //store success.
                                              //update db...
                            uploadItemRepository.UpdateUpload(itm);
                            uploadItemRepository.Commit();
                        }
                        else
                            itm.status = "7"; //store failed
                                              //
                    
                        //
                    
                }
            }
        }
    }
}
