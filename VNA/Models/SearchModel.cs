﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VNA.Models
{
    public class SearchModel
    {
        public String MedicalNo { get; set; }
        public String NativeID { get; set; }
        public String Name { get; set; }
        public String Dept { get; set; }
        public int pageLimit { get; set; }
        public int pageIndex { get; set; }
        public String SortBy { get; set; }
        public String Order { get; set; }
    }
}