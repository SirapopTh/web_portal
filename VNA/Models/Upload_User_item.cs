﻿using DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VNA.Models
{
    /// <summary>
    /// The name in the user cannot be placed in the dataModel, because there will be problems during dynamic mapping, which causes an error in the upload.
    /// </summary>
    public class Upload_User_Item : IUpload //Upload
    {
        public string user_name { get; set; }
        public string email { get; set; }

        public string src_file_name { get; set; }
    }
}