﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VNA.Models
{
    public class Upload_Status
    {
        public const string initState = "";
        public const string toDcmSuccess = "1";
        public const string toDcmFailed = "3";
        public const string storeSuccess = "4";
        public const string storeFailed = "7";
        //
        public const string audited = "11"; //Reviewed

        public const string transToVNA = "12";
        
    }
}