﻿using DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VNA.Models
{
    public class Download_User :IDownload
    {
        public string user_name { get; set; } //creator name
        public string receiver_name { get; set; } //receive name
    }
}