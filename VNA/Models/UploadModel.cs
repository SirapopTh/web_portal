﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VNA.Models
{
    public class UploadModel
    {
        public string upload_id { get; set; }
        //public int user_id_id { get; }
        public string medical_num { get; set; }
        public string native_id { get; set; }
        public string first_name { get; set; }
        public string middle_name { get; set; }
        public string last_name { get; set; }

        public DateTime create_date { get; set; }

        public string ref_physician { get; set; }

        public string source_dept { get; set; } //Department corresponding to the file...
        public string report_type { get; set; }

        public string comment { get; set; }

        public List<string> upload_files { get; set; }
    }
}