﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VNA.Models
{

    public class StudyResult
    {
        public string study_uid { get; set; }
        //public int study_uid_id { get; set; }
        public string ptn_id { get; set; }
        public string ptn_name { get; set; }
        public string modalities { get; set; }
        public string study_date { get; set; }
        public string study_time { get; set; } //Nullable<decimal> 
        public string study_id { get; set; }
        public string accession_number { get; set; }
        public string physician_name { get; set; }
        public string study_desc { get; set; }
       // public string request_service { get; set; }
        public string request_physician { get; set; }
        public int instance_of_study { get; set; }
    }
}