﻿using BaseData;
using Common;
using DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VNA//.Misc
{
    public class MiscUitl
    {
        public static int AuthorityOfRole(string roleid)
        {
            if (string.IsNullOrEmpty(roleid))
                roleid = ((int)BaseData.Role.Patient).ToString();

            VnaDbContext vnaDbContext = new VnaDbContext();
            IRoleRepository repositoryBase = new RoleRepository(vnaDbContext);
            DataModel.Role role = repositoryBase.GetRole(roleid);

          

            int iauth = 0;
            if (role != null)
            {
                //Upload=1,ViewFile=2,Send=4,Monitor=8,ViewAll=16}
                if (role.monitor_enable)
                    iauth = iauth | (int)AuthorityRights.Monitor;
                if (role.send_enable)
                    iauth = iauth | (int)AuthorityRights.SendUpload;
                if (role.upload_enable)
                    iauth = iauth | (int)AuthorityRights.Upload;
                if (role.senddown_enable)
                    iauth = iauth | (int)AuthorityRights.SendDownload;
                if (role.view_enable)
                    iauth = iauth | (int)AuthorityRights.ViewFile;
                if (role.download_enable)
                    iauth = iauth | (int)AuthorityRights.DownloadFile;
                if (role.downloadpacs_enable)
                    iauth = iauth | (int)AuthorityRights.DownloadFromPacs;
                if (role.editgroup_enable)
                    iauth = iauth | (int)AuthorityRights.EditGroup;
                if (role.editperson_enable)
                    iauth = iauth | (int)AuthorityRights.EditPersonal;
            }
            return iauth;
        }
        public static bool HasAuthority(int iAuthority, AuthorityRights authorityRights)
        {
         
            bool bhas = ((iAuthority & (int)authorityRights) > 0);
            return bhas;

        }
            /// <summary>
            /// 某role是否具有某权限...
            /// </summary>
            /// <param name="roleid"></param>
            /// <param name="authorityRights"></param>
            /// <returns></returns>
            public static bool HasAuthorityOfRole(string roleid, AuthorityRights authorityRights)
        {
            bool bhas = false;
            try
            {
                if (string.IsNullOrEmpty(roleid))
                    roleid =((int) BaseData.Role.Patient).ToString();

                VnaDbContext vnaDbContext = new VnaDbContext();
                IRoleRepository repositoryBase = new RoleRepository(vnaDbContext);
                DataModel.Role role = repositoryBase.GetRole(roleid);
                int iauth = 0;
                if (role != null)
                {
                    //Upload=1,ViewFile=2,Send=4,Monitor=8,ViewAll=16}
                    if (role.monitor_enable)
                        iauth = iauth | (int)AuthorityRights.Monitor;
                    if (role.send_enable)
                        iauth = iauth | (int)AuthorityRights.SendUpload;
                    if (role.upload_enable)
                        iauth = iauth | (int)AuthorityRights.Upload;
                    if (role.senddown_enable)
                        iauth = iauth | (int)AuthorityRights.SendDownload;
                    if (role.view_enable)
                        iauth = iauth | (int)AuthorityRights.ViewFile;
                    if (role.download_enable)
                        iauth = iauth | (int)AuthorityRights.DownloadFile;
                    if (role.downloadpacs_enable)
                        iauth = iauth | (int)AuthorityRights.DownloadFromPacs;
                    if (role.editgroup_enable)
                        iauth = iauth | (int)AuthorityRights.EditGroup;
                    if (role.editperson_enable)
                        iauth = iauth | (int)AuthorityRights.EditPersonal;
                }

                bhas = ((iauth & (int)authorityRights) > 0);
            }
            catch (Exception ex)
            {
                Log log = LogFactory.GetLogger("MiscUitl");
                log.Error(ex.Message + ex.StackTrace + ex.Source);
            }
            //

            return bhas;
        }
    }
}