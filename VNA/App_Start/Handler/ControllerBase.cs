﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Common;

namespace VNA //.App_Start.Handler
{

     [HandlerLogin] //e有自动检查是否已登录过...
    public abstract class ControllerBase : Controller
    {
        public Log FileLog
        {
            get { return LogFactory.GetLogger(this.GetType().ToString()); }
        }
        [HttpGet]
        [HandlerAuthorize]
        public virtual ActionResult Index()
        {
            return View();
        }
        [HttpGet]
        [HandlerAuthorize]
        public virtual ActionResult Form()
        {
            return View();
        }
        [HttpGet]
        [HandlerAuthorize]
        public virtual ActionResult Details()
        {
            return View();
        }
        protected virtual ActionResult Success(string message)
        {
            //return Content(new AjaxResult { state = ResultType.success.ToString(), message = message }.ToJson());
            return Json(new AjaxResult { state = ResultType.success.ToString(), message = message });
        }
        protected virtual ActionResult Success(string message, object data)
        {
           // return Content(new AjaxResult { state = ResultType.success.ToString(), message = message, data = data }.ToJson());
            return Json(new AjaxResult { state = ResultType.success.ToString(), message = message, data = data });
        }
        protected virtual ActionResult Error(string message)
        {
            // return Content(new AjaxResult { state = ResultType.error.ToString(), message = message }.ToJson());
            return Json(new AjaxResult { state = ResultType.error.ToString(), message = message });
        }
        protected virtual ActionResult Error(string message,JsonRequestBehavior behavior)
        {
            // return Content(new AjaxResult { state = ResultType.error.ToString(), message = message }.ToJson());
            return Json(new AjaxResult { state = ResultType.error.ToString(), message = message },behavior);
        }
    }
}