﻿using BaseData;
using Common;
using DataModel;
using System.Web.Mvc;
using System.Web.Routing;

namespace VNA
{
    /// <summary>
    /// 登录及相关的一些验证...初始化...
    /// </summary>
    public class HandlerLoginAttribute : AuthorizeAttribute
    {
        public bool Ignore = true;

        public Log FileLog
        {
            get { return LogFactory.GetLogger(this.GetType().ToString()); }
        }

        public HandlerLoginAttribute(bool ignore = true)
        {
            Ignore = ignore;
        }
        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            if (Ignore == false)
            {
                return;
            }
            bool bRsna = false;// true;
            if(bRsna)
            {
                if(filterContext.RequestContext.HttpContext.Request.FilePath.Contains("Home/DownloadFiles")||
                    filterContext.RequestContext.HttpContext.Request.FilePath.Contains("Home/DownloadActualStudyEs"))
                {
                    return; //允许downloadFiles不需登录
                }
            }

            //如果是post 就不进入处理...
            if (filterContext.RequestContext.HttpContext.Request.RequestType == "POST")
            {
                bool btest = false;
                if (OperatorProvider.Provider.GetCurrent() == null || btest)//RequestContext.HttpContext.Response.Write
                    filterContext.Result = new ContentResult { Content = new { state = "0", message = "session or cookie expired." }.ToJson() };
                return;
            }
           // FileLog.Debug("HanlerLogin OnAuthorization " + filterContext.RequestContext.HttpContext.Request.Url.ToString());

            if (OperatorProvider.Provider.GetCurrent() == null)
            {
                WebHelper.WriteCookie("VNA_login_error", "overdue");
               // filterContext.HttpContext.Response.Write("<script>top.location.href = '/User/Login';</script>");
               //yys
               // var url = UrlHelper.GenerateUrl(null, "Login", "User", null, RouteTable.Routes, filterContext.HttpContext.Request.RequestContext, false);
                //filterContext.HttpContext.Response.Redirect(url);
                string ReturnUrl = filterContext.RequestContext.HttpContext.Request.Url.ToString();//所带的url..login后跳转...

                filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new { controller = "User", action = "Login", ReturnUrl = ReturnUrl }));

                return;
            }
      
            OperatorModel operatorProvider = OperatorProvider.Provider.GetCurrent();
            if (operatorProvider != null)
            {
                VnaDbContext vnaDbContext = new VnaDbContext();
                IUserRepository userRepository = new UserRepository(vnaDbContext);
                Users users = userRepository.GetUser(operatorProvider.UserId);
                if (users != null)
                {
                    if (!users.is_initial && RoleActor.GetRole(operatorProvider.RoleId) == BaseData.Role.Patient)
                    {
                        //如果是病人且是第一次登陆，强行要求更改密改...

                        //  var url = UrlHelper.GenerateUrl(null, "ChangePassword", "User", null, RouteTable.Routes, filterContext.HttpContext.Request.RequestContext, false);
                        //  filterContext.HttpContext.Response.Redirect(url);
                        //原来访问的地址，处理完后要跳转回去
                        string ReturnUrl = filterContext.HttpContext.Request.Url.ToString();//.QueryString["ReturnUrl"];

                        if (filterContext.RouteData.Values["action"].ToString() == "ChangePassword")
                        {
                            //ReturnUrl = string.Empty; //避免自己跳转自己
                            return;
                        }
                        RouteValueDictionary keyValuePairs = new RouteValueDictionary( new { controller ="User" , action = "ChangePassword", ReturnUrl=ReturnUrl,userid=users.user_id});

                        filterContext.Result = new RedirectToRouteResult(keyValuePairs);

                        return;////yys------------------need
                    }
                    //如果资料不全，就跳到Profiles
                    if (string.IsNullOrEmpty(users.user_name) || string.IsNullOrEmpty(users.medical_recno)
                        || string.IsNullOrEmpty(users.id_number))
                    {

                        // var url = UrlHelper.GenerateUrl(null, "Profiles", "User", null, RouteTable.Routes, filterContext.HttpContext.Request.RequestContext, false);
                        //filterContext.HttpContext.Response.Redirect(url);
                        //原来访问的地址,处理完后要跳转回去
                        string ReturnUrl = filterContext.HttpContext.Request.Url.ToString();// QueryString["ReturnUrl"];

                        if (filterContext.RouteData.Values["action"].ToString() == "Profiles")
                        {
                            //ReturnUrl = string.Empty; //避免自己跳转自己
                            return;
                        }
                        RouteValueDictionary keyValuePairs = new RouteValueDictionary(new { controller = "User", action = "Profiles", ReturnUrl = ReturnUrl });

                        filterContext.Result = new RedirectToRouteResult(keyValuePairs);
                        return;
                    }
                }
            }
        }
    }
    
}