﻿using System.Web;
using System.Web.Optimization;

namespace VNA
{
    public class BundleConfig
    {
        //For more information about bundling, please visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to be used for development and learning. Then, when you are done
            // Production is ready, please use https://modernizr.com The build tool on the select only the required tests.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js"));

            bundles.Add(new ScriptBundle("~/bundles/jquerymd5").Include(
                       "~/Scripts/jquery.md5.js"));

            bundles.Add(new ScriptBundle("~/bundles/sweetalert2.min").Include(
                       "~/Scripts/sweetalert2.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap-select").Include(
                       "~/Scripts/bootstrap-select.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css"
                      ));//,"~/Content/site.css"
                         // "~/Content/simple-sidebar.css"));//,
                         //"~/Content/myCSS.css") );
                         // CssRewriteUrlTransform When the website is not in the destination path, the path in css will be converted incorrectly
           //https://stackoverflow.com/questions/19765238/cssrewriteurltransform-with-or-without-virtual-directory
            bundles.Add(new StyleBundle("~/Content/myCss").Include("~/Content/myCSS.css", new CssRewriteUrlTransformMyWrapper())
                .Include("~/Content/myCSS_s2.css", new CssRewriteUrlTransformMyWrapper()));

            bundles.Add(new StyleBundle("~/Content/_LayoutLogOn").Include("~/Content/_LayoutLogOn.css", new CssRewriteUrlTransformMyWrapper())
                .Include("~/Content/_LayoutLogOn.css", new CssRewriteUrlTransformMyWrapper()));

            bundles.Add(new StyleBundle("~/Content/loader").Include("~/Content/loader.css", new CssRewriteUrlTransformMyWrapper())
                .Include("~/Content/loader.css", new CssRewriteUrlTransformMyWrapper()));

            bundles.Add(new StyleBundle("~/Content/slidetogglebutton").Include("~/Content/slidetogglebutton.css", new CssRewriteUrlTransformMyWrapper())
               .Include("~/Content/slidetogglebutton.css", new CssRewriteUrlTransformMyWrapper()));

            bundles.Add(new StyleBundle("~/Content/sweetalert2.min").Include("~/Content/sweetalert2.min.css", new CssRewriteUrlTransformMyWrapper())
              .Include("~/Content/sweetalert2.min.css", new CssRewriteUrlTransformMyWrapper()));

            bundles.Add(new StyleBundle("~/Content/bootstrap-select").Include("~/Content/bootstrap-select.css", new CssRewriteUrlTransformMyWrapper())
             .Include("~/Content/bootstrap-select.css", new CssRewriteUrlTransformMyWrapper()));
            //dark style
            bundles.Add(new StyleBundle("~/Content/myCssDark").Include("~/Content/myCSS_dark.css", new CssRewriteUrlTransformMyWrapper())
         .Include("~/Content/myCSS_s2_dark.css", new CssRewriteUrlTransformMyWrapper()));
            //20200413 jqueryui css path conversion
            bundles.Add(new StyleBundle("~/jQueryUI/css").Include(
                     "~/Content/jquery-ui.css", new CssRewriteUrlTransformMyWrapper()));//CssRewriteUrlTransform

            bundles.Add(new ScriptBundle("~/bundles/jqueryUI").Include(
                      "~/Scripts/jquery-ui.js"));


            bundles.Add(new ScriptBundle("~/bundles/jqueryCamera").Include(
                      "~/Scripts/jquery.webcam.js"));

        }
    }
    /// <summary>
    ///Will handle ... the relative path in css to the correct absolute path, such as when not in the root directory..
    /// </summary>
    public class CssRewriteUrlTransformMyWrapper : IItemTransform
    {
        public string Process(string includedVirtualPath, string input)
        {
            return new CssRewriteUrlTransform().Process("~" + VirtualPathUtility.ToAbsolute(includedVirtualPath), input);
        }
    }
}
